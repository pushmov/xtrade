<?php
return array(
	'_root_'  => 'homepage/index',  // The default route
	'_404_'   => 'homepage/404',    // The main 404 route
	'pass_reset/:code/:type' => 'homepage/reset',
	'realtors' => 'realtors/dashboard',
	'brokers' => 'brokers/dashboard',
	'vendors' => 'vendors/dashboard',
	'dialog/:type' => 'homepage/dialog/$1',
	'realtors/match/export/:id' => 'realtors/match/export/$1',
    'admins' => 'admins/dashboard',
	'admins/listing/completed' => 'admins/listing/completed',
	'admins/listing/delete' => 'admins/listing/delete',
	'admins/listing/match/:id' => 'admins/listing/match/$1',
	'admins/listing/detail/:lid' => 'admins/listing/detail/$1',
	'admins/listing/:lid' => 'admins/listing/index/$1',
	'admins/profile/:type/:tid' => 'admins/profile/index/$1/$2',
	'admins/profile/billing_history/:type' => 'admins/profile/billing_history/$1',
	'admins/plans/edit/:id' => 'admins/plans/edit/$1',
	'sadmins/promo/delete' => 'admins/promo/delete',
	'realtors/tutorial/edit/:lid' => 'realtors/tutorial/edit'
);
