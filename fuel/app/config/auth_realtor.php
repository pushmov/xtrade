<?php
return array (
	'user_model' => 'realtor',
	'user_table' => 'users',
	'username' => 'email',
	'password' => 'password',
	'session' => 'session_id',
	'hash_method' => 'sha1',
	'lifetime' => 1209600,
	'session_key' => 'auth_realtor',
);