<?php
return array (
	'user_model' => 'admin',
	'user_table' => 'admins',
	'username' => 'email',
	'password' => 'password',
	'session' => 'session_id',
	'hash_method' => 'sha1',
	'lifetime' => 1209600,
	'session_key' => 'auth_admin',
);