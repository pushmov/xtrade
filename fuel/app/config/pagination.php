<?php
/**
 * Part of the Fuel framework.
 *
 * @package    Fuel
 * @version    1.7
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2015 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * NOTICE:
 *
 * If you need to make modifications to the default configuration, copy
 * this file to your app/config folder, and make them in there.
 *
 * This will allow you to upgrade fuel without losing your custom config.
 */

return array(

	// the active pagination template
	'active'                      => 'default',

	// default FuelPHP pagination template, compatible with pre-1.4 applications
	'default'                     => array(
		'wrapper'                 => '<ul class="pagination" role="navigation" aria-label="Pagination">{pagination}</ul>',

		'first'                   => '<li class="pagination-previous">{link}</li>',
		'first-marker'            => "&laquo;&laquo;",
		'first-link'              => '<a href="{uri}">{page}</a>',

		'first-inactive'          => '<li class="pagination-previous disabled">{link}</li>',
		'first-inactive-link'     => '',

		'previous'                => '<li class="pagination-previous">{link}</li>',
		'previous-marker'         => "&laquo;",
		'previous-link'           => '<a href="{uri}" aria-label="Previous page">Prev <span class="show-for-sr">page</span></a>',

		'previous-inactive'       => "<li class=\"previous-inactive\">\n\t{link}\n</li>\n",
		'previous-inactive-link'  => "\t\t<a href=\"#\" rel=\"prev\">{page}</a>\n",

		'regular'                 => '<li>{link}</li>',
		'regular-link'            => "\t\t<a href=\"{uri}\">{page}</a>\n",

		'active'                  => '<li class="current">{link}</li>',
		'active-link'             => "\t\t<a href=\"#\">{page}</a>\n",

		'next'                    => '<li class="pagination-next">{link}</li>',
		'next-marker'            => "&raquo;",
		'next-link'               => '<a href="{uri}" aria-label="Next page">Next <span class="show-for-sr">page</span></a>',

		'next-inactive'           => "<li class=\"next-inactive\">\n\t{link}\n</li>\n",
		'next-inactive-link'      => "\t\t<a href=\"#\" rel=\"next\">{page}</a>\n",

		'last'                    => "<li class=\"last\">\n\t{link}\n</li>\n",
		'last-marker'             => "&raquo;&raquo;",
		'last-link'               => "\t\t<a href=\"{uri}\">{page}</a>\n",

		'last-inactive'           => "",
		'last-inactive-link'      => "",
	),
);
