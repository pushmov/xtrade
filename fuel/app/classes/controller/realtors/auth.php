<?php
namespace Controller\Realtors;
use Model;

class Auth extends \Controller_Hybrid {
	public $template = 'member_base';
	public $member;

	public function before() {
		parent::before();
		\Lang::load('common');
		$this->auto_render = !\Input::is_ajax();

		// Login check
		if (!\Authlite::instance('auth_realtor')->logged_in()) {
			\Response::redirect('/');
		} else {
			$this->member = \Authlite::instance('auth_realtor')->get_user();
			// Used on header link
			$this->member->user_info = $this->member->first_name . ' ' . $this->member->last_name . ' (' . $this->member->realtor_id . ')';
		}

		if ($this->auto_render) {
			\Config::load('google');
			$this->template->tracking_code = \Config::get('track_code');

			// Log page info
			\Model\Realtor::forge($this->member->id)->update_last_active();

			// Initialize empty values
			$this->template->meta_title = '';
			$this->template->meta_desc = '';
			$this->template->title = '';
			$user_info = \Html::anchor('realtors', __('realtor_home'), array('title' => $this->member->user_info, 'class' => 'tooltip-user'));
			$this->template->header = \View::forge('member_header', array('user_info' => $user_info, 'logout' => 'realtors'));
            $auth_broker = \Session::get('auth_realtor');
            $subhead['location'] = '';
            $subhead['admin_status'] = 0;
			$this->template->subhead = \View::forge('realtor/subhead', $subhead);
			$this->template->content = '';

			$this->template->styles = array();
			$this->template->scripts = array();
		}
	}

	public function action_logout() {
		\Model\User::forge()->unset_user_type();
		\Authlite::instance('auth_realtor')->logout();
		\Response::redirect('/');
	}
	public function after($response) {
		return parent::after($response);
	}
}