<?php
namespace Controller\Realtors;

class Listing extends Auth {
	const PAGINATION_PER_PAGE = 10;

	public function get_new(){
		return $this->response(\View::forge('dialog/listing/new'));
	}

	public function action_edit($listing_id = null) {
		\Lang::load('listing');
		\Lang::load('common');
		$data = \Listing::forge()->render_listing($listing_id);
		if ($data['has']['id'] == null) {
			\Response::redirect('/realtors');
		}
		$data['matches_notification'] = \Model\Listing::forge()->status_completion($listing_id, false);

		//partials
		$data['client_info'] = \View::forge('listing/client_info', $data);
		$data['has_info'] = \View::forge('listing/client_has_info', $data);
		$data['image_info'] = \View::forge('listing/image_info', $data);
		$data['wants_info'] = \View::forge('listing/wants_info', $data);
		$this->template->meta_title = 'xTradeHomes Edit Listing';
		$this->template->scripts = array('jquery.tn3.min.js', 'form_data.js', 'er_check.js', 'jquery.guillotine.js', 'edit.js', 'jquery.uploadfile.min.js', 'jquery.dad.js', 'members.js', 'jquery.sumoselect.min.js');
		$this->template->styles = array_merge($this->template->styles,array('uploadfile.css', 'sumoselect.css', 'jquery.dad.css'));
		$this->template->js_vars = array(
			'listing_id' => $listing_id,
			'scrollto' => \Input::get('pos'),
			'type' => $data['has']['type_of_listing']
		);
		$this->template->content = \View::forge('listing/edit', $data);
	}

	public function get_import(){
		$response = \Model\Crea\Property::forge()->get_listing_from_external_id(\Authlite::instance('auth_realtor')->get_user()->realtor_id);
		return $this->response($response);
	}

	public function post_change(){
		$data['listing_id'] = \Input::post('listing_id');
		$data['name'] = \Input::post('name');
		return $this->response(\Model\Listing::forge()->change_status($data));
	}

	public function post_xtraded(){
		return $this->response(\Model\Listing::forge()->mark_as_xtraded(\Input::post()));
	}

	public function post_notxtraded(){
		return $this->response(\Model\Listing::forge()->mark_as_notxtraded(\Input::post()));
	}

	public function post_activate(){
		return $this->response(\Model\Listing::forge()->activate_listing(\Input::post()));
	}

	public function post_deactivate(){
		return $this->response(\Model\Listing::forge()->deactivate_listing(\Input::post()));
	}

	public function post_enable(){
		return $this->response(\Model\Listing::forge()->enable_listing(\Input::post()));
	}

	public function post_disable(){
		return $this->response(\Model\Listing::forge()->disable_listing(\Input::post()));
	}

	public function post_delete(){
		return $this->response(\Model\Listing::forge()->delete_listing(\Input::post()));
	}

	public function post_renew(){
		return $this->response(\Model\Listing::forge()->renew_listing(\Input::post()));
	}

	public function post_downgrade(){
		return $this->response(\Model\Listing::forge()->downgrade_listing(\Input::post()));
	}

	public function post_all(){
		\Lang::load('common');
		$tut = \Input::post('tutorial') ? \Input::post('tutorial') : '';
		$load_all = \Model\Matches::forge()->load_all_listings(\Input::post());
		$query_builder = $load_all['query_builder'];
		$config = array(
			'pagination_url' => '/realtors/listing/all.html',
			'total_items' => sizeof($load_all['result']),
			'per_page' => self::PAGINATION_PER_PAGE,
			'uri_segment' => 'page'
		);
		$pagination = \Pagination::forge('mypagination', $config);
		$query_builder->limit(self::PAGINATION_PER_PAGE);
		$query_builder->offset($pagination->offset);
		$data['account_status'] = \Model\Realtor::forge()->account_status();
		$data['result'] = \Model\Listing::forge()->list_ajax_format($query_builder, $tut);
		$data['tut'] = $tut;
		$data['pagination'] = $pagination;
		return $this->response(\View::forge('listing/list_ajax', $data));
	}

	public function get_wantrequestdone(){
		return $this->response(\View::forge('dialog/listing/want_request_done'));
	}

	/**
	* Upload listing picture
	*
	*/
	public function post_doupload() {
		$output_dir = DOCROOT . \Model\Listing::IMG_PATH;
		$thumb_dir = DOCROOT . \Model\Listing::IMG_PATH . \Model\Listing::IMG_PATH_THUMB;
		$json = array();

		$client_has = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', \Input::post('listing_id')));
		$pos = \Model\Photos\Xtrade::forge()->get_last_position($client_has->listing_id);
		$next = \Model\Photos\Xtrade::forge()->get_last_image($client_has->listing_id) + 1;
		if($next > $client_has->allowed_images){
			return $this->response(array('status' => 'ERROR', 'message' => 'Allowed images reached'));
		}

		$im = \Image::forge();
		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			$fileName = str_pad($client_has->id, 6, '0', STR_PAD_LEFT) .'_'. $next . '.' . strtolower($ext);

			if(move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $fileName)){
				\Model\Photos\Xtrade::forge()->insert_xtrade_photos($client_has->listing_id, $pos, $next, $fileName);
				$im->load($output_dir.$fileName);
				// Main image
				$im->resize(\Model\Photos\Xtrade::MAX_WIDTH);
				$im->save($output_dir.$fileName);
				// Thumb image
				$im->resize(\Model\Photos\Xtrade::TMB_WIDTH);
				$im->save($thumb_dir.$fileName);

				$client_has->lastupdated = date_create()->format('Y-m-d H:i:s');
				$client_has->save();
			}
			$json[] = $fileName;
			$json['listing_id'] = \Input::post('listing_id');
		}
		return $this->response($json);
	}

	public function post_completion(){
		return $this->response(\Model\Listing::forge()->status_completion(\Input::post('listing_id')));
	}

	public function post_delete_images(){
		return $this->response(\Model\Photos\Xtrade::forge()->delete_image(\Input::post()));
	}

	public function post_image_order(){
		return $this->response(\Model\Photos\Xtrade::forge()->reorder(\Input::post('order')));
	}

	public function get_matches(){
		return $this->response(\Model\Listing::forge()->status_completion(\Input::get('listing_id'), false));
	}

}
