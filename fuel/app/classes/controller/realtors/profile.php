<?php
namespace Controller\Realtors;

class Profile extends Auth {
	public function action_index() {
		\Lang::load('common');
		\Lang::load('signup');
		\Lang::load('listing');
		$view = \View::forge('member/profile');
		$view->title = 'Agent Profile';
		$view->readonly = '';

        $data = \Model\Realtor::forge($this->member->id)->profile();
        // crea & nrds tool tips
        if ($data['country'] == \Model\User::COUNTRY_CA) {
        	$view->nrb_txt = 'CREA ID';
			$view->nrb_tip = '<span>Look up your CREA ID by clicking <a href="http://mms.realtorlink.ca/indDetails.aspx" target="_blank">here</a></span>';
		} else {
        	$view->nrb_txt = 'NRDS ID';
			$view->nrb_tip = 'Look up your NRDS ID by clicking <a href="https://reg.realtor.org/roreg.nsf/retrieveID?OpenForm" target="_blank">here</a></span>';
		}
		$view->data = $data;
		$view->promo_code = \Model\Plan::forge($data['plan_id'])->promo_code;
		$this->template->meta_title = 'xTradeHomes Agent Profile';
		$this->template->styles = array('tooltipster.css','auto-complete.css');
		$this->template->scripts = array('jquery.mask.js', 'auto-complete.min.js', 'er_check.js', 'jquery.tooltipster.min.js', 'profile.js', 'members.js');
		$this->template->js_vars = array('member' => 'realtors/', 'type' => 'realtor', 'memberid' => $this->member->realtor_id);
		$this->template->content = $view;
	}

	public function get_welcome() {
		// Initial import
		\Model\Crea\Import::do_import($this->member->id);
		\Lang::load('signup');
		return $this->response(\View::forge('dialog/welcome_realtor'));
	}

	public function get_password() {
		\Lang::load('signup');
		$view = \View::forge('dialog/change_password')->bind('err_msg', $msg);
		if (\Input::get('status') == \Model\User::AS_NEEDS_PASS) {
			$msg = __('please_create_a_new_password');
		}
		$view->type = \Model\User::UT_REALTOR;
		$view->id = \Input::get('id');
		return $this->response($view);
	}

	public function post_pass_update() {
		return $this->response(\Model\Realtor::forge(\Input::post('id'))->update_password(\Input::post()));
	}

	public function get_close_account() {
		\Lang::load('signup');
		return $this->response(\View::forge('dialog/close_account'));
	}

	public function post_update() {
		\Lang::load('common');
		try {
			\Model\Realtor::forge(\Input::post('data.id'))->doupdate(\Input::post());
			$json['status'] = 'OK';
			$json['message'] = '<div class="callout success" data-closable>'.__('profile_updated').'
			<button class="close-button" aria-label="'.__('dismiss_alert').'" type="button" data-close><span aria-hidden="true">&times;</span></div>';
			return $this->response($json);
		} catch(\AutoModelerException $e) {
			return $this->response(array('errors' => $e->errors));
		}
	}

	public function get_broker_dialog() {
		return $this->response(\View::forge('dialog/broker_change'));
	}
	public function get_broker_request() {
		return $this->response(\View::forge('dialog/broker_ismember'));
	}

	public function get_broker_invite() {
		return $this->response(\View::forge('dialog/broker_invite'));
	}

	public function post_broker_invite() {
		return \Model\Realtor\Broker::forge($this->member->id)->request_new(\Input::post());
	}

	public function get_broker_change() {
		\Lang::load('broker');
		$view = \View::forge('dialog/broker_select');
		if (\Input::get('id') == 'btn_broker_member_yes') {
			$view->title = __('broker_request');
			$view->broker = __('req_assoc');
		} else {
			$view->title = __('change_broker');
			$view->broker = __('current_broker');
		}
		$view->cboBrokers = \Form::select('broker', '', \Model\Realtor\Broker::forge($this->member->id)->list_array(true,false), array('id' => 'broker'));
		return $this->response($view);
	}

	public function post_broker_request() {
		return \Model\Realtor\Broker::forge($this->member->id)->request_assoc(\Input::post('id'));
	}

	public function post_broker_request_cancel() {
		return \Model\Realtor\Broker::forge($this->member->id)->request_cancel();
	}

 	public function get_broker_cancel() {
		if ($this->member->pending_broker != 0) {
			return $this->response(\View::forge('dialog/cancel_request'));
		} else {
			return $this->response(\View::forge('dialog/broker_ismember'));
		}
	}

	public function get_broker_remove() {
		\Lang::load('signup');
		return $this->response(\View::forge('dialog/remove_broker'));
	}

	public function get_billing_history() {
		\Lang::load('orders');
		$view = \View::forge('dialog/order_history');
		$view->data = \Model\Order::forge()->get_history($this->member->realtor_id, \Model\User::UT_REALTOR);
		return $this->response($view);
	}

	public function get_change_billing() {
		\Lang::load('signup');
		$view = \View::forge('dialog/billing_details');
		$model = '\\Model\\'.  ucwords(\Model\User::forge()->get_types(\Session::get('type')));
		$user = $model::forge($this->member->id);
		$agreement = new \Model\Paypalrest\Agreement();
		$data = $agreement->details($user->paypal_sub_id);
		$view->cboCardTypes = $data['select_cc_type'];
		$months = array_map(create_function('&$v', 'return sprintf("%02d", $v);'), range(1, 12));
		$view->cboMonths = array_combine($months, $months);
		$years = range(date('Y'), date('Y') + 8);
		$view->cboYears = array_combine($years, $years);
		$view->data = $data;
		$view->data['user_xid'] = \Model\User::forge()->get_types(\Session::get('type'));
		$view->data['user_id'] = $this->member->realtor_id;
		$view->btnTxt = 'Update';
		return $this->response($view);
	}

	public function post_alert_update() {
		$realtor = \Model\Realtor::forge($this->member->id);
		$realtor->alerts = \Input::post('alerts');
		$realtor->save();
	}

	public function post_billing_update(){
		$member = (array) $this->member;
		return $this->response(\Model\Realtor\Payment::forge()->update_billing(\Input::post('data'), $member));
	}

	public function get_billing_update_success(){
		return $this->response(\View::forge('dialog/billing_details_success'));
	}

	public function get_activity(){
		$view = \View::forge('dialog/account_activity');
		$data = \Model\Realtor::forge($this->member->id)->profile();
		$data['user_id'] = $this->member->realtor_id;
		$view->data = $data;
		$view->data['login_log'] = \Model\Loginlog::forge()->user_last_success_login($this->member->realtor_id);
		return $this->response($view);
	}

}