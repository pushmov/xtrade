<?php
namespace Controller\Realtors;

class Tutorial extends Auth {
	
	public function action_index(){
		\Lang::load('listing');
		$view = \View::forge('realtor/dashboard');
		$view->set('filters', \Model\Listing::filter_array());
		$view->view = \Session::get_flash('view', '');
		$this->template->meta_title = 'xTradeHomes My Listings';
		$this->template->styles = array('tooltipster.css', 'style.css');
		$this->template->js_vars = array(
			'page' => 'dashboard',
			'listing_id' => \Model\Tutorial::LISTING_ID
		);
		$this->template->scripts = array('jquery.tooltipster.min.js', 'members.js', 'tutorial.js');
		$this->template->content = $view;
	}
	
	public function action_edit(){
		\Lang::load('listing');
		\Lang::load('common');
		$data = \Listing::forge()->render_listing($this->param('lid'));
		$data['matches_notification'] = array(
			'matches' => null
		);
		//partials
		$data['client_info'] = \View::forge('listing/client_info', $data);
		$data['has_info'] = \View::forge('listing/client_has_info', $data);
		$data['image_info'] = \View::forge('listing/image_info', $data);
		$data['wants_info'] = \View::forge('listing/wants_info', $data);
		$this->template->meta_title = 'xTradeHomes Edit Listing';
		$this->template->scripts = array('jquery.tn3.min.js', 'form_data.js', 'er_check.js', 'jquery.guillotine.js', 'jquery.uploadfile.min.js', 'jquery.dad.js', 'members.js', 'jquery.sumoselect.min.js', 'tutorial.js');
		$this->template->styles = array_merge($this->template->styles,array('uploadfile.css', 'sumoselect.css', 'jquery.dad.css'));
		$this->template->js_vars = array(
			'listing_id' => $this->param('lid'),
			'scrollto' => \Input::get('pos'),
			'type' => $data['has']['type_of_listing'],
			'page' => 'edit'
		);
		$this->template->content = \View::forge('listing/edit', $data);
	}
	
	public function get_listing(){
		$data = \Model\Tutorial::forge()->load_listing();
		return $this->response(\View::forge('listing/list_ajax', $data));
	}
	
	public function get_dialog(){
		$view = \View::forge('dialog/tutorial/'. \Model\Tutorial::forge()->load_dialog_view(\Input::get()));
		return $this->response($view);
	}
}