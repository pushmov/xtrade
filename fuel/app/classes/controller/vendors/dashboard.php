<?php
namespace Controller\Vendors;

class Dashboard extends Auth {

	public function before() {
		parent::before();
		// Needs to update password
		if (($this->member->account_status == \Model\User::AS_NEEDS_PASS) || ($this->member->account_status == \Model\User::AS_NEW)) {
			\Response::redirect('/vendors/profile');
		}
	}

	public function action_index() {
		\Lang::load('common');
		\Lang::load('vendor');
		$view = \View::forge('vendor/dashboard');
		$vendor = \Model\Vendor::forge($this->member->id);
		$data = $vendor->as_array();
		if($data['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED){
			$view->gradeBtn = \Form::button('downgrade', __('downgrade_adv'), array('type' => 'button', 'class' => 'button downgrade'));
			$view->featured_txt = __('featured');
		} else {
			$view->gradeBtn = \Form::button('upgrade', __('upgrade_adv'), array('type' => 'button', 'class' => 'button upgrade'));
			$view->featured_txt = __('nfeatured');
		}
		$data['locale'] = \Model\Vendor::forge()->areaid_to_advertise($data['locale']);
		$data['type'] = \Model\Vendor::forge()->typeid_to_services($data['type']);
		$view->data = $data;
		$view->payment = $vendor->get_payment_info();
		$this->template->js_vars = array(
			'vendor_id' => $this->member->vendor_id
		);
		$this->template->scripts = array('vendor_home.js', 'jquery.uploadfile.min.js');
		$this->template->content = $view;
	}

	public function get_ad_preview(){
		$view = \View::forge('dialog/vendor/ad_preview');
		$view->data = \Model\Vendor::forge($this->member->id)->as_array();
		$view->data['type'] = \Model\Vendor::forge()->typeid_to_services($view->data['type']);
		$view->logo = \Model\Vendor::forge()->get_vendor_logo($this->member->vendor_id);
		return $this->response($view);
	}

	public function get_upgrade(){
		\Lang::load('orders');
		$taxes = \Model\Tax::forge()->load_info($this->member->location_address);
		$view = \View::forge('dialog/payment');
		$years = range(date('Y'), date('Y') + 8);
		$view->cboYears = array_combine($years, $years);

		$vault = new \Model\Paypalrest\Vault();
		$model = '\\Model\\'.  ucwords(\Model\User::forge()->get_types(\Session::get('type')));
		$user = $model::forge($this->member->id);
		$billing = $vault->retrieve($user->paypal_sub_id);

		$data = (array) $this->member;
		$data['cpc_address'] = $billing['cpc_address'];
		$data['user_type'] = 'vendor';
		$data['broker_id'] = false;
		$data['tax'] = $data['tax_code'] = '';
		$data['fee'] = 0;
		$total = \Sysconfig::get('vendor_featured_fee');
		$view->total = $total;
		$data['featured_ad'] = 0;
		$data['featured_ad_summary'] = '';
		if ($taxes->loaded() && ($taxes->rate > 0)) {
			$tax = round($total * $taxes->rate, 2);
			$data['featured_tax'] = round(($data['featured_ad']) * $taxes->rate, 2);
			$data['regular_tax'] = $tax;
			$data['tax_code'] = $taxes->code;
			$data['tax'] = $tax;
			$view->total = $total + $tax;
		}
		$data['fee'] = $total;
		$data['summary'] = 'Monthly Featured Ad';
		$data['listing_id'] = '';
		$data['ut'] = \Session::get('type');
		$payment = new \Model\Paypalrest\Payment();
		$data['cbo_cc_type'] = $payment->get_cc_type();
		$months = array_map(create_function('&$v', 'return sprintf("%02d", $v);'), range(1, 12));
		$data['cbo_month'] = array_combine($months, $months);
		$view->set('data', array_merge($data, $billing));
		$view->broker_id = false;
		$view->vendor_id = $this->member->vendor_id;
		return $this->response($view);
	}

	public function post_upgrade(){
		$post = \Input::post('data');
		$post['user_xid'] = $post['vendor_id'] = $this->member->vendor_id;
		$post['paypal_sub_id'] = $this->member->paypal_sub_id;
		$post['email'] = $this->member->email;
		return $this->response(\Model\Vendor\Payment::forge()->upgrade($post));
	}

	public function get_downgrade(){
		$view = \View::forge('dialog/vendor_downgrade');
		return $this->response($view);
	}

	public function post_downgrade(){
		return $this->response(\Model\Vendor\Payment::forge()->downgrade($this->member));
	}

	public function get_success(){
		\Lang::load('vendor');
		$type = \Input::get('type');
		if($type === 'downgrade') {
			$txt = __('downgrade_success');
		} else if($type === 'upgrade'){
			$txt = __('upgrade_success');
		}
		$view = \View::forge('dialog/vendor_downgrade_success');
		$view->txt = $txt;
		return $this->response($view);
	}

}