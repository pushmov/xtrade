<?php
namespace Controller;

class About extends PublicTemplate {
	public function before() {
		parent::before();
		$this->template->meta_title = 'xTradeHomes About Us';
	}

	public function action_index() {
		$this->template->content = \View::forge('about');
	}
}
