<?php
namespace Controller;

class Ipn extends \Controller {
	protected $_order_fields = array();
	protected $_email;
	protected $_vendor;
	protected $_order;
	protected $_user;
	protected $_clienthas;
	protected $_broker;

	public function __construct() {
		$this->_vendor = \Model\Vendor::forge();
		$this->_order = \Model\Order::forge();
		$this->_user = \Model\Realtor::forge();
		$this->_clienthas = \Model\Client\Has::forge();
		$this->_broker = \Model\Broker::forge();
	}

	public function post_index(){

		$config = \Fuel\Core\Config::load('paypal');

		\Package::load('paypal');
		$listener = new \IpnListener();
		$listener->use_sandbox = $config['Sandbox'];

		try {
			$listener->requirePostMethod();
			$verified = $listener->processIpn();
		} catch (Exception $e) {
			return $this->response($e->getMessage());
		}

		if($verified){

			$this->_order_fields['homebase_id']     = \Input::post('rp_invoice_id');
			$this->_order_fields['subscriber_id']   = \Input::post('recurring_payment_id');
			$this->_order_fields['creation_date']   = date_create()->format('Y-m-d H:i:s');
			$this->_order_fields['period_type']     = \Input::post('period_type');
			$this->_order_fields['pay_amount']      = \Input::post('amount');
			$this->_order_fields['trans_id']        = \Input::post('txn_id') ? \Input::post('txn_id') : \Input::post('initial_payment_txn_id');
			$this->_order_fields['payment_sig']     = \Input::post('verify_sign');
			$this->_order_fields['receiver_id']     = \Input::post('receiver_id');
			$this->_order_fields['receipt_id']      = \Sysconfig::create_string('salt', NULL, true);
			$this->_order_fields['payer_id']        = \Input::post('payer_id');
			$this->_order_fields['tax_amount']      = \Input::post('tax');
			$this->_order_fields['payment_status']  = \Input::post('profile_status') == 'Active' ? 1 : 2;

			$param['pay_stat']        = \Input::post('payment_status');
			$param['trans_type']      = \Input::post('txn_type');
			$param['amt_paid']        = \Input::post('mc_gross');
			$param['listing_id']      = \Input::post('custom');
			$param['payment_date']    = \Input::post('payment_date');
			$param['inital_amount']   = \Input::post('initial_payment_amount');
			$param['prod_name']       = \Input::post('product_name');

			//action list
			$response = $this->_action($param);
			$this->_email->ipn_report($listener->getTextReport(), 'Verified IPN');
			return $this->response($response);

		}else{
			/*
			An Invalid IPN *may* be caused by a fraudulent transaction attempt. It's
			a good idea to have a developer or sys admin manually investigate any
			invalid IPN.
			*/
			$this->_email->ipn_report($listener->getTextReport(), 'Invalid IPN');
			return $this->response($response);
		}
	}


	private function payment_profile($param){
		if(stristr($this->_order_fields['homebase_id'], 'vxTH')){
			if($param['prod_name'] == 'xTh_Featured_Vendor_Upgrade'){
				$this->_order_fields['type'] = 'Upgrade Payment';
			} elseif ($param['prod_name'] == 'xTh_Featured_Vendor_Downgrade'){
				$this->_order_fields['type'] = 'Downgrade';
			} else {
				$update = $this->_vendor->load(\DB::select_array()->where('vendor_id', '=', $this->_order_fields['homebase_id']));
				$update->set_fields(array(
					'paypal_status' => $this->_order_fields['payment_status'],
					'active_till' => date_create('+30 days')->format('Y-m-d H:i:s'),
					'paypal_sub_id' => $this->_order_fields['subscriber_id']
				));
				$update_result = $update->save();
				$this->_order_fields['type'] = 'Recurring Payment';
			}

			$insert = $this->_order->doinsert($this->_order_fields);

		} elseif(stristr($this->_order_fields['homebase_id'], 'xTH')) {
			$update = $this->_user->load(\DB::select_array()->where('realtor_id', '=', $this->_order_fields['homebase_id']));
			$update->set_fields(array(
				'active_till' => date_create('+30 days')->format('Y-m-d H:i:s'),
				'paypal_status' => $this->_order_fields['payment_status'],
				'paypal_sub_id' => $this->_order_fields['subscriber_id']
			));
			$update_result = $update->save();
			$this->_order_fields['type'] = 'Recurring Payment';
			$this->_order_fields['initial_amount'] = $param['initial_amount'];
			$insert = $this->_order->doinsert($this->_order_fields);
		} elseif(stristr($this->_order_fields['homebase_id'], 'NA')) {
			$get_id = $this->_clienthas->load(\DB::select_array()->where('listing_id', '=', $this->_order_fields['homebase_id']))->as_array();
			$this->_order_fields['subscribe_id'] = $get_id['realtor_id'];
			$len = '+' . (\Sysconfig::get('featured_listing_duration') * 30) . ' days';
			$update = $this->_clienthas->load(\DB::select_array()->where('listing_id', '=', $this->_order_fields['homebase_id']));
			$update->set_fields(array(
				'featured_listing' => 1,
				'featured_end' => date_create($len)->format('Y-m-d H:i:s'),
			));
			$update_result = $update->save();
			$this->_order_fields['type'] = 'Listing Upgraded';
			$this->_order_fields['listing_id'] = $this->_order_fields['homebase_id'];
			$insert = $this->_order->doinsert($this->_order_fields);
		}
		$param['head'] = 'Payment Attempted/Made<br />';
		$param['subject'] = 'Payment Email';
		$param['sender_name'] = array('from' => 'paypal@xtradehomes.com', 'name' => 'homebasebyirex Payment');

		$response = $this->_email->ipn_mail_after(array_merge($param, $this->_order_fields));
		return $response;
	}

	private function payment_accept($param){
		if(stristr($this->_order_fields['homebase_id'], 'vxTH')){
			$update = $this->_vendor->load(\DB::select_array()->where('vendor_id', '=', $this->_order_fields['homebase_id']));
			$update->set_fields(array(
				'paypal_status' => $this->_order_fields['payment_status'],
				'active_till' => date_create('+30 days')->format('Y-m-d H:i:s'),
				'paypal_sub_id' => $this->_order_fields['subscriber_id']
			));
			$update_result = $update->save();
			$this->_order_fields['type'] = 'Recurring Payment';
			$insert = $this->_order->doinsert($this->_order_fields);
		}elseif(stristr($this->_order_fields['homebase_id'], 'xTH')){
			$update = $this->_user->load(\DB::select_array()->where('realtor_id', '=', $this->_order_fields['homebase_id']));
			$update->set_fields(array(
				'paypal_status' => $this->_order_fields['payment_status'],
				'active_till' => date_create('+30 days')->format('Y-m-d H:i:s'),
				'paypal_sub_id' => $this->_order_fields['subscriber_id']
			));
			$update_result = $update->save();
			$this->_order_fields['type'] = 'Recurring Payment';
			$this->_order_fields['initial_amount'] = $param['initial_amount'];
			$insert = $this->_order->doinsert($this->_order_fields);
		}elseif(stristr($this->_order_fields['homebase_id'], 'NA')){
			$user = $this->_clienthas->load(\DB::select_array()->where('listing_id', '=', $this->_order_fields['homebase_id']));
			$len = '+' . (\Sysconfig::get('featured_listing_duration') * 30) . ' days';
			$update = $this->_clienthas->load(\DB::select_array()->where('listing_id', '=', $this->_order_fields['homebase_id']));
			$update->set_fields(array(
				'featured_listing' => 1,
				'featured_end' => date_create($len)->format('Y-m-d H:i:s'),
			));
			$update_result = $update->save();
			$this->_order_fields['listing_id'] = $this->_order_fields['homebase_id'];
			$this->_order_fields['homebase_id'] = $user['realtor_id'];
			$this->_order_fields['type'] = 'Listing Upgraded';
			$insert = $this->_order->doinsert($this->_order_fields);
		}
		$param['head'] = 'Payment Attempted/Made:<br />';
		$param['subject'] = 'Payment Email';
		$param['sender_name'] = array('from' => 'paypal@xtradehomes.com', 'name' => 'homebasebyirex Payment');
		$response = $this->_email->ipn_mail_after(array_merge($param, $this->_order_fields));
		return $response;
	}

	private function subscr_modify($param){
		$fields = array_merge($param, $this->_order_fields);
		foreach($fields as $key => $val){
			if($key == 'pp_time' || $key == 'receipt_id'){
				continue;
			}
			$post_dets .= $val;
			$post_dets .= '<br>';
		}
		$maildata['head'] = '';
		$maildata['subject'] = 'Failed Billing Info';
		$maildata['body'] = $post_dets;
		$maildata['sender_name'] = array('from' => 'paypal@xtradehomes.com', 'name' => 'homebasebyirex Payment');
		$response = $this->_email->ipn_mail_after(array_merge($param, $this->_order_fields));
		return $response;
	}

	private function payment_failed($param){
		$get_info = $this->_user->load(\DB::select_array()->where('realtor_id', '=', $this->_order_fields['homebase_id']), NULL)->as_array();
		if(empty($get_info)){
			$get_info = $this->_broker->load(\DB::select_array()->where('broker_id', '=', $this->_order_fields['homebase_id']), NULL)->as_array();
			if(empty($get_info)){
				$get_info = $this->_vendor->load(\DB::select_array()->where('vendor_id', '=', $this->_order_fields['homebase_id']), NULL)->as_array();
			}
		}
		if($get_info){
			$app_type = 'Payments_Failed';
			$data['fname'] = $get_info[0]['first_name'];
			$data['lname'] = $get_info[0]['last_name'];
			$data['mailto'] = $get_info[0]['email'];
			$data['app_type'] = $app_type;
			$this->_email->ipn_failed_suspended($app_type, $data);
		}

		$fields = array_merge($param, $this->_order_fields);
		foreach($fields as $key => $val){
			if($key == 'pp_time' || $key == 'receipt_id'){
				continue;
			}
			$post_dets .= $val;
			$post_dets .= '<br>';
		}
		$maildata['head'] = '';
		$maildata['subject'] = 'Failed Billing Info';
		$maildata['body'] = $post_dets;
		$maildata['sender_name'] = array('from' => 'paypal@xtradehomes.com', 'name' => 'homebasebyirex Payment');

		$response = $this->_email->ipn_mail_after(array_merge($param, $this->_order_fields));
		return $response;
	}

	private function payment_suspended($param){

		$get_info = $this->_user->load(\DB::select_array()->where('realtor_id', '=', $this->_order_fields['homebase_id']))->as_array();
		if(empty($get_info)){
			$get_info = $this->_broker->load(\DB::select_array()->where('broker_id', '=', $this->_order_fields['homebase_id']))->as_array();
			if(empty($get_info)){
				$get_info = $this->_vendor->load(\DB::select_array()->where('vendor_id', '=', $this->_order_fields['homebase_id']))->as_array();
			}
		}

		if(!empty($get_info)){
			$app_type = 'Missed_Payment';
			$data['app_type'] = $app_type;
			$data['fname'] = $get_info[0]['first_name'];
			$data['lname'] = $get_info[0]['last_name'];
			$data['mailto'] = $get_info[0]['email'];
			$this->_email->ipn_failed_suspended($app_type, $data);
		}

		if(stristr($this->_order_fields['homebase_id'], 'vxTH')){
			$this->_order_fields['type'] = 'Account Suspension';
			$insert = $this->_order->doinsert($this->_order_fields);
			$update = $this->_vendor->load(\DB::select_array()->where('vendor_id', '=', $this->_order_fields['homebase_id']));
			$update->set_fields(array(
				'paypal_status' => 3,
				'paypal_sub_id' => $this->_order_fields['subscriber_id'],
				'account_status' => 4
			));
			$update_result = $update->save();

		}elseif(stristr($this->_order_fields['homebase_id'], 'xTH')){
			$this->_order_fields['type'] = 'Account Suspension';
			$insert = $this->_order->doinsert($this->_order_fields);

			$update = $this->_user->load();
			$update->set_fields(array(
				'paypal_status' => 3,
				'paypal_sub_id' => $this->_order_fields['subscriber_id'],
				'account_status' => 4
			));
			$update_result = $update->save();

		}elseif(stristr($this->_order_fields['homebase_id'], 'NA')){
            $client_has = $this->_clienthas->load(\DB::select_array()->where('listing_id', '=', $this->_order_fields['homebase_id']));
			$user = $client_has->as_array();
			$client_has->set_fields(array(
				'paypal_status' => 3
			));
			$update_result = $client_has->save();
			$this->_order_fields['listing_id'] = $this->_order_fields['homebase_id'];
			$this->_order_fields['homebase_id'] = $user['realtor_id'];
			$this->_order_fields['type'] = 'Featured Listing Suspended';
			$insert = $this->_order->doinsert($this->_order_fields);

		}

		$param['head'] = 'Realtor/Vendor '.$this->_order_fields['homebase_id'].' has been Suspended.<br>';
		$param['subject'] = 'Account Modification Email';
		$param['sender_name'] = array('from' => 'paypal@xtradehomes.com', 'name' => 'homebasebyirex Account Modification Email Success');

		$response = $this->_email->ipn_mail_after(array_merge($param, $this->_order_fields));
		return $response;
	}

	private function payment_cancel($param){


		if(stristr($this->_order_fields['homebase_id'], 'vxTH')){

			$this->_order_fields['type'] = 'Account Cancellation';
			$insert = $this->_order->doinsert($this->_order_fields);

			$update = $this->_vendor->load(\DB::select_array()->where('vendor_id', '=', $this->_order_fields['homebase_id']));
			$update->set_fields(array(
				'paypal_status' => 5,
				'paypal_sub_id' => $this->_order_fields['subscriber_id'],
				'account_status' => 2
			));
			$update_result = $update->save();

		}elseif(stristr($this->_order_fields['homebase_id'], 'xTH')){

			$this->_order_fields['type'] = 'Account Cancellation';
			$this->_order->doinsert($this->_order_fields);

			$update = $this->_user->load(\DB::select_array()->where('realtor_id', '=', $this->_order_fields['homebase_id']));
			$update->set_fields(array(
				'paypal_status' => 5,
				'paypal_sub_id' => $this->_order_fields['subscriber_id'],
				'account_status' => 2
			));
			$update->save();

		}elseif(stristr($this->_order_fields['homebase_id'], 'NA')){

			$client_has = $this->_clienthas->load(\DB::select_array()->where('listing_id', '=', $this->_order_fields['homebase_id']));
			$user = $client_has->as_array();
			$client_has->set_fields(array(
				'paypal_status' => 5
			));
			$update_result = $client_has->save();

			$this->_order_fields['realtor_id'] = $user['realtor_id'];
			$this->_order_fields['type'] = 'Featured Listing Cancelled';
			$this->_order_fields['listing_id'] = $this->_order_fields['homebase_id'];
			unset($this->_order_fields['homebase_id']);
			$insert = $this->_order->doinsert($this->_order_fields);
		}

		$param['subject'] = 'Paypal Notification';
		$param['head'] = 'Realtor/Vendor '.$this->_order_fields['homebase_id'].' Cancelled Account.<br />';
		$param['sender_name'] = array('from' => 'paypal@xtradehomes.com', 'name' => 'homebasebyirex Payment');

		$response = $this->_email->ipn_mail_after(array_merge($param, $this->_order_fields));
		return $response;
	}

	private function subscr_eot($param){

		$param['subject'] = 'End of Subscription Notification';
		$param['head'] = 'End of Subscription Period for Realtor/Vendor: '.$this->_order_fields['homebase_id'].'.<br />';
		$param['sender_name'] = array('from' => 'paypal@xtradehomes.com', 'name' => 'homebasebyirex Payment');

		$response = $this->_email->ipn_mail_after(array_merge($param, $this->_order_fields));
		return $response;
	}

	private function _action($param){

		switch ($param['trans_type']){

			case 'recurring_payment_profile_created':
				$r = $this->payment_profile($param);
				break;

			case 'recurring_payment' || 'web_accept':
				$r = $this->payment_accept($param);
				break;

			case 'subscr_modify':
				$r = $this->subscr_modify($param);
				break;

			case 'recurring_payment_failed':
				$r = $this->payment_failed($param);
				break;

			case 'recurring_payment_suspended' || 'subscr_eot' || 'recurring_payment_suspended_due_to_max_failed_payment':
				$r = $this->payment_suspended($param);
				break;

			case 'recurring_payment_profile_cancel':
				$r = $this->payment_cancel($param);
				break;

			case 'subscr_eot':
				$r = $this->subscr_eot($param);
				break;

			default:
				return $this->response('error');
		}

		return $r;

	}

}