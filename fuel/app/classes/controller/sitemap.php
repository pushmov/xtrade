<?php
namespace Controller;

class Sitemap extends PublicTemplate {
	public function before() {
		parent::before();
		$this->template->meta_title = 'xTradeHomes Site Map';
	}

	public function action_index() {
		$this->template->content = \View::forge('sitemap');
	}
}
