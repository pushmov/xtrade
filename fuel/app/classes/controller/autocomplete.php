<?php
namespace Controller;

class Autocomplete extends \Controller_Rest {
	/**
	* Get the city/state/country
	*
	*/
	public function get_cpc() {
		// Replace a space double space with a comma
		$q = str_replace(array(' ', '  '), ',', \Input::get('q'));
		// Break it out
		$q = explode(',', $q);
		$q[0] = trim($q[0]) . '%';

		$sql = '
		SELECT name,province,country
		FROM ca_cities
		JOIN ca_provinces ON ca_provinces.id = ca_cities.province_id
		JOIN country ON country.id = ca_provinces.country_id WHERE ';

		if (isset($q[1])) {
			$q[1] = trim($q[1]) . '%';
			$q[2] = trim($q[0]) . '%' . $q[1] . '%';
			$sql .= '((name LIKE :cval) AND (province LIKE :sval)) ';
			$sql .= 'OR (name LIKE :mval) ';
		} else {
			$sql .= '(name LIKE :cval) ';
		}
		$sql .= '
		UNION
		SELECT name,state,country
		FROM us_cities
		JOIN us_states ON us_states.id = us_cities.state_id
		JOIN country ON country.id = us_states.country_id WHERE ';
		if (isset($q[1])) {
			$sql .= '((name LIKE :cval) AND (state LIKE :sval)) ';
			$sql .= 'OR (name LIKE :mval)';
		} else {
			$sql .= '(name LIKE :cval) ';
		}
		$sql .= ' ORDER BY name, province, country LIMIT 30';

		$query = \DB::query($sql)->param('cval', $q[0]);
		if (isset($q[1])) {
			$query->param('sval', $q[1]);
			$query->param('mval', $q[2]);
		}
		$result = $query->execute();
		$caller = \Input::get('from');
		if (count($result) > 0) {
			$data = array();
			foreach($result as $row) {
				$addr = "{$row['name']}, {$row['province']}, {$row['country']}";
				if($caller == 'tokeninput'){
					//change array format if its coming from tokeninput plugin
					$data[] = array('id' => $addr, 'name' => $addr);
				} else {
					$data[] = $addr;
				}
			}
			return $this->response($data);
		}
	}

    public function get_stos(){
        $sql = 'SELECT id,name FROM directory_list WHERE name LIKE :val';
        $result = \DB::query($sql)->param('val', \Input::get('q') . '%')->execute();
        if (count($result) > 0) {
			$data = array();
			foreach($result as $row) {
				$data[] = "{$row['name']}";
			}
			return $this->response($data);
		}
    }
}