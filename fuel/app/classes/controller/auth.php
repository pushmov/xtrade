<?php
namespace Controller;
use Model;

class Auth extends \Controller_Hybrid {
	public $template;
	public $member;

	public function before() {
		parent::before();
		$this->auto_render = !\Input::is_ajax();

		// Login check
		if (!\Authlite::instance()->logged_in()) {
			\Response::redirect('/');
		} else {
			$this->member = \Authlite::instance()->get_user();
			// Used on header link
			$this->member->user_info = $this->member->first_name . ' ' . $this->member->last_name;// . ' (' . $this->member->first_name . ')';
			// Needs to update password
			if ($this->member->account_status == User::AS_NEEDS_PASS) {
				\Response::redirect('/members/profile');
			}
			\Response::redirect('/members');
		}

		if ($this->auto_render) {
			\Config::load('google');
			$this->template->tracking_code = \Config::get('track_code');

			// Initialize empty values
			$this->template->meta_title = '';
			$this->template->meta_desc = '';
			$this->template->title = '';
			$this->template->content = '';

			$this->template->styles = array();
			$this->template->scripts = array();
		}
	}

	public function after($response) {
		return parent::after($response);
	}
}