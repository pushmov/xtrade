<?php
namespace Controller;
use Model;
class Login extends \Controller_Rest {
	/*
	public function before() {
	// Don't run in not
	if (!\Input::is_ajax()) {
	exit;
	}
	parent::before();
	}
	*/
	public function get_login() {
		\Lang::load('public');
		$data['type'] = \Input::get('type');
		$data['title'] =  \Lang::get('login_' . $data['type'], array(), __('login_realtor'));
		$data['email'] = '';
		$data['remember'] = 0;
		$authlite = \Authlite::instance('auth_' . $data['type'])->logged_in();
		if ($authlite) {
			$data['email'] = $authlite->email;
			$data['remember'] = $authlite->remember;
		}
		return $this->response(\View::forge('dialog/login', $data));
	}
	public function get_forgot() {
		\Lang::load('public');
		$view = \View::forge('dialog/forgot');
		$view->type = \Input::get('type');
		$view->saved_email = '';
		return $this->response($view);
	}

	/**
	* Check the login
	*
	*/
	public function post_check() {
		\Lang::load('public');
		$json = array('status' => 'FAILED', 'message' => __('login_failed'));
		$type = strtolower(\Input::post('login_type'));
		// Valid types
		if (in_array($type, array('realtor','broker','vendor','admin'))) {
			$authlite = \Authlite::instance('auth_' . $type);
			$authlite->logout();
			$remember = (bool)\Input::post('remember_me', 0);
			if ($authlite->login(\Input::post('login_email'), \Input::post('login_pass'), $remember)) {
				$model = '\Model\\' . ucwords($type);
				$user = $authlite->get_user();
				if ($user->account_status == \Model\User::AS_SUSPENDED){
					//account suspended
					$authlite->logout();
					$json['message'] = __('account_suspended');
				} elseif (in_array($user->account_status, array(\Model\User::AS_VALID, \Model\User::AS_NEW, \Model\User::AS_NEEDS_PASS))) {
					//account normal
					if ($model::forge($user->id)->login($remember)) {
						\Model\User::forge()->set_user_type($type);
						$json = array('status' => 'OK', 'message' => \Uri::create($type . 's'));
					} else {
						$json['message'] = __('login_error');
					}
				} else {
					$json['message'] = __('login_error');
				}
			}
		}
		return $this->response($json);
	}
	/**
	* Send lost password info
	*
	*/
	public function post_password() {
		$type = strtolower(\Input::post('login_type'));
		// Valid types
		if (in_array($type, array('realtor','broker','vendor','admin'))) {
			$authlite = \Authlite::instance('auth_' . $type);
			$authlite->logout();
			\Lang::load('public');
			if ($authlite->force_login(\Input::post('login_email'))) {
				$model = '\Model\\' . ucwords($type);
				$user = $authlite->get_user();
				if ($model::forge($user->id)->forgot_password()) {
					return $this->response(__('email_sent'));
				} else {
					return $this->response(__('login_error'));
				}
			} else {
				return $this->response(__('email_not_found'));
			}
		}
	}
}