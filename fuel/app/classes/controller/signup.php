<?php
namespace Controller;

class Signup extends PublicTemplate {
	public $type = 'realtor';
	public function before() {
		parent::before();
		if ($this->auto_render) {
			// Logout any users
			\Authlite::instance('auth_realtor')->logout();
			\Authlite::instance('auth_broker')->logout();
			\Authlite::instance('auth_vendor')->logout();
			$this->template->meta_title = 'xTradeHomes Sign Up';
			$this->template->styles = array('auto-complete.css','token-input.css', 'tooltipster.css');
			$this->template->scripts = array('jquery.mask.js', 'auto-complete.min.js', 'jquery.tokeninput.js', 'jquery.tooltipster.min.js', 'signup.js');
		}
	}

	// Default to realtor
	public function action_index() {
		\Response::redirect('signup/realtor');
	}

	public function action_realtor() {
		$view = \View::forge('signup_realtor');
		$view->promo_code = '';
		if(\Input::get('broker') !== null) {
			$broker = \Model\Broker::forge()->load_by_id(\Input::get('broker'));
			if ($broker->loaded()){
				if(\Input::get('id') !== null) {
					$user = \Model\Realtor::forge()->load_by_id(\Input::get('id'))->as_array();
					$user['company_name'] = $broker->company_name;
					$user['promo_code'] = $view->promo_code = \Model\Plan::forge($broker->plan_id)->promo_code;
					$user['broker_id'] = $broker->broker_id;
				}
			}
		}
		if (!isset($user)) {
			$user = \Model\Realtor::forge()->as_array();
			$user['company_name'] = '';
			$user['broker_id'] = '';
			$user['promo_code'] = '';
		}
		$user['total'] = \Sysconfig::get('realtor_monthly_fee');
		$user['broker_ref'] = (\Input::get('bref') === null) ? false : true;
		\Lang::load('common');
		\Lang::load('signup');
		\Lang::load('eula');
		// Extra fields
		$user['cemail'] = ''; // Check email
		$user['cpassword'] = ''; // Check password
		$view->fee = \Sysconfig::get('realtor_monthly_fee');
		$view->data = $user;
		$view->signup_type = $this->type;
		$this->template->content = $view;
	}

	public function action_broker() {
		$this->type = 'broker';
		$view = \View::forge('signup_broker');
		$user = \Model\Broker::forge()->as_array();
		\Lang::load('common');
		\Lang::load('signup');
		\Lang::load('eula');
		// Extra fields
		$user['cemail'] = ''; // Check email
		$user['cpassword'] = ''; // Check password
		$user['email'] = \Input::get('email');
		$user['rref'] = \Input::get('rref');
		$user['realtor_id'] = \Input::get('rid');
		$view->fee = \Sysconfig::get('broker_monthly_fee');
		$view->cboAgents = \Model\Broker::forge()->get_agent_qty();
		$view->data = $user;
		$view->signup_type = $this->type;
		$this->template->content = $view;
	}

	public function action_vendor() {
		$this->type = 'vendor';
		$view = \View::forge('signup_vendor');
		$user = \Model\Vendor::forge()->as_array();
		\Lang::load('common');
		\Lang::load('signup');
		\Lang::load('eula');
		// Extra fields
		$user['cemail'] = ''; // Check email
		$user['cpassword'] = ''; // Check password
		//todo : fix dummy data
		$user['types_service'] = ''; //type of service
		$user['advert_address'] = ''; //advertiser address
		$view->fee = \Sysconfig::get('vendor_monthly_fee');
		$view->feat_fee = \Sysconfig::get('vendor_featured_fee');
		$view->data = $user;
		$view->promo_code = '';
		$view->promo_details = '';
		$view->signup_type = $this->type;
		// Text editor
		$this->template->styles = array_merge($this->template->styles,array('jquery-te.css','uploadfile.css', 'sumoselect.css', 'style.css'));
		$this->template->scripts = array_merge($this->template->scripts,array('jquery-te.min.js','jquery.uploadfile.min.js', 'jquery.sumoselect.min.js'));
		$this->template->content = $view;
	}

	public function post_save() {
		\Lang::load('signup');
		\Lang::load('common');
		$model = '\Model\\' . ucwords(\Input::post('user_type'));
		try {
			\Session::instance()->set('signup', $model::forge()->register(\Input::post()));
			return $this->response(array('status' => 'OK'));
		} catch(\AutoModelerException $e) {
			return $this->response(array('errors' => $e->errors));
		}
	}

	public function get_payment() {
		\Lang::load('orders');
		if (($signup = \Session::instance()->get('signup')) !== null) {
			$view = \View::forge('dialog/payment');
			$years = range(date('Y'), date('Y') + 8);
			$view->cboYears = array_combine($years, $years);
			$signup['featured_ad'] = 0;
			$signup['featured_ad_summary'] = '';
			$signup['promo_txt'] = '';
			$signup['summary'] = __('recurring_payment');
			if(isset($signup['featured']) && $signup['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED){
				$signup['featured_ad'] = \Sysconfig::get($signup['user_type'] . '_featured_fee');
				$signup['featured_ad_summary'] = 'Monthly Featured Ad';
				$total_fee = $signup['featured_ad'] + \Sysconfig::get($signup['user_type'] . '_monthly_fee');
			} else {
				$total_fee = \Sysconfig::get($signup['user_type'] . '_monthly_fee');
				// Get promo details
				if (trim($signup['promo_code']) != '') {
					if (trim($signup['crea_id']) != '') {
						$plan = \Model\Plan::forge()->load_by_csp(\CityState::CA, 0, $signup['promo_code']);
					} else {
						$plan = \Model\Plan::forge()->load_by_csp(\CityState::US, 0, $signup['promo_code']);
					}
					if ($plan->loaded()) {
						$signup['promo_txt'] = "{$plan['promo_code']} - First {$plan['promo_len']} " .
						\Inflector::pluralize('Month', $plan['promo_len']) . ' Free';
						$signup['summary'] = 'Monthly fee after Promo period expires';
					}
				}
			}

			$signup['fee'] = \Sysconfig::get($signup['user_type'] . '_monthly_fee');
			$view->total = $total_fee;
			$signup['tax'] = $signup['tax_code'] = '';
			$signup['summary_total'] = __('monthly_fee');
			$signup['listing_id'] = '';
			$usertypes = array_flip(\Model\User::forge()->get_types());
			$signup['ut'] = $usertypes[$signup['user_type']];
			$signup['address'] = '';
			$signup['cpc_address'] = '';
			$signup['z_p_code'] = '';

			//initial value of card
			$signup['cc_number'] = $signup['cc_cvv'] = $signup['exp_month'] =
			$signup['exp_year'] = $signup['cc_type'] = '';
			$payment = new \Model\Paypalrest\Payment();
			$signup['cbo_cc_type'] = $payment->get_cc_type();
			$months = array_map(create_function('&$v', 'return sprintf("%02d", $v);'), range(1, 12));
			$signup['cbo_month'] = array_combine($months, $months);

			$view->set('data', $signup);
			$view->broker_id = false;
		} else {
			$view = \View::forge('dialog/payment_error');
		}
		return $this->response($view);
	}

	public function post_payment(){
		if (($signup = \Session::instance()->get('signup')) !== null) {
			$model = '\Model\\' . ucwords($signup['user_type']);
			$response = $model::forge($signup['id'])->signup($signup, \Input::post('data'));
			return $this->response($response);
		}
		return $this->response('Session lost');
	}
	/**
	* Upload logo. Currently only used by vendor
	*
	*/
	public function post_doupload() {
		$output_dir = DOCROOT . \Model\Vendor::IMG_PATH;
		$json = array();

		if (!is_dir($output_dir)){
			mkdir($output_dir);
		}

		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			$fileName = \Session::key() . '.' . strtolower($ext);

			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $fileName);
			$json[] = $fileName;
		}
		return $this->response($json);
	}

	public function get_success() {
		\Lang::load('orders');
		if (($signup = \Session::instance()->get('signup')) !== null) {
			\Session::instance()->delete('signup');
			// Login the user in
			$authlite = \Authlite::instance('auth_' . $signup['user_type']);
			$authlite->force_login($signup['email']);
			$view = \View::forge('dialog/signup_success');
			$view->set('data', $signup);
		} else {
			$view = \View::forge('dialog/payment_error');
		}
		return $this->response($view);
	}

	public function get_broker_join() {
		if (($signup = \Session::instance()->get('signup')) !== null) {
			$response = \Model\Broker::forge()->signup($signup);
			$view = \View::forge('dialog/broker_success');
		} else {
			$view = \View::forge('dialog/broker_failed');
		}
		return $this->response($view);
	}

	// TODO used only for focus group
	public function get_realtor_join() {
		if (($signup = \Session::instance()->get('signup')) !== null) {
			$response = \Model\Realtor::forge()->signup($signup);
			$view = \View::forge('dialog/broker_success');
		} else {
			$view = \View::forge('dialog/broker_failed');
		}
		return $this->response($view);
	}

	public function get_taxes() {
		$json = array('tax_code' => '', 'tax_txt' => '', 'tax' => 0, 'total' => \Input::get('fee') + \Input::get('featured_ad'));
		$taxes = \Model\Tax::forge()->load_info(\Input::get('cpc'));
		if ($taxes->loaded() && ($taxes->country == \CityState::CA) && ($taxes->rate > 0)) {
			//breakdown taxes
			//note: featured_ad input only for vendor type, user type broker & realtor value always 0
			//featured_tax is based on featured_ad
			/*
			$signup['regular_tax'] = round(($signup['fee']) * $taxes->rate, 2);
			$signup['featured_tax'] = round(($signup['featured_ad']) * $taxes->rate, 2);
			$tax = $signup['regular_tax'] + $signup['featured_tax'];
			*/
			$json['tax'] = round((\Input::get('fee')) * $taxes->rate, 2);
			$json['tax_code'] = $taxes->code;
			$json['featured_tax'] = round((\Input::get('featured_ad')) * $taxes->rate, 2);
			$json['tax_txt'] = 'Taxes: $' . number_format($json['tax'] + $json['featured_tax'], 2);
			$json['total'] = '$' . number_format(\Input::get('fee') + \Input::get('featured_ad') + $json['tax'] + $json['featured_tax'], 2);
		}
		return $this->response($json);
	}
}