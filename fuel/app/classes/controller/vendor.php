<?php
namespace Controller;

class Vendor extends PublicTemplate {
    const VENDOR_LISTS_PER_PAGE = 3;

    public function action_location($location = NULL){
        \Lang::load('common');
        /*
        if(!isset($location)){
			$location = \CityState::get_visitor_location();
			if($location == ''){
				\Response::redirect('/');
			}
        }
        */
        $view = \View::forge('vendor/location_result');
        $view->location = $location;
        $this->template->meta_title = 'xTradeHomes Directory Results';
		$this->template->styles = array_merge($this->template->styles,array('sumoselect.css'));
        $this->template->scripts = array('jquery.sumoselect.min.js', 'vendor.js');
        $this->template->content = $view;
    }

    public function post_result(){
        $post = \Input::post();
        $load_all = \Model\Vendor::forge()->load_result($post);
        $view = \View::forge('vendor/result');
        $view->location = $post['location'];
		$view->type = isset($post['type']) ? join(';', $post['type']) : '';
        $config = array(
            'pagination_url' => \Uri::base(false) . 'vendor/result.html',
            'total_items' => count($load_all),
            'per_page' => self::VENDOR_LISTS_PER_PAGE,
            'uri_segment' => 'page'
        );
        $pagination = \Pagination::forge('mypagination', $config);
        $result = \Model\Vendor::forge()->load_result($post, $pagination, self::VENDOR_LISTS_PER_PAGE, \Input::get('page'));
        $view->pagination = $pagination;
        $view->result = $result;
        return $this->response($view);
    }

	public function get_logo(){
		$view = \View::forge('dialog/vendor_logo');
		$view->logo = \Model\Vendor::forge()->get_vendor_logo(\Input::get('vendor_id'));
		return $this->response($view);
	}

	public function get_locale(){
		return $this->response(\Model\Vendor::forge()->get_locale(\Input::get('vendor_id')));
	}

}