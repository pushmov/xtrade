<?php
namespace Controller;
use Model;

class PublicTemplate extends \Controller_Hybrid {
	public $template = 'public_base';
	public $member = null;

	public function before() {
		parent::before();
		\Lang::load('common');
		$header = 'public_header';
		$data = array();
		$this->auto_render = !\Input::is_ajax();
		if ($this->auto_render) {
			// Login check
			foreach(array('realtor','broker','vendor','admin') as $k) {
				if (($user = \Authlite::instance('auth_' . $k)->logged_in()) !== false) {
					// Used on header link
					$user_info = \Html::anchor($k . 's', __($k.'_home'), array('title' => "{$user->first_name} {$user->last_name}"));
					$data = array('user_info' => $user_info, 'logout' => $k . 's');
					$header = 'member_header';
					$this->member = $user;
					break;
				}
			}
			\Config::load('google');
			$this->template->tracking_code = \Config::get('track_code');

			// Initialize empty values
			$this->template->meta_title = '';
			$this->template->meta_desc = '';
			$this->template->title = '';
			$this->template->content = '';
			$this->template->saved_email = '';
			$this->template->header = \View::forge($header, $data);
			//$this->template->header->remember_check = '';
			//$this->template->header->force_popup = false;
			$this->template->styles = array();
			$this->template->scripts = array();
		}
	}
}