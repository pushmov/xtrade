<?php
namespace Controller;

class Privacy extends PublicTemplate {
	public function before() {
		parent::before();
		$this->template->meta_title = 'xTradeHomes Privacy Policy';
	}

	public function action_index() {
		\Lang::load('privacy');
		$this->template->content = \View::forge('privacy');
	}

}
