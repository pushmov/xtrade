<?php
namespace Controller;

class Faq extends PublicTemplate {
	public function before() {
		parent::before();
		$this->template->meta_title = 'xTradeHomes FAQ';
	}

	public function action_index() {
		\Lang::load('faq');
		if (\Input::get('agent') == 'faq') {
			$view = \View::forge('agent_faq');
		} else {
			$view = \View::forge('faq');
			$view->member = $this->member;
			if ($this->member !== null) {
				$view->data = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22);
			} else {
				$view->data = array(1, 2, 3, 6, 7, 9, 10, 11, 13, 15, 19, 20, 21);
			}
		}
		$this->template->content = $view;
	}

}
