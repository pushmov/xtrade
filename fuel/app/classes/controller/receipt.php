<?php
namespace Controller;

class Receipt extends PublicTemplate {
	public function action_index(){
		$gst_num = '';
		$id = (int)substr(\Input::get('id'), 1);
		$order = \Model\Order::forge()->load(\DB::select_array()->where('id', '=', $id))->as_array();
		if ($order['id'] !== NULL) {
			$type = \Model\User::forge()->get_types($order['user_type_id']);
			if (in_array($order['type_id'], array(\Model\Order::OT_LISTING, \Model\Order::OT_FEATURED, \Model\Order::OT_UPGRADE))) {
				$view = \View::forge('receipt_listing');
			} else {
				$view = \View::forge('receipt_'.$type);
			}
			$model = '\Model\\' . ucwords($type);
			$db = \DB::select_array()->where('id', '=', $order['user_id']);
			$user = $model::forge()->load($db)->as_array();
			if($order['user_type_id'] == \Model\User::UT_VENDOR) {
				list($user['city'], $user['prov_state'], $user['country']) = explode(', ',$user['location_address']);
			}
			if ($user['country'] == \Model\User::COUNTRY_CA){
				$gst_num = 'GST #: '.\Sysconfig::get('gst_no');
			}
			$view->date = date_create($order['order_date'])->format('M j, Y');
			$view->name = $user['first_name'].' '.$user['last_name'];
			$view->company_name = $user['company_name'];
			$view->amount = number_format($order['total'], 2);
			$view->initial_amount = '';
			$view->features = '';
			$view->taxes = 'Tax : $'.number_format($order['taxes'], 2);
			$view->total = number_format($order['sub_total'], 2);
			$view->gst_num = $gst_num;

			$pdf = \Pdf::forge($title = 'Receipt');
			$pdf->writeHTML($view, true, false, true, false, '');
			$pdf->Output(\Input::get('id').'.pdf', 'I');
		} else {
			throw new \HttpNotFoundException;
		}
	}

}