<?php
namespace Controller\Brokers;

class Realtor extends Auth {
	public function action_match($listing_id = NULL){
        if(!isset($listing_id)){
            \Response::redirect('/realtors');
        }
		\Lang::load('common');
		\Lang::load('matches');
		$inc = \Input::get('inc') ? true : false;
		$data = \Match::forge()->render_matches($listing_id, \Input::get('realtor_id'), $inc);
        $js_var = array(
            'id' => $listing_id,
			'realtor_id' => \Input::get('realtor_id')
        );
        $this->template->scripts = array('matches.js', 'broker_matches.js');
		$this->template->styles = array('matches.css');
        $this->template->js_vars = $js_var;
        $this->template->content = \View::forge('matches/index', $data);
    }
}