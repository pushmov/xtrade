<?php
namespace Controller;

class User extends PublicTemplate {
	public function action_activation(){
		$key = \Input::get('key');
		$user_type = \Input::get('type');
		$view = \View::forge('home_page');
		if (in_array($user_type, array('realtor','broker','vendor'))) {
			$model = '\Model\\' . ucwords($user_type);
			$response = $model::forge()->activate_user($key, $user_type);
			$js_vars = array(
				'key' => '\''.$key.'\'',
				'type' => '\''.$user_type.'\'',
				'email' => '\''.$response['email'].'\''
			);
			$view->scripts = 'activation.js';
			$view->js_vars = $js_vars;
		}
		return \Response::forge($view);
	}

	/**
	* check if use has been activated by system
	*/
	public function post_active(){
		\Lang::load('common');
		$email = \Input::post('email');
		$user_type = \Input::post('type');
		$model = '\Model\\' . ucwords($user_type);
		$response = $model::forge()->check_activate_user($email, $user_type);
		return $this->response($response);
	}

	public function action_unsubscribe(){
		$email = \Input::get('email');
		$model = '\Model\\'.ucwords(\Input::get('type'));
		if($model::forge()->user_unsubscribe_email($email)){
			\Session::set_flash('unsubscribe', TRUE);
		}
		\Response::redirect('/');
	}

	public function action_unsubscribed(){
		return $this->response(\View::forge('dialog/unsubscribe'));
	}

	public function get_forms(){
		return $this->response(\View::forge('dialog/forms'));
	}

}