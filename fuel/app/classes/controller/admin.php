<?php
namespace Controller;

class Admin extends \Controller {
	
	public function action_index(){
		\Session::set_flash('admin_dialog', true);
		$view = \View::forge('home_page');
		return \Response::forge($view);
	}
	
}