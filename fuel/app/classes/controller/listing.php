<?php
namespace Controller;

class Listing extends PublicTemplate {

	public function action_search($location) {
		\Lang::load('listing');
		\Session::set(\Model\Listing::SESS_REFERER_NAME, \Uri::current());
		$view = \View::forge('listing/no_results');
		$sort = (\Input::get('sort')) ? \Input::get('sort') : \Model\Listing::DEFAULT_SORT_VALUE;
		$limit = (\Input::get('per')) ? \Input::get('per') : \Model\Listing::DEFAULT_ITEMS_PER_PAGE;
		$page = (\Input::get('page')) ? \Input::get('page') : \Model\Listing::DEFAULT_PAGE_NUM;
		$view->location = $location;
		$view->featured = \Model\Client\Has::forge()->get_featured();
		$view->search = \Session::get('search_filter') ? \Session::get('search_filter') : \Model\Listing::forge()->user_saved_search();
		if(\Input::is_ajax()){
			$refine_search['filter'] = (\Input::get('filter')) ? \Input::get('filter') : \Session::get('search_filter', array());
			$result = \Model\Listing::forge()->search($location, $refine_search, $sort, $limit, $page);
			$refine_search['filter']['sort'] = $sort;
			$refine_search['filter']['limit'] = $limit;
			\Session::set('search_filter', $refine_search['filter']);
			$result['filter'] = $refine_search['filter'];
			$html = \View::forge('listing/result_listing', $result);
			return $this->response($html);
		}
		$this->template->styles = array('lightslider.min.css', 'tooltipster.css', 'sumoselect.css');
		$this->template->meta_title = 'xTradeHomes Listing Results';
		$this->template->content = $view;
		$this->template->scripts = array('lightslider.min.js', 'jquery.tooltipster.min.js', 'jquery.sumoselect.min.js', 'jquery.mask.js', 'search.js', 'contact_agent.js');
	}

	public function get_reset(){
		//here default value for user search defined configuration
		\Session::set('search_filter', \Model\Listing::forge()->user_saved_search());
		return $this->response(array('status' => 'OK'));
	}

	public function get_refine(){
		\Lang::load('common');
		$data['location'] = \Input::get('location');
		$url = urldecode(\Input::get('url'));
		parse_str($url, $filter);
		$data['filter'] = array_merge($filter, (\Session::get('search_filter')) ? \Session::get('search_filter') : \Model\Listing::forge()->user_saved_search());
		$view = \View::forge('dialog/refine', $data);
		return $this->response($view);
	}

	public function action_detail($listing_id = null){
		\Lang::load('listing');
		\Lang::load('common');
		$data = \Listing::forge()->listing_detail($listing_id, true);
		if($data === false){
			\Response::redirect('/realtors');
		}
		$this->template->meta_title = 'Listing details for '.$data['address'].' '.\CityState::get_area_name($data['city'], $data['prov_state'], $data['country'], false);
		$this->template->scripts = array('listing_detail.js', 'lightslider.min.js', 'contact_agent.js', 'jquery.tooltipster.min.js', 'jquery.mask.js');
		$this->template->styles = array('lightslider.min.css', 'tooltipster.css');
		$this->template->js_vars = array(
			'lat' => $data['lat'],
			'lng' => $data['lng']
		);
		$data['param'] = $listing_id;
		$data['is_logged_in'] = !($this->member === null);
		$data['nav'] = \Model\Listing::forge()->listing_navigation($listing_id);
		$this->template->content = \View::forge('listing/detail', $data);
	}

	public function get_client_form(){
		\Lang::load('listing');
		$input = \Input::get();
		$listing_id = $input['id'];
		$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id)->where('client_fill_key', '=', $input['key']));
		if(!$listing->loaded()){
			\Response::redirect('/');
		}
		$client_wants = \Model\Client\Want::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id))->as_array();
		$view = \View::forge('listing/wants_info');
		$view->listing_id = $listing_id;
		$view->key = $input['key'];
		$client_wants['address'] = \CityState::get_area_name($client_wants['city'], $client_wants['prov_state'], $client_wants['country']);
		$view->has = $listing;
		$client_wants['prop_style'] = $client_wants['prop_style'] == '' ? array(9) : str_split($client_wants['prop_style']);
		$client_wants['floor_type'] = $client_wants['floor_type'] == '' ? array(9) : str_split($client_wants['floor_type']);
		$client_wants['roof_type'] = $client_wants['roof_type'] == '' ? array(9) : str_split($client_wants['roof_type']);
		$client_wants['garage_type'] = $client_wants['garage_type'] == '' ? array(9) : str_split($client_wants['garage_type']);
		$client_wants['siding_type'] = $client_wants['siding_type'] == '' ? array(9) : str_split($client_wants['siding_type']);

		$view->wants = $client_wants;
		$view->show_request_button = FALSE;
		$view->matches_notification = array('matches' => '');
		$template = \View::forge('listing/request_client_form');
		$template->wants_info = $view;
		$template->listing_id = $listing_id;
		$this->template->meta_title = 'xTradeHomes Client Access Form';
		$this->template->styles = array('sumoselect.css', 'style.css');
		$this->template->scripts = array('common.js', 'jquery.sumoselect.min.js', 'client_form.js');
		$this->template->js_vars = array('listing_id' => '\''.$listing_id.'\'');
		$this->template->content = $template;
	}

	public function post_client_update(){
		\Lang::load('common');
		\Lang::load('listing');
		try {
			$response = \Model\Listing::forge()->save_client_wants_form(\Input::post('wants'));
		} catch(\AutomodelerException $e){
			$response = array('status' => 'ERROR', 'errors' => $e->errors, 'fields' => 'wants');
		}
		return $this->response($response);
	}

	public function post_neighinline(){
        \Lang::load('listing');
		$label_for = 'neighbourhood_h';
        $type = \Input::post('type');
        $listing_id = \Input::post('lid');
		if($type == \Model\Neighbourhoods::NEIGHBOURHOOD_TYPE_WANTS){
			$label_for = 'neighbourhood_w';
		}
        $data = \Model\Neighbourhoods::forge()->get_location_neighbourhood(\Input::post('location'), $type, $listing_id);
		$view = \View::forge('listing/wants_neighbourhood_select');
		$view->type = $type;
		$view->label_for = $label_for;
		$view->select = $data['select'];
        return $this->response($view);
    }

	public function get_view_map(){
		$lat = \Input::get('lat');
		$lon = \Input::get('lon');
		$view = \View::forge('dialog/listing/view_map');
		$view->map_key = \Sysconfig::get_map_key();
		$view->lat = $lat;
		$view->lon = $lon;
		return $this->response($view);
	}

	public function post_featured(){
		$listing_id = \Input::post('id');
		return $this->response($listing_id);
	}

	/**
	* Gets the infor for featured tooltip
	*
	*/
	public function get_featured(){
		\Lang::load('listing');
		$view = \View::forge('listing/listing_tooltip_content');
		$view->data = \Model\Client\Has::forge(\Input::get('id'))->get_tooltip_content();
		return $this->response($view);
	}

	public function get_client_form_success(){
		return $this->response(\View::forge('dialog/listing/client_form_success'));
	}

	public function get_images(){
		$listing_id = \Input::get('listing_id');
		$view = \View::forge('listing/image_container');
		$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id));
		$view->imported = $listing->imported;
		if ($listing->imported == \Model\Listing::NON_IMPORTED) {
			$view->images = \Model\Photos\Xtrade::forge()->load_photos($listing_id);
		} else {
			$view->images = \Model\Crea\Property::forge()->load_photos($listing->external_id);
		}
		// Use the array key as the current order
		$view->image_order = implode(',', array_keys($view->images));
		return $this->response($view);
	}

	public function post_update(){
		\Lang::load('common');
		return $this->response(\Model\Listing::forge()->update_listing(\Input::post()));
	}

	public function get_completed(){
		\Lang::load('listing');
		$view = \View::forge('dialog/listing/completed');
		$view->txt = \Model\Client::forge()->get_completion_code_txt(\Input::get('status'));
		return $this->response($view);
	}

	public function post_update_field(){
		\Lang::load('common');
		$area = '\\Model\\Client\\'.ucwords(\Input::post('area'));
		$data = array(
			'field' => \Input::post('field'),
			'value' => \Input::post('value'),
			'listing_id' => \Input::post('listing_id'),
			'area' => \Input::post('area'),
			'index' => \Input::post('index') ? \Input::post('index') : 0,
			'fn_name' => \Input::post('fn_name') ? \Input::post('fn_name') : ''
		);
		return $this->response($area::forge()->update_field($data));
	}

	public function post_update_wants_neighbourhood(){
		\Lang::load('common');
		return $this->response(\Model\Client\Want::forge()->update_neighbourhoods(\Input::post()));
	}

	public function post_completed_section(){
		\Lang::load('common');
		$area = '\\Model\\Client\\'.ucwords(\Input::post('area'));
		return $this->response($area::forge()->incomplete_section_check(\Input::post()));
	}

	public function get_wantrequest(){
		\Lang::load('common');
		$view = \View::forge('dialog/listing/want_request');
		$listing_id = \Input::get('listing_id');
		$view->data = \Model\Client\Want::forge()->load(\DB::select_array()
			->join('client_information', 'left')->on('client_information.listing_id', '=', 'client_wants.listing_id')
			->where('client_wants.listing_id', '=', $listing_id));
		$view->data['listing_id'] = $listing_id;
		return $this->response($view);
	}

	public function post_wantrequest(){
		return $this->response(\Model\Listing::forge()->want_request(\Input::post()));
	}
}
