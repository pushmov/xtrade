<?php
namespace Controller\Admins;

class Broker extends Auth{
	public $name = 'broker';

	public function action_index(){
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js');
		$view = \View::forge('admin/broker/broker_list');
		$this->template->content = $view;
	}

	public function get_list(){
		return $this->response(\Model\Broker::forge()->load_brokers(\Input::get()));
	}

	public function post_detail(){
		\Lang::load('common');
		$view = \View::forge('admin/broker/broker_detail');
		$broker = \Model\Broker::forge(\Input::post('broker_id'));
		$view->data = $broker->as_array();
		$view->location = \CityState::get_area_name($broker->city, $broker->prov_state, $broker->country);
		$view->suspend_or_not = \Model\Admin::forge()->suspend_or_not($broker->account_status, $broker->broker_id, 'broker');
		$view->renewal = \Model\Admin::forge()->load_broker_renewal_date($broker->paypal_sub_id);
		$view->cboPromo = \Model\Plan::forge()->promo_list($broker->country);
		return $this->response($view);
	}

	public function post_realtor(){
		$view = \View::forge('admin/broker/realtor_list');
		$view->data = \Model\Realtor::forge()->load(\DB::select_array()->where('office_id', '=', \Input::post('broker_id')), NULL)->as_array();
		$broker = \Model\Broker::forge(\Input::post('broker_id'))->as_array();
		$view->broker = $broker;
		return $this->response($view);
	}

	public function get_add(){
		$view = \View::forge('dialog/admin/broker/add_realtor_to_broker');
		$view->broker_id = \Input::get('broker_id');
		return $this->response($view);
	}

	public function post_add(){
		\Lang::load('common');
		\Lang::load('signup');
		//todo : fix how password works when invited by broker
		return $this->response(\Model\Realtor::forge()->add_realtor_to_broker(\Input::post('data')));
	}

	public function get_reset(){
		$view = \View::forge('dialog/admin/broker/reset_password');
		$view->broker_id = \Input::get('broker_id');
		return $this->response($view);
	}

	public function post_reset(){
		$model = '\Model\\' . ucwords($this->name);
		return $this->response($model::forge()->admin_reset_password(\Input::post('broker_id'), $this->name));
	}

	public function get_suspend(){
		$view = \View::forge('dialog/admin/broker/suspend_account');
		$view->broker_id = \Input::get('broker_id');
		return $this->response($view);
	}

	public function post_suspend(){
		$model = '\Model\\' . ucwords($this->name);
		return $this->response($model::forge()->suspend_user(\Input::post('data.broker_id'), $this->name));
	}

	public function get_unsuspend(){
		$view = \View::forge('dialog/admin/broker/unsuspend_account');
		$view->broker_id = \Input::get('broker_id');
		return $this->response($view);
	}

	public function post_unsuspend(){
		$model = '\Model\\' . ucwords($this->name);
		return $this->response($model::forge()->unsuspend_user(\Input::post('data.broker_id'), $this->name));
	}

	public function get_delbroker(){
		$view = \View::forge('dialog/admin/broker/delete_broker');
		$view->broker_id = \Input::get('broker_id');
		return $this->response($view);
	}

	public function post_delbroker(){
		return $this->response(\Model\Broker::forge()->delete_broker(\Input::post('data.broker_id'), $this->name));
	}

	public function get_delrealtor(){
		$view = \View::forge('dialog/admin/broker/delete_realtor');
		$view->realtor_id = \Input::get('realtor_id');
		return $this->response($view);
	}

	public function post_delrealtor(){
		return $this->response(\Model\Realtor::forge()->delete_realtor(\Input::post('data.realtor_id'), 'realtor'));
	}

	public function action_crea(){
		$name = 'crea broker';
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js');
		$view = \View::forge('admin/broker/crea_broker_list');
		$this->template->content = $view;
	}

	public function get_crea_list(){
		return $this->response(\Model\Crea\Agent::forge()->load_brokers(\Input::get()));
	}
}