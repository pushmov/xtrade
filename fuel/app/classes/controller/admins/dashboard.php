<?php
namespace Controller\Admins;

class Dashboard extends Auth {
	public $name = 'dashboard';
	public function action_index(){
		\Lang::load('listing');
		\Session::set(\Model\Listing::SESS_REFERER_NAME, \Uri::current());
		$view = \View::forge('admin/dashboard');
		$filter = \Model\Listing::filter_array();
		$view->filters = $filter['view'];
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js');
		$this->template->content = $view;
	}
	public function get_password(){
		\Lang::load('signup');
		$view = \View::forge('dialog/change_password')->bind('err_msg', $msg);
		$view->type = \Model\User::UT_ADMIN;
		$view->id = \Input::get('id');
		return $this->response($view);
	}

	public function post_pass_update() {
		return $this->response(\Model\Admin::forge(\Input::post('id'))->update_password(\Input::post()));
	}

	public function get_listings(){
		\Lang::load('listing');
		return $this->response(\Model\Admin\Listing::forge()->dash_list(\Input::get()));
	}

}