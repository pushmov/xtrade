<?php
namespace Controller\Admins;

class Contact extends \Controller_Rest {
	public function get_agent(){
		$view = \View::forge('dialog/admin/contact_agent');
		$view->data = \Model\Realtor::forge(\Input::get('id'))->as_array();
        return $this->response($view);
	}

	public function post_send() {
		$json = array('status' => 'FAILED');
		$post = array_map('trim', \Input::post('contact'));
		$validation = \Validation::forge();
		$validation->add_field('name', 'Name', 'required');
		$validation->add_field('email', 'Valid email', 'required|valid_email');
		$validation->add_field('message', 'Message', 'required');
		if ($validation->run($post)) {
            $response = \Emails::forge()->admin_contact($post);
			$json = array('status' => 'OK', 'message' => '<div class="callout success small">Your message has been sent.</div>');
		} else {
			$json['errors'] = $validation->error_message();
		}
		return $this->response($json);
	}

	public function get_delete(){
		$view = \View::forge('dialog/admin/realtor_delete', \Model\Realtor::forge(\Input::get('id'))->as_array());
        return $this->response($view);
	}

	public function post_delete() {
		$user = \Model\Realtor::forge(\Input::post('id'));
		$user->account_status = \Model\User::AS_ADMIN_CLOSED;
		$user->save();
		return $this->response('OK');
	}
}