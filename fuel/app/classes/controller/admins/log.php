<?php
namespace Controller\Admins;

class Log extends Auth {
	public $name = 'log';
	public $style = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
	public $scripts = array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js');

    public function action_email(){
        $view = \View::forge('admin/email_log');
		$this->template->styles = $this->style;
		$this->template->scripts = $this->scripts;
		$this->template->content = $view;
    }

	public function get_email_log(){
		\Lang::load('listing');
		return $this->response(\Model\Admin::forge()->email_log(\Input::get()));
	}

    public function action_login(){
        $view = \View::forge('admin/login_log');
		$this->template->styles = $this->style;
		$this->template->scripts = $this->scripts;
		$this->template->content = $view;
    }

	public function get_login_log(){
		\Lang::load('listing');
		return $this->response(\Model\Admin::forge()->login_log(\Input::get()));
	}
	
	public function get_delete(){
		$view = \View::forge('dialog/admin/log_delete');
		$view->id = \Input::get('id');
		return $this->response($view);
	}
	
	public function post_loginlog_delete(){
		return $this->response(\Model\Loginlog::forge(\Input::post('data.id'))->delete_record());
	}
	
	public function post_emaillog_delete(){
		return $this->response(\Model\Log\Email::forge(\Input::post('data.id'))->delete_record());
	}

}