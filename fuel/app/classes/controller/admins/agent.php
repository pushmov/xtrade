<?php
namespace Controller\Admins;

class Agent extends Auth{
	public $name = 'agent';

	public function action_index(){
		$this->template->styles = array('datatables.min.css', 'responsive.dataTables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array('datatables.min.js', 'dataTables.responsive.min.js', 'responsive.foundation.min.js');
		$view = \View::forge('admin/agent');
		$view->set(\Model\Crea\Agent::forge()->get_summary());
		$this->template->content = $view;
    }

	public function get_offices(){
		return $this->response(\Model\Crea\Agent::forge()->get_brokers(\Input::get()));
	}

	public function get_filter(){
		$oid = \Input::get('office_id');
		$view = \View::forge('admin/agent_left_col');
		$view->data = \Model\Admin::forge()->get_listing_by_crea_realtor($oid);
		return $this->response($view);
	}
	public function action_office_info() {
		$data = \Model\Admin::forge()->office_info();
	}

	public function action_export(){
		header("Last-Modified: " . gmdate("D, d M Y H:i:s",time()) . " GMT");
		header("Content-type: text/x-csv");
		header("Content-Disposition: attachment; filename=search_results.csv");
		$inputs = array(
			'search' => array('value' => \Input::get('search')),
			'order' => array(0 => array(
				'column' => \Input::get('ordercol'),
				'dir' => \Input::get('orderdir')
			))
		);
		$csv = \Model\Admin::forge()->create_agent_csv($inputs);
		$header = array('Broker ID', 'Name', 'City', 'Province/State', 'Agent ID', 'Agent Name');
		array_unshift($csv, $header);
		$test = \Format::forge($csv)->to_csv();
		$this->auto_render = false;
		echo $test;
		exit();
	}

}