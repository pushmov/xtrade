<?php
namespace Controller\Admins;
use \Model\Paypalrest;

class Setting extends Auth {
	public $name = 'setting';

    public function action_index(){
        $view = \View::forge('admin/setting');
        $view->settings = \Sysconfig::all();
		$this->template->content = $view;
    }

    public function post_save(){
        return $this->response(\Sysconfig::save_settings(\Input::post('data')));
    }

    public function get_agent_generate($ca = true) {
		$pp = new Paypalrest;
		$info = array('name' => 'Agent Monthly GST', 'desc' => 'Agent monthly plan with 5% GST', 'amount' => 49.95,
			'currency' => 'CAD', 'tax' => 2.50, 'trial_len' => 0);
		$result = $pp->create_plan($info);
		\Sysconfig::update('ppp_agent_monthly', $result->id);
		return $this->response($result->id);
	}
}