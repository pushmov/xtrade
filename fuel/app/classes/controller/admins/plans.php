<?php
namespace Controller\Admins;

class Plans extends Auth {
	public $name = 'plans';
	public function action_index(){
		$view = \View::forge('admin/plan/index');
		$this->template->styles = array('datatables.min.css', 'responsive.foundation.min.css');
		$this->template->scripts = array('datatables.min.js', 'responsive.foundation.min.js', 'plans.js');
		$this->template->content = $view;
	}

	public function action_edit(){
		\Lang::load('admin');
		$plan = \Model\Plan::forge($this->param('id'));
		$view = \View::forge('admin/plan/form');
		if ($plan->id == null) {
			$view->page_title = 'Create Plan';
		} elseif ($plan->status_id == \Model\Plan::S_ARCHIVED) {
			$view->page_title = 'Archived Plan';
		} else {
			$view->page_title = 'Edit Plan';
		}
		$view->readonly = '';
		if ($plan->status_id >= \Model\Plan::S_ACTIVE) {
			$view->readonly = 'disabled';
		}
		$view->data = $plan->as_array();
		$status = \Model\Plan::forge()->get_status_array();
		unset($status[0]);
		$view->status = $status;
		$this->template->styles = array_merge($this->template->styles, array('jquery-te.css', 'admin.css'));
		$this->template->scripts = array_merge($this->template->scripts, array('jquery-te.min.js', 'plans.js'));
		$this->template->content = $view;
	}

	public function post_submit(){
		return $this->response(\Model\Plan::forge()->doupdate(\Input::post('data')));
	}

	public function get_datatable(){
		\Lang::load('common');
		return $this->response(\Model\Plan::forge()->admin_list(\Input::get()));
	}

	public function get_state(){
		return $this->response(\Form::select('data[state_id]', null, \CityState::get_state(\Input::get('id')), array('id' => 'state_id')));
	}

	public function get_active(){
		return $this->response(\View::forge('dialog/admin/plan/confirm_activate'));
	}

	public function post_active(){
		return $this->response(\Model\Plan::forge()->create(\Input::post('data')));
	}

	public function get_pause(){
		return $this->response(\View::forge('dialog/admin/plan/confirm_pause'));
	}

	public function post_pause(){
		return $this->response(\Model\Plan::forge()->pause(\Input::post('data')));
	}

	public function get_delete(){
		return $this->response(\View::forge('dialog/admin/plan/confirm_delete'));
	}

	public function post_delete(){
		return $this->response(\Model\Plan::forge()->deleteplan(\Input::post('data')));
	}
	public function action_getplan(){
		$pp = new \Model\Paypalrest();
		$data = \PayPal\Api\Plan::get(\Input::get('id'), $pp->get_api());
		echo '<pre>';
		print_r($data->toArray());
		exit();
	}
}