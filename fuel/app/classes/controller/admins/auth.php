<?php
namespace Controller\Admins;
use Model;

class Auth extends \Controller_Hybrid {
	public $template = 'admin/base_page';
	public $member;
	public $name = '';

	public function before() {
		parent::before();
		$this->auto_render = !\Input::is_ajax();
		// Login check
		if (!\Authlite::instance('auth_admin')->logged_in()) {
			\Session::set_flash('admin_dialog', true);
			\Response::redirect('/admins/login');
		} else {
			$member = \Authlite::instance('auth_admin')->get_user();
			if($member->enabled != \Model\Admin::ADMIN_ENABLED){
				\Response::redirect('/admins/dashboard');
			}
			$this->member = $member;
			// Used on header link
			$this->member->user_info = $this->member->first_name . ' ' . $this->member->last_name;
		}

		if ($this->auto_render) {
			// Initialize empty values
			$this->template->meta_title = 'xTradeHomes Admin Home';
			$this->template->meta_desc = '';
			$this->template->header = \View::forge('admin/subhead', array('name' => $this->member->user_info, 'breadcrumbs' => $this->render_breadcrumbs()));
			$this->template->content = '';
			$this->template->styles = array();
			$this->template->scripts = array();
		}
	}

	public function render_breadcrumbs(){
		$b = '<ul class="breadcrumbs">
		<li><a href="/admins/dashboard">Admin Home</a></li>
		<li><a href="/admins/'.$this->name.'">'.  ucwords($this->name).'</a></li>
		</ul>';
		return $b;
	}

	public function action_logout() {
		\Model\User::forge()->unset_user_type();
		\DB::update('admins')->set(array('logged_in' => 0))->where('id', '=', \Authlite::instance('auth_admin')->get_user()->id)->execute();
		\Authlite::instance('auth_admin')->logout();
		\Response::redirect('/');
	}
}