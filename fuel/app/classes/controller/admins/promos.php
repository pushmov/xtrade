<?php
namespace Controller\Admins;

class xxPromos extends Auth {
	public $name = 'promo';

	public function action_index(){
		$data['promo'] = \Model\Promocode::forge()->load(\DB::select_array()->order_by('account_type', 'asc'),null)->as_array();
		$view = \View::forge('admin/promo/index', $data);
		$this->template->content = $view;
	}

	public function action_add(){
		$data['page_title'] = 'Add Promotion';
		$data['data'] = \Model\Promocode::forge()->state('new');
		$view = \View::forge('admin/promo/promo_form', $data);
		$this->template->content = $view;
	}

	public function action_edit(){
		$promo = \Model\Promocode::forge()->load(\DB::select_array()->where('code', '=', \Input::get('code')))->as_array();
		$data['page_title'] = 'Edit Promo - ' . $promo['code'];
		$data['data'] = $promo;
		$view = \View::forge('admin/promo/promo_form', $data);
		$this->template->content = $view;
	}

	public function post_submit(){
		return $this->response(\Model\Promocode::forge()->submit_promo(\Input::post('data')));
	}

	public function get_allpromos(){
		\Lang::load('common');
		return $this->response(\Model\Promocode::forge()->all_promos(\Input::get()));
	}

	public function get_generate_plan(){
		$plan = \Model\Promocode::forge(\Input::get('id'))->generate_paypal_plan();
		return $this->response(array('status' => 'ok', 'id' => $plan->id));
	}

}