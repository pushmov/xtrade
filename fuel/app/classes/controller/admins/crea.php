<?php

namespace Controller\Admins;

class Crea extends \Controller {
	public function action_update() {
		$crea = new \Model\Crea\Update;
		$crea->do_update(true);
	}
	public function action_post_update() {
		$crea = new \Model\Crea\Update;
		$crea->do_update(false);
		//$crea->do_post_update();
	}
	public function action_import() {
		$crea = new \Model\Crea\Import;
		$crea->do_import();
		//$crea->do_import(1);
	}

	public function action_meta() {
		$feed = new \Model\Crea\feed;
		//$result = $feed->lookup_data('Property', 'TransactionType');
		$result = $feed->lookup_data('Property', 'ArchitecturalStyle');
		$a = 8;
	}

	public function action_matches() {

	}

	public function action_test() {
			$sql = "
			SELECT users.*
			from users
			WHERE external_listings_notified = 0
			";
			$results = \DB::query($sql)->execute();
			foreach($results as $row) {
				\Emails::forge()->agent_first_download($row);
				\DB::query("UPDATE users SET external_listings_notified = 1 WHERE id = {$row['id']}", \DB::UPDATE)->execute();
			}
	}

	public function action_property() {
		$feed = new \Model\Crea\feed();
		$sql = "ID=12596809";
		$arg['Limit'] = 100;
		$arg['Format'] = 'STANDARD-XML';
		$result = $feed->property_search($sql, $arg);
		$ss = 2;

	}
}