<?php
namespace Controller\Admins;

class Paypal extends \Controller {
	protected $_paypal;

	public function before() {
		$this->_paypal = new \Model\Paypalrest();
	}
	public function action_list_plans() {
		$params = array('status' => 'CREATED');
		$plan_list = \PayPal\Api\Plan::all($params, $this->_paypal->get_api());
		if (isset($plan_list->plans)) {
			echo '<h3>Created</h3>';
			echo '<table border="1" cellpadding="8">';
			foreach($plan_list->plans as $plan) {
				echo '<tr><td>' . $plan->name . '</td>';
				echo '<td>' . $plan->id . '</td>';
				echo '<td>' . $plan->state . '</td></tr>';
			}
			echo '</table>';
		} else {
			echo 'No Plans';
		}
		$params = array('status' => 'ACTIVE', 'page_size' => 20);
		$plan_list = \PayPal\Api\Plan::all($params, $this->_paypal->get_api());
		if (isset($plan_list->plans)) {
			echo '<h3>Active</h3>';
			echo '<table border="1" cellpadding="8">';
			foreach($plan_list->plans as $plan) {
				echo '<tr><td>' . $plan->name . '</td>';
				echo '<td>' . $plan->id . '</td>';
				echo '<td>' . $plan->description . '</td>';
				echo '<td>' . $plan->state . '</td></tr>';
			}
			echo '</table>';
		} else {
			echo 'No Plans';
		}
	}

	public function action_create_plan($data) {
		$data['name'] = 'Realtor Monthly USA';
		$data['desc'] = 'Realtor monthly United States';
		$data['amount'] = 49.95;
		$data['tax'] = 0;
		$data['trial_len'] = 1;
		$data['currency'] = 'USD'; // USD

		$result = $this->_paypal->create_plan($data);
		echo '<pre>';
		print_r($result->id);
	}

	public function action_view_plan() {
		$plan = \PayPal\Api\Plan::get('P-2X825305HF313592WP67PC5A', $this->_paypal->get_api());
		echo '<pre>';
		print_r($plan);
	}
	public function action_delete_plan() {
		$params = array('status' => 'ACTIVE', 'page_size' => 20);
		$plan_list = \PayPal\Api\Plan::all($params, $this->_paypal->get_api());
		foreach($plan_list->plans as $plan) {
			$plan = \PayPal\Api\Plan::get($plan->id, $this->_paypal->get_api());
			$result = $plan->delete($this->_paypal->get_api());
		}
		echo '<pre>';
		print_r($result);
	}

	public function action_view($pid){
		$plan = \PayPal\Api\Plan::get($pid, $this->_paypal->get_api());
		echo '<pre>';
		print_r($plan);
	}

	public function action_card(){
		$card = \PayPal\Api\CreditCard::get('CARD-2N113428D9347044LK52Q3AA', $this->_paypal->get_api());
		echo '<pre>';
		print_r($card);
		echo '</pre>';
	}


	public function action_store(){
		$card = new \PayPal\Api\CreditCard();
		$card->setType('visa')
		->setNumber('4032034380317357')
		->setExpireMonth('11')
		->setExpireYear('2020')
		->setCvv2('357')
		->setFirstName('Rizki')
		->setLastName('Aprilian');

		$card->setExternalCustomerId('rizki.a.aprilian@gmail.com');
		try{
			$result = $card->create($this->_paypal->get_api());
		} catch(PayPal\Exception $e){

		}

		echo '<pre>';
		print_r($result);
	}
}