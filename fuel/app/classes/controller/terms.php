<?php
namespace Controller;

class Terms extends PublicTemplate {
	public function before() {
		parent::before();
		$this->template->meta_title = 'xTradeHomes Terms &amp; Conditions';
	}

	public function action_index() {
		\Lang::load('terms');
		$this->template->content = \View::forge('terms');
	}

}
