<?php
namespace Model\Crea;

class Building extends \Automodeler {
	protected $_class = 'building';
	protected $_table_name = 'crea_building';

	protected $_data = array(
		'id' => NULL,'bathroomtotal' => '','bedroomstotal' => '','bedroomsaboveground' => '','bedroomsbelowground' => '','amenities' => '',
		'amperage' => '','anchor' => '','appliances' => '','architecturalstyle' => '','basementdevelopment' => '',
		'basementfeatures' => '','basementtype' => '','bomarating' => '','ceilingheight' => '','ceilingtype' => '',
		'clearceilingheight' => '','constructeddate' => '','constructionmaterial' => '','constructionstatus' => '','constructionstyleattachment' => '',
		'constructionstyleother' => '','constructionstylesplitlevel' => '','coolingtype' => '','displayasyears' => '','energuiderating' => '',
		'exteriorfinish' => '','fireplacefuel' => '','fireplacepresent' => '','fireplacetotal' => '','fireplacetype' => '',
		'fireprotection' => '','fixture' => '','flooringtype' => '','foundationtype' => '',
		'halfbathtotal' => '','heatingfuel' => '','heatingtype' => '','leedscategory' => '','leedsrating' => '',
		'roofmaterial' => '','roofstyle' => '','sizeexterior' => '','sizeinterior' => '',
		'storefront' => '','storiestotal' => '','totalfinishedarea' => '','type' => '','uffi' => '','utilitypower' => '','utilitywater' => '','vacancyrate' => ''
	);

}