<?php
namespace Model\Crea;

class Xml extends \Automodeler {
    protected $_class = 'xml';
    protected $_table_name = 'crea_xml_content';

    protected $_data = array( 
        'id' => NULL,
        'realtor_id' => '',
        'office_id' => '',
        'office_name' => '',
        'url' => '',
        'listings' => ''
    );
}