<?php
namespace Model\Crea;

class feed extends \Model {
	const Q_LIMIT = 100;
	// Sandbox
	/*
	private $_rets_url = 'http://sample.data.crea.ca/Login.svc/Login';
	private $_rets_user = 'CXLHfDVrziCfvwgCuL8nUahC';
	private $_rets_pass = 'mFqMsCSPdnb5WO1gpEEtDCHH';
	*/
	private $_rets_url = 'http://data.crea.ca/Login.svc/Login';
	private $_rets_user = 'bEnWKAABJ8idCTFoV1QBvbi5';
	private $_rets_pass = 'Yyi6sLjnhZapFbWB8gvKwctr';

	protected $_rets;
	protected $_pagination = array('TotalRecords' => 0, 'Limit' => 0, 'Offset' => 0, 'TotalPages' => 0, 'RecordsReturned' => 0);

	public function __construct() {
		$this->_rets = new \phRETS();
		$this->_rets->AddHeader("RETS-Version", "RETS/1.7.2");
		$this->_rets->AddHeader('Accept', '/');
		$this->_rets->SetParam('compression_enabled', TRUE);

		if (!$this->_rets->Connect($this->_rets_url, $this->_rets_user, $this->_rets_pass)) {
			error_log(__FILE__ . ' - ' . __METHOD__ . ' : ' . __LINE__ . "\n" . print_r($this->_rets->Error(), true));
		}
	}

	public function meta_types() {
		return $this->_rets->GetMetadataTypes();
	}

	public function meta_data($resource, $class) {
		return $this->_rets->GetMetadata($resource, $class);
	}

	public function meta_data_class($class) {
		return $this->_rets->GetMetadataClasses($class);
	}

	public function meta_data_table($resource, $class) {
		return $this->_rets->GetMetadataTable($resource, $class);
	}

	public function lookup_data($resource, $name) {
		return $this->_rets->GetLookupValues($resource, $name);
	}

	public function search($resource, $class, $query = "", $optional_params = array()) {
		return $this->_rets->Search($resource, $class, $query, $optional_params);
	}

	public function search_query($resource, $class, $query = "", $optional_params = array()) {
		return $this->_rets->SearchQuery($resource, $class, $query, $optional_params);
	}

	public function total_records() {
		return $this->_pagination['TotalRecords'];
	}

	public function fetch_row($id) {
		return $this->_rets->FetchRow($id);
	}

	public function total_rows($id) {
		return $this->_rets->TotalRecordsFound($id);
	}

	public function get_object($resource, $type, $id, $photo_number = '*', $location = 0) {
		return $this->_rets->GetObject($resource, $type, $id, $photo_number, $location);
	}

	public function property_search($query = "", $optional_params = array()) {
		// setup request arguments
		$search_arguments = array();

		$search_arguments['SearchType'] = 'Property';
		$search_arguments['Class'] = 'Property';

		if ($query == "*" || preg_match('/^\((.*)\)$/', $query)) {
			$search_arguments['Query'] = $query;
		} else {
			$search_arguments['Query'] = '(' . $query . ')';
		}
		$search_arguments['QueryType'] = "DMQL2";
		// setup additional, optional request arguments
		$search_arguments['Count'] = (!array_key_exists('Count', $optional_params)) ? 1 : $optional_params['Count'];
		$search_arguments['Format'] = empty($optional_params['Format']) ? "COMPACT" : $optional_params['Format'];
		$search_arguments['Limit'] = empty($optional_params['Limit']) ? 99999999 : $optional_params['Limit'];

		if (isset($optional_params['Offset'])) {
			$search_arguments['Offset'] = $optional_params['Offset'];
		} elseif (empty($optional_params['Offset'])) {
			$search_arguments['Offset'] = 1;
		}
		$search_arguments['StandardNames'] =
		empty($optional_params['StandardNames']) ? 0 : $optional_params['StandardNames'];

		$result = $this->_rets->RETSRequest($this->_rets->capability_url['Search'], $search_arguments);

		if ($result) {
			list($headers, $body) = $result;
			$xml = @simplexml_load_string($body);

			if (isset($xml->{'RETS-RESPONSE'}->Pagination) && is_object($xml->{'RETS-RESPONSE'}->Pagination)) {
				$this->_pagination = (array)$xml->{'RETS-RESPONSE'}->Pagination;
			}

			if (isset($xml->{'RETS-RESPONSE'}->PropertyDetails) && is_object($xml->{'RETS-RESPONSE'}->PropertyDetails))
			{
//				return $xml->{'RETS-RESPONSE'}->PropertyDetails;
				return $xml->{'RETS-RESPONSE'};
			}

		} else {
			echo "No Results! \n\n"; print_r($xml->{'RETS-RESPONSE'});
			return 0;
		}
	}

	public function agent_search($query = "", $optional_params = array()) {
		// setup request arguments
		$search_arguments = array();

		$search_arguments['SearchType'] = 'Agent';
		$search_arguments['Class'] = 'Agent';

		if ($query == "*" || preg_match('/^\((.*)\)$/', $query)) {
			$search_arguments['Query'] = $query;
		} else {
			$search_arguments['Query'] = '(' . $query . ')';
		}
		$search_arguments['QueryType'] = "DMQL2";
		// setup additional, optional request arguments
		$search_arguments['Count'] = (!array_key_exists('Count', $optional_params)) ? 1 : $optional_params['Count'];
		$search_arguments['Format'] = empty($optional_params['Format']) ? "STANDARD-XML" : $optional_params['Format'];
		$search_arguments['Limit'] = empty($optional_params['Limit']) ? 99999999 : $optional_params['Limit'];

		if (isset($optional_params['Offset'])) {
			$search_arguments['Offset'] = $optional_params['Offset'];
		} elseif (empty($optional_params['Offset'])) {
			$search_arguments['Offset'] = 1;
		}
		$search_arguments['StandardNames'] = empty($optional_params['StandardNames']) ? 0 : $optional_params['StandardNames'];
		$result = $this->_rets->RETSRequest($this->_rets->capability_url['Search'], $search_arguments);
		if ($result) {
			list($headers, $body) = $result;
			$xml = @simplexml_load_string($body);

			if (isset($xml->{'RETS-RESPONSE'}->Pagination) && is_object($xml->{'RETS-RESPONSE'}->Pagination)) {
				$this->_pagination = (array)$xml->{'RETS-RESPONSE'}->Pagination;
			}

			if (isset($xml->{'RETS-RESPONSE'}->AgentDetails) && is_object($xml->{'RETS-RESPONSE'}->AgentDetails)){
				return $xml->{'RETS-RESPONSE'}->AgentDetails;
			}
		} else {
			echo "No Results! \n\n"; print_r($xml->{'RETS-RESPONSE'});
			return 0;
		}
	}
}