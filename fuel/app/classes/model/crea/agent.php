<?php
namespace Model\Crea;

class Agent extends \Automodeler {
	const S_NEW = 0;
	const S_ACTIVE = 1;
	const S_NEW_OFFICE = 5;

	protected $_class = 'agent';
	protected $_table_name = 'crea_agents';

	protected $_data = array('id' => NULL,'name' => '','phone' => '','mobile' => '','fax' => '','officeid' => '',
		'officename' => '','officeaddress' => '', 'officecity' => '', 'officeprov' => '','officepostal' => '',
		'officephone' => '', 'officemobile' => '', 'officefax' => '', 'created_at' => '', 'status_id' => 0);

	public function get_table_name(){
		return $this->_table_name;
	}

	/**
	* Agent List
	*
	* @return array Set of offices with total listing each
	*/
	public function get_brokers($params, $strip_tag = false){
		$aColumns = array('officeid', 'officename', 'officecity', 'officeprov', 'phone', 'cnt');
		$data['data'] = array();

		$sql = "SELECT officeid,officename,officecity,officeprov,officephone,COUNT(prop_id) AS cnt
		FROM crea_agents
		JOIN crea_property_agents ON crea_property_agents.agent_id = crea_agents.id";

		if (isset($params['search']) && trim($params['search']['value']) != ''){
			$sql .= " WHERE (`officeid` LIKE " . \DB::escape('%'. trim($params['search']['value']) . '%');
			$sql .= ") OR (`officename` LIKE " . \DB::escape('%'. trim($params['search']['value']) . '%');
			$sql .= ") OR (`officecity` LIKE " . \DB::escape('%'. trim($params['search']['value']) . '%');
			$sql .= ") OR (`officeprov` LIKE " . \DB::escape('%'. trim($params['search']['value']) . '%');
			$sql .= ")";
		}
		$sql .= ' GROUP BY officeid';

		$data['recordsFiltered'] = \DB::query($sql, \DB::SELECT)->execute()->count();

		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY ' . $aColumns[$params['order'][0]['column']] . ' ' . $params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY officename asc';
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= ' LIMIT ' . (int) $params['start'] . ',' . (int) $params['length'];
		}
		$result = \DB::query($sql, \DB::SELECT)->execute();
		foreach ($result as $row) {
			if ($strip_tag) {
				$row['officeid'] = $row['officeid'];
			} else {
			$row['officeid'] = \HTML::anchor('#', $row['officeid'], array('title' => $row['officeid'], 'class' => 'no-link office-filter', 'data-id' => $row['officeid']));
			}
			$data['data'][] = array_values($row);
		}

		// Total rows
		$data['recordsTotal'] = \DB::query("SELECT COUNT(id) FROM {$this->_table_name} GROUP BY officeid", \DB::SELECT)->execute()->count();
		return $data;
	}

	public function get_summary() {
		$data = array();
		$data['listings'] = \DB::query("SELECT COUNT(id) AS cnt FROM crea_property", \DB::SELECT)->execute()->get('cnt');
		$result = \DB::query("SELECT agent_id FROM crea_property_agents GROUP BY agent_id", \DB::SELECT)->execute();
		$data['agents'] = $result->count();
		$result = \DB::query("SELECT agent_id FROM crea_agents JOIN crea_property_agents ON crea_agents.id = agent_id GROUP BY officeid", \DB::SELECT)->execute();
		$data['brokers'] = $result->count();
		return $data;
	}
}