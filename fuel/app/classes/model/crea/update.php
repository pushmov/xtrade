<?php
namespace Model\Crea;

class Update extends Feed {
	const P_THUMB = 1;
	const P_GALLERY = 2;
	protected $_db;
	protected $_data = array();
	protected $_photo_path = '/crea';

	// crea fields we are using
	protected $_property =
	array('id' => '', 'LastUpdated' => '', 'ListingID' => '', 'Board' => '', 'StreetAddress' => '',
		'AddressLine1' => '', 'Addressline2' => '', 'StreetNumber' => '', 'StreetDirectionPrefix' => '',
		'StreetName' => '', 'StreetSuffix' => '', 'StreetDirectionSuffix' => '', 'UnitNumber' => '',
		'BoxNumber' => '', 'City' => '', 'Province' => '', 'Prov_State' => 0, 'PostalCode' => '', 'Country' => 0,
		'CommunityName' => '', 'Neighbourhood' => '', 'Subdivision' => '', 'AmmenitiesNearBy' => '',
		'CommunicationType' => '', 'CommunityFeatures' => '', 'Crop' => '', 'DocumentType' => '', 'Easement' => '',
		'EquipmentType' => '', 'Features' => '', 'FarmType' => '', 'IrrigationType' => '', 'Lease' => '',
		'LeasePerTime' => '', 'LeasePerUnit' => '', 'LeaseTermRemainingFreq' => '', 'LeaseTermRemaining' => '',
		'LeaseType' => '', 'LiveStockType' => '', 'LoadingType' => '', 'LocationDescription' => '',
		'Machinery' => '', 'MaintenanceFee' => '', 'MaintenanceFeePaymentUnit' => '', 'MaintenanceFeeType' => '',
		'ManagementCompany' => '', 'MunicipalId' => '', 'OwnershipType' => '', 'ParkingSpaceTotal' => '',
		'Plan' => '', 'PoolFeatures' => '', 'PoolType' => '', 'Price' => '', 'PricePerUnit' => '',
		'PropertyType' => '', 'PublicRemarks' => '', 'RentalEquipmentType' => '', 'RightType' => '',
		'RoadType' => '', 'SignType' => '', 'StorageType' => '', 'Structure' => '', 'TransactionType' => '',
		'TotalBuildings' => '', 'ViewType' => '', 'WaterFrontType' => '', 'WaterFrontName' => '',
		'ZoningDescription' => '', 'ZoningType' => '', 'AnalyticsClick' => '', 'AnalyticsView' => '', 'Active' => 1,
		'xTrade_Imported' => 0, 'Listing_Status ' => '', 'Date_Created' => 0, 'Neigh_Added' => 0, 'Longitude' => '',
		'Latitude' => '', 'City_Txt' => '');
	protected $_building = array('id' => '', 'BathroomTotal' => 0, 'BedroomsTotal' => 0, 'BedroomsAboveGround' => 0,
		'BedroomsBelowGround' => 0, 'Amenities' => '', 'Amperage' => '', 'Anchor' => '', 'Appliances' => '',
		'ArchitecturalStyle' => '', 'BasementDevelopment' => '', 'BasementFeatures' => '', 'BasementType' => '',
		'BomaRating' => '', 'CeilingHeight' => '', 'CeilingType' => '', 'ClearCeilingHeight' => '',
		'ConstructedDate' => '', 'ConstructionMaterial' => '', 'ConstructionStatus' => '',
		'ConstructionStyleAttachment' => '', 'ConstructionStyleOther' => '', 'ConstructionStyleSplitLevel' => '',
		'CoolingType' => '', 'DisplayAsYears' => '', 'EnerguideRating' => '', 'ExteriorFinish' => '',
		'FireplaceFuel' => '', 'FireplacePresent' => '', 'FireplaceTotal' => '', 'FireplaceType' => '',
		'FireProtection' => '', 'Fixture' => '', 'FlooringType' => '', 'FoundationType' => '',
		'HalfBathTotal' => '', 'HeatingFuel' => '', 'HeatingType' => '', 'LeedsCategory' => '', 'LeedsRating' => '',
		'RoofMaterial' => '', 'RoofStyle' => '', 'SizeExterior' => '', 'SizeInterior' => '', 'StoreFront' => '',
		'StoriesTotal' => '', 'TotalFinishedArea' => '', 'Type' => '', 'Uffi' => '', 'UtilityPower' => '',
		'UtilityWater' => '', 'VacancyRate' => '');
	protected $_land = array('id' => '', 'SizeTotal' => '', 'SizeFrontage' => '', 'AccessType' => '', 'Acreage' => '',
		'Amenities' => '', 'ClearedTotal' => '', 'CurrentUse' => '', 'Divisible' => '', 'FenceTotal' => '',
		'FenceType' => '', 'FrontsOn' => '', 'LandDisposition' => '', 'LandscapeFeatures' => '',
		'PastureTotal' => '', 'Sewer' => '', 'SizeDepth' => '', 'SoilEvaluation' => '', 'SoilType' => '',
		'SurfaceWater' => '', 'TiledTotal' => '');

	public function do_update($full = false) {
		date_default_timezone_set('UTC');
		$is_updated = array();
		$results = \DB::select()->from('crea_updated')->execute();
		foreach($results as $row) {
			$is_updated[] = $row['id'];
		}
		// Flag to determine if updating from master
		$master = false;
		// Get master list to compare for deleted once a day
		$md = \Sysconfig::get('crea_last_master');
		if ($md < date_create('1 day ago')->format('Y-m-d')) {
			\DBUtil::truncate_table('crea_master');
			$i = $max = 0;
			do  {
				set_time_limit(0);
				$offset = 1 + ($i * self::Q_LIMIT);
				$params = array('Limit' => self::Q_LIMIT, 'Format' => 'COMPACT', 'Offset' => $offset);
				$results = $this->search_query('Property', 'Property', '(ID=*)', $params);
				$max = ceil($this->total_rows($results)  / self::Q_LIMIT);
				while($row = $this->fetch_row($results)) {
					$dt = date_create_from_format('d/m/Y h:i:s A', $row['ModificationTimestamp'])->format(DATETIME_FORMAT_DB);
					\DB::query("INSERT INTO crea_master VALUES ({$row['ListingKey']},'{$dt}', 0)")->execute();
				}
				$i++;
			} while ($i < $max);
			\Sysconfig::update('crea_last_master', date_create()->format('Y-m-d'));
			// Delete skipped listings no in master
			$sql = "DELETE FROM crea_skipped WHERE crea_skipped.id NOT IN (SELECT crea_master.id FROM crea_master)";
			\DB::query($sql, \DB::UPDATE)->execute();
			// Update skipped flag
			$sql = "UPDATE crea_master SET skipped = 1 WHERE crea_master.id IN (SELECT crea_skipped.id FROM crea_skipped)";
			$master = true;
		}

		if ($master) {
			$sql = "
			SELECT crea_master.id
			FROM crea_master
			LEFT JOIN crea_property ON crea_property.id = crea_master.id
			WHERE
			(crea_property.id IS NULL AND skipped = 0) OR
			(crea_property.lastupdated < crea_master.updated_at AND skipped = 0)";
			$result = \DB::query($sql, \DB::SELECT)->execute();
			// Group up to 100 id together
			$count = count($result);
			$group = $i = 0;
			$ids = array();
			foreach($result as $row) {
				$ids[$group][$i] = $row['id'];
				$i++;
				if ($i > 99) {
					$i = 0;
					$group++;
				}
			}
			// Loop through the id groups
			foreach($ids as $row) {
				$dbml = '(ID=' . implode(',', $row) . ')';
				//$params = array('Limit' => self::Q_LIMIT, 'Format' => 'STANDARD-XML', 'Count' => 1);
				$params = array('Limit' => self::Q_LIMIT, 'Format' => 'STANDARD-XML', 'Count' => 1);
				$results = $this->property_search($dbml, $params);
				$this->_save_details($results, $is_updated);
			}
		} else {
			if ($full === true) {
				$sql = 'SELECT updated_at FROM crea_master ORDER BY updated_at LIMIT 1';
				$result = \DB::query($sql, \DB::SELECT)->execute()->get('updated_at');
				$dbml = "(LastUpdated=" . date_create($result)->format('Y-m-d\TH:i:00\Z') . ")";
			} else {
				$sql = 'SELECT lastupdated FROM crea_property ORDER BY lastupdated DESC LIMIT 1';
				$result = \DB::query($sql, \DB::SELECT)->execute()->get('lastupdated');
				$dbml = "(LastUpdated=" . date_create($result)->format('Y-m-d\TH:i:00\Z') . ")";
			}
			$params = array('Limit' => self::Q_LIMIT, 'Format' => 'STANDARD-XML', 'Count' => 2);
			$results = $this->property_search($dbml, $params);
			$count = $this->total_records();

			if (!empty($count)) {
				for ($i = 0; $i < ceil($count / self::Q_LIMIT); $i++) {
					$offset = 1 + ($i * self::Q_LIMIT);
					$params = array('Limit' => self::Q_LIMIT, 'Format' => 'STANDARD-XML', 'Count' => 1, 'Offset' => $offset);
					$results = $this->property_search($dbml, $params);
					$this->_save_details($results, $is_updated);
				}
			}
		}
		$this->_post_update($full);
	}

	public function get_photos($id, $pos = null) {
		$this->_download_photos($id, $pos);
	}

	protected function _download_photos($id, $pos = null) {
		if ($pos === 0) {
			return;
		}
		$path = DOCROOT . \Model\Crea\Property::IMG_CREA_PATH;
		// Remove any existing
		foreach (glob($path . "{$id}_*.jpg") as $file) {
			unlink($file);
		}
		// Remove any existing thumbs
		foreach (glob($path . "thumbs/{$id}_*.jpg") as $file) {
			unlink($file);
		}

		if (in_array(\Fuel::$env, array(\Fuel::TEST,\Fuel::DEVELOPMENT))) {
			$photos = $this->get_object('Property', 'Photo', $id, (($pos === null) ? '*' : $pos));
		} else {
			$photos = $this->get_object('Property', 'Photo', $id, (($pos === null) ? '*' : $pos));
			//			$photos = $this->get_object('Property', 'LargePhoto', $id, (($pos === null) ? '*' : $pos));
		}

		if (is_array($photos) && (count($photos) > 0)) {
			$count = 0;

			foreach ($photos as $photo) {
				if (($photo['Success'] != true) || empty($photo['Object-ID']) || (strlen($photo['Data']) < 150)) {
					continue;
				}
				$image = $id . '_' . $photo['Object-ID'] . '.jpg';
				//file_put_contents($path . $image, $photo['Data']);
				// Main image
				try {
					if (($img = imagecreatefromstring($photo['Data'])) !== false) {
						$width = imagesx($img);
						$height = imagesy($img);
						if ($width > $height) {
							$twidth = \Model\Photos\Xtrade::MAX_WIDTH;
							$theight = floor($height * ($twidth / $width));
						} else {
							$theight = \Model\Photos\Xtrade::MAX_HEIGHT;
							$twidth = floor($width * ($theight / $height));
						}
						$timg = imagecreatetruecolor($twidth, $theight);
						imagecopyresized($timg, $img, 0, 0, 0, 0, $twidth, $theight, $width, $height);
						imagejpeg($timg, $path . $image);

						// Thumbnail
						$img = imagecreatefromstring($photo['Data']);
						if ($width > $height) {
							$twidth = \Model\Photos\Xtrade::TMB_WIDTH;
							$theight = floor($height * ($twidth / $width));
						} else {
							$theight = \Model\Photos\Xtrade::TMB_HEIGHT;
							$twidth = floor($width * ($theight / $height));
						}
						$timg = imagecreatetruecolor($twidth, $theight);
						imagecopyresized($timg, $img, 0, 0, 0, 0, $twidth, $theight, $width, $height);
						imagejpeg($timg, $path . 'thumbs/' . $image);
					}
				} catch (\PhpErrorException $e) {
					\Log::error(__FILE__.' : '.__LINE__.' Get Image Error: ' . print_r($photos, true));
				}
			}
		}
	}

	public function do_post_update() {
		$this->_post_update();
	}

	protected function _delete_photos($id) {
		$path = DOCROOT . \Model\Crea\Property::IMG_CREA_PATH;
		foreach (glob($path . "{$id}_*.jpg") as $file) {
			unlink($file);
		}
		foreach (glob($path . "thumbs/{$id}_*.jpg") as $file) {
			unlink($file);
		}
	}
	protected function _post_update($full = false) {
		// Update the prov_state
		$sql = "UPDATE crea_updated cu
		JOIN crea_property cp ON cp.id = cu.id
		JOIN ca_provinces cap ON cap.province = cp.province
		SET cp.prov_state = cap.id";
		\DB::query($sql, \DB::UPDATE)->execute();
		// Update the city id
		$sql = "UPDATE crea_updated cu
		JOIN crea_property cp ON cp.id = cu.id
		JOIN ca_cities cac ON cac.name = cp.city_txt AND cac.province_id = cp.prov_state
		SET cp.city = cac.id";
		\DB::query($sql, \DB::UPDATE)->execute();
		// No city then not active
		\DB::query("UPDATE crea_property SET active = 0 WHERE city = 0", \DB::UPDATE)->execute();
		// Remove listings no longer crea active
		$sql = "SELECT crea_property.id
		FROM crea_property
		LEFT OUTER JOIN crea_master
		ON (crea_master.id=crea_property.id)
		WHERE crea_master.id IS NULL";
		$results = \DB::query($sql, \DB::SELECT)->execute();
		foreach($results as $row) {
			\DB::query('DELETE FROM crea_property WHERE id = ' . $row['id'], \DB::DELETE)->execute();
			$this->_delete_photos($row['id']);
		}
		// As of 2016-07-16 delete from xtrade database
		$sql = "SELECT listing_id,client_has.external_id
		FROM client_has
		LEFT OUTER JOIN crea_master
		ON (crea_master.id=client_has.external_id)
		WHERE crea_master.id IS NULL AND external_id <> ''";
		$results = \DB::query($sql, \DB::SELECT)->execute();
		foreach($results as $row) {
			\Model\Listing::forge()->delete_listing(array('listing_id' => $row['listing_id'], true));
		}

		if ($full === false) {
			// New office
			$sql = "
			SELECT officename,officeaddress,officecity,officephone,officeid, MAX(status_id) AS status
			FROM crea_agents
			GROUP BY officeid
			HAVING status = 0";

			$result = \DB::query($sql, \DB::SELECT)->execute();
			$new_off = array();
			foreach($result as $row) {
				$sql = "
				SELECT COUNT(prop_id) AS cnt
				FROM crea_agents
				LEFT JOIN crea_property_agents ON agent_id = id
				WHERE officeid = :oid";
				$cnt = \DB::query($sql, \DB::SELECT)->param(':oid', $row['officeid'])->execute()->get('cnt');
				if ($cnt > 0) {
					$row['cnt'] = $cnt;
					$new_off[] = $row;
				}
			}
			\Emails::forge()->new_crea_office($new_off);
			\DB::query("UPDATE crea_agents SET status_id = 1 WHERE status_id = 0", \DB::UPDATE)->execute();

			// Send Agent an email if first listing is downloaded
			$sql = "
			SELECT users.*
			FROM crea_updated
			JOIN crea_property_agents ON prop_id = crea_updated.id
			JOIN users ON country = 1 AND nrb_id = agent_id
			WHERE external_listings_notified = 0
			GROUP BY nrb_id";
			$results = \DB::query($sql)->execute();
			foreach($results as $row) {
				\Emails::forge()->agent_first_download($row);
				\DB::query("UPDATE users SET external_listings_notified = 1 WHERE id = {$row['id']}", \DB::UPDATE)->execute();
			}
			// Only get first photo for new updates
			$results = \DB::select()->from('crea_updated')->execute();
			foreach($results as $row) {
				// Retrieve the 1st photo
				$this->_download_photos($row['id'], 0);
			}
		}

		\Model\Crea\Import::do_import();

		// Empty the updated table
		\DBUtil::truncate_table('crea_updated');
	}

	/**
	* Add record to the skipped table
	*
	* @param mixed $data
	*/
	protected function _skip($listing) {
		$id = (string)$listing->attributes()->ID;
		$query = \DB::query('INSERT IGNORE INTO crea_skipped VALUES (:id,:created,:ai,:mls,:tt,:pt,:bt,:city)',\DB::INSERT);
		$query->param(':id', $id);
		$query->param(':created', date_create()->format('Y-m-d H:i:s'));
		$query->param(':ai', (string)$listing->AgentDetails->CreaID);
		$query->param(':mls', (string)$listing->ListingID);
		$query->param(':tt', (string)$listing->TransactionType);
		$query->param(':pt', (string)$listing->PropertyType);
		$query->param(':bt', (string)$listing->Building->Type);
		if (isset($listing->Address->City)) {
			$query->param(':city', (string)$listing->Address->City);
		} else {
			$query->param(':city', '');
		}
		$query->execute();
		// Add update master
		$dt = date_create_from_format('D, d M Y H:i:s e', (string)$listing->attributes()->LastUpdated);
		$lud = $dt->format(DATETIME_FORMAT_DB);
		\DB::query('DELETE FROM crea_master WHERE id = :id', \DB::DELETE)->param(':id', $id)->execute();
		$query = \DB::query('INSERT INTO crea_master VALUES (:id,:created,1)',\DB::INSERT);
		$query->param(':id', $id)->param(':created', $lud)->execute();
	}

	protected function _save_details($results, $is_updated) {
		$last_id = 0;
		foreach ($results->PropertyDetails as $listing) {
			set_time_limit(0);
			// Only Single Family For Sale
			if ((stripos((string)$listing->PropertyType,'Single Family') === false) ||
			(stripos((string)$listing->TransactionType, 'For Sale') === false) ||
			(!in_array((string)$listing->Building->Type, array('Condominium', 'Residential', 'Recreational', 'House', 'Apartment', 'Multi-Family', 'Duplex', 'Mobile Home', 'Row / Townhouse', 'Residential Commercial Mix', 'Triplex', 'Fourplex')))) {
				$this->_skip($listing);
				continue;
			}
			$data = array();
			$building = array();
			$land = array();
			$agents = array();
			$data['id'] = (string)$listing->attributes()->ID;
			if (in_array($data['id'], $is_updated)) {
				continue;
			}
			$data['current_images'] = 0;
			// Hack to break out of loop
			if ($last_id == $data['id']) {
				break;
			}
			$last_id = $data['id'];

			foreach ($listing as $k => $v) {
				switch ($k) {
					case 'Address' :
						$data['country'] = 1;
						$data['province'] = trim((string)$v->Province);
						$data['city_txt'] = trim((string)$v->City);
						// Seems Alberta adds the neighbourhood to the city
						if (($ci = strpos($data['city_txt'], ',')) !== false) {
							$data['city_txt'] = trim(substr($data['city_txt'], $ci + 1));
						}
						// No city no save
						if ($data['city_txt'] == '') {
							$this->_skip($listing);
							continue 2;
						}

						$data['neighbourhood'] = (string)$v->Neighbourhood;
						$data['streetaddress'] = (string)$v->StreetAddress;
						$data['streetnumber'] = (string)$v->StreetNumber;
						$data['streetname'] = (string)$v->StreetName;
						$data['streetsuffix'] = (string)$v->StreetSuffix;
						$data['unitnumber'] = (string)$v->UnitNumber;
						$data['postalcode'] = (string)$v->PostalCode;
						break;
					case 'AgentDetails':
						// If multiple only use the first one
						if (is_array($v)) {
							$v = $v[0];
						}
						// No id no save
						if (!isset($v->CreaID)) {
							$this->_skip($listing);
							continue 2;
						}
						$agents[$data['id']]['id'] = (int)$v->CreaID;
						$agents[$data['id']]['name'] = (string)$v->Name;

						// Loop through phones
						if (isset($v->Phones->Phone)) {
							foreach ($v->Phones->Phone as $p) {
								$pt = (string)$p->attributes()->PhoneType;

								if ($pt == 'Telephone') {
									$agents[$data['id']]['phone'] = (string)$p;
								} elseif ($pt == 'Mobile') {
									$agents[$data['id']]['mobile'] = (string)$p;
								} elseif ($pt == 'Fax') {
									$agents[$data['id']]['fax'] = (string)$p;
								}
							}
						}

						$agents[$data['id']]['officeid'] = (string)$v->Office->CreaID;
						$agents[$data['id']]['officename'] = (string)$v->Office->Name;
						$agents[$data['id']]['officeaddress'] = (string)$v->Office->Address->StreetAddress;
						$agents[$data['id']]['officecity'] = (string)$v->Office->Address->City;
						$agents[$data['id']]['officeprov'] = (string)$v->Office->Address->Province;
						$agents[$data['id']]['officepostal'] = (string)$v->Office->Address->PostalCode;

						if (isset($v->Office->Phones->Phone)) {
							foreach ($v->Office->Phones->Phone as $p) {
								$pt = (string)$p->attributes()->PhoneType;

								if ($pt == 'Telephone') {
									$agents[$data['id']]['officephone'] = (string)$p;
								} elseif ($pt == 'Mobile') {
									$agents[$data['id']]['officemobile'] = (string)$p;
								} elseif ($pt == 'Fax') {
									$agents[$data['id']]['officefax'] = (string)$p;
								}
							}
						}
						break;
					case 'Building' :
						foreach ((array)$listing->Building as $k => $v) {
							if (array_key_exists($k, $this->_building)) {
								$building[strtolower($k)] = (string)$v;
							}
						}
						break;
					case 'Land' :
						foreach ((array)$listing->Land as $k => $v) {
							if (array_key_exists($k, $this->_land)) {
								$land[strtolower($k)] = (string)$v;
							}
						}
						break;
					case 'Photo' :
						$data['current_images'] = count($listing->Photo->PropertyPhoto);
						break;
					default:
						// Save only the data that matches our tables
						if (array_key_exists($k, $this->_property)) {
							// Strip chars from these fields
							if (in_array($k, array('AnalyticsClick', 'AnalyticsView'))) {
								$v = substr((string)$v, 9);
								$v = substr((string)$v, 0, -3);
							}
							$data[strtolower($k)] = (string)$v;
						}
				}
			}
			if (!isset($data['city_txt'])) {
				$this->_skip($listing);
				continue;
			}
			$data['lastupdated'] = date_create((string)$listing->attributes()->LastUpdated)->format(DATETIME_FORMAT);
			$prop = Property::forge($data['id']);
			$prop->set_fields($data);
			if (!$prop->loaded()) {
				$prop->date_created = date_create()->format(DATETIME_FORMAT);
			}
			$prop->active = 1;
			$prop->listing_status = \Model\Listing::LS_ACTIVE;
			$prop->save();
			$bld = Building::forge($data['id']);
			$bld->set_fields($building);
			$bld->id = $data['id'];
			$bld->save();
			$lnd = Land::forge($data['id']);
			$lnd->set_fields($land);
			$lnd->id = $data['id'];
			$lnd->save();
			// Add to updated table
			$query = \DB::query('INSERT IGNORE INTO crea_updated VALUES (:k)',\DB::INSERT);
			$query->param(':k', $data['id'])->execute();

			// Save / Update agent info
			foreach ($agents as $k => $agent) {
				$agnt = Agent::forge($agent['id']);
				$agnt->set_fields($agent);
				$agnt->created_at = date_create()->format(DATETIME_FORMAT_DB);
				$agnt->save();

				// Link agent to the property
				$query = \DB::query('INSERT IGNORE INTO crea_property_agents VALUES (:k,:a,1)',\DB::INSERT);
				$query->param(':k', $data['id'])->param(':a', $agent['id'])->execute();
			}
		}
	}
}