<?php
namespace Model\Crea;

class Xref extends \Automodeler {
	protected $_class = 'xref';
	protected $_table_name = 'crea_xref';
	protected $_data = array( 'id' => NULL, 'payload' => NULL, 'name' => NULL, 'dbtable' => NULL, 'tblfield' => NULL, 'localfld' => NULL, 'lookup' => NULL);

	//  public function save($validation){
	//    /**
	//     * not implemented yet
	//     */
	//    return parent::save($validation);
	//  }
	
	public function get_selected_column(){
		$xref = $this->load(\DB::select_array()
			->where('tblfield', '<>', '')
			->where('dbtable', '<>', 'crea_agents')
			->where('dbtable', '<>', 'crea_property_agents')->order_by('dbtable', 'desc')->order_by('tblfield', 'desc'), NULL)->as_array();
		$select = array();
		$select[] = array('crea_property.id', 'id');
		// Add tracking code
		foreach ($xref as $row) {
			if($row['localfld'] != ''){
				$select[] = array($row['dbtable'].'.'.$row['tblfield'], $row['localfld']);
			}else{
				$select[] = $row['dbtable'] . '.' . $row['tblfield'];
			}
		}
		return $select;
	}


}