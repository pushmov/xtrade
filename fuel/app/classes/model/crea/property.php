<?php
namespace Model\Crea;

class Property extends \Automodeler {

	const IMG_ASSET_PATH = 'listings/crea/';
	const IMG_CREA_PATH = 'assets/images/listings/crea/';

	protected $_class = 'property';
	protected $_table_name = 'crea_property';

	protected $_data = array(
		'id' => NULL,'lastupdated' => '','listingid' => '','board' => '','streetaddress' => '','addressline1' => '',
		'addressline2' => '','streetnumber' => '','streetdirectionprefix' => '','streetname' => '',	'streetsuffix' => '',
		'streetdirectionsuffix' => '','unitnumber' => '','boxnumber' => '','city' => '','city_txt' => '','province' => '',
		'prov_state' => '','postalcode' => '','country' => 1,'communityname' => '','neighbourhood' => '','longitude' => '',
		'latitude' => '','subdivision' => '','ammenitiesnearby' => '','communicationtype' => '','communityfeatures' => '',
		'crop' => '','documenttype' => '','easement' => '','equipmenttype' => '','features' => '','farmtype' => '',
		'irrigationtype' => '','lease' => '','leasepertime' => '','leaseperunit' => '','leasetermremainingfreq' => '',
		'leasetermremaining' => '','leasetype' => '','livestocktype' => '','loadingtype' => '','locationdescription' => '',
		'machinery' => '','maintenancefee' => '','maintenancefeepaymentunit' => '','maintenancefeetype' => '',
		'managementcompany' => '','municipalid' => '','ownershiptype' => '','parkingspacetotal' => '','plan' => '',
		'poolfeatures' => '','pooltype' => '','price' => '','priceperunit' => '','propertytype' => '','publicremarks' => '',
		'rentalequipmenttype' => '','righttype' => '','roadtype' => '','signtype' => '','storagetype' => '','structure' => '',
		'transactiontype' => '','totalbuildings' => '','viewtype' => '','waterfronttype' => '','waterfrontname' => '',
		'zoningdescription' => '','zoningtype' => '','analyticsclick' => '','analyticsview' => '','active' => 1,
		'xtrade_imported' => 0,	'listing_status' => '', 'date_created' => '', 'neigh_added' => 0, 'featured_listing' => 0,
		'current_images' => 0
	);

	protected $_feature = array('condo_features' => 'Condo Features', 'interior_features' => 'Interior Features',
		'kitchen' => 'Kitchen Features', 'laundry_features' => 'Laundry Features','property_features' => 'Property Features',
		'neighbourhood_features' => 'Neighbourhood Features', 'other_features' => 'Other_Features');

	/**
	* Get the crea image
	*
	* @param int $id - the crea id
	* @param int $pos - the image position or null for all
	* @param bool $thumb - true for thumbnail version
	*/
	public function get_image($id, $pos = null, $thumb = true) {
		// Strip prefix from id
		$id = (strpos($id, '2_') === false) ? $id : substr($id, 2);
		$path = DOCROOT . self::IMG_CREA_PATH;
		$url = 'no-image.png';
		if ($pos == 1) {
			// If first image is not there then load download them all
			if (!is_readable($path . "{$id}_1.jpg")) {
				$crea = new \Model\Crea\Update;
				$crea->get_photos($id, 1);
			}
			// Check again
			if (is_readable($path . "{$id}_1.jpg")) {
				if ($thumb === true) {
					$url = "listings/crea/thumbs/{$id}_1.jpg";
				} else {
					$url = "listings/crea/{$id}_1.jpg";
				}
			}
		} else {
			// If second image is not there then load download them all
			if (!is_readable($path . "{$id}_2.jpg")) {
				$crea = new \Model\Crea\Update;
				$crea->get_photos($id);
			}

			if ($pos === null) {
				// Its now an array or urls
				$url = array();
				if ($thumb == true) {
					$path .= 'thumbs/';
				}
				foreach (glob($path . "{$id}_*.jpg") as $file) {
					// Romove all up to the path
					$url[] = substr($file, strpos($file, 'listings'));
				}

			} else {
				// Check again
				if (is_readable($path . "{$id}_{$pos}.jpg")) {
					if ($thumb == true) {
						$url = "listings/crea/thumbs/{$id}_{$pos}.jpg";
					} else {
						$url = "listings/crea/{$id}_{$pos}.jpg";
					}
				}

			}
		}
		return $url;
	}

	public function load_photos($id){
		$return = array();
		$data = $this->get_image($id);
		foreach($data as $k => $v){
			$return[$k] = array('id' => $k, 'url' => $v);
		}
		return $return;
	}

	public function load_photo_detail($id){
		$data['images_list'] = '';
		$crea_photos = $this->get_image($id, null, false);
		if(is_array($crea_photos) && !empty($crea_photos)){
			foreach($crea_photos as $ik => $row) {
				$thumb = \Asset::get_file(str_replace('/crea/', '/crea/thumbs/', $row), 'img');
				$data['images_list'] .= '<li data-thumb="' . $thumb . '">'.\Asset::img($row).'</li>';
			}
		}
		return $data;
	}

	public function listing_detail($id){
		$select = \Model\Crea\Xref::forge()->get_selected_column();
		$select[] = array('crea_property.latitude', 'lat');
		$select[] = array('crea_property.longitude', 'lng');
		$data = \DB::select_array($select)->from('crea_property')->where('crea_property.id', '=', $id)
            ->join('crea_building', 'left')->on('crea_building.id', '=', 'crea_property.id')
            ->join('crea_land', 'left')->on('crea_land.id', '=', 'crea_property.id')
            ->execute()->current();
		return $data;
	}

	public function get_listing_geolocation($id){
		$this->load(\DB::select_array()->where('id', '=', $id));
		$lat_long = \CityState::get_lat_lng($this->streetaddress, $this->city, $this->prov_state, $this->country);
		if($lat_long){
			$this->set_fields(array('latitude' => $lat_long['lat'], 'longitude' => $lat_long['lng']));
			$this->save();
		}
		return $lat_long;
	}

	public function get_features_list($data){
		$features = array();
		foreach ($data as $k => $v) {
			if (array_key_exists($k, $this->_feature) && !empty($v)) {
				// Work with an array
				if (!is_array($v)) {
					$v = explode(',', $v);
				}
				$features[$this->_feature[$k]] = '<ul>';
					foreach ($v as $item) {
						$features[$this->_feature[$k]] .= "<li>$item</li>";
				}
				$features[$this->_feature[$k]] .= '</ul>';
			}
		}
		return $features;
	}

	public function get_user_imported($id){
		$imported_listing = \DB::select_array()->from('crea_property')
			->join('crea_building', 'inner')->on('crea_building.id', '=', 'crea_property.id')
			->join('crea_land', 'inner')->on('crea_land.id', '=', 'crea_property.id')
			->where('crea_property.id', '=', $id)
			->execute()->current();
		return $imported_listing;
	}

	public function get_listing_from_external_id($realtor_id){
		\Lang::load('listing');
		$external_id = \Model\Realtor::forge()->last_import();
		$client_has = \Model\Client\Has::forge()->load(\DB::select_array(array('external_id', 'lastupdated'))
			->where('realtor_id', '=', $realtor_id)
			->where('imported', '=', \Model\Listing::IMPORTED)
			->order_by('lastupdated', 'desc'), NULL)->as_array();
		$current_imported_listings_array = array();
		if(!empty($client_has)){
			foreach($client_has as $row){
				$current_imported_listings_array[$row['external_id']] = $row['lastupdated'];
			}
		}
		$imported_listings = \DB::select_array(array('crea_property_agents.prop_id','crea_property.lastupdated'))
			->from('crea_property_agents')
			->join('crea_property', 'inner')->on('prop_id', '=', 'crea_property.id')
			->where('agent_id', '=', $external_id)
			->where('active', '=', 1)
			->order_by('lastupdated', 'desc')->execute()->as_array();

		$imported_listings_array = array();
		if(!empty($imported_listings)){
			foreach($imported_listings as $row){
				$imported_listings_array[$row['prop_id']] = $row['lastupdated'];
			}
		}

		foreach($imported_listings_array as $key => $value){
			foreach($current_imported_listings_array as $key_2 => $value_2){
				if($key == $key_2){
					if($value != $value_2){
						$diff_result[] = $key;
					}
				}
			}
			if(!array_key_exists($key, $current_imported_listings_array)){
				$diff_result[] = $key;
			}
		}

		if(!empty($diff_result)){
			foreach($diff_result as $value){
				$new_id = \Sysconfig::create_string('listing_id');
				$created = date_create()->format('Y-m-d H:i:s');
				$type = \Model\Listing::FEED_NON_IMPORTED;
				$imported_listing = $this->get_user_imported($value);
				if($imported_listing['id'] == ''){
					continue;
				}
				if(array_key_exists($imported_listing['id'], $current_imported_listings_array) && $imported_listing['active'] == 1){
					$listing_status = \Model\Listing::LS_ACTIVE;
				}elseif($imported_listing['active'] != 1){
					$listing_status = \Model\Listing::LS_INACTIVE_IMPORT;
				}else{
					$listing_status = \Model\Listing::LS_XCOMPLETE_IMPORT;
				}
				$city = $imported_listing['city'];
				if($imported_listing['city'] == '' || $imported_listing['city'] == 0){
					$lat = $imported_listing['latitude'];
					$long = $imported_listing['longitude'];
					$prov = $imported_listing['province'];
					$country = \Sysconfig::country_name($imported_listing['country']);
					$city_name = \CityState::reverse_lat_lng($lat, $long);
					$area = \CityState::get_area_id($city_name.','.$prov.','.$country, TRUE);
					$city = $area[0];
				}

				if($imported_listing['city'] == '' || $imported_listing['city'] == 0){
					$listing_status = \Model\Listing::LS_NOCITY;
				}

				$prov_state   = $imported_listing['prov_state'];
				$country      = $imported_listing['country'];
				$address      = $imported_listing['streetaddress'];
				$zip          = $imported_listing['postalcode'];
				$price        = $imported_listing['price'];
				$bed          = $imported_listing['bedroomstotal'];
				$bath         = $imported_listing['bathroomtotal'];
				$lastupdate   = $imported_listing['lastupdated'];
				$attachment   = $imported_listing['constructionstyleattachment'];
				$title        = $imported_listing['ownershiptype'];


				$title_options = array(
					__('title_freehold') => 'd',
					__('title_condostrata') => 'c',
					__('title_lease') => 'a',
					__('title_fee') => 'a',
					__('title_freehold_condo') => 'd',
					__('title_condo') => 'c',
					__('title_lease_condo') => 'c',
					__('title_share_coop') => 'b',
					__('title_coown') => 'c',
					__('title_timeshare') => 'c',
					__('title_life_lease') => 'a'
				);

				if(in_array($key, $title_options)){
					$title = $value;
				}
				$prop_style = \Listing::forge()->get_prop_options($imported_listing['type'], 'style', false, true);
				$prop_type = \Listing::forge()->get_prop_options($imported_listing['type'], 'type', false, true);
				if(!$prop_style){
					$errors = 'Missing '.$imported_listing['architecturalstyle'].' from our list. Import Listing ID '.$imported_listing['id'].' our ID: '.$new_id;
//					return array('error' => $errors);
				} elseif ($prop_style == 'c' && $attachment == 'Detached'){
					$prop_style = 'd';

				}

				$client_has = \Model\Client\Has::forge()->load(\DB::select_array()->where('external_id', '=', $imported_listing['id'])->where('external_id', '<>', ''));
				$if_ext_exists = $client_has->as_array();

				if($if_ext_exists['external_id'] != ''){

					$fields = array(
						'listing_price' => $price,
						'address' => $address,
						'city' => $city,
						'prov_state' => $prov_state,
						'country' => $country,
						'z_p_code' => $zip,
						'bedrooms' => $bed,
						'bathrooms' => $bath,
						'listing_status' => $if_ext_exists['listing_status']
					);
					$client_has->set_fields($fields);
					$client_has->save();

				} else {

					$has = \Model\Client\Has::forge()->load(\DB::select_array());
					$has->set_fields(array(
						'listing_id' => $new_id,
						'realtor_id' => $realtor_id,
						'type_of_listing' => $type,
						'date_created' => $created,
						'listing_status' => $listing_status,
						'completed_section' => 0,
						'allowed_images' => \Model\Photos\Xtrade::MAX_ALLOWED_IMAGES,
						'imported' => \Model\Listing::IMPORTED,
						'address' => $address,
						'city' => $city,
						'prov_state' => $prov_state,
						'country' => $country,
						'z_p_code' => $zip,
						'listing_price' => $price,
						'bedrooms' => $bed,
						'bathrooms' => $bath,
						'prop_type' => $prop_type,
						'prop_style' => $prop_style,
						'external_id' => $value,
						'ext_update_time' => $lastupdate,
						'current_images' => 1
					));
					$result = $has->save();

					if($result){

						$info = \Model\Client\Information::forge();
						$info->set_fields(array(
							'listing_id' => $new_id,
							'realtor_id' => $realtor_id,
							'completed_section' => \Model\Client::STATUS_INCOMPLETE
						));
						$info->save();

						$wants = \Model\Client\Want::forge();
						$wants->set_fields(array(
							'listing_id' => $new_id,
							'realtor_id' => $realtor_id,
							'completed_section' => \Model\Client::STATUS_INCOMPLETE
						));
						$result_wants = $wants->save();

						$crea_prop = \Model\Crea\Property::forge()->load(\DB::select_array()->where('id', '=', $imported_listing['id']));
						$crea_prop->set_fields(array('xtrade_imported' => 1));
						$crea_prop->save();

						if(!$result && !$result_wants){
							\DB::delete('client_has')->where('listing_id', '=', $new_id)->execute();
							\DB::delete('client_information')->where('listing_id', '=', $new_id)->execute();
							\DB::delete('listing_orders')->where('listing_id', '=', $new_id)->execute();
						}
					}
				}
			}
		}

		$realtor = \Model\Realtor::forge()->load(\DB::select_array()->where('realtor_id', '=', $realtor_id));
		$realtor->last_import = date_create()->format('Y-m-d H:i:s');
		return array('result' => $realtor->save());
	}

	public function xref_view($value) {
		if (stripos($value, 'ocean') !== false) {
			return 'd';
		}
		if (stripos($value, 'mountain') !== false) {
			return 'c';
		}
		if (stripos($value, 'lake') !== false) {
			return 'e';
		}
		if (stripos($value, 'river') !== false) {
			return 'f';
		}
		if (stripos($value, 'city') !== false) {
			return 'b';
		}
		return 'a';
	}

	public function xref_title($value = null) {
		$data = array('' => '', 'Condominium' => 'c','Condominium/Strata' => 'c','Cooperative' => 'b',
		'Freehold' => 'd','Freehold Condo' => 'd','Leasehold' => 'a','Leasehold Condo/Strata' => 'a','Life Lease' => 'a',
		'Other, See Remarks' => '','Shares in Co-operative' => 'b','Strata' => 'c','Timeshare/Fractional' => '',
		'Undivided Co-ownership' => '','Unknown' => '');
		if ($value === null) {
			return $data;
		}
		if (isset($data[$value])) {
			return $data[$value];
		}
		return '';
	}
	public function xref_maintenance($value = null) {

	}
}