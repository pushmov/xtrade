<?php
namespace Model\Crea;

class Propertyagent extends \Model {
	protected $_class = 'propertyagent';
    protected $_table_name = 'crea_property_agents';
    protected $_data = array('prop_id' => NULL, 'agent_id' => NULL, 'pos' => 0);


	public function get_prop_agent($prop_id){
		$this->load(\DB::select()->where('prop_id', '=', $prop_id));
		return $this;
	}
}