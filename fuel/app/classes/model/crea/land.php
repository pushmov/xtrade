<?php
namespace Model\Crea;

class Land extends \Automodeler {
	protected $_class = 'land';
	protected $_table_name = 'crea_land';

	protected $_data = array(
		'id' => NULL,'sizetotal' => '', 'sizefrontage' => '', 'accesstype' => '', 'acreage' => '',
		'amenities' => '', 'clearedtotal' => '', 'currentuse' => '', 'divisible' => '', 'fencetotal' => '',
		'fencetype' => '', 'frontson' => '', 'landdisposition' => '', 'landscapefeatures' => '',
		'pasturetotal' => '', 'sewer' => '', 'sizedepth' => '', 'soilevaluation' => '', 'soiltype' => '',
		'surfacewater' => '', 'tiledtotal' => ''
	);

}