<?php
namespace Model;

class Payment extends \Model\Paypalrest {
	protected $_cc_type = array( '' => 'Select', 'visa' => 'Visa', 'mastercard' => 'Mastercard' );

	protected $_validation;

	public static function forge() {
		return new static();
	}

	public function get_cc_type(){
		return $this->_cc_type;
	}

	/**
	* The inital order and payment
	*
	* @param array $post
	*/
	public function realtor($post){
		$this->_set_validation();
		if($this->_validation->run($post) === FALSE){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}
		$post['agreement_name'] = 'Realtor Monthly';
		$post['agreement_description'] = 'Realtor monthy payment agreement';
		// Create Agreement
		$agreement = new Paypalrest\Agreement;
		return $agreement->create($post);
	}

	/**
	* Direct payment for upgrade to featured listing
	*
	* @param array $post - credit card data posted
	* @return array
	*/
	public function upgrade_listing($post){
		$this->_set_validation();
		if($this->_validation->run($post) === FALSE){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}
		list($city, $prov_state, $country) = \CityState::get_area_id($post['cpc_address'], TRUE);
		list($city_name, $prov_state_name, $country_name) = \CityState::get_area_name($city, $prov_state, $country, TRUE);

		// Create and save the order
		$order = new \Model\Order;
		$order->user_id = $post['realtor_id'];
		$order->user_type_id = User::UT_REALTOR;
		$order->type_id = $order::OT_FEATURED;
		$order->total = $post['fee'] + $post['tax'];
		$order->sub_total = $post['fee'];
		$order->taxes = $post['tax'];
		$order->tax_code = $post['tax_code'];
		$order->order_date = date_create()->format(DATETIME_FORMAT_DB);
		// Get the actual listing id
		$listing = \Model\Client\Has::forge()->load_by_listing_id($post['listing_id']);
		$order->listing_id = $listing->id;
		$order->status_id = $order::OS_PENDING;
		$order->save();

		// Add the order id
		$post['order_id'] = $order->get_number();
		$post['city'] = $city_name;
		$post['prov_state'] = \CityState::get_state_code($prov_state_name, $country_name);
		$post['country'] = \Sysconfig::country_code($country);
		$post['item_description'] = 'Featured Listing ' . $post['listing_id'];
		$pay = new \Model\Paypalrest\Payment;
		$result = $pay->send($post);

		if (isset($result->state) && ($result->state == 'approved')) {
			$order->status_id = $order::OS_COMPLETE;
			$order->trans_id = $result->id;
			$order->save();
			$listing = new \Model\Client\Has($order->listing_id);
			$listing->featured_listing = 1;
			$listing->featured_start = date_create()->format(DATETIME_FORMAT_DB);
			$listing->featured_end = date_create('+' . \Sysconfig::get('listing_featured_len') . ' days')->format(DATETIME_FORMAT_DB);
			$listing->save();
			\Emails::forge()->upgrade_listing_recurring(array_merge($post, $listing->as_array()));
			return array('status' => 'OK', 'id' => $order->id);
		}
		// TODO: log the error
		return array('status' => 'ERROR', 'message' => 'Payment processing error.<br>Check your infomation and try again.');
	}

	public function vendor($post){
		$this->_set_validation();
		if($this->_validation->run($post) === FALSE){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}
		$post['agreement_name'] = 'Vendor Monthly';
		$post['agreement_description'] = 'Agreement for vendor monthly payment';
		// Create Agreement
		$agreement = new Paypalrest\Agreement;
		return $agreement->create($post);
	}

	public function update_billing($post){
		$this->_set_validation();
		if($this->_validation->run($post) === FALSE){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}
		$pp = new \Model\Paypalrest();
		$agreementId = \PayPal\Api\Agreement::get($post['paypal_sub_id'], $pp->get_api());
		$agreementDetails = $agreementId->getAgreementDetails();
		$agreement_details = $agreementDetails->toArray();
		$post['start_date'] = $agreement_details['next_billing_date'];
		$post['agreement_name'] = 'Vendor Monthly';
		$post['agreement_description'] = 'Agreement for vendor monthly payment';
		$agreement = new \Model\Paypalrest\Agreement;
		if ($agreementId->getState() == $agreement->get_status(\Model\Paypalrest\Agreement::STATE_ACTIVE)) {
			$cancel = $agreement->cancel(array('id' => $post['paypal_sub_id'], 'note' => 'Update billing details'));
			if ($cancel['status'] == 'error') {
				return array('status' => 'ERROR', 'message' => $cancel['error']);
			}
		}
		return $agreement->create($post);
	}

	public function vendor_upgrade($post){
		/**
		* todo : old() with swtich = 'Vendor_Upgrade'
		*/
		$vendor_id = $post['realtor_id'];
		$pcode = isset($post['pcode']) ? $post['pcode'] : '';
		$vendor = $this->_vendor->load(\DB::select_array('vendor_id', '=', $vendor_id))->as_array();


		$this->_validation->add_field('first_name', 'First Name', 'required');
		$this->_validation->add_field('last_name', 'Last Name', 'required');
		$this->_validation->add_field('z_p_code', 'Zip/Postal Code', 'required');
		$this->_validation->add_field('cpc_address', 'Address', 'required');
		$this->_validation->add_field('cc_number', 'Card Number', 'required');
		$this->_validation->add_field('cc_type', 'Card type', 'required');
		$this->_validation->add_field('exp_month', 'Card Expiration Month', 'required');
		$this->_validation->add_field('exp_year', 'Card Expiration Year', 'required');
		$this->_validation->add_field('cc_cvv', 'CVV', 'required');

		if($this->_validation->run($post) === FALSE){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}

		$post['email']= $vendor['email'];
		$area_id = \CityState::get_area_id($post['cpc_address'], TRUE);
		$location = \CityState::get_area_name($area_id[0], $area_id[1], $area_id[2]);

		$post['address'] = $vendor['address'].' ' .implode(', ', $location) . ' '.$post['z_p_code'];
		$payment_info = $this->payment_info('', 'vendor_upgrade', $vendor_id, implode(',', $area_id));

		if($pcode != ''){
			$free_period_end = $payment_info['start_date'];
		}else{
			$free_period_end = 0;
		}
		$post['payment_info'] = $payment_info;

		$this->set_data($post);
		$PayPalResult = $this->_paypal->CreateRecurringPaymentsProfile($this->get_vendor_upgrade());

		if($PayPalResult['ACK'] == 'Success'){

			$trans_id = $PayPalResult['TRANSACTIONID'];
			$pp_profile_id = $PayPalResult['PROFILEID'];
			if($payment_info['total_due'] > 0){
				$receipt_id = \Sysconfig::create_string('salt');
				$receipt = \Html::anchor('receipt/'.$receipt_id, 'here') . 'for a copy of your receipt.';
			}

			$GRPPDFields = array(
				'profileid' => $vendor['paypal_sub_id']			// Profile ID of the profile you want to get details for.
			);
			$PayPalRequestData = array('GRPPDFields'=>$GRPPDFields);
			$PayPalResult = $this->_paypal->GetRecurringPaymentsProfileDetails($PayPalRequestData);

			$recurring_data = array(
				'profile' => $PayPalResult,
				'vendor_id' => $vendor_id,
				'new_amount' => $payment_info['new_amount'],
				'new_amount_tax' => $payment_info['new_amount_tax']
			);
			$this->set_data($recurring_data);
			$PayPalResult_2 = $this->_paypal->UpdateRecurringPaymentsProfile($this->get_vendor_recurring());

			$update = $this->_vendor->load(\DB::select_array()->where('vendor_id', '=', $vendor_id));
			$update->set_fields(array('featured' => 1));
			$update->save();

			$maildata = array(
				'receipt_id' => $receipt_id,
				'receipt' => $receipt,
				'vendor' => $vendor
			);
			$this->_emails->upgradevendor($maildata);

			$orders = $this->_orders->load();
			$fields = array(
				'type' => $payment_info['trans_id'],
				'homebase_id' => $vendor_id,
				'subscriber_id' => $pp_profile_id,
				'creation_date' => date_create()->format('Y-m-d H:i:s'),
				'period_type' => '',
				'pay_amount' => $payment_info['fee_with_tax'],
				'payment_sig' => '',
				'payer_id' => '',
				'initial_amount' => $payment_info['setup_fee_total'],
				'payment_status' => '',
				'tax_amount' => $payment_info['fee_tax'],
				'trans_id' => $trans_id,
				'status' => 'Pending',
				'gst' => $payment_info['reg_gst'],
				'pst' => $payment_info['reg_pst'],
				'hst' => $payment_info['reg_hst'],
				'qst' => $payment_info['reg_qst'],
				'featured_amount' => $payment_info['featured_cost'],
				'receipt_id' => $receipt_id
			);
			$orders->set_fields($fields);
			$orders->save();

			$response = array('status' => 'OK', 'Conf_Num' => $pp_profile_id);

		}elseif($PayPalResult['ACK'] == 'Failure' || $PayPalResult['ACK'] == ''){

			$response = array('status' => 'ERROR');
		}

		return $response;
	}

	public function vendor_downgrade($post){
		/**
		* todo : old() with swtich = 'Vendor_Downgrade'
		*/
		$vendor_id = $post['realtor_id'];
		$model = $this->_vendor->load(\DB::select_array()->where('vendor_id', '=', $vendor_id));
		$vendor = $model->as_array();

		$payment_info = $this->payment_info('', 'vendor_downgrade', $vendor_id);
		$GRPPDFields = array(
			'profileid' => $vendor['paypal_sub_id']
		);
		$grppd_fields = array('GRPPDFields'=>$GRPPDFields);
		$recurring_result = $this->_paypal->GetRecurringPaymentsProfileDetails($grppd_fields);
		$data['vendor'] = $vendor;
		$data['response'] = $recurring_result;
		$data['vendor']['new_amount'] = $payment_info['new_amount'];
		$data['vendor']['new_amount_tax'] = $payment_info['new_amount_tax'];

		$this->set_data($data);
		$PayPalResult = $this->_paypal->UpdateRecurringPaymentsProfile($this->get_config_update_vendor_profile());
		if($PayPalResult['ACK'] == 'Success'){
			$model->set_fields(array('featured' => 0));
			$model->save();
			$this->_emails->vendor_downgrade(array('vendor_id' => $vendor['vendor_id']));
			$response = array('status' => 'OK');
		}elseif($PayPalResult['ACK'] == 'Failure' || $PayPalResult['ACK'] == ''){
			$response = array('status' => 'ERROR', 'reply' => $PayPalResult, 'mem_dets' => $recurring_result);
		}

		return $response;
	}

	/**
	* Returns validation instance
	*
	*/
	protected function cc_validation($use_cc_number=true) {
		$this->_validation = \Validation::forge();
		$this->_validation->add_field('first_name', 'First Name', 'required');
		$this->_validation->add_field('last_name', 'Last Name', 'required');
		$this->_validation->add_field('address', 'Address', 'required');
		$this->_validation->add_field('cpc_address', 'City/State/Country', 'required');
		$this->_validation->add_field('z_p_code', 'Zip/Postal code', 'required');
		$this->_validation->add_field('cc_type', 'Card Type', 'required');
		if ($use_cc_number) {
			$this->_validation->add_field('cc_number', 'Card Number', 'required');
			$this->_validation->add_field('cc_cvv', 'CVV', 'required');
		}
		$this->_validation->add_field('exp_month', 'Exp Month', 'required');
		$this->_validation->add_field('exp_year', 'Exp Year', 'required');
		return $this->_validation;
	}

	function money_format($amount){
		$locale = localeconv();
		return $locale['currency_symbol'].''. number_format($amount, 2, $locale['decimal_point'], $locale['thousands_sep']);
	}

	protected function _set_validation() {
		$this->_validation = \Validation::forge();
		$this->_validation->add_field('first_name', 'First Name', 'required');
		$this->_validation->add_field('last_name', 'Last Name', 'required');
		$this->_validation->add_field('address', 'Address', 'required');
		$this->_validation->add_field('cpc_address', 'City/State/Country', 'required');
		$this->_validation->add_field('z_p_code', 'Zip/Postal code', 'required');
		$this->_validation->add_field('cc_number', 'Credit Card', 'required');
		$this->_validation->add_field('exp_month', 'Exp Month', 'required');
		$this->_validation->add_field('exp_year', 'Exp Year', 'required');
	}
}