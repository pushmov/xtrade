<?php
namespace Model;

class Loginlog extends \Model\Log {
	const FAILED = 0;
	const SUCCESS = 1;

	protected $_table_name = 'login_log';
	protected $_data = array('id' => NULL, 'user_id' => '', 'user_name' => '', 'ip' => '', 'message' => '', 'date' => '');

	protected $_login_status = array(
		self::FAILED => 'Failed',
		self::SUCCESS => 'Success'
	);

	public function get_login_status($status=null){
		if(isset($status)){
			return $this->_login_status[$status];
		}
		return $this->_login_status;
	}

	public function user_activity($user_id){
		$this->load(\DB::select()->where('user_id', '=', $user_id)
			->order_by('date', 'desc')
			->limit(1));
		return $this->as_array();
	}

	public function user_last_success_login($user_id){
		$this->load(\DB::select()->where('user_id', '=', $user_id)
				->where('message', '=', $this->get_login_status(self::SUCCESS))
				->order_by('date', 'desc')
				->limit(1));
		return $this->as_array();
	}
}
