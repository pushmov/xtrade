<?php

namespace Model\City;

class Usa extends \Automodeler{
  
  
	protected $_class = 'usa';
	protected $_table_name = 'us_cities';
	protected $_data =	array( 'id' => NULL, 'city' => '', 'county_id' => '', 'state_id' => '');
  
  
}