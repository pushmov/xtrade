<?php

namespace Model\Country;

class Canada extends \Automodeler {
  
	protected $_class = 'canada';
	protected $_table_name = 'ca_neighbourhoods';
	protected $_data =	array( 'id' => NULL, 'neigh_name' => '', 'city_id' => '', 'province_id' => '', 'ob_id' => '', 'hood_id' => '');
  
}