<?php

namespace Model\Photos;

class Xtrade extends \Automodeler {
	const MAX_ALLOWED_IMAGES = 50;
	const MAX_WIDTH = 900;
	const MAX_HEIGHT = 675;
	const TMB_WIDTH = 100;
	const TMB_HEIGHT = 75;

	protected $_class = 'xtrade';
	protected $_table_name = 'xtrade_photos';
	protected $_data =	array( 'id' => NULL, 'listing_id' => '', 'pos' => '', 'style' => '', 'img_num' => 1, 'url' => NULL );
	
	/**
	* remove_missing_photos
	 * Remove not found photos in asset associated with xtrade.photos record
	*
	* @param string $listing_id
	*/
	public function remove_missing_photos($listing_id) {
		$photos = \DB::select()->from($this->_table_name)->where('listing_id', '=', $listing_id)
				->execute();
		foreach($photos as $photo){
			if(is_readable(\Model\Listing::IMG_PATH.$photo['url']) === false){
				\DB::delete($this->_table_name)->where('id', '=', $photo['id'])->execute();
			}
		}
	}

	/**
	* Gets the last image position
	*
	* @param string $listing_id
	*/
	public function get_last_position($listing_id){
		$next = 1;
		$this->load(\DB::select_array()->where('listing_id', '=', $listing_id)->order_by('pos', 'desc')->limit(1));
		if($this->loaded()){
			$next = $this->pos + 1;
		}
		return $next;
	}
	/**
	* Gets the last image number
	*
	* @param string $listing_id
	*/
	public function get_last_image($listing_id){
		$this->load(\DB::select_array()->where('listing_id', '=', $listing_id)->order_by('img_num', 'desc')->limit(1));
		if($this->loaded()) {
			return $this->img_num;
		}
		return 0;
	}

	public function insert_xtrade_photos($listing_id, $pos, $num, $filename){
		$this->listing_id = $listing_id;
		$this->pos = $pos;
		$this->style = 2;
		$this->img_num = $num;
		$this->url = $filename;
		return $this->save();
	}

	public function delete_image($post){
		$response['status'] = 'ERROR';
		$xtrade = \Model\Photos\Xtrade::forge($post['id']);
		if($xtrade->loaded()){
			unlink(DOCROOT . \Model\Listing::IMG_PATH . $xtrade->url);
			unlink(DOCROOT . \Model\Listing::IMG_PATH . \Model\Listing::IMG_PATH_THUMB . $xtrade->url);
			$client_has = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $xtrade->listing_id));
			if($client_has->loaded()){
				$client_has->current_images = $client_has->current_images - 1;
				$client_has->lastupdated = date_create()->format('Y-m-d H:i:s');
				$client_has->save();
			}
			$xtrade->remove();
			$response['status'] = 'OK';
		}
		return $response;
	}
	public function delete_all($listing_id){
		$this->load(\DB::select_array()->where('listing_id', '=', $listing_id), null);
		if($this->loaded()){
			foreach($this->as_array() as $photo) {
				$this->remove();
				unlink(DOCROOT . \Model\Listing::IMG_PATH . $photo['url']);
				unlink(DOCROOT . \Model\Listing::IMG_PATH . \Model\Listing::IMG_PATH_THUMB . $photo['url']);
			}
		}

	}

	public function get_first_photo($listing_id, $cron_flag=false){
		if($cron_flag === false){
			$this->remove_missing_photos($listing_id);
		}
		$img = $this->load(\DB::select_array()->where('listing_id', '=', $listing_id)->order_by('pos', 'asc'));
		if ($img->loaded()) {
			if($cron_flag === true){
				$path =  DOCROOT . 'public_html/' . \Model\Listing::IMG_PATH . $img->url;
			} else {
				$path =  DOCROOT . \Model\Listing::IMG_PATH . $img->url;
			}
			if(is_readable($path)){
				return ($cron_flag === true) ? \Model\Listing::IMG_DIR_NAME . $img->url : $img->url;
			}
		}
		return 'no-image.png';
	}


	public static function update_photos($pos, $url){
		$row = $this->load(\DB::select_array('id')->where('url', '=', $url))->as_array();
		$this->id = $row['id'];
		$this->set_fields(array('pos' => $pos));
		return $this->save();
	}

	public function get_images($id, $thumb = false){
		$image_url = '';
		$image = $this->load(\DB::select_array()->where('listing_id', '=', $id)->order_by('pos', 'desc'), null);
		foreach($image as $row){
			if(is_readable(DOCROOT. \Model\Listing::IMG_ASSET_DIR.$row['url'])){
				$image_url = $row['url'];
				break;
			}
		}
		if($image_url == ''){
			$image_url = 'no-image.png';
		}
		return $image_url;
	}

	function get_xphoto($id, $pos=0, $thumb = 1){
		$image_url = '';
		do{
			$image = $this->load(\DB::select_array()->where('listing_id', '=', $id)
				->where('pos', '=', $pos));
			$image_url = $image->url;
			$pos++;
		}while($image['url'] == '' && $pos <= 10);

		if($image_url == ''){
			$image_url = 'no-image.png';
		}

		return $image_url;
	}

	public function get_photo($id, $pos, $type = 2) {
		$query = $this->load(\DB::select_array()->where('listing_id', '=', $id)
			->where('pos', '=', $pos)
			->where('style', '=', $type))->as_array();

		do{
			$image = $query['url'];
			$pos++;
		}while(!$image && $pos <= 10);

		return $image;
	}

	public function load_photos($listing_id, $thumb = false){
		$return = array();
		$data = $this->load(\DB::select_array()->where('listing_id', '=', $listing_id)->order_by('pos', 'asc'), NULL);
		foreach($data as $row){
			if(is_readable(DOCROOT . \Model\Listing::IMG_PATH . $row->url)){
				$img = $row->as_array();
				if ($thumb === false) {
					$img['url'] = \Model\Listing::IMG_DIR_NAME . $img['url'];
				} else {
					$img['url'] = \Model\Listing::IMG_DIR_NAME . \Model\Listing::IMG_PATH_THUMB . $img['url'];
				}
				$return[$row->id] = $img;
			}
		}
		return $return;
	}

	public function load_photo_detail($listing_id){
		$data['images_list'] = '';
		$data['bullet_list'] = '';
		$xtrade = $this->load(\DB::select_array()
			->where('listing_id', '=', $listing_id)
			->order_by('pos', 'asc')
			->order_by('style', 'desc'), NULL)->as_array();
		foreach($xtrade as $idx => $row){
			$thumb = $picture = 'no-image.png';
			if(is_readable(DOCROOT . \Model\Listing::IMG_PATH.$row['url'])){
				$picture = \Model\Listing::IMG_DIR_NAME . $row['url'];
				$thumb = \Model\Listing::IMG_DIR_NAME. \Model\Listing::IMG_PATH_THUMB .$row['url'];
			}
			$data['images_list'] .= '<li data-thumb="'.\Asset::get_file($thumb, 'img').'">'.\Asset::img($picture).'</li>';
		}
		return $data;
	}

	public function reorder($order) {
 		$ids = explode(',', $order);
 		$i = 1;
 		foreach($ids as $row) {
 			$this->load(\DB::select_array()->where('id', '=', $row));
 			$this->pos = $i;
 			$this->save();
			$i++;
 		}
		$response['status'] = 'OK';
		return $response;
 	}
	
	/**
	 * this function is same as \Crea\Property get_image() 
	 * difference : this function is not download fresh photo through phrets class, instead already downloaded photos in /assets/images/listings/crea
	 * or no-image if not found
	 *
	 * @param int $id - the crea id
	 * @param int $pos - the image position or null for all
	 * @param bool $thumb - true for thumbnail version
	 */
	public function get_local_imported_photo($id, $pos = null, $thumb = true){
		// Strip prefix from id
		$id = (strpos($id, '2_') === false) ? $id : substr($id, 2);
		$path = DOCROOT . \Model\Crea\Property::IMG_CREA_PATH;
//		$path = DOCROOT .'public_html/'. \Model\Crea\Property::IMG_CREA_PATH;
		$url = 'no-image.png';
		if ($pos == 1) {
			if (is_readable($path . "{$id}_1.jpg")) {
				if ($thumb === true) {
					$url = "listings/crea/thumbs/{$id}_1.jpg";
				} else {
					$url = "listings/crea/{$id}_1.jpg";
				}
			}
		} else {
			if ($pos === null) {
				// Its now an array or urls
				$url = array();
				if ($thumb == true) {
					$path .= 'thumbs/';
				}
				foreach (glob($path . "{$id}_*.jpg") as $file) {
					// Romove all up to the path
					$url[] = substr($file, strpos($file, 'listings'));
				}

			} else {
				// Check again
				if (is_readable($path . "{$id}_{$pos}.jpg")) {
					if ($thumb == true) {
						$url = "listings/crea/thumbs/{$id}_{$pos}.jpg";
					} else {
						$url = "listings/crea/{$id}_{$pos}.jpg";
					}
				}
			}
		}

		return $url;
	}
}