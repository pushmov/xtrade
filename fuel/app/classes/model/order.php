<?php
namespace Model;

class Order extends \Automodeler {
	// Order types
	const OT_SETUP = 1;
	const OT_SIGNUP = 2;
	const OT_MONTHLY = 3;
	const OT_FEATURED = 4;
	const OT_LISTING = 5;
	const OT_UPGRADE = 6;
	// Order status
	const OS_PENDING = 0;
	const OS_COMPLETE = 1;

	protected $_table_name = 'orders';
	protected $_data = array('id' => null, 'user_id' => null, 'user_type_id' => 0, 'type_id' => '', 'listing_id' => 0,
		'total' => '', 'sub_total' => '', 'taxes' => '', 'tax_code' => '', 'agreement_id' => '', 'trans_id' => '',
		'order_date' => '', 'status_id' => '');
	protected $_types = array(1 => 'setup','signup','monthly','featured','listing','upgrade');
	protected $_rules = array();

	/**
	* Get the order number as a padded string
	*
	* @param int $id - the order number or null
	* @return string
	*/
	public function get_number($id = null) {
		if ($id !== null) {
			return 'T' . str_pad($id, 6, '0', STR_PAD_LEFT);
		} elseif ($this->id !== null) {
			return 'T' . str_pad($this->id, 6, '0', STR_PAD_LEFT);
		}
		return '';
	}
	public function get_type_txt($id = null) {
		\Lang::load('orders');
		if ($id === null) {
			$data = array();
			foreach($this->_types as $v) {
				$data[$v] = $v;
			}
			return $data;
		} else {
			$txt = $this->_types[$id];
			return __($txt);
		}
		return '';
	}

	/**
	* Load the last order for a user
	*
	* @param int $user_id - the user's id
	* @param int $type_id - the user type (realtor, broker, vendor)
	* @return Orders
	*/
	public function load_last($user_id, $type_id) {
		$query = \DB::select_array()->where('user_id', '=', $user_id)->where('user_type_id', '=', $type_id)->order_by('order_date','desc');
		$this->load($query);
		return $this;
	}
	/**
	* Get the order history for a user
	*
	* @param int $user_id - the user's id
	* @param int $type_id - the user type (realtor, broker, vendor)
	* @return array
	*/
	public function get_history($user_id, $type_id) {
		$data = array();
		$result = \DB::select('type_id','total','order_date','sub_total')->from($this->_table_name)
		->where('user_id', '=', $user_id)
		->where('user_type_id', '=', $type_id)
		->order_by('order_date','desc')
		->execute();
		foreach($result as $row) {
			$row['type'] = __($row['type_id']);
			$row['total'] = number_format($row['total'], 2);
			$row['order_date'] = date_create($row['order_date'])->format(DATE_FORMAT);
			$data[] = $row;
		}
		return $data;
	}

	public function doinsert($order_fields){
		$this->set_fields(array_map('trim', $order_fields));
		return $this->save();
	}

	public function get_type($id){
		if(!array_key_exists($id, $this->_types)){
			return '';
		}
		return $this->_types[$id];
	}

	public function change_plan($plan_id, $change_plan_to=null){
		$agreement = new \Model\Paypalrest\Agreement();
		$trans = $this->load(\DB::select()->where('paypal_plan_id', '=', $plan_id), null)->as_array();
		$ut = \Model\User::forge()->get_types();
		$plan = \Config::load('paypal', 'plan', true);

		foreach($trans as $t){
			$model = '\\Model\\' . ucwords($ut[$t['user_type_id']]);
			$user = $model::forge($t['user_id']);
			$data['card_id'] = $user->paypal_sub_id;
			$data['plan_id'] = ($change_plan_to === null) ? $plan['plan'][\Sysconfig::country_code($user->country)] : $change_plan_to;
			$data['agreement_name'] = 'xTH Monthly Membership';
			$data['agreement_description'] = 'Monthly Membership Payment';
			$data['email'] = $user->email;
			$data['first_name'] = $user->first_name;
			$data['last_name'] = $user->last_name;
			$agreement_id = $agreement->create($data);

			$this->load(\DB::select()->where('id', '=', $t['id']));
			$this->paypal_plan_id = $data['plan_id'];
			$this->paypal_agreement_id = $agreement_id->id;
			$this->save();
		}
		return;
	}
}