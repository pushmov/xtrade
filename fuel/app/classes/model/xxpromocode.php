<?php
/**
* This model is no longer used as we are going to use a simpler one for now
*/
namespace Model;

class xxPromocode extends \Automodeler {
	const RT_FLAT = 1;  //rate type default value
	const RT_PERCENT = 2; // As a decimal

    const PROMO_ENABLED = 1;
    const PROMO_DISABLED = 0;
    const PROMO_AS_EXPIRED = 1;
    const PROMO_AS_ACTIVE = 0;
    const PROMO_DEFAULT_DURATION = 'Month';
    const PROMO_FREQUENT = 1;

	const MODE_ADD = 1;
	const MODE_EDIT = 2;

	protected $_table_name = 'promo_codes';
	protected $_data = array( 'id' => NULL, 'user_type' => '',
        'code' => '', 'rate' => 0.00, 'duration' => 0, 'rate_type' => self::RT_FLAT,
		'details' => '', 'terms' => '', 'fine_print' => '',
        'valid_from' => '', 'valid_till' => '', 'enabled' => self::PROMO_ENABLED,
		'expired' => self::PROMO_AS_ACTIVE, 'featured' => '', 'on_setup_fee' => '', 'paypal_plan_id' => '');
	protected $_rate_types = array(self::RT_FLAT => 'flat', self::RT_PERCENT => 'percent');
	protected $_rules = array();

	public function _validation_unique_code($val) {
		$result = \DB::select()->from($this->_table_name)->where('code', '=', trim($val))->execute();
		return (count($result) == 0) ? true : false;
	}

	/**
	* Load the promo info by code
	*
	* @param mixed $code
	*/
	public function load_by_code($code) {
		$this->load(\DB::select_array()->where('code', '=', $code));
		return $this;
	}
	/**
	* Load a valid promo code
	*
	* @param string $code - the code
	* @return Promocode
	*/
	public function load_valid($code) {
		$this->load(\DB::select_array()
			->where('code', '=', $code)
			->where('enabled', '=', self::PROMO_ENABLED)
			->where('expired', '=', self::PROMO_AS_ACTIVE)
			->where('valid_from', '<=', date_create()->format('Y-m-d'))
			->where('valid_till', '>=', date_create()->format('Y-m-d')));
		return $this;
	}

	public function load_active($code){
		$this->load_by_code($code);
		$plan = new \Model\Paypalrest\Plan();
		$state = $plan->get_state($this->paypal_plan_id);
		if($state == \Model\Paypalrest\Plan::STATE_ACTIVE){
			return true;
		}

		return false;
	}
	/**
	* Generate select promo for user
	*
	* @param string $code - the code
	* @return Promocode
	*/
	public function select_user_type(){
		$user = '\Model\\User';
		$select = array(
			$user::UT_REALTOR => ucwords($user::forge()->get_types($user::UT_REALTOR)),
			$user::UT_BROKER => ucwords($user::forge()->get_types($user::UT_BROKER)),
			$user::UT_VENDOR => ucwords($user::forge()->get_types($user::UT_VENDOR))
		);
		return $select;
	}
	/**
	* Get readable rate type
	*
	* @param int $id - the id
	* @return array rate type
	*/
	public function get_rate_type($id = null){
		if($id === null){
			return $this->_rate_types;
		}
		return ucwords($this->_rate_types[$id]);
	}
	/**
	* Get all promo records
	*
	* @param array params - datatables params
	* @return array promo records
	*/
	public function all_promos($params){
		$data['data'] = $drow = array();
		$aColumns = array('user_type', 'code', 'rate', 'duration', 'rate_type', 'enabled', 'expired', 'valid_from', 'valid_till', 'paypal_plan_id', 'action');
		$db = \DB::select();
		if (isset($params['start']) && $params['length'] != '-1') {
			$db->limit((int) $params['length'])->offset((int) $params['start']);
		}
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$db->order_by($aColumns[$params['order'][0]['column']], $params['order'][0]['dir']);
		} else {
			$db->order_by('id', 'asc');
		}
		if (isset($params['search']) && $params['search'] != ''){
			$db->where('code', 'like', '%'.$params['search']['value'].'%');
		}
		$promos = $this->load($db, null)->as_array();
		foreach($promos as $row){
			$drow['user_type'] = ucwords(\Model\User::forge()->get_types($row['user_type']));
			$drow['code'] = $row['code'];
			$drow['rate'] = $row['rate'];
			$drow['duration'] = $row['duration'];
			$drow['rate_type'] = $this->get_rate_type($row['rate_type']);
			$drow['enabled'] = ($row['enabled'] == 1) ? __('enabled') : __('disabled');
			$drow['expired'] = ($row['expired'] == 1) ? __('expired') : __('active');
			$drow['valid_from'] = date_create($row['valid_from'])->format(DATE_FORMAT);
			$drow['valid_till'] = date_create($row['valid_till'])->format(DATE_FORMAT);
			if ($row['paypal_plan_id'] != '') {
				$plan = \Html::anchor('admins/paypal/view/' . $row['paypal_plan_id'], 'View', array('class' => 'actions edit inline-action'));
			} else {
				$plan = \Html::anchor('#', 'Create', array('class' => 'actions edit inline-action promo-create-plan', 'id' => $row['id']));
			}
			$drow['plan'] = $plan;
			$drow['action'] = \Html::anchor('admins/promo/edit/'.$row['code'], __('edit'), array('class' => 'actions edit inline-action'));
			$user = \Model\Realtor::forge()->load(\DB::select('id')->where('promo_code', '=', $row['code']), null);
			if(count($user) <= 0){
				$attr = array('class' => 'actions no-link delete inline-action promo-delete', 'data-target' => $row['code']);
				$drow['action'] .= \Html::anchor('admins/promo/delete/'.$row['code'], __('delete'), $attr);
			}
			$data['data'][] = array_values($drow);
        }
		$total = \DB::select()->from($this->_table_name)->execute()->count();
		$data['recordsTotal'] = $total;
		$data['recordsFiltered'] = $total;
		return $data;
	}

	/**
	* Delete promo by code
	* unused atm
	* @param string $code - promo code
	* @return array response general format
	*/
	public function delete_promo($code){
		$response['message'] = 'Delete action failed';
		$response['status'] = 'ERROR';
		$this->load(\DB::select()->where('code', '=', $code));
		if($this->loaded()){
			\DB::delete($this->_table_name)->where('code', '=', $code)->execute();
			$response['message'] = 'Success. Promo deleted';
			$response['status'] = 'OK';
			$plan = new \Model\Paypalrest\Plan();
			$response['plan'] = $plan->delete($this->paypal_plan_id);
		}

		//is this accessible ?
		//delete action only viewable when there is no users attached
		$this->change_agreement($code, 'default');
		return $response;
	}

	/**
     * Create promo submitted
     *
     * @param array $post Data sets of promo form
     * @return array Status response
     */
    public function submit_promo($post){
        \Lang::load('common');
		\Lang::load('admin');
		$data = $post['data'];
		$validation = \Validation::forge();
		$validation->add_callable($this);
		$validation->add_field('code', __('promocode'), 'required');
		$validation->add_field('valid_from', __('valid_from'), 'required');
		$validation->add_field('valid_till', __('valid_till'), 'required');
		$validation->add_field('details', __('promo_detail'), 'required');
		$validation->add_field('terms', __('promo_terms'), 'required');
		if ($data['mode'] == self::MODE_ADD) {
			$validation->field('code')->add_rule('unique_code');
		}
		if (!$validation->run($data)) {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
        $response['status'] = 'error';
        $model = \Model\Promocode::forge($data['id']);
        $model->id = $data['id'];
        $model->set_fields(array_map('trim', $data));
        if($model->save()){
            $response['status'] = 'ok';
            \Session::set_flash('message', __('promo_saved'));
        }
		$result = $this->generate_paypal_plan($model->id);
		$model->paypal_plan_id = $result->id;

		$plan = new \Model\Paypalrest\Plan();
		$state = $plan->get_state($model->paypal_plan_id);
		if($state == \Model\Paypalrest\Plan::STATE_ACTIVE && $model->enabled == self::PROMO_DISABLED){
			$plan->update_state(array('plan_id' => $model->paypal_plan_id, 'state' => \Model\Paypalrest\Plan::STATE_INACTIVE));
		}
		if($state == \Model\Paypalrest\Plan::STATE_INACTIVE && $model->enabled == self::PROMO_ENABLED){
			$plan->update_state(array('plan_id' => $model->paypal_plan_id, 'state' => \Model\Paypalrest\Plan::STATE_ACTIVE));
		}
        return $response;
    }

	/**
	 * update promo data only
	 */
	public function update_promo($post){
		\Lang::load('common');
		\Lang::load('admin');
		$data = $post['data'];
		$response['status'] = 'error';
		$validation = \Validation::forge();
		$validation->add_callable($this);
		$validation->add_field('valid_from', __('valid_from'), 'required');
		$validation->add_field('valid_till', __('valid_till'), 'required');
		$validation->add_field('details', __('promo_detail'), 'required');
		$validation->add_field('terms', __('promo_terms'), 'required');
		if (!$validation->run($data)) {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}

		$this->load_by_code($data['code']);
		$this->enabled		= $data['enabled'];
		$this->expired		= $data['expired'];
		$this->valid_from	= $data['valid_from'];
		$this->valid_till	= $data['valid_till'];
		$this->details		= $data['details'];
		$this->terms		= $data['terms'];
		$this->fine_print	= $data['fine_print'];

		$users = \Model\Realtor::forge()->load(\DB::select('id')->where('promo_code', '=', $this->code), null);
		if(count($users) <= 0){
			$this->rate_type = $data['rate_type'];
			$this->rate = $data['rate'];
			$this->duration = $data['duration'];
		}
		if($this->save()){
			$response['status'] = 'ok';
            \Session::set_flash('message', __('promo_saved'));
		}
		if(count($users) <= 0){
			$this->paypal_plan_id = $this->generate_paypal_plan($this->id);
		}
		$plan = new \Model\Paypalrest\Plan();
		$state = $plan->get_state($this->paypal_plan_id);

		if($state == \Model\Paypalrest\Plan::STATE_ACTIVE && $data['enabled'] == self::PROMO_DISABLED){
			$plan->update_state(array('plan_id' => $this->paypal_plan_id, 'state' => \Model\Paypalrest\Plan::STATE_INACTIVE));
			$this->change_agreement($this->code, 'default');
		}
		if($state == \Model\Paypalrest\Plan::STATE_INACTIVE && $data['enabled'] == self::PROMO_ENABLED){
			$plan->update_state(array('plan_id' => $this->paypal_plan_id, 'state' => \Model\Paypalrest\Plan::STATE_ACTIVE));
			$this->change_agreement($this->code, 'promo');
		}
		return $response;
	}

	/**
	 * set users monthly subscription plan agreement from promo to default plan
	 * @param String $code Promo code
	 * @param String $type Type of plan
	 */
	public function change_agreement($code, $type='default'){
		$this->load_by_code($code);
		$ut = \Model\User::forge()->get_types();
		$model = '\\Model\\' . ucwords($ut[$this->user_type]);
		$users = $model::forge()->load(\DB::select()->where('promo_code', '=', $code), null)->as_array();
		$agreement = new \Model\Paypalrest\Agreement();
		foreach($users as $user){

			if($type == 'default'){
				$plan = \Config::load('paypal', 'plan', true);
				if($this->user_type == \Model\User::UT_VENDOR){
					list($city, $prov_state, $country) = \CityState::get_area_id($user['location_address']);
					$plan_id	= $plan['plan'][\Sysconfig::country_name($country)];
				} else {
					$plan_id	= $plan['plan'][\Sysconfig::country_code($user['country'])];
				}
			} elseif($type == 'promo') {
				$plan_id = $this->paypal_plan_id;
			}

			$data['agreement_name']			= 'monthly membership';
			$data['agreement_description']	= 'monthly membership description';
			$data['plan_id']				= $plan_id;
			$data['email']					= $user['email'];
			$data['first_name']				= $user['first_name'];
			$data['last_name']				= $user['last_name'];
			$data['card_id']				= $user['paypal_sub_id'];
			$agreement->create_from_vault($data);
		}
		return;
	}

	public function generate_paypal_plan($id){
		$this->load(\DB::select()->where('id', '=', $id));
		$paypal = new \Model\Paypalrest();
		switch($this->user_type){
			case \Model\User::UT_REALTOR:
				$amount = \Sysconfig::get('realtor_monthly_fee');
				break;
			case \Model\User::UT_BROKER:
				$amount = \Sysconfig::get('broker_monthly_fee');
				break;
			default:
				$amount = \Sysconfig::get('vendor_monthly_fee');
				break;
		}
		$info['name'] = $this->code.' promo';
		$info['desc'] = $this->code.' promo detail';
		$info['amount'] = $amount;
		$info['tax'] = 0;
		$info['trial_len'] = $this->duration == 0 ? 0 : 1;
		$info['trial_duration'] = $this->duration;
		$info['trial_amt'] = $this->rate;
		$info['currency'] = 'CAD';
		if ($this->rate_type == self::RT_PERCENT) {
			//todo: percent based sysconfig / paypal plan
			$info['trial_amt'] = ($this->rate / 100) * $amount;
		}
		$result = $paypal->create_plan($info);
		$this->paypal_plan_id = $result->id;
		$this->save();
		return $result;
	}

	/**
     * Generate paired array of promo rate dropdown
     *
     * @return array Promo rate array dropdown
     */
    public function select_promo_rate(){
		\Lang::load('common');
        $select = array(
            self::RT_FLAT => __('flat'),
            self::RT_PERCENT => __('percent')
        );
        return $select;
    }




}