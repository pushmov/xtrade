<?php
namespace Model\Client;

class Has extends \Model\Client {
	const ID_PREFIX = 'xTH';

	protected $_class = 'has';
	protected $_table_name = 'client_has';
	protected $_data = array( 'id' => NULL, 'listing_id' => '', 'realtor_id' => '', 'feedupdated' => '',
		'lastupdated' => '', 'country' => '','prov_state' => '','city' => '','county' => NULL,'address' => '',
		'z_p_code' => '', 'street_name' => '','street_number' => '','suite' => NULL,'prop_type' => '','bedrooms' => '',
		'bathrooms' => '', 'laundry' => '00000','kitchen' => '00000000','finished_basement' => '','year_built' => '',
		'prop_style' => '','sq_footage' => '','lot_size' => '',	'heating' => '','cooling' => '','water' => '','sewer' => '',
		'property_features' => '0000000000000','interior_features' => '000000000',	'neighbourhood_features' => '000000',
		'strata_restrictions' => '0000000000','strata_fees' => '','mobile_features' => '0000000000','condo_features' => '0000000000',
		'view' => '','waterfront' => '','school_info' => '','comments' => '','listing_price' => '','total_acreage' => '',
		'prop_tax' => '','neighbourhood' => 'none','heat_source' => '','roof_type' => '','floor_type' => '','siding_type' => '',
		'garage_type' => '','lat' => '','lng' => '','date_listed' => '','date_expire' => '','expiry_notified' => '','list_as_sold' => '0',
		'paypal_status' => '0','sold_date' => '','allowed_images' => '10','current_images' => '0','type_of_listing' => '','youtube_link' => '',
		'virtual_tour_link' => '','date_created' => '','incomplete_notified' => '','fsbo' => '0','matches_found' => '0',
		'buyers_agent_listing' => '0','external_id' => '','completed_section' => '0','initial_search' => '','matches' => '0',
		'featured_listing' => '0','featured_start' => NULL,'featured_end' => NULL,'image_page' => '0','deleted' => '0','p_title' => NULL,
		'imported' => '0','client_fill_key' => '','lastupdate' => '','listing_status' => '','not_tradable' => ''
	);

	protected $_valrules = array(
		'address' => array('paddress', 'required'),
		'c_address' => array('city_state_country', 'required'),
		'neighbourhood' => array('neigh', 'required'),
		'z_p_code' => array('postal', 'required'),
		'listing_price' => array('price', 'required'),
		'sq_footage' => array('sq_ftg', 'required'),
		'prop_type' => array('ptype', 'required'),
		'bedrooms' => array('bed', 'required'),
		'bathrooms' => array('baths', 'required'),
		'p_title' => array('prop_title', 'required'),
		'prop_style' => array('listing_propopt', 'required'),
		'finished_basement' => array('basement', 'required'),
		'garage_type' => array('garage', 'required'),
		'youtube_link' => array('youtube_link', 'valid_url'),
		'virtual_tour_link' => array('virtual_tour_link', 'valid_url')
	);

	protected $_features = array('multiple' => array(
		'kitchen', 'laundry', 'property_features', 'interior_features', 'neighbourhood_features',
		'siding_type', 'floor_type', 'roof_type'),
		'single' => array('prop_type', 'finished_basement', 'prop_style', 'heating', 'cooling', 'water', 'sewer',
			'view', 'heat_source', 'garage_type', 'p_title', 'year_built')
	);

	protected $_feature = array('interior_features' => 'ifeature',
		'kitchen' => 'kfeature', 'laundry_features' => 'lfeatures','property_features' => 'pfeature',
		'neighbourhood_features' => 'nfeature'
	);

	protected $_incomplete_values = array(
		'address' => '', 'city' => 0, 'prov_state' => 0, 'country' => 0,
		'neighbourhood' => '', 'z_p_code' => '', 'listing_price' => 0,
		'sq_footage' => 0, 'prop_type' => '', 'bedrooms' => 0, 'bathrooms' => 0, 'p_title' => null,
		'prop_style' => '', 'finished_basement' => '', 'garage_type' => ''
	);

	// These fields could have multiple values
	protected $_multi_values = array(
		'interior_features','kitchen','laundry','property_features','neighbourhood_features',
		'siding_type','floor_type','roof_type'
	);

	public function get_features($type='multiple'){
		return $this->_features[$type];
	}

	public function load_by_external_id($id) {
		$this->load(\DB::select_array()->where('external_id', '=', $id));
		return $this;
	}

	public function load_by_listing_id($id) {
		$this->load(\DB::select_array()->where('listing_id', '=', $id));
		return $this;
	}

	public function save($validation=NULL){
		$this->z_p_code = strtoupper(str_replace(' ', '', $this->z_p_code));
		$this->sq_footage = preg_replace("/[^0-9.]*/", "", $this->sq_footage);
		$this->lot_size = preg_replace("/[^0-9.]*/", "", $this->lot_size);
		$this->date_expire = date_create('+' . \Sysconfig::get('listing_length') . ' days')->format(DATETIME_FORMAT_DB);
		return parent::save($validation);
	}

	public function update_client_has($post){
		$validation = \Validation::forge($this->_class);
		$this->set_fields($post);
		if($post['c_address'] != ''){
			list($this->city,$this->prov_state,$this->country) = \CityState::get_area_id($post['c_address'], true);
		}
		foreach($this->_valrules as $field => $rules) {
			$validation->add_field($field, __($rules[0]), $rules[1]);
		}
		if($validation->run($post) === false){
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
		$location = \CityState::get_lat_lng($this->address, $this->city,$this->prov_state,$this->country);
		$this->lat = $location['lat'];
		$this->lng = $location['lng'];
		if(!isset($post['neighbourhoods'])){
			$this->neighbourhood = 'none';
		}
		$this->interior_features = join('', $post['interior_features']);
		$this->kitchen = join('', $post['kitchen']);
		$this->laundry = join('', $post['laundry']);
		$this->property_features = join('', $post['property_features']);
		$this->neighbourhood_features = join('', $post['neighbourhood_features']);
		$this->siding_type = join('', $post['siding_type']);
		$this->floor_type = join('', $post['floor_type']);
		$this->roof_type = join('', $post['roof_type']);
		$this->date_expire = date_create('+' . \Sysconfig::get('listing_length') . ' days')->format(DATETIME_FORMAT);
		$this->completed_section = self::STATUS_COMPLETE;
		$this->lastupdated = $this->lastupdate = date_create()->format(DATETIME_FORMAT_DB);
		$save = $this->save();
		$response = array('status' => $save, 'message' => '', 'completed_section' => $this->completed_section);
		return $response;
	}

	/**
	* Get a count of each listing type for the profile page
	*
	* @param int $id - the realtor id
	*/
	public function get_profile_count($id) {
		$data = array('total' => 0, 'exclusive' => 0, 'active' => 0, 'finished' => 0);
		$result = \DB::select()->from($this->_table_name)->where('realtor_id', '=', $id)->execute();
		foreach($result as $row) {
			if ($row['imported'] == \Model\Listing::NON_IMPORTED) {
				$data['exclusive']++;
			}
			if ($row['listing_status'] != \Model\Listing::LS_HIDDEN_ADMIN) {
				$data['total']++;
			}
			if ($row['listing_status'] == \Model\Listing::LS_ACTIVE) {
				if ($row['list_as_sold'] == 0) {
					$data['active']++;
				} else {
					$data['finished']++;
				}
			}
		}
		return $data;

	}
	public function join(\Database_Query_Builder_Select $query = NULL){
		$q = $query->execute();
		return $q->as_array();
	}

	public function p_title($v=null) {
		$data = array(
			'' => __('title_stitle'),
			'a' => __('title_lease'),
			'b' => __('title_coop'),
			'c' => __('title_fee'),
			'd' => __('title_freehold')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function prop_style($v=null){
		$data = array(
			'' => __('prop_style'),
			'a' => __('listing_propopt_aptcondo'),
			'i' => __('listing_propopt_townhouse'),
			'f' => __('listing_propopt_onestoratt'),
			'g' => __('listing_propopt_splitlvlatt'),
			'c' => __('listing_propopt_multistoratt'),
			'j' => __('listing_propopt_onestordet'),
			'h' => __('listing_propopt_splitlvldet'),
			'd' => __('listing_propopt_multistordet'),
			'e' => __('listing_propopt_multiunit'),
			'b' => __('listing_propopt_mfg'),
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function prop_style_full($v=null) {
		$data = array(
			'' => __('prop_style'),
			'a' => __('prop_style_a'),
			'b' => __('prop_style_b'),
			'c' => __('prop_style_c'),
			'd' => __('prop_style_d'),
			'e' => __('prop_style_e'),
			'f' => __('prop_style_f'),
			'g' => __('prop_style_g'),
			'h' => __('prop_style_h'),
			'i' => __('prop_style_i'),
			'j' => __('prop_style_j'),
			'k' => __('prop_style_k'),
			'l' => __('prop_style_l'),
			'm' => __('prop_style_m'),
			'n' => __('prop_style_n'),
			'o' => __('prop_style_o'),
			'p' => __('prop_style_p'),
			'q' => __('prop_style_q'),
			'r' => __('prop_style_r'),
			's' => __('prop_style_s'),
			't' => __('prop_style_t'),
			'u' => __('prop_style_u'),
			'v' => __('prop_style_v')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function finished_basement($v=null) {
		$data = array(
			'' => __('sbasement'),
			'a' => __('base_n'),
			'b' => __('base_unfin'),
			'c' => __('base_pfinish'),
			'd' => __('base_finish_base')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function garage_type($v=null) {
		$data = array(
			'' => __('sgarage'),
			'a' => __('garage_ng'),
			'b' => __('garage_sbg'),
			'c' => __('garage_dbg'),
			'd' => __('garage_tbg'),
			'e' => __('garage_dp'),
			'f' => __('garage_sp'),
			'g' => __('garage_up'),
			'h' => __('garage_cp'),
			'i' => __('other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function view($v=null) {
		$data = array(
			'' => __('sview'),
			'a' => __('none'),
			'b' => __('view_city'),
			'c' => __('view_mountain'),
			'd' => __('view_ocean'),
			'e' => __('view_lake'),
			'f' => __('view_river'),
			'g' => __('view_golf'),
			'h' => __('view_harbour')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function heat_source($v=null) {
		$data = array(
			'' => __('sheat_source'),
			'a' => __('heats_ngas'),
			'b' => __('heats_electric'),
			'c' => __('heats_propane'),
			'd' => __('heats_oil'),
			'e' => __('heats_wood'),
			'f' => __('heats_solar'),
			'g' => __('heats_coal'),
			'h' => __('heats_water'),
			'i' => __('other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function heating($v=null) {
		$data = array(
			'' => __('sheat_type'),
			'a' => __('heat_cheating'),
			'b' => __('heat_bheating'),
			'c' => __('heat_geo'),
			'd' => __('heat_floor'),
			'e' => __('other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function cooling($v=null) {
		$data = array(
			'' => __('scool_type'),
			'a' => __('none'),
			'b' => __('cool_ac'),
			'c' => __('heat_geo'),
			'd' => __('other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function water($v=null) {
		$data = array(
			'' => __('swater_type'),
			'a' => __('water_type_cwater'),
			'b' => __('water_type_well'),
			'c' => __('water_type_cystern'),
			'd' => __('other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function sewer($v=null) {
		$data = array(
			'' => __('ssewer_type'),
			'a' => __('sewer_type_city'),
			'b' => __('sewer_type_septic'),
			'c' => __('sewer_type_cystern'),
			'd' => __('other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function floor_type($v=null) {
		$data = array(
			'' => 'Select Flooring',
			'a' => __('floor_carpet'),
			'b' => __('floor_concrete'),
			'c' => __('floor_hardwood'),
			'd' => __('floor_laminate'),
			'e' => __('floor_lino'),
			'f' => __('floor_slate'),
			'g' => __('floor_tile'),
			'h' => __('floor_stone'),
			'i' => __('floor_mixed'),
			'j' => __('other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function roof_type($v=null) {
		$data = array(
			'' => __('sroof_type'),
			'a' => __('roof_ashphalt'),
			'b' => __('roof_buildup'),
			'c' => __('roof_compo'),
			'd' => __('roof_metal'),
			'e' => __('roof_shake'),
			'f' => __('roof_slate'),
			'g' => __('roof_tile'),
			'h' => __('other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function year_built($v = null) {
		$years = range(date('Y'), 1890, -1);
		$data = array(
			'' => __('syear_built'),
			'a' => __('new'),
			'b' => __('older')
		) + array_combine($years, $years);
		if (($v !== null) && isset($data[$v])){
			return $v;
		}
		return $data;
	}

	/**
	* multiple contained values
	*/
	public function condo_features(){
		return array();
	}

	public function interior_features($val) {
		$txt = array(
			0 => __('ifeature_fire'),
			1 => __('ifeature_fans'),
			2 => __('ifeature_attic'),
			3 => __('ifeature_intercom'),
			4 => __('ifeature_tub'),
			5 => __('ifeature_ssystem'),
			6 => __('ifeature_sky'),
			7 => __('ifeature_vceiling'),
			8 => __('other'),
		);
		if(is_array($val)){
			$array_val = array();
			foreach($val as $x => $item){
				if($item == 1){
					$array_val[] = $txt[$x];
				}
			}
			return $array_val;
		}
		return array_key_exists($val, $txt) ? $txt[$val] : '';
	}

	public function kitchen($val){
		$txt = array(
			0 => __('kfeature_fridge'),
			1 => __('kfeature_stove'),
			2 => __('kfeature_built'),
			3 => __('kfeature_microwave'),
			4 => __('kfeature_dish'),
			5 => __('kfeature_garb'),
			6 => __('kfeature_trash'),
			7 => __('other')
		);
		if(is_array($val)){
			$array_val = array();
			foreach($val as $x => $item){
				if($item == 1){
					$array_val[] = $txt[$x];
				}
			}
			return $array_val;
		}
		return array_key_exists($val, $txt) ? $txt[$val] : '';
	}

	public function laundry($val){
		$txt = array(
			0 => __('lfeatures_washer'),
			1 => __('lfeatures_dryer'),
			2 => __('lfeatures_combo'),
			3 => __('lfeatures_sink'),
			4 => __('other')
		);
		if(is_array($val)){
			$array_val = array();
			foreach($val as $x => $item){
				if($item == 1){
					$array_val[] = $txt[$x];
				}
			}
			return $array_val;
		}
		return array_key_exists($val, $txt) ? $txt[$val] : '';
	}

	public function property_features($val){
		$txt = array(
			0 => __('pfeature_pool'),
			1 => __('pfeature_acreage'),
			2 => __('pfeature_shop'),
			3 => __('pfeature_deck'),
			4 => __('pfeature_dock'),
			5 => __('pfeature_green'),
			6 => __('pfeature_spa'),
			7 => __('pfeature_sauna'),
			8 => __('pfeature_pond'),
			9 => __('pfeature_sprinkler'),
			10 => __('pfeature_water'),
			11 => __('pfeature_patio'),
			12 => __('ofeature')
		);
		if(is_array($val)){
			$array_val = array();
			foreach($val as $x => $item){
				if($item == 1){
					$array_val[] = $txt[$x];
				}
			}
			return $array_val;
		}
		return array_key_exists($val, $txt) ? $txt[$val] : '';
	}

	public function neighbourhood_features($val){
		$txt = array(
			0 => __('nfeature_compool'),
			1 => __('nfeature_gate'),
			2 => __('nfeature_golf'),
			3 => __('nfeature_club'),
			4 => __('nfeature_water'),
			5 => __('ofeature')
		);
		if(is_array($val)){
			$array_val = array();
			foreach($val as $x => $item){
				if($item == 1){
					$array_val[] = $txt[$x];
				}
			}
			return $array_val;
		}
		return array_key_exists($val, $txt) ? $txt[$val] : '';
	}

	public function featured_prop_style($val){
		$data = array(
			'a' => __('property_sfh'),
			'b' => __('property_du'),
			'c' => __('property_tri'),
			'd' => __('property_th'),
			'e' => __('property_ap'),
			'f' => __('property_con'),
			'g' => __('property_moh'),
			'h' => __('property_fl'),
			'i' => __('property_mh'),
			'j' => __('property_four'),
		);
		return $data[$val];
	}

	public function featured_prop_type($val){
		$data = array(
			'a' => __('rancher_home'),
			'b' => __('bungalow_home'),
			'c' => __('split_home'),
			'd' => __('bi_home'),
			'e' => __('multi_home'),
			'f' => __('low_home'),
			'g' => __('hi_home'),
			'h' => __('2_home'),
			'i' => __('3_home'),
			'j' => __('other'),
		);
		return $data[$val];
	}

	public function get_featured(){
		$data = array();
		$featured = \DB::select_array()->where('listing_status', '=', \Model\Listing::LS_ACTIVE)
		->from($this->_table_name)
		->where('type_of_listing', '<>', \Model\Listing::TYPE_BUY_ONLY)
		->where('featured_listing', '=', \Model\Listing::AS_FEATURED)->execute()->as_array();
		foreach($featured as $row) {
			$listing['id'] = $row['id'];
			if ($row['imported'] == \Model\Listing::IMPORTED) {
				$listing['picture'] = \Model\Crea\Property::forge()->get_image($row['external_id'], 1, false);
				$listing['target'] = '2_'.$row['external_id'];
			} else {
				$listing['picture'] = 'listings/' . \Model\Photos\Xtrade::forge()->get_first_photo($row['listing_id']);
				$listing['target'] = '0_'.$row['listing_id'];
			}
			$data[] = $listing;
		}
		return $data;
	}

	/**
	* Load matches listing only from a realtor
	*
	* @param String $realtor_id Realtor ID
	* @return array Sets of matched listing
	*/
	public function load_matches_listing($realtor_id){
		$data = $tmp = array();
		$listings = $this->load(\DB::select_array()->where('realtor_id', '=', $realtor_id), NULL)->as_array();
		foreach($listings as $row){
			$tmp['listing_id'] = $row['listing_id'];
			$tmp['realtor_id'] = $row['realtor_id'];
			if($row['address'] == ''){
				if($row['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
					$tmp['location'] = __('addr_buyer_only');
				} else {
					$tmp['location'] = __('addr_notlisted');
				}
			} else {
				$tmp['location'] = $row['address'].', '.\CityState::get_area_name($row['city'], $row['prov_state'], $row['country']);
			}
			$total = count(\Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $row['listing_id']), NULL)->as_array());
			$num_total = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $row['listing_id'])
				->where(\DB::expr('ignore_listings IS NULL'))
				->where(\DB::expr('save_listings IS NULL')), NULL)->as_array();
			$saved_listings = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $row['listing_id'])
				->where(\DB::expr('save_listings IS NOT NULL')), NULL)->as_array();
			$num_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $row['listing_id'])
				->where_open()
				->where(\DB::expr('ignore_listings IS NOT NULL'))
				->or_where(\DB::expr('save_listings IS NOT NULL'))
				->where_close(), NULL)->as_array();
			$tmp['total_matches'] = count($num_total);
			$tmp['saved_matches'] = count($saved_listings);
			$tmp['ignore_matches'] = count($num_ignore);
			if($total > 0){
				$data[] = $tmp;
			}
		}
		return $data;
	}

	public function get_listing_geolocation($listing_id){
		$this->load(\DB::select_array()->where('listing_id', '=', $listing_id));
		$lat_long = \CityState::get_lat_lng($this->address, $this->city, $this->prov_state, $this->country);
		if($lat_long){
			$this->set_fields(array('lat' => $lat_long['lat'], 'lng' => $lat_long['lng']));
			$this->save();
		}
		return $lat_long;
	}

	public function get_features_list($data){
		$features = array();
		foreach ($data as $k => $v) {
			if (array_key_exists($k, $this->_feature) && !empty($v)) {
				if (!is_array($v)) {
					$v = explode(',', $v);
				}
				foreach ($v as $item) {
					$vals = $this->$k(str_split($item));
					if (empty($vals)) {
						continue;
					}
					$features[$this->_feature[$k]] = '<ul>';
					foreach($vals as $i) {
						$features[$this->_feature[$k]] .= '<li>'.$i.'</li>';
					}
					$features[$this->_feature[$k]] .= '</ul>';
				}
			}
		}
		return $features;
	}

	public function get_tooltip_content(){
		$data = array();
		if ($this->loaded()) {
			$data['location'] = \CityState::get_area_name($this->city, $this->prov_state, $this->country);
			$data['sqft'] = is_numeric($this->sq_footage) ? number_format($this->sq_footage,0) . ' ' . __('sq_ft') : $this->sq_footage;
			$data['style'] = $this->prop_type($this->prop_type) .' - '. $this->prop_style($this->prop_style);
			$data['bedrooms'] = $this->bedrooms . ' ' . \Inflector::pluralize(__('bedroom'), $this->bedrooms);
			$data['bathrooms'] = $this->bathrooms . ' ' . \Inflector::pluralize(__('bathroom'), $this->bathrooms);
			if($this->imported == \Model\Listing::IMPORTED){
				$picture = \Model\Crea\Property::forge()->get_image($this->external_id, 1, true);
			} else {
				$picture = 'listings/thumbs/' . \Model\Photos\Xtrade::forge()->get_first_photo($this->listing_id);
			}
			$data['picture'] = $picture;
		}
		return $data;
	}

	public function admin_listing_status(){
		$data = array(
			\Model\Listing::LS_REMOVED_EXACTIVE => \Model\Listing::forge()->get_status(\Model\Listing::LS_REMOVED_EXACTIVE),
			\Model\Listing::LS_ACTIVE => \Model\Listing::forge()->get_status(\Model\Listing::LS_ACTIVE),
			\Model\Listing::LS_HIDDEN_PUBLIC => \Model\Listing::forge()->get_status(\Model\Listing::LS_HIDDEN_PUBLIC),
			\Model\Listing::LS_DELETED => \Model\Listing::forge()->get_status(\Model\Listing::LS_DELETED)
		);
		return $data;
	}

}