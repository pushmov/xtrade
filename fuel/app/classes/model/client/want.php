<?php
namespace Model\Client;

class Want extends \Model\Client {

    const ID_PREFIX = 'xTH';

	protected $_class = 'want';
    protected $_table_name = 'client_wants';

    protected $_data = array(
        'id' => NULL,'listing_id' => '','realtor_id' => '','country' => '','prov_state' => '','city' => '',
        'county' => '','prop_type' => '','bedrooms' => '','bathrooms' => '','laundry' => '','kitchen' => '',
        'finished_basement' => '','year_built' => '','prop_style' => '','sq_footage' => '','heating' => '','cooling' => '',
        'water' => '','sewer' => '','property_features' => '','interior_features' => '','neighbourhood_features' => '000000','mobile_features' => '0',
        'condo_features' => '0000000000','strata_features' => '0000000000','view' => '','waterfront' => '','neighbourhood' => '','school_info' => '',
        'heat_source' => '','roof_type' => 9,'floor_type' => 9,'siding_type' => '','total_acreage' => '9999999','total_lot_size' => '9999999',
        'garage_type' => '9','p_title' => NULL,'listing_price' => '','buyers_agent_listing' => 0,'completed_section' => 0,
        'listing_price_low' => 0,'listing_price_high' => 0,'sq_footage_low' => 0,'sq_footage_high' => 0,'surrounding_area' => 0, 'updated' => ''
    );

    protected $_valrules = array(
        'address' => array('city_state_country', 'required'),
        'listing_price_low' => array('min_price', 'required'),
        'listing_price_high' => array('max_price', 'required'),
        'sq_footage' => array('sq_ftg', 'required'),
        'prop_type' => array('ptype', 'required'),
        'bedrooms' => array('bed', 'required'),
        'bathrooms' => array('bath', 'required'),
        'p_title' => array('title_title', 'required'),
        'prop_style' => array('listing_propopt', 'required'),
    );

	protected $_features = array(
		'multiple' => array( 'prop_style', 'siding_type', 'garage_type', 'floor_type', 'roof_type' ),
		'single' => array( 'listing_price_low', 'listing_price_high', 'sq_footage', 'prop_type', 'bedrooms', 'bathrooms', 'p_title',
			'total_lot_size', 'total_acreage', 'finished_basement', 'view', 'heat_source', 'heating',
			'cooling', 'water', 'sewer')
	);
	// These fields could have multiple values
	protected $_multi_values = array('siding_type', 'garage_type','floor_type','roof_type','prop_style');

	protected $_incomplete_values = array();

    public function save($validation=null){
		$sq_f = explode('|', $this->sq_footage);
        $this->sq_footage_low = count($sq_f) > 1 ? $sq_f[0] : 0;
        // This is always the max
        $this->sq_footage_high = 9999999;
        $this->listing_price = $this->listing_price_low . ' AND ' . $this->listing_price_high;

        return parent::save($validation);
    }

    public function update_client_wants($post){
		$neighbourhoods = isset($post['neighbourhood']) ? $post['neighbourhood'] : array();
		unset($post['neighbourhood']);
		$this->set_fields($post);
		$validation = \Validation::forge($this->_class);
		$response['status'] = false;
        list($this->city,$this->prov_state,$this->country) = \CityState::get_area_id($post['address'], true);
        foreach($this->_valrules as $field => $rules) {
            $validation->add_field($field, __($rules[0]), $rules[1]);
        }
		$this->siding_type = count($post['siding_type']) > 1 ? join('', $post['siding_type']) : $post['siding_type'][0];
		$this->garage_type = count($post['garage_type']) > 1 ? join('', $post['garage_type']) : $post['garage_type'][0];
		$this->floor_type = count($post['floor_type']) > 1 ? join('', $post['floor_type']) : $post['floor_type'][0];
		$this->roof_type = count($post['roof_type']) > 1 ? join('', $post['roof_type']) : $post['roof_type'][0];
		$this->prop_style = count($post['prop_style']) > 1 ? join('', $post['prop_style']) : $post['prop_style'][0];
		$this->completed_section = self::STATUS_COMPLETE;
		$this->updated = date_create()->format(DATETIME_FORMAT_DB);
		if($this->save()){
			$response = array('status' => true, 'completed_section' => $this->completed_section);
			$client_has = \Model\Client\Has::forge()->load_by_listing_id($this->listing_id);
			$client_has->date_expire = date_create('+' . \Sysconfig::get('listing_length') . ' days')->format(DATETIME_FORMAT_DB);
			$client_has->expiry_notified = 0;
			$client_has->lastupdate = date_create()->format(DATETIME_FORMAT_DB);
			$client_has->save();
			\Model\Neighbourhoods::forge()->set_wants_neighbourhood($neighbourhoods, $post['listing_id']);
		}
		// Validate after saving the current settings
		if($validation->run($post) === false){
			// Section is not complete
			$this->completed_section = self::STATUS_INCOMPLETE;
			$this->save();
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}

		return $response;
    }

	public function listing_price_low() {

		$amounts = array(
			__('wants_sprice'),

			'0', '25000', '50000', '75000', '100000', '125000', '150000', '175000', '200000', '225000', '250000',

			'275000', '300000', '325000', '350000', '375000', '400000', '425000', '450000', '475000', '500000',

			'550000', '600000', '650000', '700000', '750000', '800000', '850000', '900000', '950000', '1000000',

			'1250000','1500000','1750000','2000000','2250000','2500000','2750000', '3000000', '3250000', '3500000',

			'3750000','4000000','4250000','4500000','4750000','5000000',

			'5500000','6000000','6500000', '7000000','7500000','8000000','8500000','9500000','9000000','10000000',

			'12000000','15000000','20000000',

			'25000000','30000000','35000000','40000000','45000000','50000000','55000000','60000000','65000000','70000000',

			'75000000','80000000','85000000','90000000','95000000','100000000'
		);

		$return = array();
		foreach($amounts as $k => $amount) {
			if($k > 0) {
				$return[$amount] = '$' . number_format($amount);
			} else {
				$return[0] = $amount;
			}
		}
		return $return;
	}

	public function listing_price_high() {
		$amounts = array(
			'4000000000' => 'Unlimited',
			'25000', '50000', '75000', '100000', '125000', '150000', '175000', '200000', '225000',
			'250000', '275000', '300000', '325000', '350000', '375000', '400000', '425000', '450000', '475000',
			'500000', '550000', '600000', '650000', '700000', '750000', '800000', '850000', '900000', '950000',
			'1000000','1250000','1500000','1750000','2000000','2250000','2500000','2750000', '3000000', '3250000',
			'3500000','3750000','4000000','4250000','4500000','4750000','5000000','5500000','6000000','6500000',
			'7000000','7500000','8000000','8500000','9500000','9000000','10000000','12000000','15000000','20000000',
			'25000000','30000000','35000000','40000000','45000000','50000000','55000000','60000000','65000000','70000000',
			'75000000','80000000','85000000','90000000','95000000','100000000'
		);

		$return = array();
		foreach($amounts as $k => $amount)
		{
			if($k != '4000000000') {
				$return[$amount] = '$' . number_format($amount);
			} else {
				$return[$k] = $amount;
			}
		}

		return $return;
	}

	public function sq_footage($val=NULL) {
		$key = __('sqf');
		$return = array(
			'' => __('wants_ssqft'),
			'1|9999999' => 'Any ' . $key,
			'600|1200' => '600+ ' . $key,
			'1200|2400' => '1200+ ' . $key,
			'2400|3600' => '2400+ ' . $key,
			'3600|4800' => '3600+ ' . $key,
			'4800|5600' => '4800+ ' . $key,
			'5600|7400' => '5600+ ' . $key,
			'7400|10000' => '7400+ ' . $key,
			'10000|12000' => '10,000+ ' . $key,
			'12000|14000' => '12,000+ ' . $key,
			'14000|16000' => '14,000+ ' . $key,
			'16000|18000' => '16,000+ ' . $key,
			'18000|20000' => '18,000+ ' . $key
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function bedrooms($val=NULL) {
		$key = __('beds');
		$return = array(
			'' => __('select_amounts'),
			'1' => '1+ ' . $key,
			'2' => '2+ ' . $key,
			'3' => '3+ ' . $key,
			'4' => '4+ ' . $key,
			'5' => '5+ ' . $key,
			'6' => '6+ ' . $key,
			'7' => '7+ ' . $key
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function bathrooms($val=NULL) {
		$key = __('baths');
		$return = array(
			'' => __('select_amounts'),
			'1' => '1+ ' . $key,
			'2' => '2+ ' . $key,
			'3' => '3+ ' . $key,
			'4' => '4+ ' . $key,
			'5' => '5+ ' . $key,
			'6' => '6+ ' . $key,
			'7' => '7+ ' . $key
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function p_title($val=NULL) {
		$return = array(
			'' => __('title_stitle'),
			'a' => __('title_lease'),
			'b' => __('title_coop'),
			'c' => __('title_fee'),
			'd' => __('title_freehold')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function prop_style($val=null){
		$return = array(
			'9' => __('prop_style_any'),
			'a' => __('listing_propopt_aptcondo'),
			'i' => __('listing_propopt_townhouse'),
			'f' => __('listing_propopt_onestoratt'),
			'g' => __('listing_propopt_splitlvlatt'),
			'c' => __('listing_propopt_multistoratt'),
			'j' => __('listing_propopt_onestordet'),
			'h' => __('listing_propopt_splitlvldet'),
			'd' => __('listing_propopt_multistordet'),
			'e' => __('listing_propopt_multiunit'),
			'b' => __('listing_propopt_mfg'),
		);

		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function prop_style_full($val=NULL) {
		$return = array(
			9 => __('prop_style_any'),
			'a' => __('prop_style_a'),
			'b' => __('prop_style_b'),
			'c' => __('prop_style_c'),
			'd' => __('prop_style_d'),
			'e' => __('prop_style_e'),
			'f' => __('prop_style_f'),
			'g' => __('prop_style_g'),
			'h' => __('prop_style_h'),
			'i' => __('prop_style_i'),
			'j' => __('prop_style_j'),
			'k' => __('prop_style_k'),
			'l' => __('prop_style_l'),
			'm' => __('prop_style_m'),
			'n' => __('prop_style_n'),
			'o' => __('prop_style_o'),
			'p' => __('prop_style_p'),
			'q' => __('prop_style_q'),
			'r' => __('prop_style_r'),
			's' => __('prop_style_s'),
			't' => __('prop_style_t'),
			'u' => __('prop_style_u'),
			'v' => __('prop_style_v')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function total_lot_size($val=NULL) {
		$key = __('sqf');
		$return = array(
			'1 AND 6000' => '0-6000 ' . $key,
			'6000 AND 10000' => '6000-10,000 ' . $key,
			'10000 AND 2000000' => '10,000+ ' . $key,
			'9999999' => __('any')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function total_acreage($val=NULL) {
		$return = array(
			'1 AND 2' => '1-2',
			'2 AND 6' => '2-6',
			'6 AND 12' => '6-12',
			'12 AND 30' => '12-30',
			'30 AND 60' => '30-60',
			'60 AND 20000' => '60+',
			'9999999' => __('any')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function siding_type($v=null){
		$return = array(
			'9' => __('any'),
			'a' => __('siding_brick'),
			'b' => __('siding_cement'),
			'c' => __('siding_compo'),
			'd' => __('siding_metal'),
			'e' => __('siding_shingle'),
			'f' => __('siding_stone'),
			'g' => __('siding_stucco'),
			'h' => __('siding_vinyl'),
			'i' => __('siding_wood'),
			'j' => __('siding_mixed'),
			'k' => __('other'),
			'l' => __('floor_concrete')
		);
		if(!isset($v)){
			return $return;
		}
		return isset($v) && array_key_exists($v, $return) ? $return[$v] : '';
	}

	public function finished_basement($val=NULL) {
		$return = array(
			'9' => __('any'),
			'a' => __('base_n'),
			'b' => __('base_unfin'),
			'c' => __('base_pfinish'),
			'd' => __('base_finish_base')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function garage_type($val=NULL) {
		$return = array(
			'9' => __('any'),
			'a' => __('garage_ng'),
			'b' => __('garage_sbg'),
			'c' => __('garage_dbg'),
			'd' => __('garage_tbg'),
			'e' => __('garage_dp'),
			'f' => __('garage_sp'),
			'g' => __('garage_up'),
			'h' => __('garage_cp'),
			'i' => __('other')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function view(){
		return array(
			'9' => __('any'),
			'a' => __('none'),
			'b' => __('view_city'),
			'c' => __('view_mountain'),
			'd' => __('view_ocean'),
			'e' => __('view_lake'),
			'f' => __('view_river'),
			'g' => __('view_golf'),
			'h' => __('view_harbour')
		);
	}

	public function heat_source() {
		return array(
			'9' => __('any'),
			'a' => __('heats_ngas'),
			'b' => __('heats_electric'),
			'c' => __('heats_propane'),
			'd' => __('heats_oil'),
			'e' => __('heats_wood'),
			'f' => __('heats_solar'),
			'g' => __('heats_coal'),
			'h' => __('heats_water')
		);
	}

	public function heating($val=NULL) {
		$return = array(
			'9' => __('any'),
			'a' => __('heat_cheating'),
			'b' => __('heat_bheating'),
			'c' => __('heat_geo'),
			'd' => __('heat_floor'),
			'e' => __('other')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function cooling($val=NULL) {
		$return = array(
			'9' => __('any'),
			'a' => __('none'),
			'b' => __('cool_ac'),
			'c' => __('heat_geo'),
			'd' => __('other')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}
	public function water($val=NULL) {
		$return = array(
			'9' => __('any'),
			'a' => __('water_type_cwater'),
			'b' => __('water_type_well'),
			'c' => __('water_type_cystern'),
			'd' => __('other')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function sewer($val=NULL) {
		$return = array(
			'9' => __('any'),
			'a' => __('sewer_type_city'),
			'b' => __('sewer_type_septic'),
			'c' => __('sewer_type_cystern'),
			'd' => __('other')
		);
		if(!isset($val)){
			return $return;
		}
		return isset($val) && array_key_exists($val, $return) ? $return[$val] : '';
	}

	public function floor_type() {
		return array(
			'9' => __('any'),
			'a' => __('floor_carpet'),
			'b' => __('floor_concrete'),
			'c' => __('floor_hardwood'),
			'd' => __('floor_laminate'),
			'e' => __('floor_lino'),
			'f' => __('floor_slate'),
			'g' => __('floor_tile'),
			'h' => __('floor_stone'),
			'i' => __('floor_mixed'),
			'j' => __('other')
		);
	}

	public function roof_type() {
		return array(
			'9' => __('any'),
			'a' => __('roof_ashphalt'),
			'b' => __('roof_buildup'),
			'c' => __('roof_compo'),
			'd' => __('roof_metal'),
			'e' => __('roof_shake'),
			'f' => __('roof_slate'),
			'g' => __('roof_tile'),
			'h' => __('other')
		);
	}

	public function update_neighbourhoods($data){
		$neighbourhoods = isset($data['value']) ? $data['value'] : array();
		return \Model\Neighbourhoods::forge()->set_wants_neighbourhood($neighbourhoods, $data['listing_id']);
	}

}