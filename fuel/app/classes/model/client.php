<?php

namespace Model;

class Client extends \Automodeler {
	const FEATURES_CHECKED_VALUE = 1;
	const FEATURES_UNCHECKED_VALUE = 0;
	const YEAR_BUILT_NEW = 1;
	const YEAR_BUILT_OLDER = 2;

	const STATUS_INCOMPLETE = 0;
	const STATUS_COMPLETE = 1;

	const CC_INFO_INCOMPLETE = 1;
	const CC_HAS_INCOMPLETE = 2;
	const CC_WANTS_INCOMPLETE = 3;
	const CC_PICTURES_INCOMPLETE = 5;
	const CC_SUCCESS_IMPORTED = 6;
	const CC_SUCCESS = 4;

	protected $_data =	array('id' => NULL);
	protected $_multi_values = array();

	public function load_by_listing_id($id) {
		$this->load(\DB::select_array()->where('listing_id', '=', $id));
		return $this;
	}

	public function get_completion_code_txt($id=null){
		$txt = array(
			self::CC_INFO_INCOMPLETE => __('info_incomplete'),
			self::CC_HAS_INCOMPLETE => __('has_incomplete'),
			self::CC_WANTS_INCOMPLETE => __('wants_incomplete'),
			self::CC_SUCCESS => __('listing_completed'),
			self::CC_PICTURES_INCOMPLETE => __('pictures_incomplete'),
			self::CC_SUCCESS_IMPORTED => __('listing_completed')
		);
		if($id === null){
			return $txt;
		}
		return $txt[$id];
	}

	public function year_built_txt($val=null){
		$txt = array(
			0 => 0,
			self::YEAR_BUILT_NEW => __('new'),
			self::YEAR_BUILT_OLDER => __('older')
		);
		if (!isset($txt[$val])){
			return $val;
		}
		return $txt[$val];
	}

	public function prop_type($v=null){
		$data = array(
			'' => __('storey'),
			'a' => __('storey_residential'),
			'b' => __('storey_multifamily'),
			'c' => __('storey_condominium'),
			'd' => __('storey_farm'),
			'e' => __('storey_recreational'),
			'f' => __('storey_other')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
		return '';
	}

	public function siding_type($v=null) {
		$data = array(
			'' => __('ssiding_type'),
			'a' => __('siding_brick'),
			'b' => __('siding_cement'),
			'c' => __('siding_compo'),
			'd' => __('siding_metal'),
			'e' => __('siding_shingle'),
			'f' => __('siding_stone'),
			'g' => __('siding_stucco'),
			'h' => __('siding_vinyl'),
			'i' => __('siding_wood'),
			'j' => __('siding_mixed'),
			'k' => __('other'),
			'l' => __('floor_concrete')
		);
		if ($v === null) {
			return $data;
		} elseif(isset($data[$v])){
			return $data[$v];
		}
	}

	public function multiple_txt_values($values, $function){
		$vals = array();
		foreach(str_split($values) as $idx => $v){
			if($v == self::FEATURES_CHECKED_VALUE){
				$vals[] = $this->$function($idx);
			}
		}
		return join(',', $vals);
	}

	public function update_field($data){
		$this->load_by_listing_id($data['listing_id']);
		$field = $data['field'];
		if(($data['field'] == 'c_address' && $data['area'] == 'has') || $data['field'] == 'address' && $data['area'] == 'want' ){
			list($city, $prov_state, $country) = \CityState::get_area_id($data['value'], true);
			$this->city = $city;
			$this->prov_state = $prov_state;
			$this->country = $country;
		} elseif($data['area'] == 'has' && in_array($data['field'], array_flip($this->_feature))) {
			//checkbox input features :
			$fn_name = $data['fn_name'];
			$arr_feature_default_val = str_split($this->$fn_name);
			foreach($arr_feature_default_val as $idx => $value){
				if($idx == $data['index']){
					$arr_feature_default_val[$idx] = $data['value'];
				}
			}
			$this->$field = join('', $arr_feature_default_val);
		} elseif (in_array($data['field'], $this->_multi_values) && is_array($data['value'])) {
			$this->$field = join('', $data['value']);
		} else {
			$this->$field = $data['value'];
		}
		$response = array(
			'status' => 'error'
		);
		if($this->save()){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function incomplete_section_check($data){
		$status_completion = self::STATUS_COMPLETE;
		$this->load_by_listing_id($data['listing_id']);
		foreach($this->_incomplete_values as $field => $incomplete_val){
			if($this->$field == $incomplete_val){
				$status_completion = self::STATUS_INCOMPLETE;
				break;
			}
		}
		$this->completed_section = $status_completion;
		$this->save();
	}

	public function get_valrules(){
		return $this->_valrules;
	}

}