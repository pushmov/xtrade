<?php
namespace Model\Log;

class Email extends \Model\Log {
	
	protected $_table_name = 'email_log';
	protected $_data = array('id' => NULL, 'date' => '', 
		'email_from_name' => '', 'email_from' => '', 'email_to_name' => '', 
		'email_to' => '', 'cc' => '', 'bcc' => '', 'subject' => '', 'homebase_id' => '');
}