<?php
namespace Model\Vendor;

class View extends User {
	protected $_class_name = 'view';
	protected $_table_name = 'directory_views';
	protected $_data = array('id' => NULL, 'vendor_id' => '', 'views' => '', 'category' => '');
}