<?php
namespace Model\Vendor;

class Dlist extends \Automodeler {
    protected $_class_name = 'dlist';
    protected $_table_name = 'directory_list';

	protected $_data = array('id' => NULL, 'name' => '', 'category' => '');

    public function get_id_from_name($name){
        $row = $this->load(\DB::select_array()->where('name', '=', $name))->as_array();
        return $row['id'];
    }
    
    public function select_category(){
		\Lang::load('listing');
        $select = array('all' => __('all_categories'));
        $result = $this->load(\DB::select_array()->order_by('name', 'asc'), NULL)->as_array();
        foreach($result as $row){
            $select[$row['id']] = $row['name'];
        }
        return $select;
    }
    
}