<?php
namespace Model\Vendor;

class Payment extends \Model\Payment {

	/**
	* Process the signup payment
	*
	* @param array $post - data from the payment details form
	* @param array $signup - data from the signup form
	*/
	public function signup($post, $signup) {
		\Lang::load('common');
		$post = array_map('trim', $post);
        list($post['city'], $post['prov_state'], $post['country']) = \CityState::get_area_id($post['cpc_address'], true);
		// If the city is not set then clear the address for validation
		if (empty($post['city'])) {
			$post['cpc_address'] = '';
		}
		$this->cc_validation();
		if($this->_validation->run($post) === false){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}

		$signup['vendor_id'] = \Sysconfig::create_string($post['user_type']);
		$signup['address'] = $post['address'];
		$signup['z_p_code'] = $post['z_p_code'];
		$signup['account_creation'] = date_create()->format(DATETIME_FORMAT_DB);
//        $signup['user_name'] = $signup['email'];
        $signup['activate'] = \Sysconfig::create_string('activate');

		// Set for gateway
		$post = array_merge($post, $signup);
		$post['currency'] = \Config::get('currency.' . $post['country']);
		$signup['locale'] = \Model\Vendor::forge()->advertise_to_areaid($post['locale']);
		$signup['type'] = \Model\Vendor::forge()->services_to_typeid($post['type']);
		// Get the names for gateway
		list($post['city'], $post['prov_state'], $post['country']) = explode(', ', $post['cpc_address']);
		$post['start_date'] = date_create(null, new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z');
		$post['user_xid'] = $signup['vendor_id'];
		$post['trans_id'] = 'vendor_monthly';
		$post['promo_rate'] = 0;
		$post['promo_duration'] = 0;
        $post['promo_fine_print'] = '';
		$post['type_of_services'] = \Model\Vendor::forge()->typeid_to_services(join(';', $post['type']));

		//direct payment / setup fee
		$post['direct']['itemamt'] = $post['fee'];
		$post['direct']['taxamt'] = $post['tax'];
		$post['direct']['amt'] = $post['fee'] + $post['tax'];

		//recurring with promo variables
		$post['payment']['itemamt'] = $post['fee'];
		$post['payment']['trialamt'] = 0.00;
		// Get valid promo details

		$promo = \Model\Promocode::forge()->load_valid($post['promo_code']);
		if ($promo->loaded()) {
			$post['promo_duration'] = $promo->duration;
            $post['promo_fine_print'] = $promo->fine_print;
			if ($promo->rate_type == \Model\Promocode::RT_FLAT) {
				$post['promo_rate'] = $promo->rate;
			} else {
				$post['promo_rate'] = $post['fee'] * $promo->rate;
			}
			$post['payment']['itemamt'] = $post['payment']['trialamt'] = $post['promo_rate'];
		}
		$post['payment']['duration'] = $post['promo_duration'];
		$post['payment']['regular'] = $post['fee'];
		$post['payment']['taxamt'] = $post['tax'];
		$post['payment']['amt'] = $post['payment']['itemamt'] + $post['payment']['taxamt'];

		$vault = new \Model\Paypalrest\Vault();
		$card = $vault->store($post);

		$plan = new \Model\Paypalrest\Plan();
		$post['plan_id'] = $plan->vendor_plan();
		$post['agreement_name'] = 'xTH Monthly Membership';
		$post['agreement_description'] = 'Monthly Membership Payment';
		$agreement = new \Model\Paypalrest\Agreement();
		$paypal_result = $agreement->create($post);

		if ($paypal_result['error']) {
			$message = '<ul>This transaction cannot be processed : '.$paypal_result['error'].'.</ul>';
            $response = array('status' => 'ERROR', 'message' => $message, 'area' => 'direct');
		} else {
			$vendor = new \Model\Vendor;
			$pp_profile = $vault->retrieve($card['id']);
			$signup['ip_address'] = \Input::ip();
			$signup['paypal_sub_id'] = $paypal_result['id'];
			$signup['paypal_status'] = ($pp_profile['state'] == 'ok') ? parent::PP_ACTIVE : parent::PP_SUSPEND;
			if ($post['promo_duration'] != \Model\Promocode::PROMO_AS_EXPIRED) {
				$signup['promotional_end'] = date_create('+' . $post['promo_duration'] . ' months')->format('Y-m-d');
			}
			$signup['account_status'] = \Model\User::AS_PENDING_VERIFY;
			$billing = $agreement->detail($paypal_result['id']);
			$vendor->set_fields($signup);
			$vendor->password = \Authlite::instance('auth_vendor')->hash($vendor->password);
			$vendor->save();

			/* logo process */
			if($signup['data']['logo'] != '' && is_readable(DOCROOT.\Model\Vendor::IMG_PATH.$signup['data']['logo'])){
				$ext = pathinfo (DOCROOT.\Model\Vendor::IMG_PATH.$signup['data']['logo'], PATHINFO_EXTENSION);
				$filename = str_pad($vendor->id, 8, '0', STR_PAD_RIGHT).'.'.$ext;
				rename(DOCROOT. \Model\Vendor::IMG_PATH.$signup['data']['logo'], DOCROOT.\Model\Vendor::IMG_PATH.$filename);
			}
			$date_order = date_create()->format(DATETIME_FORMAT_DB);
			$order = \Model\Order::forge();
			//regular monthly order receipt & welcome email
			$order_fields = array(
				'user_id' => $vendor->id,
				'user_type_id' => \Model\User::UT_VENDOR,
				'homebase_id' => $vendor->vendor_id,
				'subscriber_id' => $vendor->paypal_sub_id,
				'order_date' => $date_order,
				'tax_code' => $post['tax_code'],
				'trans_id' => $paypal_result->plan->id,
				'status_id' => \Model\Order::OS_PENDING,
			);
			$order_fields['type_id'] = \Model\Order::OT_SIGNUP;
			$order_fields['total'] = $post['fee'];
			$order_fields['sub_total'] = $post['fee'] + $post['tax'];
			$order_fields['taxes'] = $post['tax'];
			$order_fields['receipt_id'] = \Sysconfig::create_string('salt');
			$order->set_fields($order_fields);
			$order->save();
			$user = \Model\Vendor::forge()->load(\DB::select_array()->where('id', '=', $vendor->id))->as_array();
			$user['receipt_id'] = $order_fields['receipt_id'];
			$email_data = array_merge($user, $post);
			\Emails::forge()->welcome($email_data, $signup['cpassword'], 'vendor');
				//featured ad vendor notification email & transaction id
			if(isset($post['featured']) && $post['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED){
				$order = \Model\Order::forge();
				$order_fields['type_id'] = \Model\Order::OT_FEATURED;
				$order_fields['total'] = $post['featured_ad'];
				$order_fields['sub_total'] = $post['featured_ad'] + $post['featured_tax'];
				$order_fields['taxes'] = $post['featured_tax'];
				$order_fields['receipt_id'] = \Sysconfig::create_string('salt');
				$order->set_fields($order_fields);
				$order->save();
				$email_data['receipt'] = $order_fields['receipt_id'];
				\Emails::forge()->upgradevendor($email_data);
			}
			$response = array('status' => 'OK', 'Conf_Num' => $card['id']);
		}
        return $response;
	}

	public function upgrade($post){
		\Lang::load('common');
		$response = array();
		list($city, $prov_state, $country) = explode(',', $post['cpc_address']);
		$post['country'] = trim($country);
		$post['start_date'] = date_create(null, new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z');
		$post['currency'] = \Config::get('currency.' . $post['country']);
		$account_monthly = \Sysconfig::get('vendor_monthly_fee');
		$featured_monthly = \Sysconfig::get('vendor_featured_fee');
		$post['new_amount'] = $account_monthly + $featured_monthly;
		$post['new_amount_tax'] = $post['tax'];
		$this->cc_validation();
		if($this->_validation->run($post) === false){
			return array('status' => 'ERROR', 'message' => $this->_validation->show_errors());
		}
		$taxes = \Model\Tax::forge()->load_info($post['cpc_address']);
		if ($taxes->loaded() && ($taxes->rate > 0)) {
			$tax_regular = round($account_monthly * $taxes->rate, 2);
			$tax_featured = round($featured_monthly * $taxes->rate, 2);
			$post['new_amount_tax'] = $tax_regular + $tax_featured;
		}

		$post['item_name'] = 'vTh_Upgrade_Advertisement';
		$post['item_description'] = 'One time payment for featured ad';
		$post['city'] = $city;
		$post['prov_state'] = $prov_state;


		$plan = new \Model\Paypalrest\Plan();
		$post['plan_id'] = $plan->vendor_featured_plan();
		$agreement = new \Model\Paypalrest\Agreement();
		$result = $agreement->create($post);

		if ($result['error']) {
			$message = '<ul>This transaction cannot be processed : '.$result['error'].'.</ul>';
            $response = array('status' => 'ERROR', 'message' => $message, 'area' => 'direct');
		} else {
			$vendor = \Model\Vendor::forge()->load(\DB::select_array()->where('vendor_id', '=', $post['vendor_id']));

			//- update user data on paypal
			$vault = new \Model\Paypalrest\Vault();
			$card = $vault->retrieve($vendor['paypal_sub_id']);
			$vendor->set_fields(array(
				'featured' => \Model\Vendor::VENDOR_STATUS_FEATURED
			));
			$vendor->save();
			$receipt = \Sysconfig::create_string('salt');
			$order = \Model\Order::forge();
			$order_fields = array(
            	'user_id' => $vendor->id,
            	'user_type_id' => \Model\User::UT_VENDOR,
                'type_id' => \Model\Order::OT_UPGRADE,
                'homebase_id' => $vendor->vendor_id,
                'subscriber_id' => $vendor->paypal_sub_id,
                'order_date' => date_create()->format('Y-m-d H:i:s'),
                'total' => $post['fee'],
                'sub_total' => $post['fee'] + $post['tax'],
                'taxes' => $post['tax'],
                'tax_code' => $post['tax_code'],
                'trans_id' => $result['id'],
                'status_id' => \Model\Order::OS_COMPLETE,
                'receipt_id' => $receipt
            );
			$order->set_fields($order_fields);
			$order->save();
			$post['company_name'] = $vendor->company_name;
			$post['receipt'] = $receipt;
			\Emails::forge()->upgradevendor($post);
			$response = array('status' => 'OK', 'Conf_Num' => $card->id);
		}
		return $response;
	}

	public function downgrade($vendor){
		\Lang::load('common');
		$new_amount = \Sysconfig::get('vendor_monthly_fee');
		$taxes = \Model\Tax::forge()->load_info($vendor->location_address);
		if ($taxes->loaded() && ($taxes->rate > 0)) {
			$new_amount_tax = round($new_amount * $taxes->rate, 2);
		}
		list($data['city'], $data['state'], $country) = explode(',',$vendor->location_address);
		$data['vendor_id'] = $vendor->vendor_id;
		$data['paypal_sub_id'] = $vendor->paypal_sub_id;
		$data['email'] = $vendor->email;
		$data['first_name'] = $vendor->first_name;
		$data['last_name'] = $vendor->last_name;
		$data['new_amount'] = $new_amount;
		$data['new_amount_tax'] = $new_amount_tax;
		$data['country'] = trim($country);
		$data['street'] = $vendor->address;
		$data['countrycode'] = \Sysconfig::country_code($country);
		$data['zip'] = $vendor->z_p_code;


		$order = \Model\Order::forge()->load(\DB::select()->where('user_id', '=', $vendor->id)
			->where('user_type_id', '=', \Model\User::UT_VENDOR)
			->where('type_id', '=', \Model\Order::OT_UPGRADE)
			->where('status_id', '=', \Model\Order::OS_COMPLETE)
			->order_by('order_date','desc')
			->limit(1));

		$result = new \Model\Paypalrest\Agreement();
		$paypal_result = $result->cancel(array('id' => $order->trans_id, 'note' => 'Downgrade featured advertisement'));
		if($paypal_result['status'] == 'success') {
			$model = \Model\Vendor::forge($vendor->id);
			$model->set_fields(array(
				'featured' => \Model\Vendor::VENDOR_STATUS_NON_FEATURED
			));
			$model->save();
			$response = array('status' => 'OK', 'Conf_Num' => $order->trans_id);
		} else {
			$message = '<ul>'.$paypal_result['error'].'</ul>';
            $response = array('status' => 'ERROR', 'message' => $message);
		}
		return $response;
	}

}