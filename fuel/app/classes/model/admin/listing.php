<?php
namespace Model\Admin;

class Listing extends \Model {

	public static function forge(){
		return new self;
	}

	public function dash_list($params) {
		$status = \Model\Listing::forge()->get_status();
		// The columns defined in the table
		$aColumns = array('listing_id', 'type_of_listing', 'address', 'city', 'state', 'price', 'member', 'matches', 'status');
		$aListingStatus = \Model\Listing::forge()->get_status();

		//build sql listing status translation
		$sqlStatus = '(CASE ';
		foreach($aListingStatus as $val => $name){
			$sqlStatus .= 'WHEN client_has.listing_status = \''.$val.'\' THEN \''.$name.'\'';
		}
		$sqlStatus .= ' END)';

		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$subsql = "
			(SELECT client_has.id,client_has.listing_id,client_has.realtor_id AS member,
			(CASE client_has.country WHEN '1' THEN ca_cities.name ELSE us_cities.name END) AS city,
			(CASE client_has.country WHEN '1' THEN ca_provinces.prov_abb ELSE us_states.state_code END) AS state,
			".$sqlStatus." AS status,
			client_has.listing_price AS price,
			client_has.imported,
			client_has.external_id,
			client_has.matches,
			client_has.listing_status,
			client_has.address,
			client_has.featured_listing,
			client_has.type_of_listing,
			client_has.list_as_sold,
			client_wants.completed_section
			FROM client_has
			LEFT JOIN ca_cities ON ca_cities.id = client_has.city
			LEFT JOIN ca_provinces ON ca_provinces.id = client_has.prov_state
			LEFT JOIN us_cities ON us_cities.id = client_has.city
			LEFT JOIN us_states ON us_states.id = client_has.prov_state
			LEFT JOIN client_wants ON client_has.listing_id = client_wants.listing_id) AS tmp";

		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ".$subsql;

		/** filter view dropdown */
		/** todo: find why client_has create blank record after admin update */
		if (isset($params['view'])){
			$options = \Model\Listing::forge()->sort_view($params['view']);
			$has_prefix = 'client_has.';
			$wants_prefix = 'client_wants.';
			if(substr($options['col'], 0, strlen($has_prefix)) == $has_prefix) {
				$options['col'] = substr($options['col'], strlen($has_prefix));
			}
			if(substr($options['col'], 0, strlen($wants_prefix)) == $wants_prefix) {
				$options['col'] = substr($options['col'], strlen($wants_prefix));
			}
			$sql .= ' WHERE '.$options['col'].' '.$options['operator'].' \''.$options['val'].'\' AND listing_id <> \'\'';
		}

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= 'AND (';
			$sql .= 'address LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR member LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR listing_id LIKE \'%'.$params['search']['value'].'%\'';
			if(intval($params['search']['value']) > 0){
				$sql .= ' OR price LIKE \'%'.intval($params['search']['value']).'%\'';
			}
			$sql .= ' OR state LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR city LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR status LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}

		/** orderable */
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= ' ORDER BY '.$aColumns[$params['order'][0]['column']].' '.$params['order'][0]['dir'];
		} else {
			$sql .= ' ORDER BY listing_id';
		}

		$total = \DB::query($sql,  \DB::SELECT)->execute()->count();

		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();
		// Create an associate array with empty values
		$aColumns = array_fill_keys($aColumns, '');
		$data['data'] = array();
		$types = \Model\Listing::forge()->get_short_types();
		foreach ($result->as_array() as $row) {
			$id = $row['id'];
			$drow = Arr::overwrite($aColumns, $row);
			$drow['listing_id'] = $row['listing_id'];
			$drow['type_of_listing'] = $types[$row['type_of_listing']];
			$drow['price'] = '$' . number_format($row['price'], 0);
			$drow['matches'] = $row['matches'];
			$drow['status'] = $status[$row['listing_status']];
			$drow['action'] = \HTML::anchor('admins/listing/' . $row['id'], 'Edit', array('title' => __('edit_this_listing'), 'class' => 'inline-action'));
			$viewable = array(\Model\Listing::LS_ACTIVE,\Model\Listing::LS_EXPIRED,\Model\Listing::LS_MATCHABLE);
			if (in_array($row['listing_status'], $viewable)) {
				$drow['action'] .= \HTML::anchor('admins/listing/detail/0_'.$row['listing_id'], 'View', array('title' => __('view_listing_detail'), 'class' => 'inline-action'));
			}
			//delete link
			$drow['action'] .= \HTML::anchor('admins/listing/delete/'.$id, __('delete'), array('title' => __('delete'), 'class' => 'inline-action red delete-listing', 'data-target' => $row['listing_id']));
			$row = $drow;
			$row['DT_RowId'] = $id;
			$data['data'][] = array_values($row);
		}
		// Total rows
		$data['recordsFiltered'] = $total;
//		$sql = 'SELECT COUNT(officeid) AS cnt FROM crea_agents GROUP BY officeid';
		$data['recordsTotal'] = $total;
		return $data;
	}
}