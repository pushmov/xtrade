<?php
namespace Model;

class Realtor extends User {
	const ID_PREFIX = 'xTH';
	const VCARD_PATH = 'assets/images/vcard/';
	const VIEW_MEMBER_DEFAULT = 0;
	const VIEW_MEMBER_TO_MEMBER = 1;
	const VIEW_MEMBER_TO_NON_MEMBER = 2;
	protected $_class = 'realtor';
	protected $_table_name = 'users';
	protected $_data =	array('id' => NULL, 'realtor_id' => '', 'nrb_id' => '', 'office_id' => '',
		'first_name' => '', 'last_name' => '', 'password' => '', 'temp_pass' => NULL, 'email' => '',
		'phone' => '', 'cell' => '', 'address' => '', 'address2' => '', 'city' => '', 'county' => '',
		'prov_state' => '', 'country' => '', 'z_p_code' => '', 'company_name' => '', 'web_site' => '',
		'promo_code' => '', 'last_login' => '', 'logged_in' => '', 'cancelled_on' => '',
		'last_import' => '', 'session_id' => '', 'security_question' => '',	'security_answer' => '',
		'account_creation' => '', 'activate' => '', 'pass_reset' => '', 'ip_address' => '',	'account_type' => '',
		'fsbo' => '', 'paypal_status' => 0, 'paypal_sub_id' => '', 'plan_id' => 0,'alerts' => '',
		'location' => '', 'remember' => '', 'browser' => '', 'last_active' => '', 'last_email_date' => '',
		'email_type' => '',	'promotional_end' => '', 'pending_broker' => '', 'broker_ref' => '',
		'external_listings_notified' => '','account_status' => '', );
	// We dont want to check every time there is a save
	protected $_valrules =	array(
		'first_name' => array('First name', 'required'),
		'last_name' => array('Last name', 'required'),
		'company_name' => array('Company name', 'required'),
		//'address' => array('Address', 'required'),
		//'city' => array('City', 'required'),
		//'prov_state' => array('Provice/State', 'required'),
		//'z_p_code' => array('Postal/Zip code', 'required'),
		'phone' => array('Phone number', 'required|valid_phone'),
		'cell' => array('Cell number', 'valid_phone'),
		'web_site' => array('Web Site', 'valid_domain'),
		'email' => array('Email', 'required|valid_email'));

	public function load_by_id($id, $type = 'realtor') {
		$this->load(\DB::select_array()->where($type . '_id', '=', $id));
		return $this;
	}

	public function save($validation = NULL) {
		// Do some fomatting before saving
		$this->phone = preg_replace('/\D+/', '', $this->phone);
		$this->cell = preg_replace('/\D+/', '', $this->cell);
		$this->email = strtolower($this->email);
		$this->web_site = $this->format_url($this->web_site);
		$this->nrb_id = ($this->nrb_id == '') ? NULL : $this->nrb_id;
		return parent::save($validation);
	}

	public function doupdate($post) {
		unset($post['password']);
		$extra['cemail'] = $post['cemail'];
		$validation = \Validation::forge();
		$validation->add_callable(new \Model\Realtor);
		$validation->add_callable('rules');
		// Common rules
		foreach($this->_valrules as $field => $rules) {
			$validation->add_field($field, $rules[0], $rules[1]);
		}
		$validation->add_field('cemail', __('this_email'), 'match_field[email]');

		$post = array_map('trim', $post['data']);
		// Unique validation
		if ($post['email'] != $this->email) {
			$validation->field('email')->add_rule('unique_email');
		}
		$validation->add_field('nrb_id', 'This field', 'required');
		if (($post['nrb_id'] != '') && ($post['nrb_id'] != $this->nrb_id)) {
			if ($this->country == \CityState::CA) {
				$validation->field('nrb_id')->add_rule('unique_crea');
				$validation->set_message('unique_crea', __('crea_exists'));
			} else {
				$validation->field('nrds_id')->add_rule('unique_nrds');
				$validation->set_message('unique_nrds', __('nrds_exists'));
			}
		}
		$need_import = !($this->nrb_id == $post['nrb_id']);
		$this->set_fields($post);
		if ($validation->run(array_merge($this->as_array(), $extra))) {
			$this->account_status = \Model\User::AS_VALID;
			$result = $this->save();
			// Update session info with updated data
			\Authlite::instance('auth_realtor')->force_login($this->email);
			// load in crea listings
			if ($need_import) {
				\Model\Crea\Import::do_import($this->id);
			}
			return $result;
		} else {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
	}

	/**
	* Validate the fields from new signup
	*
	* @param array $post
	*/
	public function register($post) {
		$data = array_map('trim', $post['data']);
		unset($post['data']);
		$data2 = array_map('trim', $post);
		$validation = \Validation::forge();
		$validation->add_callable(new \Model\Realtor);
		$validation->add_callable(new \Rules);
		// Common rules
		foreach($this->_valrules as $field => $rules) {
			$validation->add_field($field, $rules[0], $rules[1]);
		}

		//if from broker refference
		$broker_ref = \Model\Broker::forge()->load_by_id($data['broker_id']);
		if(!$broker_ref->loaded()){
			$validation->field('email')->add_rule('unique_email');
		}
		$validation->add_field('crea_id', __('crea_in_use'), 'unique_nrb');
		$validation->add_field('cemail', __('this_email'), 'match_field[email]');
		$validation->add_field('password', __('password'), 'required|min_length[5]');
		$validation->add_field('cpassword', __('password'), 'match_field[password]');
		$validation->add_field('tos', __('terms'), 'required');
		$validation->add_field('promo_code', __('promo_code_invalid'), 'valid_promo_r');
		$validation->set_message('valid_promo_r', __('promo_code_invalid'));
		if ($validation->run(array_merge($data, $data2))) {
			// Set based on filled field
			$data['nrb_id'] = ($data['crea_id'] == '') ? $data['nrds_id'] : $data['crea_id'];
			return array_merge($data, $data2);
		} else {
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
	}

	/**
	* Returns data used for the profile page
	*
	*/
	public function profile() {
		\Lang::load('listing');
		$data = parent::profile();
		$listings = \Model\Client\Has::forge()->get_profile_count($this->realtor_id);
		$extra['member_id'] = $this->realtor_id;
		$extra['info'] = __('you_have')." {$listings['total']} ".__('total_listing').", {$listings['active']} ".__('are_active_listing').", {$listings['exclusive']} ".__('exclusive_xtrade')." {$listings['finished']} ".__('are_complete_and_matching');
		$extra['total'] = $listings['total'];
		$extra['exclusive_listing'] = $listings['exclusive'];
		$extra['downloaded_listing'] = \Model\Listing::forge()->downloaded_listing($this->realtor_id);
		$extra['matches_listing'] = \Model\Matches::forge()->realtor_matches($this->realtor_id);
		if($listings['finished'] < $listings['total']){
			$extra['info'] .= ' '.__('should_update_wants');
		}
		// if($update_wants){ echo $update_wants;";
		$extra['broker_info'] = $this->profile_broker();
		$exclusive_listing = __('xlisting_txt');
		$exclusive_listing .= ($extra['exclusive_listing'] > 0) ? ' ('.$extra['exclusive_listing'].')' : '';
		$downloaded_listing = __('olisting_txt');
		$downloaded_listing .= (sizeof($extra['downloaded_listing']) > 0) ? ' ('.sizeof($extra['downloaded_listing']).')' : '';
		$matches_listing = __('mlisting_txt');
		$matches_listing .= ($extra['matches_listing'] > 0) ? ' ('.$extra['matches_listing'].')' : '';

		$listing_btn = \Form::button('exclusive_listing', $exclusive_listing, array('type' => 'button', 'class' => 'button'));
		$listing_btn .= \Form::button('downloaded_listing', $downloaded_listing, array('type' => 'button', 'class' => 'button'));
		$listing_btn .= \Form::button('matching_listing', $matches_listing, array('type' => 'button', 'class' => 'button'));

		$data['listing_btn'] = $listing_btn;
		return array_merge($data, $extra);
	}

	/**
	* Sets the broker information
	*
	*/
	public function profile_broker() {
		\Lang::load('common');
		if ($this->pending_broker == -1) {
			$data['info'] = __('req_decline');
			$data['button'] = '<button type="button" class="button tiny broker-info-btn" id="btn_broker_cancel">'.__('cancel_req').'</button>';
		} elseif ($this->pending_broker != 0) {
			$id = \Model\Broker::forge($this->pending_broker)->broker_id;
			$data['info'] = __('req_sent').' (' . $id . ')';
			$data['button'] = '<button type="button" class="button tiny broker-info-btn" id="btn_broker_cancel">'.__('cancel_req').'</button>';
		} elseif ($this->office_id != 0) {
			$name = \Model\Broker::forge($this->office_id)->company_name;
			$data['info'] = $name;
			$data['button'] = '<button type="button" class="button tiny broker-info-btn" id="btn_broker_request">'.__('remove_assoc').'</button>';
		} else {
			$data['info'] = __('no_broker');
			$data['button'] = '<button type="button" class="button tiny broker-info-btn" id="btn_broker_request">'.__('req_assoc').'</button>';
		}
		return $data;
	}

	/**
	* Load all realtors record from users table
	*
	* @param Object $pagination Fuel's pagination instance
	* @param int $limit Limit
	* @param int $offset Offset
	*
	* @return array Set of realtors record
	*/
	public function load_users($params){
		$data['data'] = $drow = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$status = \Model\User::forge()->get_status();
		$aColumns = array('realtor_id', 'name', 'account_status', 'account_creation');
		$subsql = "(SELECT id, realtor_id, CONCAT(first_name, ' ', last_name) as name, account_status, account_creation	FROM users) AS tmp ";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " . $subsql;

		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= ' WHERE (';
			$sql .= 'realtor_id LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ' OR name LIKE \'%'.$params['search']['value'].'%\'';
			$sql .= ')';
		}

		if (isset($params['order']) && !empty($params['order'])) {
			$sql .= ' ORDER BY ';
			$oa = array();
			foreach($params['order'] as $order) {
				$oa[] = $aColumns[$order['column']].' '.$order['dir'];
			}
			$sql .= implode(',', $oa);
		} else {
			$sql .= ' ORDER BY last_name,first_name,account_status';
		}

		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}

		$result = \DB::query($sql, \DB::SELECT)->execute();

		foreach ($result as $row) {
			$listings = \DB::select_array()->from('client_has')->where('realtor_id', '=', $row['realtor_id'])->execute()->as_array();
			$cnt = 0;
			foreach($listings as $listing){
				$cnt += count(\Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing['listing_id']), NULL)->as_array());
			}
			$matches_txt = ($cnt > 0) ? ($cnt . \Inflector::pluralize(' Listing', $cnt)) : 'None';

			$result = \DB::select()->from('client_has')->where('realtor_id', '=', $row['realtor_id'])
			->where('imported', '=', \Model\Listing::NON_IMPORTED)
			->where('listing_status', 'IN', array(0,1,2,3))
			->execute();
			$cnt = \DB::count_last_query();
			$xlisting_txt = ($cnt > 0) ? ($cnt . \Inflector::pluralize(' Listing', $cnt)) : 'None';

			$result = \DB::select()->from('client_has')->where('realtor_id', '=', $row['realtor_id'])
			->where('imported', '=', \Model\Listing::IMPORTED)
			->execute();
			$cnt = \DB::count_last_query();
			$olisting_txt = ($cnt > 0) ? ($cnt . \Inflector::pluralize(' Listing', $cnt)) : 'None';

			$result = \DB::select()->from('client_has')->where('realtor_id', '=', $row['realtor_id'])
			->where('completed_section', '=', 1)
			->where('type_of_listing', '<>', Listing::TYPE_BUY_ONLY)
			->execute();
			$cnt = \DB::count_last_query();
			$rlisting_txt = ($cnt > 0) ? ($cnt . \Inflector::pluralize(' Listing', $cnt)) : 'None';

			$drow['realtor_id'] = \Html::anchor('admins/profile/realtor/'.$row['id'], $row['realtor_id'], array('title' => 'Click for profile'));
			$drow['name'] = \Html::anchor('#', $row['name'], array('class' => 'email-user', 'title' => 'Click to email', 'id' => $row['id']));
			$drow['account_status'] = $status[$row['account_status']];
			$drow['account_creation'] = date_create($row['account_creation'])->format(DATE_FORMAT);
			$drow['exclusive'] = \Html::anchor('#', $xlisting_txt, array('class' => 'actions edit inline-action realtor-exclusive', 'id' => $row['id']));
			$drow['downloaded'] = \Html::anchor('#', $olisting_txt, array('class' => 'actions edit inline-action realtor-downloaded', 'id' => $row['id']));
			$drow['ready'] = \Html::anchor('#', $rlisting_txt, array('class' => 'actions edit inline-action realtor-ready', 'id' => $row['id']));
			$drow['matches'] = \Html::anchor('#', $matches_txt, array('class' => 'actions edit inline-action realtor-matches', 'id' => $row['id']));
			$drow['action'] = \Html::anchor('#', \Html::img('assets/images/delete.png'), array('id' => $row['id'], 'class' => 'remove', 'title' => 'Remove member'));
			if($row['account_status'] == \Model\User::AS_VALID){
				$drow['action'] = \Html::anchor('#', \Html::img('assets/images/mask.png'), array('id' => $row['realtor_id'], 'class' => 'impersonate', 'title' => 'Impersonate member')) . ' | ' . $drow['action'];
			}
			$data['data'][] = array_values($drow);
		}

		$total = \DB::select()->from($this->_table_name)->execute()->count();
		$data['recordsTotal'] = $total;
		$data['recordsFiltered'] = $total;
		return $data;
	}

	/**
	* Add new realtor under broker selected
	*
	* @param array $post Realtor data collection
	* @return array Response status
	*/
	public function add_realtor_to_broker($post){
		$response['status'] = 'error';
		$response['message'] = __('error_add_realtor');
		$validation = \Validation::forge();
		$validation->add_callable(new \Model\Realtor());
		$validation->add_callable('rules');
		$validation->add_field('first_name', __('first'), 'required');
		$validation->add_field('last_name', __('last'), 'required');
		$validation->add_field('email', __('email'), array('required', 'valid_email'))
		->add_rule('unique', 'users.email', 'Email :value has already been used');
		$validation->add_field('crea_id', __('crea_in_use'), 'unique_nrb');
		if($validation->run($post) === FALSE){
			$response['message'] = $validation->show_errors();
			return $response;
		}
		$post['realtor_id'] = \Sysconfig::create_string('realtor');
		$data = array_map('trim', $post);
		$broker = \Model\Broker::forge()->load_by_id($post['broker_id'])->as_array();
		$data['office_id'] = $broker['broker_id'];
		$data['account_creation'] = time();
		$data['company_name'] = $broker['company_name'];
		//        $data['user_name'] = $post['email'];
		$data['account_status'] = \Model\User::AS_BROKER_USER;
		$data['nrb_id'] = $post['crea_id'];
		$data['office_id'] = $broker['id'];
		$data['password'] = '';
		if($this->save_user($data)){
			$data['broker'] = $broker;
			$response = \Emails::forge()->new_realtor_from_broker($data);
		}
		return $response;
	}

	/**
	* Delete user
	*
	* @param String $id Realtorid
	* @param String $type User type [vendor, realtor, broker]
	*/
	public function delete_realtor($id){
		$response['status'] = 'error';
		$listings = \Model\Client\Has::forge()->load(\DB::select_array()->where('realtor_id', '=', $id), NULL)->as_array();
		foreach($listings as $row){
			\DB::delete('client_has')->where('listing_id', '=', $row['listing_id'])->execute();
			\DB::delete('client_information')->where('listing_id', '=', $row['listing_id'])->execute();
			\DB::delete('matches')->where('listing_id', '=', $row['listing_id'])
			->or_where('matched_listings', '=', $row['listing_id'])
			->or_where('ignore_listings', '=', $row['listing_id'])
			->or_where('save_listings', '=', $row['listing_id'])
			->or_where('buyers_match', '=', $row['listing_id'])
			->execute();
		}
		\DB::delete('users')->where('realtor_id', '=', $id)->execute();
		$response['status'] = 'ok';
		$response['id'] = $id;
		return $response;
	}

	public function last_import(){
		$user = \Authlite::instance('auth_realtor')->get_user();
		if($user->nrb_id == ''){
			$realtor = $this->load(\DB::select_array()->where('realtor_id', '=', $user->realtor_id));
			$realtor->last_import = date_create()->format(DATETIME_FORMAT_DB);
			return $realtor->save();
		}
		return ($user->nrb_id != '') ? $user->nrb_id : $user->nrds_id;
	}

	public function agent_info($listing_id){
		list($feed, $id) = explode('_', $listing_id);
		$user_type = \Model\User::forge()->get_types(\Session::get('type'));
		$data['name'] = '';
		$data['phone'] = '';
		$data['email'] = '';
		$data['officename'] = '';
		$data['message'] = '';
		$data['direction'] = self::VIEW_MEMBER_DEFAULT;
		if(\Session::get('type') && \Authlite::instance('auth_'.$user_type)->logged_in()){
			//user logged in
			if(\Session::get('type') == \Model\User::UT_REALTOR){
				$user_id = \Authlite::instance('auth_'.$user_type)->get_user()->realtor_id;
			} elseif (\Session::get('type') == \Model\User::UT_BROKER){
				$user_id = \Authlite::instance('auth_'.$user_type)->get_user()->broker_id;
			}
			$data = \DB::select_array(array('id',\DB::expr("CONCAT(first_name, ' ', last_name) as name"), 'phone', 'email', array('company_name', 'officename')))
			->from(\Model\User::forge()->get_actual_table(\Session::get('type')))
			->where($user_type.'_id', '=', $user_id)->execute()->current();
			$data['message'] = '';
			if($feed == \Model\Listing::FEED_NON_IMPORTED) {
				$data['direction'] = self::VIEW_MEMBER_TO_MEMBER;
			}
		}
		if($feed == \Model\Listing::FEED_IMPORTED) {
			$data['listing_id'] = $id;
			$data['direction'] = self::VIEW_MEMBER_TO_NON_MEMBER;
		} else {
			$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $id))->as_array();
			$data['listing_id'] = $listing['listing_id'];
		}
		$data['feed'] = $feed;
		return $data;
	}

	public function contact_agent($post){
		\Lang::load('agent');
		$validation = \Validation::forge();
		$validation->add_field('name', __('name'), 'required');
		$validation->add_field('email', __('email'), 'required|valid_email');
		$validation->add_field('message', __('message'), 'required');
		$validation->add_field('working_realtor', __('working_realtor'), 'required');
		$validation->add_field('trade', __('trade'), 'required');
		if(!$validation->run($post)){
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
		$id = substr($post['listing_id'], 2);

		if($post['feed'] == \Model\Listing::NON_IMPORTED){
			$use_column = 'id';
		} else {
			$use_column = 'external_id';
		}
		$has = \Model\Client\Has::forge()->load(\DB::select_array()->where($use_column, '=', $id));

		if($has->loaded()){
			// Listing found in client_has table
			$post['not_found_flag'] = false;
			$post['address'] = $has->address . ', ';
			$post['address'] .= implode(', ', \CityState::get_area_name($has->city, $has->prov_state, $has->country, true));
			$post['address'] .= ', ' . $has->z_p_code;
			$post['listing_id'] = $has->listing_id;
			$this->load_by_id($has->realtor_id);
			/*
			$user_contacted['company_name'] = '';
			if($post['ut'] != ''){
			//logged in user, get the company name value of this user
			$model = '\\Model\\'.  ucwords($post['ut']);
			$user_contacted = $model::forge()->load(\DB::select()->where($post['ut'].'_id', '=', $post['user_xid']))->as_array();
			}
			$post['user_contacted']['name'] = $post['name'];
			$post['user_contacted']['officename'] = $user_contacted['company_name'];
			*/
			$post['agent_email'] = $this->email;
			$post['agent_name'] = $this->first_name . ' ' . $this->last_name;
			$post['agent_phone'] = $this->phone;
			$post['unsubscribe_link'] = 'user/unsubscribe/?email='.$post['agent_email'].'&type=realtor';
			$post['gen'] = \Sysconfig::create_string('egen');
		} else {
			// Listing only available in crea table
			$post['not_found_flag'] = true;
			// No agent email from crea so send to xTrade
			$post['agent_email'] = '';

			$listing = \Model\Crea\Property::forge()->listing_detail($id);
			$post['address'] = $listing['address'];
			$post['address'] .= ', ' . \CityState::get_area_name($listing['city'], $listing['prov_state'], $listing['country']);
			$post['address'] .= ', ' . $listing['z_p_code'];

			$prop_agent = \DB::select()->from('crea_property_agents')->where('prop_id', '=', $id)->execute()->current();
			$agent = \Model\Crea\Agent::forge($prop_agent['agent_id'])->as_array();
			$agent['address'] = $agent['officeaddress'].', '.$agent['officecity'].', '.$agent['officepostal'];
			$post['agent'] = $agent;
		}

		$post['working'] = $post['working_realtor'] == 1 ? __('are') : __('arent');
		return \Emails::forge()->contact_agent($post);
	}
	// TODO used only for focus group
	/*
	public function signup($signup) {
	$this->set_fields($signup);
	$this->realtor_id = \Sysconfig::create_string('realtor');
	$this->account_creation = date_create()->format('Y-m-d H:i:s');
	$this->activate = \Sysconfig::create_string('activate');
	$this->password = \Authlite::instance('auth_realtor')->hash($signup['password']);
	$this->paypal_sub_id = 'xxyz';
	$this->paypal_status = \Model\Payment::PP_ACTIVE;
	$this->country = \Cookie::get('pilot_country', \CityState::CA);
	\Cookie::delete('pilot_country');

	// Broker invitation process
	if (isset($signup['broker_ref'])) {
	$broker = \Model\Broker::forge()->load(\DB::select_array()->where('broker_id', '=', $signup['broker_ref']));
	if ($broker->loaded()) {
	$this->broker_id = $broker->id;
	}
	}
	$id = $this->save();
	\Emails::forge()->activate($this->as_array(), 'realtor');
	}
	*/
	/**
	* Complete the signup and get set payment
	*
	*/
	public function signup($data, $payment) {
		list($city,$state,$country) = \CityState::get_area_id($payment['cpc_address'], true);
		list($payment['city'],$payment['state'],$payment['country']) = \CityState::get_area_name($city, $state, $country, true);
		// Load plan info
		$plan = Plan::forge()->load_by_csp($country, $state, $payment['promo_code']);
		if ($plan->loaded() && ($plan->type_id == \Model\Plan::PT_REALTOR)) {
			$payment['plan_id'] = $plan->plan_id;
			$payment['email'] = $data['email'];
			try {
				$result = Payment::forge()->realtor($payment);
				if ($result['status'] == 'OK') {
					$this->set_fields($data);
					$this->realtor_id = \Sysconfig::create_string('realtor');
					$this->account_creation = date_create()->format('Y-m-d H:i:s');
					$this->ip_address = \Input::ip();
					$this->password = \Authlite::instance('auth_realtor')->hash($data['password']);
					$this->paypal_sub_id = $result['id'];
					$this->plan_id = $plan->id;
					$this->country = $country;
					$this->prov_state = $state;
					$this->save();
					// Send welcome email
					\Emails::forge()->welcome($this->as_array(), 'realtor');
					// Create and save the order
					$order = new Order;
					$order->user_id = $this->id;
					$order->user_type_id = User::UT_REALTOR;
					$order->type_id = Order::OT_SIGNUP;
					$order->total = $plan->rate + $plan->rate_tax;
					$order->sub_total = $plan->rate;
					$order->taxes = $plan->rate_tax;
					$order->tax_code = Tax::forge()->load_by_country_state($country, $state)->code;
					$order->agreement_id = $this->paypal_sub_id;
					$order->order_date = date_create()->format('Y-m-d H:i:s');
					$order->status_id = Order::OS_COMPLETE;
					$order->save();
					$result = array('status' => 'OK', 'Conf_Num' => $this->id);
				} else {
					\Sysconfig::logerror(__FILE__ . ' : ' , __LINE__, $result);
				}
			} catch (\Exception $e) {
				\Sysconfig::logerror(__FILE__ . ' : ' , __LINE__, $result);
				\Sysconfig::logerror(__FILE__ . ' : ' , __LINE__, $e);
				return array('status' => 'FAIL', 'result' => $result);
			}
		} else {
			\Sysconfig::logerror(__FILE__ . ' : ' , __LINE__, $payment);
			\Sysconfig::logerror(__FILE__ . ' : ' , __LINE__, $data);
			return array('status' => 'FAIL', 'result' => 'There was an error create this order');
		}
		return $result;
	}
}