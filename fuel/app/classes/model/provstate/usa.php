<?php

namespace Model\Provstate;

class Usa extends \Automodeler{
	
	protected $_class = 'usa';
	protected $_table_name = 'us_states';
	protected $_data =	array( 'id' => NULL, 'state_code' => '', 'state' => '', 'lat' => '', 'lng' => '', 'country_id' => '');
	
	public function get_state($state_id = null){
		$data = array(0 => 'All');
		$db = \DB::select('id', 'state');
		if ($state_id !== null) {
			$db->where('id', '=', $state_id);
		}
		$state = $this->load($db, null)->as_array();
		foreach($state as $row) {
			$data[$row['id']] = $row['state'];
		}
		return $data;
	}
	
}