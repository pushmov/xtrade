<?php

namespace Model\Provstate;

class Canada extends \Automodeler{
	
	protected $_class = 'canada';
	protected $_table_name = 'ca_provinces';
	protected $_data =	array( 'id' => NULL, 'province' => '', 'lat' => '', 'lng' => '', 'prov_abb' => '', 'country_id' => '');
	protected static $_has_many = array('ca_cities');
	
	public function get_id($name){
		$this->load(\DB::select()->where('province', '=', trim($name))
				->or_where('prov_abb', '=', trim($name)));
		return $this;
	}
	
	public function get_province($province_id=null){
		$data = array(0 => 'All');
		$db = \DB::select('id', 'province');
		if ($province_id !== null) {
			$db->where('id', '=', $province_id);
		}
		$province = $this->load($db, null)->as_array();
		foreach($province as $row) {
			$data[$row['id']] = $row['province'];
		}
		return $data;
	}
	
	public function get_prov_abbv($country_name){
		$this->load(\DB::select()->where('province', '=', trim($country_name)));
		return $this;
	}
	
	public function get_prov_name($abbv){
		$this->load(\DB::select()->where('prov_abb', '=', trim($abbv)));
		return $this;
	}
	
}