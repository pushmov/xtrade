<?php
namespace Model\Realtor;
use Model\Realtor;

/**
* Handles the broker associations
*/
class Broker extends Realtor {
	public function request_new($post) {
		\Lang::load('common');
		\Lang::load('signup');
		$data = array('status' => 'OK', 'message' => __('invite_sent'));
		$post = array_map('trim', $post);
		$validation = \Validation::forge();
		$validation->add_field('invite_name', 'name', 'required');
		$validation->add_field('invite_email', 'email', 'required|valid_email');
		if ($validation->run($post)) {
			$response = \Emails::forge()->broker_request_new(array_merge($post, $this->as_array()));
			if($response['status'] == 'OK'){
				$this->pending_broker = -1;
				$this->save();
			}
			$data['assoc'] = $this->profile_broker();
		} else {
			$data['status'] = 'ERROR';
			$data['errors'] = $validation->error_message();
		}
		return $data;
	}

	public function request_assoc($id) {
		\Lang::load('common');
		if (empty($id)) {
			return array('status' => 'FAILED', 'message' => '<div class="error">' . __('select_broker') . '</div>');
		}
		$response = \Emails::forge()->broker_request_assoc(\Model\Broker::forge($id), $this);
		if($response['status'] === 'OK'){
			$this->pending_broker = $id;
			$this->save();
			$data = array('status' => 'OK', 'message' => '<div class="callout success small">Email sent to broker</div>');
		}
		$assoc = $this->profile_broker();
		$data['assoc'] = $assoc['info'] . ' ' . $assoc['button'];
		return $data;
	}
	/**
	* Realtor cancelled broker join request
	*
	*/

	public function request_cancel() {
		$this->pending_broker = '';
		$this->save();
		$assoc = $this->profile_broker();
		$data['assoc'] = $assoc['info'] . ' ' . $assoc['button'];
		return $data;
	}

	/**
	* Returns a select list of active brokers
	*
	* @param bool $blank - if true include blank option
	* @param bool $all - if true return even one already associated with
	*/
	public function list_array($blank = true, $all = true) {
		\Lang::load('common');
		$query = \DB::select()->where('account_status', '=', \Model\User::AS_VALID);
		// Don't include the associated broker
		if (($this->office_id != 0) && !$all) {
			$query->where('id', '<>', $this->office_id);
		}
		$data = array();
		$brokers = \Model\Broker::forge()->load($query, null);
		foreach($brokers as $row){
			$data[$row->id] = $row->company_name;
		}
		if ($blank) {
			return array('' => __('select_choice')) + $data;
		}
		return $data;
	}
}