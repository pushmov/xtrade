<?php
namespace Model\Broker;
use Model\Broker;

/**
* Handles the realtor related associations
*/
class Realtor extends Broker {
	/**
	* Gets a list of realtors link to the broker account
	*
	* @return array
	*/
	public function get_dash_list() {
		$data = array();
		$result = \DB::select()->from('users')->where('office_id', '=', $this->id)->execute();
		foreach($result as $row) {
			$row['xlistings'] = count(\DB::select()->from('client_has')->where('realtor_id', '=', $row['realtor_id'])
					->where('imported', '=', \Model\Listing::NON_IMPORTED)
					//->where('listing_status', '=', \Model\Listing::LS_ACTIVE)
					->execute());
			$row['olistings'] = count(\DB::select()->from('client_has')->where('realtor_id', '=', $row['realtor_id'])
					->where('imported', '=', \Model\Listing::IMPORTED)
					->execute());
			$row['matches'] = $this->count_matching_listing($row['realtor_id']);
			$row['cn_id'] = $row['nrb_id'];
			$row['account_creation'] = $row['account_creation'] != '0000-00-00 00:00:00' ? date_create($row['account_creation'])->format(DATE_FORMAT) : __('not_yet_activated');
			$row['last_login'] = $row['last_login'] != '0000-00-00 00:00:00' ? date_create($row['last_login'])->format(DATETIME_FORMAT) : __('not_yet_login');
			$row['alerts'] = $this->get_alerts($row['alerts']);
			$data[] = $row;
		}
		return $data;
	}

	public function count_matching_listing($realtor_id){
		$total = 0;
		$listings = \DB::select_array()->from('client_has')
				->join('client_wants', 'left')->on('client_wants.listing_id', '=', 'client_has.listing_id')
				->where('client_has.realtor_id', '=', $realtor_id)
				->where('client_wants.completed_section', '=', 1)
				->where('client_has.completed_section', '=', 1)
				->where('client_has.listing_status', '=', \Model\Listing::LS_ACTIVE)
				->where('client_has.list_as_sold', '=', \Model\Listing::AS_NOT_SOLD)
				->where('client_has.type_of_listing', '=', \Model\Listing::TYPE_XTRADED)
				->group_by('client_has.listing_id')->execute();

		if(count($listings) > 0){
			foreach($listings as $row){
				$total += count(\DB::select()->from('matches')
					->where('listing_id', '=', $row['listing_id'])
					->where('realtor_id', '=', $realtor_id)
					->where(\DB::expr('ignore_listings IS NULL'))
					->where(\DB::expr('save_listings IS NULL'))
					->execute());
			}
		}
		return $total;

	}

	/**
	* Gets a list of realtor pending requests
	*
	* @return string
	*/
	public function get_dash_requests() {
		$html = '';
		$result = \DB::select()->from('users')->where('pending_broker', '=', $this->id)->execute();
		foreach($result as $row) {
			$cn_id = $row['nrb_id'];
			$html .= "<li>{$row['first_name']} {$row['last_name']} ({$cn_id}) ";
			$html .= '<button class="button small no-margin approve-request" id="ar_' . $row['id'] . '">'.__('approve').'</button> ';
			$html .= '<button class="button small no-margin alert decline-request" id="dr_' . $row['id'] . '">'.__('decline').'</button></li>';
		}
		return $html;
	}

	/**
	* Gets the data needed for the realtors details
	*
	* @param int $id - the realtor's id
	* @return array
	*/
	public function get_details($id) {
		$data = array();
		$results = \DB::select()->from('users')->where('office_id', '=', $this->id)->where('id', '=', $id)->execute()->current();
		$agreement = new \Model\Paypalrest\Agreement();
		if ($results) {
			$data = $results;
			$area = \CityState::get_area_name($data['city'], $data['prov_state'], $data['country'], true);
			$data['city'] = $area[0];
			$data['prov_state'] = $area[1];
			$data['country'] = $area[2];
			$data['office_id'] = $this->broker_id;
			$data['next_date'] = __('addr_notlisted');
			if($results['paypal_sub_id'] != ''){
				$detail = $agreement->detail($results['paypal_sub_id']);
				if ($detail['state'] == $agreement->get_status(\Model\Paypalrest\Agreement::STATE_ACTIVE) && $detail['next_date'] != '') {
					$data['next_date'] = date_create($detail['next_date'])->format(DATE_FORMAT);
				}
			}
		}
		return $data;
	}

	public function request_new($post) {
		\Lang::load('common');
		\Lang::load('signup');
		$result = array('status' => 'OK', 'message' => __('invite_sent'));
		$post = array_map('trim', $post);
		$validation = \Validation::forge();
		$validation->add_field('invite_name', __('name'), 'required');
		$validation->add_field('invite_email', __('email'), 'required|valid_email');
		if ($validation->run($post)) {
			\Package::load('email');
			$email = \Email::forge();
			$email->bcc(\Sysconfig::get('email_bcc'));
			$email->to($post['invite_email']);
			$email->subject("Message From {$this->first_name} {$this->last_name} Regarding xTradeHomes!");
			$vars['cname'] = $post['invite_name'];
			$vars['msg'][1] = "{$this->first_name} {$this->last_name}, has requested that you join xTradeHomes;";
			$vars['msg'][2] = \Html::anchor("broker_signup/?rid={$this->id}&rref=true", 'Click here');
			$vars['msg'][2] .= ' to signup today to take advantage of all the things xTradeHomes has to offer!';
			$vars['msg'][3] = 'If you cannot click on the link, copy and paste this url ' . \Uri::base(false) . 'broker_signup/?rid={$this->id}&rref=true';
			$email->html_body(\View::forge('email/broker_request', $vars));
			try	{
				$email->send();
				$this->pending_broker = -1;
				$this->save();
				$result['assoc'] = $this->profile_broker();
				\Emails::forge()->track_email('', $post['invite_email'], 'xTradeHomes Customer Service', 'no-reply@xtradehomes.com', 'invite request');
			} catch(\EmailValidationFailedException $e)	{
				// The validation failed
			} catch(\EmailSendingFailedException $e) {
				// The driver could not send the email
			}
		} else {
			$result = array('status' => 'ERROR', 'message' => $validation->error_message());
		}
		return $result;
	}
	/**
	* Agent request broker association to existing broker account
	*
	*/
	public function request_assoc($id) {
		\Lang::load('common');
		if (empty($id)) {
			return array('status' => 'ERROR', 'message' => __('select_broker'));
		}
		$broker = \Model\Broker::forge($id);
		\Package::load('email');
		$email = \Email::forge();
		$email->bcc(\Sysconfig::get('email_bcc'));
		\Lang::load('public');
		$email->to($broker->email);
		$email->subject("Message From {$this->first_name} {$this->last_name} Regarding xTradeHomes!");
		$vars['cname'] = "{$broker->first_name} {$broker->last_name} at {$broker->company_name}";
		$vars['msg'][1] = "{$this->first_name} {$this->last_name}, has requested to go under your broker account at xTradeHomes;";
		$vars['msg'][2] = \Html::anchor("broker_signup/?rid={$this->id}&rref=true", 'Click here');
		$vars['msg'][2] .= ' to login and approve or deny this request.';
		$vars['msg'][3] = 'If you cannot click on the link, copy and paste this url ' . \Uri::base(false) . 'broker_signup/?rid={$this->id}&rref=true';
		$email->html_body(\View::forge('email/broker_request', $vars));
		try	{
			$email->send();
			$this->pending_broker = $id;
			$this->save();
			\Emails::forge()->track_email('', $broker->email, 'xTradeHomes Customer Service', 'no-reply@xtradehomes.com', 'Association request');
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
		}
		return array('status' => 'OK', 'message' => __('broker_email_sent'), 'assoc' => $this->profile_broker());
	}

	public function request_change() {


	}

	/**
	* Removes a realtors association
	*
	* @param mixed $id
	*/
	public function remove_realtor($id) {
		$result = array('status' => 'OK', 'message' => __('realtor_removed'));
		// Need to remove the rr_ prefix
		$realtor = \Model\Realtor::forge(substr($id, 3));
		if ($this->loaded() && $realtor->loaded()) {
			// Change status to valid otherwise keep current status
			if ($realtor->account_status == \Model\User::AS_BROKER_USER) {
				$realtor->account_status = \Model\User::AS_VALID;
			}
				$realtor->office_id = 0;
				$realtor->save();
				/*
				TODO: need to check if realtor has a paypal account and made an update if required
				$post = $realtor->as_array();
				$post['price'] = \Sysconfig::get('realtor_monthly_membership_fee');
				list($city, $prov_state, $country) = \CityState::get_area_name($post['city'], $post['prov_state'], $post['country'], true);
				$post['country'] = $country;
				$post['user_xid'] = $post['realtor_id'];
				$post['new_amount'] = $post['new_amount_tax'] = 0;
				$gateway = \Model\Gateway::forge();
				$gateway->set_data($post);
				if ($gateway->update_profile($post) === false) {
					$result = array('status' => 'ERROR', 'message' => __('realtor_rem_error'));
				} else {
					$pp = new \Model\Paypal();
					$pp->profile_id = $post['paypal_sub_id'];
					$billing = $pp->profile();
					\Package::load('email');
					$email = \Email::forge();
					$email->bcc(\Sysconfig::get('email_bcc'));
					\Lang::load('public');
					$email->to($realtor->email);
					$email->subject("xTradeHomes Account Change Alert!");
					$vars['next_date'] = date_create($billing['next_date'])->format(DATE_FORMAT);
					$vars['price'] = $post['price'];
					$email->html_body(\View::forge('email/broker_realtor_removed', $vars));
					try	{
						$email->send();
						$realtor->office_id = '';
						$realtor->save();
						\Emails::forge()->track_email('', $this->email, 'xTradeHomes Customer Service', 'no-reply@xtradehomes.com', 'Realtor removed');
					} catch(\EmailValidationFailedException $e)	{
						// The validation failed
					} catch(\EmailSendingFailedException $e) {
						// The driver could not send the email
					}
					*/
			}
		return $result;
	}

	/**
	* Returns a select list of active brokers
	*
	* @param bool $blank - if true include blank option
	* @param bool $all - if true return even one already associated with
	*/
	public function list_array($blank = true, $all = true) {
		\Lang::load('common');
		$query = \DB::select()->where('account_status', '=', \Model\User::AS_VALID);
		// Don't include the associated broker
		if (($this->office_id != 0) && !$all) {
			$query->where('id', '<>', $this->office_id);
		}
		$data = \Model\Broker::forge()->select_list('id', 'company_name', $query);
		if ($blank) {
			return array('' => __('select_choice')) + $data;
		}
		return $data;
	}

	public function approve($id) {
		\Lang::load('common');
		$result = array('status' => 'OK', 'message' => __('realtor_approved'));
		$realtor = \Model\Realtor::forge($id);
		if ($realtor->loaded()) {
			//what realtor payment for?
//			$pricing = \Model\Payment::forge()->payment_info(null,'realtor', $realtor->id, null, null, $this->id);
//			$gateway = \Model\Gateway::forge();
			$data = $realtor->as_array();
			list($data['city'], $data['prov_state'], $data['country']) = \CityState::get_area_name($data['city'], $data['prov_state'], $data['country'], true);
			$data['user_xid'] = $data['realtor_id'];
//			$gateway->set_data(array_merge($pricing, $data));
//			$payment = $gateway->update_profile();
			$response = \Emails::forge()->broker_approve_request_existing($realtor, $this);
			if($response['status'] == 'OK'){
				//save office id to new broker
				$realtor->office_id = $this->id;
				$realtor->company_name = $this->company_name;
				$realtor->pending_broker = \Model\Broker::AS_VALID;
				$realtor->save();
			}

		} else {
			//non registered realtor
			$result = array('status' => 'ERROR', 'message' => __('realtor_not_found'));
		}
		return $result;
	}

	public function decline($id) {
		\Lang::load('common');
		$result = array('status' => 'OK', 'message' => __('realtor_declined'));
		$realtor = \Model\Realtor::forge($id);
		if ($realtor->loaded()) {
			$realtor->office_id = 0;
			$realtor->pending_broker = -1;
			$realtor->save();
			\Emails::forge()->broker_decline_request($realtor);
		} else {
			$result = array('status' => 'ERROR', 'message' => __('realtor_not_found'));
		}
		return $result;
	}

	/**
	* send an invitation to a realtor by broker
	* @param array $post - the required realtor info (first name, last name, email)
	* @return array
	*/
	public function invite_realtor($post){
		$broker = \Authlite::instance('auth_broker')->get_user();
		$response['status'] = 'error';
		$response['message'] = __('error_add_realtor');
		$validation = \Validation::forge();
		$validation->add_callable(new \Model\Realtor);
		$validation->add_callable('rules');
		$validation->add_field('first_name', __('first'), 'required');
		$validation->add_field('last_name', __('last'), 'required');
		$validation->add_field('email', __('email'), array('required', 'valid_email'));
		$validation->field('email')->add_rule('unique_email');
		if($validation->run($post) === FALSE){
			$response['errors'] = $validation->error_message();
			return $response;
		}
		$realtor = \Model\Realtor::forge();
		$realtor->set_fields(array_map('trim', $post));
		$realtor->realtor_id = \Sysconfig::create_string('realtor');
		$realtor->office_id = $broker->id;
		$realtor->account_creation = date_create()->format(DATETIME_FORMAT_DB);
		$realtor->company_name = $broker->company_name;
//		$realtor->user_name = $realtor->email;
		$realtor->account_status = \Model\User::AS_BROKER_USER;

		if($realtor->save()){
			$data = array(
				'first_name' => $post['first_name'],
				'last_name' => $post['last_name'],
				'to' => $post['email'],
				'company_name' => $broker->company_name,
				'broker_id' => $broker->broker_id,
				'realtor_id' => $realtor->realtor_id
			);
			$response = \Emails::forge()->invite_realtor($data);
		} else {
			$response = array('status' => 'error', 'message' => __('error_add_realtor'));
		}
		return $response;
	}

	/**
	* Gets a list of xtradehome listing
	* @param int $id - the realtor's id
	* @return array
	*/
	public function get_local_listing($id){

		$user = \Model\Realtor::forge($id);
		$listing = \Model\Client\Has::forge()->load(\DB::select_array()
			->where('realtor_id', '=', $user['realtor_id'])
			//->where('listing_status', '=', \Model\Listing::LS_ACTIVE)
			->where('imported', '=', \Model\Listing::NON_IMPORTED), NULL)->as_array();

		$data['listing'] = array();
		$data['name'] = $user['first_name'] . ' ' . $user['last_name'] .'\'s';

		foreach($listing as $row){
			if($row['address'] == '' && $row['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
				$row['address'] = __('addr_buyer_only');
			}elseif($row['address'] == '' && $row['type_of_listing'] != \Model\Listing::TYPE_BUY_ONLY){
				$row['address'] = __('addr_notlisted');
			}

			$data['listing'][] = $row;
		}

		return $data;
	}

	/**
	* Gets a list of realtors's imported listing
	* @param int $id - the realtor's id
	* @return array
	*/
	public function get_external_listing($id){

		$user = \Model\Realtor::forge($id);
		$listing = \Model\Client\Has::forge()->load(\DB::select_array()
			->where('realtor_id', '=', $user['realtor_id'])
			//->where('listing_status', '=', \Model\Listing::LS_ACTIVE)
			->where('imported', '=', \Model\Listing::IMPORTED), NULL)->as_array();

		$data['listing'] = array();
		$data['name'] = $user['first_name'] . ' ' . $user['last_name'] .'\'s';

		foreach($listing as $row){
			if($row['address'] == '' && $row['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
				$row['address'] = __('addr_buyer_only');
			}elseif($row['address'] == '' && $row['type_of_listing'] != \Model\Listing::TYPE_BUY_ONLY){
				$row['address'] = __('addr_notlisted');
			}

			$data['listing'][] = $row;
		}

		return $data;
	}

	/**
	* Gets a list of realtors's matches listing
	* @param int $id - the realtor's id
	* @return array
	*/
	public function get_matches_listing($id){
		$data = array();
		$data['listing'] = array();
		$user = \Model\Realtor::forge($id);
		$data['name'] = $user['first_name'].' '.$user['last_name'] . '\'s';

		$client_has = \Model\Client\Has::forge()->load(\DB::select()
				->join('client_wants', 'left')->on('client_wants.listing_id', '=', 'client_has.listing_id')
				->where('client_has.realtor_id', '=', $user['realtor_id'])
				->where('client_wants.completed_section', '=', 1)
				->where('client_has.completed_section', '=', 1)
				->where('client_has.listing_status', '=', \Model\Listing::LS_ACTIVE)
				->where('client_has.list_as_sold', '=', \Model\Listing::AS_NOT_SOLD)
				->where('client_has.type_of_listing', '=', \Model\Listing::TYPE_XTRADED)
				->group_by('client_has.listing_id')
			, null)->as_array();

		if(!empty($client_has)){
			$i = 0;
			$matches = \Match::forge();
			foreach($client_has as $row){
				$matches->find_all_way_matches($row['listing_id']);
				$num_total = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $row['listing_id'])
						->where(\DB::expr('ignore_listings IS NULL'))
						->where(\DB::expr('save_listings IS NULL')), NULL)->as_array();
				$saved_listings = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $row['listing_id'])
						->where(\DB::expr('save_listings IS NOT NULL')), NULL)->as_array();

				if(sizeof($num_total) > 0 || sizeof($saved_listings) > 0){

					$area_name = \CityState::get_area_name($row['city'], $row['prov_state'], $row['country']);
					$address = $row['address'].', '.  $area_name;
					if($row['address'] == '' || $area_name == ''){
						if($row['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
							$address = __('addr_buyer_only');
						}else{
							$address = __('addr_notlisted');
						}
					}
					$data['listing'][$i]['address'] = $address;
					$data['listing'][$i]['listing_id'] = $row['listing_id'];


					$data['listing'][$i]['num_total'] = sizeof($num_total);
					$data['listing'][$i]['realtor_id'] = $row['realtor_id'];

					$num_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $row['listing_id'])
						->where_open()
						->where(\DB::expr('ignore_listings IS NOT NULL'))
						->or_where(\DB::expr('save_listings IS NOT NULL'))
						->where_close(), NULL)->as_array();
					$data['listing'][$i]['num_ignore'] = sizeof($num_ignore);
					$i++;
				}
			}
		}
		return $data;
	}
}