<?php
namespace Model;

class Listing extends \Model {
	const IMPORTED = 1;
	const NON_IMPORTED = 0;
	const TYPE_XTRADED = 0;
	const TYPE_SELL_ONLY = 1;
	const TYPE_BUY_ONLY = 2;
	const AS_FEATURED = 1;
	const AS_NON_FEATURED = 0;
	const AS_SOLD = 1;
	const AS_NOT_SOLD = 0;
	const IMG_PATH = 'assets/images/listings/';
	const IMG_ASSET_DIR = 'assets/images/';
	const IMG_DIR_NAME = 'listings/';
	const IMG_PATH_THUMB = 'thumbs/';

	// Listing Codes
	const LS_ACTIVE = 0;
	const LS_EXPIRED = 1;
	const LS_DELETED = 2;
	const LS_HIDDEN_PUBLIC = 3;
	const LS_USER_DELETED = 4;
	const LS_HIDDEN_ADMIN = 5;
	const LS_XTRADED = 6;
	const LS_INACTIVE_IMPORT = 7;
	const LS_XCOMPLETE_IMPORT = 8;
	const LS_XCOMPLETE = 9;
	const LS_SEARCHABLE = 10;
	const LS_MATCHABLE = 11;
	const LS_SOLD_EX = 12;
	const LS_PENDING_EX = 13;
	const LS_NOCITY = 14;
	const LS_REMOVED_EXACTIVE = 15;

	const SESS_REFERER_NAME = 'listing_referer';
	const SESS_NAVIGATION = 'listing_pagination';
	const INCOMPLETE_CLASS = 'incomp-sect tooltip';
	const SESS_REFFERER_DASHBOARD = 'dashboard';

	const FEED_NON_IMPORTED = 0;
	const FEED_IMPORTED = 2;

	const DEFAULT_SORT_VALUE = 1;
	const DEFAULT_ITEMS_PER_PAGE = 5;
	const DEFAULT_PAGE_NUM = 1;

	protected $_codes = array(0 => 'Visible/Completed Listing', 'Expired Listings', 'Deleted Listing',
		'Hidden From General Public', 'User Deleted Completed Listing', 'Hidden From Users By Admin', 'Marked Xtraded by User',
		'Marked As Inactive By Import', 'Incomplete Imports', 'Incomplete Listing', 'Searchable / Not Matchable',
		'Not Searchable / Matchable','Listed As Sold By External Source','Listed As Sale Pending By External Source',
		'Imported Listing Has No Associated City','Removed By External Source');

	protected $_xtrade_status = array(self::AS_NOT_SOLD => 'Not xTraded', self::AS_SOLD => 'xTraded');

	protected $_type_of_listing = array(self::TYPE_XTRADED => 'all', self::TYPE_SELL_ONLY => 'seller', self::TYPE_BUY_ONLY => 'buyer');
	protected $_short_type = array(self::TYPE_XTRADED => 'XT', self::TYPE_SELL_ONLY => 'SO', self::TYPE_BUY_ONLY => 'BO');
	protected $_status = array(self::LS_ACTIVE => 'Active', self::LS_EXPIRED => 'Expired', self::LS_DELETED => 'Delete', self::LS_HIDDEN_PUBLIC => 'Hidden', self::LS_USER_DELETED => 'User Deleted', self::LS_HIDDEN_ADMIN => 'Hidden Admin', self::LS_XTRADED => 'xTraded',	self::LS_INACTIVE_IMPORT => 'Inactive Import', self::LS_XCOMPLETE_IMPORT => 'xComplete Import',	self::LS_XCOMPLETE => 'xComplete', self::LS_SEARCHABLE => 'Searchable',	self::LS_MATCHABLE => 'Matchable', self::LS_SOLD_EX => 'Sold External',	self::LS_PENDING_EX => 'Pending External', self::LS_NOCITY => 'No City', self::LS_REMOVED_EXACTIVE => 'Removed',);

	public function __construct() {
		\Lang::load('listing');
	}

	public static function forge(){
		return new self;
	}

	public function get_status($id = null){
		if($id === null){
			return $this->_status;
		}
		return $this->_status[$id];
	}

	public function get_types($id = null){
		if($id === null){
			return $this->_type_of_listing;
		}
		return $this->_type_of_listing[$id];
	}

	public function get_short_types($id = null){
		if($id === null){
			return $this->_short_type;
		}
		return $this->_short_type[$id];
	}

	public function get_xtrade_status($id = null){
		if($id === null){
			return $this->_xtrade_status;
		}
		return $this->_xtrade_status[$id];
	}

	/**
	* Returns an array with filter options
	*
	*/
	public static function filter_array() {
		$data['view'] = array(
			'all_listings' => __('all_listing'),
			'check_for_matches' => __('check_matches'),
			'missing_wants' => __('missing_wants'),
			'imported' => __('imported'),
			'exclusive' => __('exclusive'),
			'featured_listings' => __('featured_listing'),
			'seller_listings' => __('sell_only'),
			'sold' => __('xtraded'),
		);

		$data['order'] = array(
			'lp_hl' => __('lp_hl'),
			'lp_lh' => __('lp_lh'),
			'dc_hl' => __('dc_hl'),
			'dc_lh' => __('dc_lh'),
			'last_updated' => __('last_updated'),
		);
		return $data;
	}

	//style-import options data model
	public function prop_options_style_import(){
		$options = array(
			__('listing_propopt_aptcondo') => 'a',
			__('listing_propopt_townhouse') => 'i',
			__('listing_propopt_onestoratt') => 'f',
			__('listing_propopt_splitlvlatt') => 'g',
			__('listing_propopt_multistoratt') => 'c',
			__('listing_propopt_onestordet') => 'j',
			__('listing_propopt_splitlvldet') => 'h',
			__('listing_propopt_multistordet') => 'd',
			__('listing_propopt_multiunit') => 'e',
			__('listing_propopt_mfg') => 'b'
		);
		return $options;
	}

	public function prop_options_style_non_import(){
		$options = array(
			'a' => array(
				__('listing_propopt_a_1apt'),
				__('listing_propopt_a_2apt'),
				__('listing_propopt_a_1sto'),
				__('listing_propopt_a_apt'),
				__('listing_propopt_a_loft'),
				__('listing_propopt_a_pent'),
				__('listing_propopt_a_stack'),
			),

			'b' => array(
				__('listing_propopt_b_stor'),
				__('listing_propopt_b_manufact'),
				__('listing_propopt_b_mini'),
				__('listing_propopt_b_mobi'),
			),

			'c' => array(
				__('listing_propopt_c_mlvl'),
				__('listing_propopt_c_1stodet'),
				__('listing_propopt_c_2stodup'),
				__('listing_propopt_c_2stodet'),
				__('listing_propopt_c_2stodup'),
				__('listing_propopt_c_2stolin'),
				__('listing_propopt_c_2stosemidet'),
				__('listing_propopt_c_2stostack'),
				__('listing_propopt_c_3stodup'),
				__('listing_propopt_c_3stosemidet'),
				__('listing_propopt_c_3stostack'),
			),

			'd' => array(
				__('listing_propopt_d_1stry'),
				__('listing_propopt_d_1storey'),
				__('listing_propopt_d_1stodet'),
				__('listing_propopt_d_1storecrea'),
				__('listing_propopt_d_1stoloft'),
				__('listing_propopt_d_134sto'),
				__('listing_propopt_d_212sto'),
				__('listing_propopt_d_212stodet'),
				__('listing_propopt_d_212stoloft'),
				__('listing_propopt_d_2sto'),
				__('listing_propopt_d_2storecrea'),
				__('listing_propopt_d_2stodet'),
				__('listing_propopt_d_2stoloft'),
				__('listing_propopt_d_2sto'),
				__('listing_propopt_d_3lv'),
				__('listing_propopt_d_3sto'),
				__('listing_propopt_d_3stodet'),
				__('listing_propopt_d_4lvch'),
				__('listing_propopt_d_4lv'),
				__('listing_propopt_d_bas'),
				__('listing_propopt_d_bilvcod'),
				__('listing_propopt_d_bilvcha'),
				__('listing_propopt_d_bilvcont'),
				__('listing_propopt_d_capbilv'),
				__('listing_propopt_d_cathent'),
				__('listing_propopt_d_chalet'),
				__('listing_propopt_d_chaletcottage'),
				__('listing_propopt_d_contemp'),
				__('listing_propopt_d_gndlv'),
				__('listing_propopt_d_raisedranch'),
			),

			'e' => array(
				__('listing_propopt_e_sto'),
				__('listing_propopt_e_four'),
				__('listing_propopt_e_multi'),
				__('listing_propopt_e_tri'),
			),

			'f' => array(
				__('listing_propopt_f_duplx'),
				__('listing_propopt_f_bn'),
				__('listing_propopt_f_1stolink'),
				__('listing_propopt_f_1stodet'),
				__('listing_propopt_f_rbun'),
				__('listing_propopt_f_relev'),
				__('listing_propopt_f_fam'),
			),

			'g' => array(
				__('listing_propopt_g_4lvdet'),
				__('listing_propopt_g_4lvlink'),
				__('listing_propopt_g_4lvsemidet'),
			),

			'h' => array(
				__('listing_propopt_h_2stosplit'),
				__('listing_propopt_h_3lvback'),
				__('listing_propopt_h_3lvsplit'),
				__('listing_propopt_h_3lvsidesp'),
				__('listing_propopt_h_4lvside'),
				__('listing_propopt_h_4lvspl'),
				__('listing_propopt_h_5lvback'),
				__('listing_propopt_h_5lvspl'),
				__('listing_propopt_h_5lvside'),
				__('listing_propopt_h_splv'),
			),

			'i' => array(
				__('listing_propopt_i_12sto'),
				__('listing_propopt_i_1sto'),
				__('listing_propopt_i_212sto'),
				__('listing_propopt_i_2sto'),
				__('listing_propopt_i_3lvsp'),
				__('listing_propopt_i_3sto'),
				__('listing_propopt_i_4lvback'),
				__('listing_propopt_i_relv'),
				__('listing_propopt_i_town'),
				__('listing_propopt_i_townrunit'),
			),

			'j' => array(
				__('listing_propopt_j_1storecrea'),
				__('listing_propopt_j_1stodet'),
				__('listing_propopt_j_1stoloft'),
				__('listing_propopt_j_2lv'),
				__('listing_propopt_j_aframe'),
				__('listing_propopt_j_bn'),
				__('listing_propopt_j_ranch'),
				__('listing_propopt_j_bilv'),
				__('listing_propopt_j_camp'),
				__('listing_propopt_j_cot'),
				__('listing_propopt_j_det'),
				__('listing_propopt_j_log'),
				__('listing_propopt_j_ranch'),
				__('listing_propopt_j_relvdet'),
				__('listing_propopt_j_relvsdet'),
				__('listing_propopt_j_stor'),
			),

		);

		$return = array();
		foreach($options as $key => $vals){
			foreach($vals as $row){
				$return[$row] = $key;
			}
		}

		return $return;
	}

	public function prop_options_nonstyle_import(){
		$options = array(
			__('listing_propopt_resident') => 'a',
			__('listing_propopt_multifam') => 'b',
			__('listing_propopt_condo') => 'c',
			__('listing_propopt_farm') => 'd',
			__('listing_propopt_recr') => 'e',
			__('listing_propopt_othr') => 'f'
		);

		return $options;
	}

	public function prop_options_nonstyle_nonimport_rev(){
		$options = array(
			__('listing_propopt_singlfam') => 'a',
			__('listing_propopt_multifam') => 'b',
			__('listing_propopt_condo') => 'c',
			__('listing_propopt_agri') => 'd',
			__('listing_propopt_recr') => 'e',
			__('listing_propopt_othr') => 'f'
		);

		return $options;
	}

	public function prop_options_nonstyle_nonimport_nonrev(){

		$options = array(
			__('listing_propopt_resident') => 'a',
			__('listing_propopt_multifam') => 'b',
			__('listing_propopt_condo') => 'c',
			__('listing_propopt_farm') => 'd',
			__('listing_propopt_recr') => 'e',
			__('listing_propopt_othr') => 'f'
		);

		return $options;
	}

	//database function
	public function get_desires(\Database_Query_Builder_Select $query_builder, $lID){
		$query_builder->where('listing_id', '=', $lID);
		$query_builder->where('type_of_listing', '!=', self::TYPE_SELL_ONLY);

		$result = \Model\Client\Has::forge()->load($query_builder, NULL);
		return $result->as_array();
	}

	public function search_result_get_desires($Checker_ID, \Database_Query_Builder_Select $query_builder, $ID2Check){

		$query_builder->where('listing_status', '=', self::LS_ACTIVE);
		$query_builder->where('list_as_sold', '=', self::AS_NOT_SOLD);

		if(!$Checker_ID){
			$query_builder->where(\DB::expr('listing_id NOT LIKE \''.$ID2Check.'\''));
		}else{
			$query_builder->where(\DB::expr('listing_id LIKE \''.$Checker_ID.'\''));
		}

		$result = \Model\Client\Has::forge()->load($query_builder, NULL);
		return $result->as_array();
	}

	public function get_desirable_listings($listing_type, $listing_id, \Database_Query_Builder_Select $query_builder){

		$query_builder->where(\DB::expr('listing_id NOT LIKE \''.$listing_id.'\''));
		$query_builder->where('list_as_sold', '=', self::AS_NOT_SOLD);
		$query_builder->where('listing_status', '=', self::LS_ACTIVE);

		if($listing_type == '0'){
			$query_builder->where('type_of_listing', '=', self::TYPE_XTRADED);
		}else{
			$query_builder->where('type_of_listing', '!=', self::TYPE_BUY_ONLY);
		}

		$result = \Model\Client\Has::forge()->load($query_builder, NULL)->as_array();
		$return = array();

		if(!empty($result)){
			foreach($result as $row){
				$return[] = $row['listing_id'];
			}
		}

		return $return;
	}

	public function list_ajax_format(\Database_Query_Builder_Select $query_builder, $istut){
		$a = $query_builder->execute();
		$result = $query_builder->execute()->as_array();
		$return = array();
		if(!empty($result)){
			foreach($result as $row){
				if (($row['listing_status'] == \Model\Listing::LS_ACTIVE) && ($row['completed_section'] == 1)){
					$match = \Match::forge();
					$match->find_all_way_matches($row['listing_id']);
				}
				$row['data']['expiry'] = '';
				if($row['imported'] == self::NON_IMPORTED && $row['listing_status'] != self::LS_XCOMPLETE && $row['listing_status'] != self::LS_XCOMPLETE_IMPORT){
					if(date_create($row['date_expire']) >= date_create()){
						$row['data']['expiry'] = __('expires_on').' '.date_create($row['date_expire'])->format(DATE_FORMAT);
					}else{
						$row['data']['expiry'] = __('listing_expired').'<br>';
						$row['data']['expiry'] .= \Html::anchor('#', __('renew_now'), array('class' => 'no-link renew', 'id' => $row['listing_id']));
					}
				}

				if(($row['listing_status'] == self::LS_XCOMPLETE || $row['listing_status'] == self::LS_XCOMPLETE_IMPORT) || ($row['imported'] == self::IMPORTED && $row['listing_status'] == self::LS_EXPIRED)){
					$row['data']['heading'] = '<div class="clearfix members-listing padded">';
				}elseif($row['listing_status'] == self::LS_PENDING_EX){
					$row['data']['heading'] = '<div class="clearfix members-listing padded" style="background-image: url(../../images/pending_sale_bg.png);">';
				}else{
					if($row['featured_listing'] == \Model\Listing::AS_FEATURED && time() < strtotime($row['featured_end']) && $row['featured_end'] != '' && $row['featured_start'] != ''){
						$row['data']['heading'] = '<div class="clearfix members-listing padded">';
						$row['data']['heading'] .= '<div class="clearfix featured tooltip" title="'.__('feat_until').' ' . date_create($row['featured_end'])->format(DATE_FORMAT) .'"></div>';
					}else{
						//make sure featured data is null
						$client_has = \Model\Client\Has::forge($row['id']);
						if($client_has->loaded()){
							$client_has->featured_listing = \Model\Listing::AS_NON_FEATURED;
							$client_has->featured_start = null;
							$client_has->featured_end = null;
							$client_has->save();
						}
						$row['data']['heading'] = '<div class="clearfix members-listing padded">';
					}
				}

				$location = '';
				list($city, $prov_state, $country) = \CityState::get_area_name($row['city'], $row['prov_state'], $row['country'], true);
				$location .= $city;
				$location .= ($location == '') ? $prov_state : ', '.$prov_state;
				$location .= ($location == '') ? $country : ', '.$country;
				$row['data']['location'] = $location;

				if($row['type_of_listing'] == self::TYPE_XTRADED){
					$row['data']['type_of_listing'] = __('type_xtrade');
				}elseif($row['type_of_listing'] == self::TYPE_SELL_ONLY){
					$row['data']['type_of_listing'] = __('type_sell');
				}elseif($row['type_of_listing'] == self::TYPE_BUY_ONLY){
					$row['data']['type_of_listing'] = __('type_buy');
				}

				$row['data']['extra_stats'] = '';
				$row['data']['buyer_add'] = '';

				if($row['email'] != ''){
					$row['data']['clients_name'] = \Html::mail_to($row['email'], $row['first_name'].' '.$row['last_name'], null, array('title' => $row['email'], 'class' => 'tooltip'));
				}else{
					$row['data']['clients_name'] = $row['first_name'].' '.$row['last_name'];
				}

				if($row['first_name'] == '' && $row['last_name'] == ''){
					$row['data']['clients_name'] = \Html::anchor('realtors/listing/edit/'.$row['listing_id'].'?pos=client_info', __('not_in_system'));
				}

				$inactive_status_array = array(self::LS_EXPIRED, self::LS_DELETED, self::LS_HIDDEN_PUBLIC,
					self::LS_USER_DELETED, self::LS_HIDDEN_ADMIN, self::LS_XTRADED, self::LS_INACTIVE_IMPORT,
					self::LS_XCOMPLETE_IMPORT, self::LS_XCOMPLETE);

				if((bool)$row['imported']){
					$row['data']['pstatus'] = '<span class="active">'.__('active').'</span>';
					if($row['list_as_sold'] == self::AS_SOLD && $row['listing_status'] == self::LS_XTRADED){
						$row['data']['xstatus'] = __('xtraded');
					}elseif($row['completed_section'] == \Model\Client::STATUS_INCOMPLETE || $row['wants_completed_section'] == \Model\Client::STATUS_INCOMPLETE){
						$row['data']['xstatus'] = '<span class="incomplete" title="'.__('incomplete').'">'.__('nready_xtrade').'</span>';
					}elseif($row['list_as_sold'] == self::AS_NOT_SOLD && $row['listing_status'] != self::LS_XCOMPLETE_IMPORT && $row['listing_status'] != self::LS_XCOMPLETE && $row['listing_status'] != self::LS_EXPIRED){
						$row['data']['xstatus'] = '<span class="active">'.__('ready_xtrade').'</span>';
					}elseif($row['listing_status'] == self::LS_XCOMPLETE_IMPORT || $row['listing_status'] == self::LS_XCOMPLETE || $row['listing_status'] == self::LS_EXPIRED){
						$row['data']['xstatus'] = '<span class="incomplete" title="'.__('incomplete').'">'.__('nready_xtrade').'</span>';
					}
				} else {
					$row['data']['pstatus'] = '';
					if($row['listing_status'] == self::LS_ACTIVE){
						$row['data']['pstatus'] = '<span class="active">'.__('active').'</span>';
					}elseif(in_array($row['listing_status'], $inactive_status_array)){
						$row['data']['pstatus'] = '<span class="inactive">'.__('inactive').'</span>';
					}

					if($row['list_as_sold'] == self::AS_SOLD && $row['listing_status'] == self::LS_XTRADED){
						$row['data']['xstatus'] = __('xtraded');
					}elseif($row['completed_section'] == \Model\Client::STATUS_INCOMPLETE || $row['wants_completed_section'] == \Model\Client::STATUS_INCOMPLETE){
						$row['data']['xstatus'] = '<span class="incomplete" title="'.__('incomplete').'">'.__('nready_xtrade').'</span>';
					}elseif($row['list_as_sold'] == self::AS_NOT_SOLD && $row['listing_status'] != self::LS_XCOMPLETE){
						$row['data']['xstatus'] = '<span class="active">'.__('ready_xtrade').'</span>';
					}elseif($row['listing_status'] == self::LS_XCOMPLETE){
						$row['data']['xstatus'] = '<span class="incomplete" title="'.__('incomplete').'">'.__('nready_xtrade').'</span>';
					}
				}

				if($row['type_of_listing'] != \Model\Listing::TYPE_BUY_ONLY && $row['imported'] == \Model\Listing::NON_IMPORTED){
					$xtrade_picture = \Model\Photos\Xtrade::forge()->get_first_photo($row['listing_id']);
					$row['picture'] = \Html::anchor('listing/detail/0_'.$row['listing_id'],
						\Asset::img(self::IMG_DIR_NAME . $xtrade_picture, array('alt'=> $row['address'], 'class' => 'listing-image-bg')),
						array('title' => __('view_public_listing')));
				} elseif($row['imported']) {
					$row['picture'] = \Asset::img(\Model\Crea\Property::forge()->get_image($row['external_id'], 1, false),
						array('alt'=> $row['address'], 'class' => 'listing-image-bg'));
				} else {
					$row['picture'] = \Asset::img('buyers.jpg', array('alt' => __('buyer_only'), 'class' => 'listing-image-bg'));
				}

				$row['data']['tut_url'] = '';
				if($istut){
					$row['data']['tut_url'] = '&tutorial=true';
				}

				if($row['info_completed_section'] == \Model\Client::STATUS_INCOMPLETE){
					$row['data']['client_incomp'] = $istut == true ? 'Click here to continue' : __('incomplete_sect');
					$row['data']['client_incomp_class'] = $istut == true ? 'continue '.self::INCOMPLETE_CLASS : self::INCOMPLETE_CLASS;
				} else {
					$row['data']['client_incomp'] = '';
					$row['data']['client_incomp_class'] = '';
				}

				if($row['completed_section'] == \Model\Client::STATUS_INCOMPLETE){
					$row['data']['has_incomp'] = $istut == true ? '' : __('incomplete_sect');
					$row['data']['has_incomp_class'] = self::INCOMPLETE_CLASS;
				} else {
					$row['data']['has_incomp'] = '';
					$row['data']['has_incomp_class'] = '';
				}

				if($row['wants_completed_section'] == \Model\Client::STATUS_INCOMPLETE){
					$row['data']['wants_incomp'] = $istut == true ? '' : __('incomplete_sect');
					$row['data']['wants_incomp_class'] = self::INCOMPLETE_CLASS;
				} else {
					$row['data']['wants_incomp'] = '';
					$row['data']['wants_incomp_class'] = '';
				}

				$photos = \Model\Photos\Xtrade::forge()->load(\DB::select_array()->where('listing_id', '=', $row['listing_id'])
					->order_by('img_num', 'asc')->limit(1));
				if($photos->img_num == \Model\Client::STATUS_INCOMPLETE){
					$row['data']['image_incomp'] = __('no_images');
					$row['data']['image_incomp_class'] = self::INCOMPLETE_CLASS;
				} else {
					$row['data']['image_incomp'] = '';
					$row['data']['image_incomp_class'] = '';
				}

				$row['data']['w_req_title'] = '';
				$row['data']['w_req'] = '';
				if($row['client_fill_key']){
					$row['data']['w_req'] = '<span style="color: red;">*</span>';
					$row['data']['w_req_title'] = __('wants_req_sent');
				}

				$matches = \Model\Matches::forge()->count_seen_unseen_matches($row['listing_id']);
				$row['data']['seen_matches'] = $matches['seen'];
				$row['data']['unseen_matches'] = $matches['unseen'];
				$row['data']['total_matches'] = \Model\Matches::forge()->count_matches($row['listing_id']);
				$row['data']['new_matches_class'] = '';
				if($row['data']['unseen_matches'] >= 1){
					$row['data']['new_matches_class'] = 'class="new-matches"';
				}

				if($row['imported']) {
					$row['data']['details_url'] = \Html::anchor('listing/detail/2_'.$row['external_id'], $row['external_id']);
				} elseif($row['type_of_listing'] != self::TYPE_BUY_ONLY) {
					$row['data']['details_url'] = \Html::anchor('listing/detail/0_'.$row['listing_id'], $row['listing_id']);
				} else {
					$row['data']['details_url'] = $row['listing_id'];
				}

				$return[] = $row;
			}
		}

		return $return;
	}

	public function filter_views($id = NULL, $rev = FALSE) {
		if (empty($id)) {
			return '';
		}
		$txt = array(
			__('listing_none') => 'a',
			__('listing_view_city') => 'b',
			__('listing_view_mountain') => 'c',
			__('listing_view_ocean') => 'd',
			__('listing_view_lake') => 'e',
			__('listing_view_river') => 'f',
			__('listing_view_golf') => 'g',
			__('listing_view_harbour') => 'h');

		if (!$rev){
			$txt = array_flip($txt);
		}
		if (isset($txt[$id])) {
			return $txt[$id];
		} else {
			return current($txt);
		}
	}

	public function filter_sort_fields($value=NULL){
		$fields = array(
			1 => __('filter_newold'),
			2 => __('filter_oldnew'),
			3 => __('filter_lowhi'),
			4 => __('filter_hilow')
		);

		return (isset($value)) ? $fields[$value] : $fields;
	}

	public function db_translate_sort_field($value){
		$fields = array(
			1 => '`lastupdate` desc',
			2 => '`lastupdate` asc',
			3 => '`listing_price` asc',
			4 => '`listing_price` desc'
		);

		return (isset($value)) ? $fields[$value] : $fields;
	}

	public function filter_per_page(){
		$fields = array(
			5 => '5 '.__('per_page'),
			25 => '25 '.__('per_page'),
			50 => '50 '.__('per_page'),
			100 => '100 '.__('per_page')
		);

		return $fields;
	}

	public function filter_prices($value){
		$txt = array(0 => __('price'), '$100,000+', '$200,000+', '$300,000+', '$400,000+', '$500,000+', '$600,000+', '$700,000+', '$800,000+', '$900,000+', '$1000,000+', '$1,100,000+', '$1,200,000+', '$1,300,000+', '$1,400,000+', '$1,500,000+');
		$sql = array(0 => '', '100000 AND 200000', 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000, 1000000, 1100000, 1200000, 1300000, 1400000, 1500000);

		return $sql[$value];
	}

	public function filter_beds(){

		$fields = array(
			1 => __('beds'),
			2 => '2+ '.__('beds'),
			3 => '3+ '.__('beds'),
			4 => '4+ '.__('beds'),
			5 => '5+ '.__('beds'),
			6 => '6+ '.__('beds'),
			7 => '7+ '.__('beds'),
			8 => '8+ '.__('beds')
		);

		return $fields;
	}

	public function filter_baths(){

		$fields = array(
			1 => __('baths'),
			2 => '2+ '.__('baths'),
			3 => '3+ '.__('baths'),
			4 => '4+ '.__('baths'),
			5 => '5+ '.__('baths'),
			6 => '6+ '.__('baths'),
			7 => '7+ '.__('baths'),
			8 => '8+ '.__('baths')
		);

		return $fields;
	}

	public function filter_prop_type_checkbox(){
		$data = array(
			__('property_any') => '',
			__('storey_residential') => 'a',
			__('storey_condominium') => 'c',
			__('storey_recreational') => 'e'
		);
		return array_flip($data);
	}

	public function filter_prop_type($value, $rev=FALSE){
		if(!$rev){
			$sql = array('0' => '', 'a' => 'a','b' => 'b','c' => 'c','d' => 'd','e' => 'e','f' => 'f','g' => 'g','h' => 'h','i' => 'i','j' => 'j','k' => 'k','l' => 'l','m' => 'm','n' => 'n','o' => 'o','p' => 'p','q' => 'q','r' => 'r','s' => 's','t' => 't','u' => 'u','v' => 'v');
		}else{
			$sql = array('0' => '',
				'a' => __('listing_propopt_resident'), 'b' => __('listing_propopt_multifam'),'c' => __('listing_propopt_condo'),
				'd' => __('listing_propopt_farm'),'e' => __('listing_propopt_recr'), 'f' => __('listing_propopt_othr'));
		}
		return $value == '' ? '' : $sql[$value];
	}

	public function filter_prop_style_checkbox(){
		$data = array(
			__('listing_propopt_any') => '', __('listing_propopt_aptcondo') => 'a', __('listing_propopt_mfg') => 'b',
			__('listing_propopt_multistoratt') => 'c', __('listing_propopt_multistordet') =>'d', __('listing_propopt_multiunit') => 'e',
			__('listing_propopt_onestoratt') => 'f', __('listing_propopt_splitlvlatt') => 'g',__('listing_propopt_splitlvldet') => 'h',
			__('listing_propopt_townhouse') => 'i',	__('listing_propopt_onestordet') => 'j');
		return array_flip($data);
	}

	public function filter_prop_style($value, $rev=FALSE){
		$txt = array(__('listing_propopt') => '',__('listing_propopt_aptcondo') => 'a',__('listing_propopt_mfg') => 'b',
			__('listing_propopt_multistoratt') => 'c', __('listing_propopt_multistordet') => 'd',__('listing_propopt_multiunit') => 'e',
			__('listing_propopt_onestoratt') => 'f',__('listing_propopt_splitlvlatt') => 'g',
			__('listing_propopt_splitlvldet') => 'h',__('listing_propopt_townhouse') => 'i', __('listing_propopt_onestordet') => 'j');
		if(!$rev){
			$txt = array_flip($txt);
		}
		$sql = array('0' => '', 'a' => 'a','b' => 'b','c' => 'c','d' => 'd','e' => 'e','f' => 'f','g' => 'g','h' => 'h','i' => 'i','j' => 'j',
			'k' => 'k','l' => 'l','m' => 'm','n' => 'n','o' => 'o','p' => 'p','q' => 'q','r' => 'r','s' => 's','t' => 't','u' => 'u','v' => 'v');
		return $value == '' ? '' : $sql[$value];
	}

	public function filter_property_styles_p2($id) {
		$choice_array = array(
			__('listing_propopt_a_pent') => 'a',
			__('listing_propopt_a_apt') => 'a',
			__('listing_propopt_a_1apt') => 'a',
			__('listing_propopt_a_2apt') => 'a',
			__('listing_propopt_a_loft') => 'a',
			__('listing_propopt_a_stack') => 'a',
			__('listing_propopt_a_1sto') => 'a',
			__('listing_propopt_b_mini') => 'b',
			__('listing_propopt_b_manufact') => 'b',
			__('listing_propopt_b_stor') => 'b',
			__('listing_propopt_b_mobi') => 'b',
			__('listing_propopt_c_2stosemidet') => 'c',
			__('listing_propopt_c_3stosemidet') => 'c',
			__('listing_propopt_c_3stodup') => 'c',
			__('listing_propopt_c_2stodup') => 'c',
			__('listing_propopt_c_2stostack') => 'c',
			__('listing_propopt_c_2stodup') => 'c',
			__('listing_propopt_c_2stolin') => 'c',
			__('listing_propopt_c_2stodet') => 'c',
			__('listing_propopt_c_1stodet') => 'c',
			__('listing_propopt_c_3stostack') => 'c',
			__('listing_propopt_c_mlvl') => 'c',
			__('listing_propopt_d_212sto') => 'd',
			__('listing_propopt_d_134sto') => 'd',
			__('listing_propopt_d_1storey') => 'd',
			__('listing_propopt_d_2sto') => 'd',
			__('listing_propopt_d_1stry') => 'd',
			__('listing_propopt_d_3sto') => 'd',
			__('listing_propopt_d_2stodet') => 'd',
			__('listing_propopt_d_1stodet') => 'd',
			__('listing_propopt_d_3stodet') => 'd',
			__('listing_propopt_d_212stodet') => 'd',
			__('listing_propopt_d_2storecrea') => 'd',
			__('listing_propopt_d_1stoloft') => 'd',
			__('listing_propopt_d_2stoloft') => 'd',
			__('listing_propopt_d_212stoloft') => 'd',
			__('listing_propopt_d_2sto') => 'd',
			__('listing_propopt_d_1storecrea') => 'd',
			__('listing_propopt_d_gndlv') => 'd',
			__('listing_propopt_d_contemp') => 'd',
			__('listing_propopt_d_chalet') => 'd',
			__('listing_propopt_d_bilvcha') => 'd',
			__('listing_propopt_d_raisedranch') => 'd',
			__('listing_propopt_d_4lv') => 'd',
			__('listing_propopt_d_3lv') => 'd',
			__('listing_cape_cod') => '',
			__('listing_propopt_d_bas') => 'd',
			__('listing_propopt_d_bilvcont') => 'd',
			__('listing_propopt_d_bilvcod') => 'd',
			__('listing_propopt_d_chaletcottage') => 'd',
			__('listing_propopt_d_cathent') => 'd',
			__('listing_propopt_d_capbilv') => 'd',
			__('listing_propopt_d_4lvch') => 'd',
			__('listing_propopt_e_multi') => 'e',
			__('listing_propopt_e_tri') => 'e',
			__('listing_propopt_e_four') => 'e',
			__('listing_propopt_e_sto') => 'e',
			__('listing_propopt_f_duplx') => 'f',
			__('listing_propopt_f_1stodet') => 'f',
			__('listing_propopt_f_1stolink') => 'f',
			__('listing_propopt_f_relev') => 'f',
			__('listing_propopt_f_fam') => 'f',
			__('listing_propopt_f_rbun') => 'f',
			__('listing_propopt_f_bn') => 'f',
			__('listing_propopt_j_bn') => 'j',
			__('listing_propopt_j_ranch') => 'j',
			__('listing_propopt_j_bilv') => 'j',
			__('listing_propopt_j_1stodet') => 'j',
			__('listing_propopt_j_1storecrea') => 'j',
			__('listing_propopt_j_relvdet') => 'j',
			__('listing_propopt_j_det') => 'j',
			__('listing_propopt_j_1stoloft') => 'j',
			__('listing_propopt_j_stor') => 'j',
			__('listing_propopt_j_relvsdet') => 'j',
			__('listing_propopt_j_2lv')  => 'j',
			__('listing_propopt_j_ranch') => 'j',
			__('listing_propopt_j_log') => 'j',
			__('listing_propopt_j_cot') => 'j',
			__('listing_propopt_j_camp') => 'j',
			__('listing_propopt_j_aframe') => 'j',
			__('listing_propopt_g_4lvdet') => 'g',
			__('listing_propopt_g_4lvsemidet') => 'g',
			__('listing_propopt_g_4lvlink') => 'g',
			__('listing_propopt_h_2stosplit') => 'h',
			__('listing_propopt_h_4lvspl') => 'h',
			__('listing_propopt_h_3lvsplit') => 'h',
			__('listing_propopt_h_5lvspl') => 'h',
			__('listing_propopt_h_4lvside') => 'h',
			__('listing_propopt_h_3lvsidesp') => 'h',
			__('listing_propopt_h_5lvside') => 'h',
			__('listing_propopt_h_5lvback') => 'h',
			__('listing_propopt_h_3lvback') => 'h',
			__('listing_propopt_h_splv') => 'h',
			__('listing_propopt_i_3sto') => 'i',
			__('listing_propopt_i_12sto') => 'i',
			__('listing_propopt_i_town') => 'i',
			__('listing_propopt_i_2sto') => 'i',
			__('listing_propopt_i_3lvsp') => 'i',
			__('listing_propopt_i_1sto') => 'i',
			__('listing_propopt_i_212sto') => 'i',
			__('listing_propopt_i_relv') => 'i',
			__('listing_propopt_i_townrunit') => 'i',
			__('listing_propopt_i_4lvback') => 'i',
		);
		$filter = array();
		foreach($choice_array as $key => $val){
			if(in_array($val, $id)){
				$filter[] = $key;
			}
		}
		return $filter;
	}

	public function change_status($data){
		$response = array('status' => 'error');
		switch($data['name']){
			case 'xtrade':
				$status_change = self::TYPE_XTRADED;
				break;
			case 'sell':
				$status_change = self::TYPE_SELL_ONLY;
				break;
			case 'buy':
				$status_change = self::TYPE_BUY_ONLY;
				break;
		}
		$client = Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $data['listing_id']));
		$client->set_fields(array('type_of_listing' => $status_change));
		if($client->save()){
			//
			$response = array('status' => 'ok');
		}
		return $response;
	}

	public function mark_as_xtraded($post){
		$response = array('status' => 'error');
		$db = \DB::select_array()->where('listing_id', '=', $post['listing_id']);
		$client_has = \Model\Client\Has::forge()->load($db);
		$client_has->set_fields(array(
			'list_as_sold' => self::AS_SOLD,
			'listing_status' => self::LS_XTRADED
		));
		if($client_has->save()){
			$response['status'] = 'ok';
		}
		\Model\Matches::forge()->remove_by_id($post['listing_id']);
		return $response;
	}

	public function mark_as_notxtraded($post){
		$response = array('status' => 'error');
		$db = \DB::select_array()->where('listing_id', '=', $post['listing_id']);
		$client_has = \Model\Client\Has::forge()->load($db);
		$client_has->set_fields(array('list_as_sold' => self::AS_NOT_SOLD, 'listing_status' => self::LS_ACTIVE));
		if($client_has->save()){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function activate_listing($post){
		$response = array('status' => 'error');
		$db = \DB::select_array()->where('listing_id', '=', $post['listing_id']);
		$client_has = \Model\Client\Has::forge()->load($db);
		$client_has->set_fields(array('listing_status' => self::LS_ACTIVE));
		if($client_has->save()){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function deactivate_listing($post){
		$response = array('status' => 'error');
		$db = \DB::select_array()->where('listing_id', '=', $post['listing_id']);
		$client_has = \Model\Client\Has::forge()->load($db);
		$client_has->set_fields(array('listing_status' => self::LS_HIDDEN_PUBLIC));
		$save = $client_has->save();
		\Model\Matches::forge()->remove_by_id($oist['listing_id']);
		if($save){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function enable_listing($post){
		$response = array('status' => 'error');
		$client_has = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']));
		$client_has->set_fields(array('listing_status' => self::LS_ACTIVE));
		if($client_has->save()){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function disable_listing($post){
		$response = array('status' => 'error');
		$client_has = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']));
		$client_has->set_fields(array('listing_status' => self::LS_HIDDEN_ADMIN));
		if($client_has->save()){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function delete_listing($post, $system = false){
		$client_has = \Model\Client\Has::forge()->load_by_listing_id($post['listing_id']);
		if(($system === true) || ($client_has->listing_status != self::LS_XCOMPLETE) || \Authlite::instance('auth_admin')->logged_in()){
			\DB::delete('client_has')->where('listing_id', '=', $client_has->listing_id)->execute();
			\DB::delete('client_wants')->where('listing_id', '=', $client_has->listing_id)->execute();
			\DB::delete('client_information')->where('listing_id', '=', $client_has->listing_id)->execute();
		} else {
			$client_has->set_fields(array('listing_status' => self::LS_USER_DELETED));
			$client_has->save();
		}
		\Model\Photos\Xtrade::forge()->delete_all($post['listing_id']);
		\Model\Matches::forge()->remove_by_id($post['listing_id']);
		return array('status' => 'ok');
	}

	public function renew_listing($post){
		$response = array('status' => 'ok');
		$account_status = \Authlite::instance('auth_realtor')->get_user();
		$db = \DB::select_array()->where('listing_id', '=', $post['listing_id']);
		$db->where('realtor_id', '=', $account_status->realtor_id);
		$client_has = \Model\Client\Has::forge()->load($db);
		if($client_has->loaded()){
			$client_has->set_fields(array('listing_status' => self::LS_ACTIVE, 'date_expire' => date_create('+' . \Sysconfig::get('listing_length') . ' days')->format('Y-m-d H:i:s')));
			if($client_has->save()){
				$response['status'] = 'ok';
			}
		}
		return $response;
	}

	public function downgrade_listing($post){
		$response = array('status' => 'error');
		$db = \DB::select_array()->where('listing_id', '=', $post['listing_id']);
		$account_status = \Session::get('auth_realtor');
		$db->where('realtor_id', '=', $account_status->realtor_id);
		$db->order_by('creation_date', 'desc');
		$get_profile = \Model\Orders::forge()->load($db)->as_array();
		$profile_id = $get_profile['subscribers_id'];

		$config = \Fuel\Core\Config::load('paypal');
		$pp_obj = new \PayPal($config);

		$PayPalRequestData = \Helper\Payment::cancel(array(
			'profile_id' => $profile_id
		));
		$pp_response = $pp_obj->ManageRecurringPaymentsProfileStatus($PayPalRequestData);
		$response['pp_response'] = $pp_response;
		return $response;
	}

	public function get_client_has_display_value($feed, $key, $val){
		if($feed == self::FEED_NON_IMPORTED){
			$val = \Model\Client\Has::forge()->$key($val);
		}
		return $val;
	}

	public function build_left_option_list($data){
		$item = '';
		if(isset($data['bedrooms']) && $data['bedrooms'] != '0' && $data['bedrooms'] != ''){
			$item .= '<li><span>'.__('bed').'</span>: <strong>'.$data['bedrooms'].'</strong></li>';
		}
		if(isset($data['bathrooms']) && $data['bathrooms'] != '0' && $data['bathrooms'] != ''){
			$item .= '<li><span>'.__('bath').'</span>: <strong>'.$data['bathrooms'].'</strong></li>';
		}
		if(isset($data['sq_footage']) && !empty($data['sq_footage'])){
			$item .= '<li><span>'.__('size').'</span>: <strong>'.\Sysconfig::format_sqft($data['sq_footage']).'</strong></li>';
		}
		if(isset($data['prop_type']) && $data['prop_type'] != '0' && $data['prop_type'] != ''){
			$item .= '<li><span>'.__('type').'</span>: <strong>' .
			$this->get_client_has_display_value($data['feed'], 'prop_type', $data['prop_type']).'</strong></li>';
		}
		if(isset($data['finished_basement']) && $data['finished_basement'] != '0' && $data['finished_basement'] != ''){
			$item .= '<li><span>'.__('basement').'</span>: <strong>' .
			$this->get_client_has_display_value($data['feed'], 'finished_basement', $data['finished_basement']).'</strong></li>';
		}
		if ($item != '') {
			return '<ul class="unstyled option-list">'.$item.'</ul>';
		}
		return '';
	}

	public function build_right_option_list($data){
		$item = '';
		if(isset($data['p_title']) && !empty($data['p_title'])){
			$item .= '<li><span>'.__('title_title').'</span>: <strong>' .
			$this->get_client_has_display_value($data['feed'], 'p_title', $data['p_title']).'</strong></li>';
		}
		if(isset($data['prop_style']) && !empty($data['prop_style'])){
			$item .= '<li><span>'.__('style').'</span>: <strong>' .
			$this->get_client_has_display_value($data['feed'], 'prop_style', $data['prop_style']).'</strong></li>';
		}
		if(isset($data['garage_type']) && !empty($data['garage_type'])){
			$item .= '<li><span>'.__('garag').'</span>: <strong>' .
			$this->get_client_has_display_value($data['feed'], 'garage_type', $data['garage_type']).'</strong></li>';
		}
		if(isset($data['view']) && !empty($data['view'])){
			$item .= '<li><span>'.__('view').'</span>: <strong>' .
			$this->get_client_has_display_value($data['feed'], 'view', $data['view']) .'</strong></li>';
		}
		if(isset($data['year_built']) && !empty($data['year_built'])){
			$item .= '<li><span>'.__('ybuilt').'</span>: <strong>' .
			$this->get_client_has_display_value($data['feed'], 'year_built', $data['year_built']).'</strong></li>';
		}
		if(isset($data['lot_size']) && !empty($data['lot_size'])){
			$item .= '<li><span>'.__('lot').'</span>: <strong>'. \Sysconfig::format_sqft($data['lot_size']).'</strong></li>';
		}
		if(isset($data['neighbourhood']) && !empty($data['neighbourhood']) && $data['neighbourhood'] != 'none'){
			$item .= '<li><span>'.__('neigh').'</span>: <strong>'.$data['neighbourhood'].'</strong></li>';
		}
		if ($item != '') {
			return '<ul class="unstyled option-list">'.$item.'</ul>';
		}
		return '';
	}

	/**
	* Some of these will be options that may have more than one value
	*
	* @param mixed $data
	*/
	public function build_extra_option_list($data) {
		$item = '';
		// For now don't do imported
		if ($data['feed'] != 2) {
			$has = \Model\Client\Has::forge();
			if(isset($data['siding_type']) && !empty($data['siding_type'])) {
				$str = array();
				$vals = str_split($data['siding_type']);
				foreach($vals as $v) {
					$str[] = \Model\Client::forge()->siding_type($v);
				}
				$item .= '<div class="column"><span>'.__('siding_type').'</span>: <strong>'.implode(', ', $str).'</strong></div>';
			}
			if(isset($data['heat_source']) && !empty($data['heat_source'])) {
				$item .= '<div class="column"><span>'.__('heat_source').'</span>: <strong>'.$has->heat_source($data['heat_source']).'</strong></div>';
			}
			if(isset($data['heating']) && !empty($data['heating'])) {
				$item .= '<div class="column"><span>'.__('heat_type').'</span>: <strong>'.$has->heating($data['heating']).'</strong></div>';
			}
			if(isset($data['cooling']) && !empty($data['cooling'])) {
				$item .= '<div class="column"><span>'.__('cool_type').'</span>: <strong>'.$has->cooling($data['cooling']).'</strong></div>';
			}
			if(isset($data['water']) && !empty($data['water'])) {
				$item .= '<div class="column"><span>'.__('water_type').'</span>: <strong>'.$has->water($data['water']).'</strong></div>';
			}
			if(isset($data['sewer']) && !empty($data['sewer'])) {
				$item .= '<div class="column"><span>'.__('sewer_type').'</span>: <strong>'.$has->sewer($data['sewer']).'</strong></div>';
			}
			if(isset($data['floor_type']) && !empty($data['floor_type'])) {
				$str = array();
				$vals = str_split($data['floor_type']);
				foreach($vals as $v) {
					$str[] = $has->floor_type($v);
				}
				$item .= '<div class="column"><span>'.__('floor_type').'</span>: <strong>'.implode(', ', $str).'</strong></div>';
			}
			if(isset($data['roof_type']) && !empty($data['roof_type'])) {
				$str = array();
				$vals = str_split($data['roof_type']);
				foreach($vals as $v) {
					$str[] = $has->roof_type($v);
				}
				$item .= '<div class="column"><span>'.__('roof_type').'</span>: <strong>'.implode(', ', $str).'</strong></div>';
			}
			if(isset($data['school_info']) && !empty($data['school_info'])) {
				$item .= '<div class="column"><span>'.__('lsd').'</span>: <strong>'.$data['school_info'].'</strong></div>';
			}
			if(isset($data['youtube_link']) && !empty($data['youtube_link'])) {
				$item .= '<div class="column"><span>'.__('youtube_link').'</span>: <strong>'.\Html::anchor($data['youtube_link'], $data['youtube_link'], array('target' => '_blank')).'</strong></div>';
			}
			if(isset($data['virtual_tour_link']) && !empty($data['virtual_tour_link'])) {
				$item .= '<div class="column"><span>'.__('vt_link').'</span>: <strong>'.\Html::anchor($data['virtual_tour_link'],$data['virtual_tour_link'], array('target' => '_blank')).'</strong></div>';
			}
		}
		return $item;
	}

	public function prepare_search_view($result){
		\Lang::load('common');
		$return = array();
		foreach($result as $row){
			$location_names = \CityState::get_area_name($row['city'], $row['prov_state'], $row['country']);
			$row['full_address'] = $row['address'].'<br>'.$location_names.', ' . \CityState::format_zip($row['z_p_code']);
			// An id that includes the source
			$row['listing_id'] = $row['feed'] . '_' . $row['id'];
			if ($row['feed'] == self::FEED_IMPORTED) {
				$row['image'] = \Model\Crea\Property::forge()->get_image($row['listing_id'], 1, false);
				$row['year_built'] = $row['year_built'];
				$row['btn_detail'] = \Form::button('view', __('view_listing_detail'), array('class' => 'button small view-details', 'type' => 'button', 'data-id' => $row['feed'].'_'.$row['id']));
			} elseif ($row['feed'] == self::FEED_NON_IMPORTED) {
				//local listing
				$row['image'] = 'listings/' . \Model\Photos\Xtrade::forge()->get_first_photo($row['xlisting_id']);
				$row['prop_style'] = $this->filter_prop_style($row['prop_style']);
				$row['prop_type'] = $this->filter_prop_type($row['prop_type']);
				$row['view'] = $this->filter_views($row['view']);
				$row['year_built'] = \Model\Client\Has::forge()->year_built_txt($row['year_built']);
				$row['btn_detail'] = \Form::button('view', 'View Listing Details', array('class' => 'button small view-details', 'type' => 'button', 'data-id' => $row['feed'].'_'.$row['xlisting_id']));
			}
			if (!isset($row['image'])) {
				$row['image'] = 'no-image.png';
			}
			$row['left'] = $this->build_left_option_list($row);
			$row['right'] = $this->build_right_option_list($row);
			$return[] = $row;
		}
		return $return;
	}

	public function filter_min_price(){
		$return = array(
			25000, 50000, 75000, 100000, 125000, 150000, 175000, 200000, 225000, 250000, 275000, 300000,
			325000, 350000, 375000, 400000, 425000, 450000, 475000, 500000, 550000, 600000, 650000, 700000,
			750000, 800000, 850000, 900000, 950000, 1000000, 1250000, 1500000, 1750000, 2000000, 2250000,
			2500000, 2750000, 3000000, 3250000, 3500000, 3750000, 4000000, 4250000, 4500000, 4750000, 5000000,
			5500000, 6000000, 6500000, 7000000, 7500000, 8000000, 8500000, 9500000, 9000000, 10000000, 12000000,
			15000000, 20000000, 25000000, 30000000, 35000000, 40000000, 45000000, 50000000, 55000000, 60000000,
			65000000, 70000000, 75000000, 80000000, 85000000, 90000000, 95000000, 100000000
		);
		$r[''] = __('nolimit');
		$r[1] = '$0';
		foreach($return as $k => $item){
			$r[$item] = '$'.number_format((double)$item);
		}
		return $r;
	}

	public function filter_max_price(){
		$r = array(
			25000, 50000, 75000, 100000, 125000, 150000, 175000, 200000, 225000, 250000, 275000, 300000,
			325000, 350000, 375000, 400000, 425000, 450000, 475000, 500000, 550000, 600000, 650000, 700000,
			750000, 800000, 850000, 900000, 950000, 1000000, 1250000, 1500000, 1750000, 2000000, 2250000, 2500000,
			2750000, 3000000, 3250000, 3500000, 3750000, 4000000, 4250000, 4500000, 4750000, 5000000, 5500000, 6000000,
			6500000, 7000000, 7500000, 8000000, 8500000, 9000000, 9500000, 10000000, 12000000, 15000000, 20000000, 25000000,
			30000000, 35000000, 40000000, 45000000, 50000000, 55000000, 60000000, 65000000, 70000000, 75000000, 80000000,
			85000000, 90000000, 95000000, 100000000
		);

		$return[''] = __('nolimit');
		$return['9000000000'] = __('unlimited');
		foreach($r as $k => $item){
			$return[$item] = '$'.number_format((double)$item);
		}

		return $return;
	}

	public function want_request($post){
		$validation = \Validation::forge();
		$validation->add_field('email', __('email'), 'required');
		if($validation->run($post) === FALSE){
			return array('status' => 'ERROR', 'message' => $validation->show_errors());
		}
		$member = (array)\Authlite::instance('auth_realtor')->get_user();
		$key = \Sysconfig::create_string('key');
		$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']));
		$client_info = \Model\Client\Information::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']));
		if($listing->loaded()){
			$listing->set_fields(array(
				'client_fill_key' => $key,
				'lastupdated' => date_create()->format(DATETIME_FORMAT_DB)
			));
			$listing->save();
			$client_info->set_fields(array(
				'email' => $post['email']
			));
			$email_data = array(
				'emailto' => $post['email'],
				'r_name' => $member['first_name'].' '.$member['last_name'],
				'form_link' => 'listing/client_form/?key='.$key .'&id='. $post['listing_id'],
				'unsubscribe_link' => 'user/unsubscribe/?email='.$post['email'].'&type=realtor',
				'company' => $member['company_name']
			);
			$emails = \Emails::forge();
			return $emails->request_client_wants($email_data);
		}
	}

	public function save_client_wants_form($post){
		$validation = \Validation::forge();
		$validation->add_field('address', __('address'), 'required');
		$validation->add_field('neighbourhood', __('neigh'), 'required');
		$validation->add_field('sq_footage', __('sqfeet'), 'required');
		$validation->add_field('prop_type', __('ptype'), 'required');
		$validation->add_field('bedrooms', __('bed'), 'required');
		$validation->add_field('bathrooms', __('bath'), 'required');
		$validation->add_field('p_title', __('prop_title'), 'required');
		if($validation->run($post) === FALSE){
			throw new \AutomodelerException('error', array(), $validation->error_message());
		}
		$neighbourhoods = isset($post['neighbourhood']) ? $post['neighbourhood'] : array();
		unset($post['neighbourhood']);
		list($post['city'], $post['prov_state'], $post['country']) = \CityState::get_area_id($post['address'], TRUE);
		list($post['sq_footage_low'], $post['sq_footage_high']) = explode('|', $post['sq_footage']);
		$post['prop_style'] = $post['prop_style'] == '' || $post['prop_style'] == '9' ? '9' : join('',$post['prop_style']);
		$post['completed_section'] = \Model\Client::STATUS_COMPLETE;
		$post['siding_type'] = count($post['siding_type']) > 1 ? join('', $post['siding_type']) : $post['siding_type'][0];
		$post['garage_type'] = count($post['garage_type']) > 1 ? join('', $post['garage_type']) : $post['garage_type'][0];
		$post['floor_type'] = count($post['floor_type']) > 1 ? join('', $post['floor_type']) : $post['floor_type'][0];
		$post['roof_type'] = count($post['roof_type']) > 1 ? join('', $post['roof_type']) : $post['roof_type'][0];
		$post['prop_style'] = count($post['prop_style']) > 1 ? join('', $post['prop_style']) : $post['prop_style'][0];
		$client_wants = \Model\Client\Want::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']));
		if($client_wants->loaded()){
			$client_wants->set_fields($post);
			if($client_wants->save()){
				\Model\Neighbourhoods::forge()->set_wants_neighbourhood($neighbourhoods, $post['listing_id']);
				$data['realtor'] = \Model\Realtor::forge()->load(\DB::select_array()->where('realtor_id', '=', $client_wants->realtor_id))->as_array();
				$data['client'] = \Model\Client\Information::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']))->as_array();
				$data['client_has'] = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $post['listing_id']))->as_array();
				\Emails::forge()->complete_client_wants($data);
				$client_has = \Model\Client\Has::forge()->load(\DB::select_array()->where('client_fill_key', '=', $post['key']));
				$client_has->set_fields(array(
					'client_fill_key' => ''
				));
				if ($client_has->loaded()) {
					$client_has->save();
				}
				return array('message' => __('data_updated'), 'status' => 'OK');
			}
		}
	}

	public function search($location, $params = array(), $sortby = 3, $limit = 5, $page = 1){
		$data['result'] = array();
		$data['records_num'] = 0;
		$data['paginate'] = '';
		$area = \CityState::get_area_id($location);
		$area_array = explode(',', $area);

		if(count($area_array) < 3){
			return $data;
		}

		// Need either Canada or US country defined
		if (in_array($area_array[2], array(\Model\User::COUNTRY_CA, \Model\User::COUNTRY_US))){
			$where0 = $where2 = array();
			$filter = (isset($params['filter'])) ? $params['filter'] : array();
			$filter['city'] = $area_array[0];
			$filter['prov_state'] = $area_array[1];
			$filter['country'] = $area_array[2];
			// Make the filter keys to placeholders
			if (isset($filter['price_l']) && !empty($filter['price_l']) && $filter['price_l'] != '') {
				$where0[] = 'listing_price BETWEEN :price_l AND :price_h';
				$where2[] = 'price BETWEEN :price_l AND :price_h';
			} else {
				$where0[] = 'listing_price BETWEEN 1 AND 9000000000';
				$where2[] = 'price BETWEEN 1 AND 9000000000';
			}

			if (isset($filter['bedrooms']) && array_key_exists($filter['bedrooms'], \Model\Listing::forge()->filter_beds()) && !empty($filter['bedrooms'])) {
				$where0[] = 'bedrooms >= :bedrooms';
				$where2[] = 'bedroomstotal >= :bedrooms';
			} else {
				$where0[] = 'bedrooms >= 1';
				$where2[] = 'bedroomstotal >= 1';
			}

			if (isset($filter['bathrooms']) && array_key_exists($filter['bathrooms'], \Model\Listing::forge()->filter_baths()) && !empty($filter['bathrooms'])) {
				$where0[] = 'bathrooms >= :bathrooms';
				$where2[] = 'bathroomtotal >= :bathrooms';
			} else {
				$where0[] = 'bathrooms >= 1';
				$where2[] = 'bathroomtotal >= 1';
			}

			//multiple select
			if(isset($filter['neighbourhood']) && count($filter['neighbourhood']) > 0 && !in_array('', $filter['neighbourhood'])){
				$filter['neighbourhood'] = join('|', $filter['neighbourhood']);
				$where0[] = 'neighbourhood REGEXP :neighbourhood';
				$where2[] = 'neighbourhood REGEXP :neighbourhood';
			}

			if(isset($filter['prop_type']) && $filter['prop_type'] != ''){
				$filter['prop_type_a'] = \Model\Listing::forge()->filter_prop_type($filter['prop_type'], FALSE);
				$filter['prop_type_b'] = \Model\Listing::forge()->filter_prop_type($filter['prop_type'], TRUE);
				if($filter['prop_type'] == 'c'){
					$where0[] = 'prop_type = :prop_type';
					//$where2[] = '(propertytype = :prop_type_a OR propertytype = :prop_type_b OR propertytype = \'a\' OR propertytype REGEXP \'Apartment|Condominium\')';
				} else {
					$where0[] = '(prop_type = :prop_type_a OR prop_type = :prop_type_b)';
					//$where2[] = '(propertytype = :prop_type_a OR propertytype = :prop_type_b)';
				}

			}

			//multiple select
			if(isset($filter['prop_style']) && count($filter['prop_style']) > 0 && !in_array('', $filter['prop_style'])){
				$filter['prop_style_a'] = join('|', $filter['prop_style']);
				$filter['prop_style_b'] = join('|', $this->filter_property_styles_p2($filter['prop_style']));
				$where0[] = 'prop_style REGEXP :prop_style_a';
				$where2[] = 'type REGEXP :prop_style_b';
			}

			// Get a list of matching fields
			$xref = \Model\Crea\Xref::forge()->load(\DB::select_array()->where('localfld', '<>', '')
				->order_by('dbtable', 'asc')
				->order_by('tblfield', 'asc'), NULL)->as_array();

			// xTrade listings
			$sql = "((SELECT '0' AS feed, featured_listing, client_has.id AS id, users.company_name AS broker, client_has.listing_id as xlisting_id,'' AS analyticsview";
			foreach ($xref as $row) {
				$sql .= ", client_has.".  strtolower($row['localfld']);
			}
			$sql .= ', NULL AS arch';
			$sql .= ' FROM client_has JOIN users ON users.realtor_id = client_has.realtor_id';
			$sql .= ' WHERE client_has.city = :city AND client_has.prov_state = :prov_state AND client_has.country = :country';
			$sql .= ' AND client_has.listing_status = '.self::LS_ACTIVE.' AND client_has.imported != '.self::IMPORTED.' AND client_has.type_of_listing != '. \Model\Listing::TYPE_BUY_ONLY;
			if (!empty($where0)) {
				$sql .= ' AND ' . implode(' AND ', $where0);
			}

			$sql .= ') UNION ';

			// Crea listings
			$sql .= "(SELECT '2' AS feed, crea_property.featured_listing, crea_property.id AS id, crea_agents.officename AS broker, crea_property.listingid as xlisting_id, analyticsview";
			foreach ($xref as $row) {
				$sql .= strtolower(", {$row['dbtable']}.{$row['tblfield']} AS {$row['localfld']}");
			}
			$sql .= ', crea_building.architecturalstyle AS arch';
			$sql .= ' FROM crea_property';
			$sql .= ' LEFT JOIN crea_building ON crea_building.id = crea_property.id';
			$sql .= ' LEFT JOIN crea_land ON crea_land.id = crea_property.id';
			$sql .= ' LEFT JOIN crea_property_agents ON crea_property_agents.prop_id = crea_property.id';
			$sql .= ' LEFT JOIN crea_agents ON crea_agents.id = crea_property_agents.agent_id';
			$sql .= ' WHERE active = 1 AND city = :city AND prov_state = :prov_state AND country = :country';
			if (!empty($where2)) {
				$sql .= ' AND ' . implode(' AND ', $where2);
			}
			$sql .= ')) AS listings';

			$orderby = $this->db_translate_sort_field($sortby);
			$sort = ' ORDER BY '.$orderby;

			if (isset($params['sort']) && array_key_exists($params['sort'], $this->sort_fields())
			&& ($params['sort'] != 0)) {
				$sort .= ' , '.$this->sort_fields($params['sort'], FALSE);
			}

			// Load all the id's for the details prev / next array
			$result = \DB::query('SELECT feed,id FROM '. $sql . $sort)->parameters($filter)->cached(1800)->execute();
			$total = count($result);
			$ids = array();
			foreach($result as $row) {
				$ids[] = $row['feed'] . '_' . $row['id'];
			}
			\Session::set(self::SESS_NAVIGATION, $ids);

			$query = 'SELECT * FROM '. $sql . $sort . ' LIMIT ' . ($page - 1) * $limit. ',' . $limit;
			// Cache the results for 30 minutes
			$result = \DB::query($query)->parameters($filter)->cached(1800)->execute()->as_array();
			$pagin_config = array(
				'pagination_url' => \Uri::base(false) . 'listing/search/'.$location.'.html',
				'total_items' => $total,
				'per_page' => $limit,
				'uri_segment' => 'page'
			);
			$data = array();
			$data['paginate'] = \Pagination::forge('pagination_search', $pagin_config);
			$search_view_result = $this->prepare_search_view($result);
			$data['result'] = $search_view_result;
			$data['records_num'] = $total;
		}

		return $data;
	}

	public function listing_navigation($listing_id){
		if(\Session::get(self::SESS_REFERER_NAME)){
			$back_button = \Session::get(self::SESS_REFERER_NAME);
		} else {
			$back_button = \Uri::base();
		}
		$return = \Html::anchor($back_button, __('back_nav'));
		$listing_pagination = !\Session::get(self::SESS_NAVIGATION) ? array() : \Session::get(self::SESS_NAVIGATION);
		if(empty($listing_pagination)){
			return $return;
		}
		if(\Session::get('SESS_REFFERER_DASHBOARD')){
			return $return;
		}
		$pos = array_search($listing_id, $listing_pagination);
		if ($pos != 0) {
			$return = \Html::anchor('listing/detail/'.$listing_pagination[$pos - 1], __('prev')) . ' | ' . $return ;
		}
		if ($pos != (count($listing_pagination) - 1)) {
			$return = $return . ' | ' . \Html::anchor('listing/detail/'.$listing_pagination[$pos + 1], __('next'));
		}
		return $return;
	}

	public function load_listing_agent($listing_id){
		//default data
		$data = array( 'name' => '', 'mobile' => '',
			'phone' => '', 'broker' => '', 'pos' => '' );
		list($feed, $id) = explode('_', $listing_id);
		if($feed == self::FEED_IMPORTED){
			$data = \DB::select_array(array('name', 'phone', 'mobile', array('officename', 'broker'), 'pos'))
			->from(\Model\Crea\Agent::forge()->get_table_name())
			->where('prop_id', $id)
			->join('crea_property_agents', 'left')->on('id', '=', 'agent_id')
			->order_by('pos', 'desc')->execute()->current();
		} elseif($feed == self::FEED_NON_IMPORTED) {
			$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $id));
			$data = \DB::select_array(array(\DB::expr("CONCAT(users.first_name, ' ', users.last_name) as name"), 'users.phone', array('users.cell', 'mobile'), array('brokers.company_name', 'broker'), 'users.company_name'))
			->from('users')
			->where('realtor_id', '=', $listing['realtor_id'])
			->join('brokers', 'left')
			->on('brokers.id', '=', 'users.office_id')
			->execute()->current();
			if( empty($data) ) {
				$data['name'] = $data['phone'] =
				$data['mobile'] = $data['broker'] = $data['company_name'] = '';
			}
			if($data['broker'] == ''){
				$data['broker'] = $data['company_name'];
			}
			$data['pos'] = ''; //FIX ME!
		}
		return $data;
	}

	public function update_listing($post){
		\Lang::load('signup');
		\Lang::load('listing');

		$client_info = \Model\Client\Information::forge()->load_by_listing_id($post['listing_id']);
		$client_has = \Model\Client\Has::forge()->load_by_listing_id($post['listing_id']);
		$client_wants = \Model\Client\Want::forge()->load_by_listing_id($post['listing_id']);

		//directly save the listing status if user is admin
		//and listing status is defined in the model
		if ( isset($post['has']['listing_status'])
		&& in_array($post['has']['listing_status'], \Model\Admin::forge()->allow_listing_incomplete_status())
		&& \Authlite::instance('auth_admin')->get_user())
		{
			$client_has->listing_status = $post['has']['listing_status'];
			$client_has->save();
			return $this->status_completion($post['listing_id'], true);
		} try {
			$client_info->update_client_info($post['info']);
		} catch(\AutomodelerException $e){
			return array('errors' => $e->errors, 'area' => 'client_info', 'fields' => 'info', 'matches' => __('refine_wants'));
		}
		if($client_has->type_of_listing == self::TYPE_SELL_ONLY || $client_has->type_of_listing == self::TYPE_XTRADED){
			try {
				$client_has->update_client_has($post['has']);
			} catch(\AutomodelerException $e){
				return array('errors' => $e->errors, 'area' => 'client_has', 'fields' => 'has', 'matches' => __('refine_wants'));
			}
		}
		if($client_has->type_of_listing == self::TYPE_BUY_ONLY || $client_has->type_of_listing == self::TYPE_XTRADED){
			try {
				$client_wants->update_client_wants($post['wants']);
			} catch(\AutomodelerException $e){
				return array('errors' => $e->errors, 'area' => 'client_wants', 'fields' => 'wants', 'matches' => __('refine_wants'));
			}
		}
		$match = \Match::forge();
		$match->reset_record($post['listing_id']);
		$match->remove_matches($post['listing_id']);
		$match->matches($post['listing_id']);
		return $this->status_completion($post['listing_id'], false);
	}

	public function status_completion($listing_id, $admin_ls){
		$client_info = \Model\Client\Information::forge()->load_by_listing_id($listing_id);
		$client_has = \Model\Client\Has::forge()->load_by_listing_id($listing_id);
		$client_wants = \Model\Client\Want::forge()->load_by_listing_id($listing_id);
		$xtrade_photos = \Model\Photos\Xtrade::forge()->load(\DB::select()->where('listing_id', '=', $client_has->listing_id)
			->order_by('img_num', 'asc')->limit(1));
		if($client_info->completed_section == \Model\Client::STATUS_INCOMPLETE){
			$listing_status = ($admin_ls === true) ? $client_has->listing_status : self::LS_XCOMPLETE;
			$return = \Model\Client::CC_INFO_INCOMPLETE;
		}elseif($client_has->completed_section == \Model\Client::STATUS_INCOMPLETE && $client_has->type_of_listing != self::TYPE_BUY_ONLY){
			$listing_status = ($admin_ls === true) ? $client_has->listing_status : self::LS_XCOMPLETE;
			$return = \Model\Client::CC_HAS_INCOMPLETE;
		}elseif($xtrade_photos->img_num == \Model\Client::STATUS_INCOMPLETE && $client_has->type_of_listing != self::TYPE_BUY_ONLY
		&& $client_has->imported != self::IMPORTED){
			$listing_status = ($admin_ls === true) ? $client_has->listing_status : self::LS_XCOMPLETE;
			$return = \Model\Client::CC_PICTURES_INCOMPLETE;
		}elseif($client_wants->completed_section == \Model\Client::STATUS_INCOMPLETE && $client_has->type_of_listing != self::TYPE_SELL_ONLY){
			$listing_status = ($admin_ls === true) ? $client_has->listing_status : self::LS_XCOMPLETE;
			$return = \Model\Client::CC_WANTS_INCOMPLETE;
		}else{
			$listing_status = ($admin_ls === true) ? $client_has->listing_status : self::LS_ACTIVE;
			$return = $client_has->imported == self::IMPORTED ? \Model\Client::CC_SUCCESS_IMPORTED : \Model\Client::CC_SUCCESS;
		}
		$client_has->listing_status = $listing_status;
		$client_has->save();
		if (\Session::get('type') === \Model\User::UT_ADMIN) {
			$url = '/admins/listing/completed.html';
		} else {
			$url = '/listing/completed.html';
		}
		if($client_has->type_of_listing == \Model\Listing::TYPE_XTRADED){
			$response = array('status' => $return, 'matches' => $this->get_matches($client_has), 'ok' => 'OK', 'url' => $url);
		}else{
			$response = array('status' => $return, 'matches' => '', 'ok' => 'OK', 'url' => $url);
		}
		return $response;
	}

	public function get_matches($client_has){
		$match_txt = '';
		if($client_has->type_of_listing == \Model\Listing::TYPE_XTRADED){
			$match_txt = '<strong class="matches-link" data-target="/realtors/match/id/'.$client_has->listing_id.'">'.__('too_many_matches').'</strong>';
			/**
			*
			if($client_has->matches_found < \Sysconfig::get('max_matches')){
			$matches = \Match::forge();
			$match_txt = $matches->matches($client_has->listing_id);
			$matches->notify_all_matched_listings($client_has->listing_id);
			}
			*/
			$matches = \Match::forge();
			$match_txt = $matches->matches($client_has->listing_id);
			$matches->notify_all_matched_listings($client_has->listing_id);
		}
		return $match_txt;
	}

	public function sort_view($sort_view){
		$return = array();
		switch($sort_view){
			case 'all_listings':
				$value = array(self::LS_ACTIVE,
					self::LS_EXPIRED,
					self::LS_HIDDEN_PUBLIC,
					self::LS_XTRADED,
					self::LS_XCOMPLETE_IMPORT,
					self::LS_XCOMPLETE,
					self::LS_PENDING_EX);
				if(\Authlite::instance('auth_admin')->logged_in()){
					$value[] = self::LS_DELETED;
				}
				$return['col'] = 'client_has.listing_status';
				$return['operator'] = 'REGEXP';
				$return['val'] = join('|', $value);
				break;
			case 'active_listings':
				$value = array(self::LS_ACTIVE,
					self::LS_XCOMPLETE_IMPORT,
					self::LS_XCOMPLETE,
					self::LS_PENDING_EX);
				$return['col'] = 'client_has.listing_status';
				$return['operator'] = 'REGEXP';
				$return['val'] = join('|', $value);
				break;
			case 'inactive_listings':
				$value = array(self::LS_EXPIRED, self::LS_HIDDEN_PUBLIC);
				$return['col'] = 'client_has.listing_status';
				$return['operator'] = 'REGEXP';
				$return['val'] = join('|', $value);
				break;
			case 'xtrade_listings':
				$return['col'] = 'client_has.type_of_listing';
				$return['operator'] = '=';
				$return['val'] = self::TYPE_XTRADED;
				break;
			case 'seller_listings':
				$return['col'] = 'client_has.type_of_listing';
				$return['operator'] = '=';
				$return['val'] = self::TYPE_SELL_ONLY;
				break;
			case 'buyer_listings':
				$return['col'] = 'client_has.type_of_listing';
				$return['operator'] = '=';
				$return['val'] = self::TYPE_BUY_ONLY;
				break;
			case 'sold':
				$return['col'] = 'client_has.list_as_sold';
				$return['operator'] = '=';
				$return['val'] = self::AS_SOLD;
				break;
			case 'imported':
				$return['col'] = 'client_has.imported';
				$return['operator'] = '=';
				$return['val'] = self::IMPORTED;
				break;
			case 'exclusive':
				$return['col'] = 'client_has.imported';
				$return['operator'] = '=';
				$return['val'] = self::NON_IMPORTED;
				break;
			case 'ignored':
				$return['col'] = 'client_has.type_of_listing';
				$return['operator'] = '=';
				$return['val'] = self::TYPE_SELL_ONLY;
				break;
			case 'missing_wants':
				// I dont believe the following is correct. made logic changes
				/*
				$value = array($use::LS_EXPIRED, $use::LS_XCOMPLETE_IMPORT, $use::LS_XCOMPLETE);
				$return['col'] = 'client_has.listing_status';
				$return['operator'] = 'REGEXP';
				$return['val'] = join('|', $value);
				*/
				$return['col'] = 'client_wants.completed_section';
				$return['operator'] = '=';
				$return['val'] = 0;
				break;
			case 'check_for_matches':
				$return['col'] = 'client_has.matches';
				$return['operator'] = '>=';
				$return['val'] = 1;
				break;
			case 'featured_listings':
				$return['col'] = 'client_has.featured_listing';
				$return['operator'] = '=';
				$return['val'] = self::AS_FEATURED;
				break;
		}

		return $return;
	}

	public function user_saved_search(){
		$data = array(
			'sort' => self::DEFAULT_SORT_VALUE,
			'limit' => self::DEFAULT_ITEMS_PER_PAGE,
			'neighbourhood' => array(''),
			'prop_style' => array(''),
			'price_l' => 1,
			'price_h' => 9000000000,
			'bedrooms' => 1,
			'bathrooms' => 1,
			'prop_type' => ''
		);
		return $data;
	}

	/**
	* Load all agent listing from property agent table
	*
	* @param String $realtor_id Realtor ID
	* @return Array Sets of listing
	*/
	public function downloaded_listing($realtor_id){
		$data = array();
		$result = \DB::select_array()->from('client_has')->where('realtor_id', '=', $realtor_id)
		->where('imported', '=', \Model\Listing::IMPORTED)->execute()->as_array();
		foreach($result as $row){
			$row['ext_id'] = $row['external_id'];
			$row['address'] = ($row['address'] == '') ? __('addr_notlisted') : $row['address'];
			$row['imported'] = __('imported');
			$data[] = $row;
		}
		return $data;
	}

	/**
	* find all required fields for both client_has or client_wants,
	* if there is a field is blank, it will disable the finding match process
	*
	* @param String $listing_id Listing ID
	* @return boolean listing is ready for match process or not
	*/
	public function is_ready_for_xtrade($listing_id) {
		\Lang::load('signup');
		\Lang::load('listing');
		$return = true;
		$has = \Model\Client\Has::forge()->load_by_listing_id($listing_id);
		if ($has->loaded()) {
			if ($has->type_of_listing != \Model\Listing::TYPE_BUY_ONLY) {
				$valrules = $has->get_valrules();
				$data = $has->as_array();
				$data['c_address'] = \CityState::get_area_name($has->city, $has->prov_state, $has->country);
				foreach($valrules as $field => $rules){
					if($rules[1] == 'required' && $data[$field] == ''){
						$return = false;
						break;
					}
				}
			}
		} else {
			$return = false;
		}
		$wants = \Model\Client\Want::forge()->load_by_listing_id($listing_id);
		if ($wants->loaded()) {
			$valrules = $wants->get_valrules();
			$data = $wants->as_array();
			$data['address'] = \CityState::get_area_name($wants->city, $wants->prov_state, $wants->country);
			foreach($valrules as $field => $rules){
				if($rules[1] == 'required' && $data[$field] == ''){
					$return = false;
					break;
				}
			}
		}
		return $return;
	}
}