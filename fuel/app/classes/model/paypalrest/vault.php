<?php
namespace Model\Paypalrest;

class Vault extends \Model\Paypalrest {
	protected $_cc_type = array( '' => 'Select', 'visa' => 'Visa', 'mastercard' => 'Mastercard' );

	public function get_cc_type(){
		return $this->_cc_type;
	}

	public function store($data){
		$card = new \PayPal\Api\CreditCard();
		$card->setType(strtolower($data['cc_type']));
		$card->setNumber($data['cc_number']);
		$card->setExpireMonth($data['exp_month']);
		$card->setExpireYear($data['exp_year']);
		$card->setCvv2($data['cc_cvv']);
		$card->setFirstName($data['first_name']);
		$card->setLastName($data['last_name']);
		$card->setBillingAddress(new \PayPal\Api\Address(array(
			'line1' => $data['address'],
			'city' => trim($data['city']),
			'country_code' => \Sysconfig::country_code(trim($data['country'])),
			'postal_code' => $data['z_p_code'],
			'state' => \CityState::get_state_code(trim($data['prov_state']), trim($data['country']))
		)));
		$card->setPayerId($data['email']);
		try {
			$output = $card->create($this->_api_context);
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			return array('error' => $e->getMessage());
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			return array('error' => $e->getMessage());
		}
		return $output;
	}


	public function retrieve($pp_sub_id){
		$data = array('id' => '', 'first_name' => '', 'last_name' => '', 'address' => '', 'cpc_address' => '',
			'z_p_code' => '', 'cc_number' => '', 'cc_type' => '', 'exp_month' => '', 'exp_year' => '',
			'cc_cvv' => '', 'state' => '');
		try {
			$card = \PayPal\Api\CreditCard::get($pp_sub_id, $this->_api_context);
			$data['card_id']				= $card->id;
			$data['first_name']		= $card->first_name;
			$data['last_name']		= $card->last_name;
			$data['address']		= $card->billing_address->line1;
			$data['cpc_address']	= $this->card_cpc_address($card);
			$data['z_p_code']		= $card->billing_address->postal_code;
			$data['cc_number']		= $card->number;
			$data['cc_type']		= $card->type;
			$data['exp_month']		= sprintf("%02d", $card->expire_month);
			$data['exp_year']		= $card->expire_year;
			$data['cc_cvv']			= $card->cvv2;
			$data['state']			= $card->state;
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			$data['error'] = $e->getMessage();
			return $data;
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			$data['error'] = $e->getMessage();
			return $data;
		}
		$data['select_cc_type'] = $this->get_cc_type();
		return $data;
	}

	public function update($data){

		//retrieve the card
		$card = \PayPal\Api\CreditCard::get($data['card_id'], $this->_api_context);
		$pathRequest = new \PayPal\Api\PatchRequest();

		//exp month
		$pathOperation4 = new \PayPal\Api\Patch();
		$pathOperation4->setOp('replace');
		$pathOperation4->setPath('/expire_month');
		$pathOperation4->setValue($data['exp_month']);
		$pathRequest->addPatch($pathOperation4);

		//exp year
		$pathOperation5 = new \PayPal\Api\Patch();
		$pathOperation5->setOp('replace');
		$pathOperation5->setPath('/expire_year');
		$pathOperation5->setValue($data['exp_year']);
		$pathRequest->addPatch($pathOperation5);

		//fname
		$pathOperation6 = new \PayPal\Api\Patch();
		$pathOperation6->setOp('replace');
		$pathOperation6->setPath('/first_name');
		$pathOperation6->setValue($data['first_name']);
		$pathRequest->addPatch($pathOperation6);

		//lname
		$pathOperation7 = new \PayPal\Api\Patch();
		$pathOperation7->setOp('replace');
		$pathOperation7->setPath('/last_name');
		$pathOperation7->setValue($data['last_name']);
		$pathRequest->addPatch($pathOperation7);

		//billing addr object
		$location = array(
			'line1' => $data['address'],
			'city' => trim($data['city']),
			'country_code' => \Sysconfig::country_code(trim($data['country'])),
			'state' => \CityState::get_state_code(trim($data['prov_state']), trim($data['country'])),
			'postal_code' => $data['z_p_code']
		);
		$pathOperation8 = new \PayPal\Api\Patch();
		$pathOperation8->setOp('replace');
		$pathOperation8->setPath('/billing_address');
		$pathOperation8->setValue(new \PayPal\Api\Address($location));
		$pathRequest->addPatch($pathOperation8);

		try {
			$card->update($pathRequest, $this->_api_context);
			$return['status'] = 'OK';
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			$return['error'] = $e->getMessage();
			return $return;
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			$return['error'] = $e->getMessage();
			return $return;
		}
		return $return;
	}

	public function card_cpc_address($card){
		$country = array_flip(\Sysconfig::country_code(null));
		$country = $country[$card->billing_address->country_code];
		$cpc_address = $card->billing_address->city.', '.\CityState::get_state_name($card->billing_address->state, $country).', '.$country;
		return $cpc_address;
	}

}