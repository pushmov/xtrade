<?php
namespace Model\Paypalrest;

class Payment extends \Model\Paypalrest {
	protected $_cc_type = array( '' => 'Select', 'visa' => 'Visa', 'mastercard' => 'Mastercard' );

	public function get_cc_type(){
		return $this->_cc_type;
	}

	public function send($data){
		$card = new \PayPal\Api\CreditCard();
		$card->setType(strtolower($data['cc_type']));
		$card->setNumber($data['cc_number']);
		$card->setExpireMonth($data['exp_month']);
		$card->setExpireYear($data['exp_year']);
		$card->setCvv2($data['cc_cvv']);
		$card->setFirstName($data['first_name']);
		$card->setLastName($data['last_name']);
		$card->setBillingAddress(new \PayPal\Api\Address(array(
			'line1' => $data['address'],
			'city' => $data['city'],
			'country_code' => $data['country'],
			'postal_code' => $data['z_p_code'],
			'state' => $data['prov_state'],
		)));

		$fi = new \PayPal\Api\FundingInstrument();
		$fi->setCreditCard($card);

		$payer = new \PayPal\Api\Payer();
		$payer->setPaymentMethod('credit_card');
		$payer->setFundingInstruments(array($fi));

		$details = new \PayPal\Api\Details();
		$details->setTax($data['tax']);
		$details->setSubtotal($data['fee']);

		$amount = new \PayPal\Api\Amount();
		$amount->setCurrency($this->_currency[$data['country']]);
		$amount->setTotal($data['fee'] + $data['tax']);
		$amount->setDetails($details);

		$transaction = new \PayPal\Api\Transaction();
		$transaction->setAmount($amount);
		$transaction->setDescription($data['item_description']);
		$transaction->setInvoiceNumber($data['order_id']);

		$payment = new \PayPal\Api\Payment();
		$payment->setIntent("sale");
		$payment->setPayer($payer);
		$payment->setTransactions(array($transaction));

		try {
			$output = $payment->create($this->_api_context);
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			return array('error' => $e->getMessage());
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			return array('error' => $e->getMessage());
		}
		return $output;
	}

	public function get_info($sub_id) {


	}
}