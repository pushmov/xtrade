<?php
namespace Model\Paypalrest;

class Plan extends \Model\Paypalrest {
	const STATE_CREATED = 'CREATED';
	const STATE_ACTIVE = 'ACTIVE';
	const STATE_INACTIVE = 'INACTIVE';
	const STATE_DELETED = 'DELETED';

	protected $_state = array('CREATED' => 'Created', 'ACTIVE' => 'Active', 'INACTIVE' => 'Inactive', 'DELETED' => 'Deleted');

	public function get_state($plan_id=null){
		if($plan_id === null){
			return $this->_state;
		}

		try {
			$plan = \PayPal\Api\Plan::get($plan_id, $this->_api_context);
			$return = $plan->getState();
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			$return['error'] = $e->getMessage();
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			$return['error'] = $e->getMessage();
		}
		return $return;
	}

	public static function all($params, $apiContext = null, $restCall = null)
	{
		ArgumentValidator::validate($params, 'params');
		$payLoad = "";
		$allowedParams = array(
			'page_size' => 1,
			'status' => 1,
			'page' => 1,
			'total_required' => 1
		);
		$json = self::executeCall(
			"/v1/payments/billing-plans/" . "?" . http_build_query(array_intersect_key($params, $allowedParams)),
			"GET",
			$payLoad,
			null,
			$apiContext,
			$restCall
		);
		$ret = new PlanList();
		$ret->fromJson($json);
		return $ret;
	}

	public function delete($plan_id){
		$plan = \PayPal\Api\Plan::get($plan_id, $this->_api_context);
		try {
			$output = $plan->delete($this->_api_context);
		} catch (\PayPal\Exception\PayPalConnectionException $e){
			return array('error' => $e->getMessage());
		} catch (\PayPal\Exception\PayPalMissingCredentialException $e){
			return array('error' => $e->getMessage());
		}
		return $output;
	}

	public function update_state($data){
		$plan = \PayPal\Api\Plan::get($data['plan_id'], $this->_api_context);
		$patch = new \PayPal\Api\Patch();
		$value = new \PayPal\Common\PayPalModel('{
			"state":"'.$data['state'].'"
		}');
		$patch->setOp('replace')
		->setPath('/')
		->setValue($value);
		$patchRequest = new \PayPal\Api\PatchRequest();
		$patchRequest->addPatch($patch);
		$plan->update($patchRequest, $this->_api_context);
		return \PayPal\Api\Plan::get($plan->getId(), $this->_api_context);
	}
}