<?php
namespace Model;

class Location extends \Automodeler {

	private static function _get_($select, $from, $conditions) {

		$result = \DB::select($select)
		->from($from)
		->where($conditions['key'], $conditions['val'])
		->execute();

		return $result->current();
	}

	public static function get_area_name($city, $province, $country) {
		$location = array();

		if($country === "1"){
			$country = "Canada";
			$prov_state_select = "ca_provinces";
			$sort_by = "id";
			$city_db_select = "ca_cities";
			$city_sort = "province";
			$city_select = "name";

		}elseif($country === "2"){
			$country = "United States";
			$prov_state_select = "us_states";
			$sort_by = "id";
			$city_db_select = "us_cities";
			$city_sort = "state";
			$city_select = "name";
		}

		$sprov_state = self::_get_($city_sort, $prov_state_select, array('key' => $sort_by, 'val' => $province));
		$scity = self::_get_($city_select, $city_db_select, array('key' => $sort_by, 'val' => $city));

		array_push($location, $scity[$city_select]);
		array_push($location, $sprov_state[$city_sort]);
		array_push($location, $country);

		return $location;
	}

	public static function load_us_info($city, $prov_state){
		$result = \DB::query("SELECT name, state FROM us_cities, us_states WHERE us_cities.id = ".$city." AND us_states.id = ".$prov_state)->execute();
		return $result->current();
	}

	public static function load_ca_info($city, $prov_state){
		$result = \DB::query("SELECT name, province FROM ca_cities, ca_provinces WHERE ca_cities.id = ".$city." AND ca_provinces.id = ".$prov_state)->execute();
		return $result->current();
	}

	public static function update_lat_ln($listing_id, $lat, $lng){
		$result = \DB::update('client_has')
		->set(array(
			'lat' => $lat,
			'lng' => $lng
		))
		->where('listing_id', $listing_id)
		->execute();

		return $result;
	}

}
