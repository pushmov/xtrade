<?php
namespace Model;

class Tutorial extends \Model\Client\Has {
	
	const LISTING_ID = 'NA5377587207';
	
	public $tut_data = array(
		'listing_id' => self::LISTING_ID, 'realtor_id' => 'tutorial', 'country' => 1, 'prov_state' => 2, 'city' => 3202, 
		'address' => '12345 Tutorial St.', 'z_p_code' => 'V1V2V1', 'prop_type' => 'a', 'bedrooms' => 2, 'bathrooms' => 2, 
		'year_built' => 0, 'prop_style' => 'f', 'sq_footage' => '2300', 'listing_price' => '540000', 'neighbourhoods' => 'none', 
		'lat' => '49.2827291', 'lng' => '-123.1207375', 'external_id' => 'Tutorial', 'listing_status' => \Model\Listing::LS_XCOMPLETE
	);
	public $tut_picture = array(
		'listing_id' => 'NA5377587207', 'pos' => 1, 'style' => 1, 'img_num' => 1, 'url' => 'gallery.jpg'
	);

	public function load_listing(){
		//init data
		$this->init_data();
		
		//init picture
		$pmodel = \Model\Photos\Xtrade::forge();
		$photo = $pmodel->load(\DB::select()->where('listing_id', '=', self::LISTING_ID));
		if(!$photo->loaded()){
			$pmodel->set_fields($this->tut_picture);
			$pmodel->save();
		}
		$params = array(
			'page' => 1,
			'sort_order' => 'lp_hl',
			'sort_view' => 'all_listings',
			'term' => ''
		);
		\Lang::load('common');
		$load_all = \Model\Matches::forge()->load_all_listings($params, $tutorial=true);
		$query_builder = $load_all['query_builder'];
		$data['account_status'] = \Model\Realtor::forge()->account_status();
		$data['result'] = \Model\Listing::forge()->list_ajax_format($query_builder, $tutorial=true);
		$data['tut'] = '';
		$data['pagination'] = '';
		return $data;
	}
	
	public function load_dialog_view($params){
		return $params['page'].'_'.$params['step'];
	}
	
	public function init_data(){
		$has = \Model\Client\Has::forge();
		$has->load(\DB::select()->where('listing_id', '=', self::LISTING_ID));
		if(!$has->loaded()){
			$has->set_fields($this->tut_data);
			$has->save();
		}
		
		$info = \Model\Client\Information::forge();
		$info->load(\DB::select()->where('listing_id', '=', self::LISTING_ID));
		if(!$info->loaded()){
			$info->listing_id = self::LISTING_ID;
			$info->save();
		}
		
		$want = \Model\Client\Want::forge();
		$want->load(\DB::select()->where('listing_id', '=', self::LISTING_ID));
		if(!$want->loaded()){
			$want->listing_id = self::LISTING_ID;
			$want->save();
		}
		
	}
	
}