<?php
namespace Model;

class Tax extends \Automodeler {
	protected $_class_name = 'tax';
	protected $_table_name = 'tax_rates';
	protected $_data = array('id' => null, 'country' => '', 'prov_state' => '', 'code' => '', 'rate' => 0);

	public function load_info($location) {
        $cpc = \CityState::get_area_id($location, true);
        if (isset($cpc[2])) {
        	$this->load(\DB::select_array()->where('country', '=', $cpc[2])->where('prov_state', '=', $cpc[1]));
		}
		return $this;
	}

	/**
	* Get the tax info based on country and state
	*
	* @param mixed $country
	* @param mixed $state
	* @return Tax
	*/
	public function load_by_country_state($country, $state) {
		if (($country == 'Canada') || ($country == 'CA')) {
			$country = \CityState::CA;
		} elseif (($country == 'United States') || ($country == 'US')) {
			$country = \CityState::US;
		}
		// Get using int values
		if (empty($state)) {
			$state = \CityState::get_state_id($state, $country);
		}
       	$this->load(\DB::select_array()->where('country', '=', $country)->where('prov_state', '=', $state));
		return $this;
	}
}