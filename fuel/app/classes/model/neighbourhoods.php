<?php
namespace Model;

class Neighbourhoods extends \Automodeler {

    const ID_PREFIX = 'xTH';
	const NEIGHBOURHOOD_TYPE_HAS = 1;
	const NEIGHBOURHOOD_TYPE_WANTS = 2;
	
	protected $_class = 'neighbourhoods';
    protected $_table_name = 'neighbourhoods';
    protected $_data = array('id' => NULL, 'listing_id' => '', 'neigh_name' => '', 'priority' => 1);

    public function save($validation=NULL){
        return parent::save($validation);
    }

    public function delete_neighbourhoods($listing_id){
        return \DB::delete($this->_table_name)->where('listing_id', '=', $listing_id);
    }

    public function insert_neighbourhoods($data){
        $this->set_fields(array_map('trim', $data));
        return $this->save();
    }
	
	public function set_wants_neighbourhood($data, $listing_id){
		\DB::delete('neighbourhoods')->where('listing_id', '=', $listing_id)->execute();
		$priority = 1;
		//if there was a "none" value, it will save as none only bypass other values
		if(in_array('none', $data)){
			$data = array();
		}
		foreach($data as $n){
			$fields = array(
				'listing_id' => $listing_id,
				'neigh_name' => $n,
				'priority' => $priority
			);
			\DB::insert('neighbourhoods')->set($fields)->execute();
			$priority++;
		}
		return true;
	}

    public static function get_neigh_priority($listing_id) {

        $result = \DB::select('neigh_name')
            ->from('neighbourhoods')
            ->where('listing_id', $listing_id)
            ->order_by('priority', 'asc')
            ->execute();

        $neigh_w = array();

        foreach($result->as_array() as $row){
            $neigh_w[] = utf8_encode($row['neigh_name']);
        }

        return $neigh_w;

    }

    public function get_neighbourhood($location, $selected = FALSE){
		\Lang::load('listing');
        $area_id = \CityState::get_area_id($location, true);
        $city = $area_id[0];
        $country = $area_id[2];
        if($country == \Model\User::COUNTRY_CA){
            $get_neigh = \DB::select_array()->from('ca_neighbourhoods')
                ->distinct(true)
                ->where('city_id', '=', $city)
                ->order_by('neigh_name', 'asc')
                ->execute()->as_array();
        } else {
            $get_neigh = \DB::select_array()->from('us_neighbourhoods')
                ->distinct(true)
                ->where('city_id', '=', $city)
                ->order_by('neigh_name')
                ->execute()->as_array();
        }
        if(sizeof($get_neigh) > 0){

            if(empty($selected)){
                $response[''] = __('neigh_any');
            }else{
                $response[''] = __('neigh_any');
            }
            foreach($get_neigh as $neighs_w){
                if(is_array($selected)){
                    if(in_array($neighs_w['neigh_name'], $selected)){
                        $response[$neighs_w['neigh_name']] = $neighs_w['neigh_name'];
                    }else{
                        $response[$neighs_w['neigh_name']] = $neighs_w['neigh_name'];
                    }
                }else{
                    $response[$neighs_w['neigh_name']] = $neighs_w['neigh_name'];
                }
            }
        }else{
            $response[''] = __('neigh_any');
        }

        return $response;
    }

    public function get_location_neighbourhood($location, $type, $listing_id){

		$vals = array();
		$area_id = \CityState::get_area_id($location, true);

		if(count($area_id) < 3){
			return;
		}
		$city = $area_id[0];
		$country = $area_id[2];

		if($country == \Model\User::COUNTRY_CA){
			$neigh = \Model\Country\Canada::forge()->load(\DB::select_array()->where('city_id', '=', $city), NULL)->as_array();
		} else {
			$neigh = \Model\Country\Usa::forge()->load(\DB::select_array()->where('city_id', '=', $city), NULL)->as_array();
		}

		if(sizeof($neigh) > 0){
			if($type == self::NEIGHBOURHOOD_TYPE_WANTS){
				$client = \Model\Client\Want::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id))->as_array();
				$neigh_w = \Model\Neighbourhoods::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id)->order_by('priority', 'asc'), NULL)->as_array();
				$vals['select']['none'] = __('neigh_any');
				if(!empty($neigh_w)){
					$vals['value'] = array();
					foreach($neigh_w as $row){
						$vals['value'][] = $row['neigh_name'];
					}
				} else {
					$vals['value'] = array('none');
				}

				foreach($neigh as $row){
					$vals['select'][$row['neigh_name']] = $row['neigh_name'];
				}

			} elseif($type == self::NEIGHBOURHOOD_TYPE_HAS) {

				$client = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id))->as_array();
				$vals['value'] = $client['neighbourhood'];
				$vals['select'][''] = __('sneigh');
				$vals['select']['none'] = __('nlisted');

				foreach($neigh as $row){
					$vals['select'][$row['neigh_name']] = $row['neigh_name'];

				}

			}
		} else {
			$vals['select']['none'] = __('neigh_any');
			$vals['value'] = array('none');
		}
		
		if($type == self::NEIGHBOURHOOD_TYPE_HAS){
			$field_name = 'has[neighbourhood]';
			$attr = array('id' => 'neighbourhood_h', 'class' => 'no_multi req', 'name' => 'has[neighbourhood]', 'data-field' => 'neighbourhood');
        } elseif($type == self::NEIGHBOURHOOD_TYPE_WANTS) {
			$field_name = 'wants[neighbourhood][]';
			$attr = array('id' => 'neighbourhood_w', 'class' => 'req', 'multiple' => 'multiple', 'style' => 'min-width:220px', 'data-field' => 'neighbourhood');
        }
		$data = array();
		$data['select'] = \Form::select($field_name, isset($vals['value']) ? $vals['value'] : '', $vals['select'], $attr);
		$data['label'] = __('neigh');
		return $data;
    }

}
