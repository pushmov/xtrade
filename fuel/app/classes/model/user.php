<?php
namespace Model;

class User extends \Automodeler {
	const AS_VALID = 0;
	const AS_PENDING_VERIFY = 1;
	const AS_SUSPENDED = 4;
	const AS_PAST_CLOSED = 5;
	const AS_PENDING_CANCEL = 6;
	const AS_NEEDS_PASS = 7;
	const AS_NEW = 8;
	const AS_BROKER_USER = 9;
	const AS_APPROVED = 10;
	const AS_VERIFIED = 11;
	const AS_USER_CLOSED = 12;
	const AS_ADMIN_CLOSED = 13;

	const UT_REALTOR = 1;
	const UT_BROKER = 2;
	const UT_VENDOR = 3;
	const UT_ADMIN = 0;

	const ALERT_NONE = 0;
	const ALERT_WEEKLY = 1;
	const ALERT_DAILY = 2;

	const COUNTRY_CA = 1;
	const COUNTRY_US = 2;

	const STATUS_LOGGED_IN = 1;
	const STATUS_ENABLED = 1;

	const EMAIL_SUBSCRIBE = 0;
	const EMAIL_UNSUBSCRIBE = 3;
	// Required field for automodeler
	protected $_data =	array('id' => NULL);

	protected $_account_status = array(0 => 'Valid','Pending Verification',	4 => 'Suspended','Past Verification',
		'Pending Cancellation','Needs Password','New','Broker Added', 'Approved Not Verified',
		'Verified Not Approved','User Closed', 'Admin Closed');
	protected $_alerts = array(self::ALERT_DAILY => 'Daily', self::ALERT_WEEKLY => 'Weekly', self::ALERT_NONE => 'None');
	protected $_types = array(self::UT_REALTOR => 'realtor', self::UT_BROKER => 'broker', self::UT_VENDOR => 'vendor', self::UT_ADMIN => 'admin');
	protected $_class = '';
	protected $_actual_table = array('realtor' => 'users', 'vendor' => 'directory_vendors', 'broker' => 'brokers');

	public function get_status($id = null) {
		if ($id === null) {
			return $this->_account_status;
		}
		if (isset($this->_account_status[$id])) {
			return $this->_account_status[$id];
		}
		return '';
	}

	public function get_session_xid($type){
		$user_xid = '';
		if($type === self::UT_REALTOR){
			$sess = \Authlite::instance('auth_realtor')->get_user();
			$user_xid = $sess->realtor_id;
		} elseif($type === self::UT_BROKER){
			$sess = \Authlite::instance('auth_broker')->get_user();
			$user_xid = $sess->broker_id;
		} elseif($type === self::UT_VENDOR){
			$sess = \Authlite::instance('auth_vendor')->get_user();
			$user_xid = $sess->vendor_id;
		}
		return $user_xid;
	}

	public function get_alerts($id = null) {
		if ($id === null) {
			return $this->_alerts;
		}
		if (isset($this->_alerts[$id])) {
			return $this->_alerts[$id];
		}
		return '';
	}

	public function get_types($id = null) {
		if ($id === null) {
			return $this->_types;
		}
		if (isset($this->_types[$id])) {
			return $this->_types[$id];
		}
		return '';
	}

	public function set_user_type($name){
		if (in_array($name, $this->_types)) {
			$type = array_keys($this->_types, $name);
			\Session::set('type', current($type));
		}
	}

	public function unset_user_type(){
		\Session::set('type', null);
	}

	public function get_actual_table($user_type){
		$ut = $this->get_types($user_type);
		return $this->_actual_table[$ut];
	}

	/**
	* Record the login
	*
	* @param string $remember
	* @return bool
	*/
	public function login($remember = '') {
		$id = $this->_class . '_id';
		if (in_array($this->account_status, array(self::AS_VALID, self::AS_NEW, self::AS_NEEDS_PASS))) {
			$this->last_login = date_create()->format(DATETIME_FORMAT_DB);
			$this->ip_address = \Input::ip();
			$this->browser = \Input::user_agent();
			$this->remember = $remember;
			\Sysconfig::logit($this->$id, $this->email, $this->ip_address, 'Success');
			parent::save();
			return true;
		} else {
			\Sysconfig::logit($this->$id, $this->email, $this->ip_address, 'Failed');
			return false;
		}
	}

	public function forgot_password() {
		$result = false;
		if($this->_class != $this->get_types(self::UT_ADMIN)){
			$in_array = in_array($this->account_status, array(0,1,3,7,8,10,11));
		} else {
			$in_array = true;
		}
		if ($in_array) {
			$reset_code = \sysconfig::create_string('activate');
			$password = \sysconfig::create_string('temp_pass');
			$this->pass_reset = $reset_code;
			if($this->_class != $this->get_types(self::UT_ADMIN)){
				$this->account_status = self::AS_ADMIN_CLOSED;
				$this->temp_pass = \Authlite::instance('auth_' . $this->_class)->hash($password);
			}
			parent::save();
			$result = \Emails::forge()->forgot($this->as_array(), $password, $this->_class);
		}
		return $result;
	}

	/**
	* Set the temporary password and set the needs password flag
	*
	* @param string $code - the reset code
	*/
	public function reset_password($code) {
		$this->load(\DB::select_array()->where('pass_reset', '=', $code));
		if ($this->loaded()) {
			$this->password = $this->temp_pass;
			$this->temp_pass = '';
			$this->account_status = self::AS_NEEDS_PASS;
			$this->logged_in = 1;
			$this->ip_address = \Input::ip();
			parent::save();
			// Forced login to allow setting new password
			\Authlite::instance('auth_' . $this->_class)->force_login($this->email);
			return true;
		}
		return false;
	}

	/**
	* Verify and update the password
	*
	* @param array $post
	*/
	public function update_password($post) {
		\Lang::load('signup');
		$data = array('status' => 'OK', 'message' => __('password_changed'));
		$post = array_map('trim', $post);
		$validation = \Validation::forge();
		$validation->add_field('password', __('password'), 'required|min_length[5]');
		$validation->add_field('cpassword', __('password'), 'match_field[password]');
		if ($validation->run($post)) {
			$fieldset = $validation->fieldset();
			$this->password = \Authlite::instance('auth_' . $this->_class)->hash($post['password']);
			if ($this->account_status == self::AS_NEEDS_PASS) {
				$this->account_status = self::AS_VALID;
			}
			parent::save();
			// Update the session data
			\Authlite::instance('auth_' . $this->_class)->force_login($this->email);
		} else {
			$data['status'] = 'FAILED';
			$data['message'] = $validation->show_errors();
		}
		return $data;
	}

	public static function format_url($str = '') {
		if ($str != '') {
			// If no protocol given, prepend "http://" by default
			if (strpos($str, '//') === FALSE) {
				return 'http://' . $str;
			}
		}
		// Return the original URL
		return $str;
	}

	/**
	* Validation functions
	*/
	public function _validation_valid_phone($val) {
		if (trim($val) != '') {
			return (bool)preg_match("/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/", trim($val));
		} else {
			return true;
		}
	}

	public function _validation_unique_email($val) {
		$result = \DB::select()->from($this->_table_name)->where('email', '=', trim($val))
		//except broker's invitation
		->where('account_status', '<>', self::AS_BROKER_USER)->execute();
		return (count($result) == 0) ? true : false;
	}

	public function _validation_unique_crea($val) {
		if (($val = trim($val)) == '') {
			return true;
		} else {
			$result = \DB::select()->from($this->_table_name)->where('country', '=', 1)->where('nrb_id', '=', trim($val))->execute();
			return (count($result) == 0) ? true : false;
		}
	}

	public function _validation_unique_nrds($val) {
		if (($val = trim($val)) == '') {
			return true;
		} else {
			$result = \DB::select()->from($this->_table_name)->where('country', '=', 2)->where('nrb_id', '=', trim($val))->execute();
			return (count($result) == 0) ? true : false;
		}
	}

	public function _validation_unique_nrb($value) {
		$val = \Validation::active();
		$crea_id = $val->input('crea_id');
		$nrds_id = $val->input('nrds_id');
		if (($crea_id == '') && ($nrds_id == '')) {
			$val->set_message('unique_nrb', 'CREA or NRDS ID Required');
			return false;
		}
		if (($crea_id != '') && ($nrds_id != '')) {
			$val->set_message('unique_nrb', 'Enter ony CREA or NRDS ID');
			return false;
		}
		if ($crea_id != '') {
			$result = \DB::select()->from($this->_table_name)->where('nrb_id', '=', trim($value))->where('country', '=', 1)->execute();
			if (count($result) != 0) {
				$val->set_message('unique_nrb', 'CREA ID already exists');
				return false;
			}
		}
		if ($nrds_id != '') {
			$result = \DB::select()->from($this->_table_name)->where('nrb_id', '=', trim($value))->where('country', '=', 2)->execute();
			if (count($result) != 0) {
				$val->set_message('unique_nrb', 'NRDS ID already exists');
				return false;
			}
		}
		return true;
	}
	/**
	* Returns common profile information for realtor & broker
	*
	*/

	public function profile() {
		\Lang::load('common');
		// Update and save the new flag
		if ($this->account_status == self::AS_NEW) {
			$this->account_status = self::AS_VALID;
			$this->save();
			// Update the user session info
			\Session::set('auth_' . $this->_class, (object)$this->as_array());
			// Set back to new so popup in profile will show
			$this->account_status = self::AS_NEW;
		}
		$extra = array();
		// Phone number formatting
		//		$this->phone = \Num::format_phone($this->phone);
		//		$this->cell = \Num::format_phone($this->cell);
		$this->account_creation = date_create($this->account_creation)->format(DATE_FORMAT);
		$this->last_login = date_create($this->last_login)->format(DATETIME_FORMAT);
		if ($this->promotional_end != '0000-00-00') {
			if (date_create($this->promotional_end) <= date_create()){
				$extra['promo_ends'] = __('ended');
			} else {
				$extra['promo_ends'] = date_create($this->promotional_end)->format(DATE_FORMAT);
			}
		}
		//fix me
		if($this->_class == 'vendor'){
			list($city, $prov_state, $country) = \CityState::get_area_id($this->location_address, true);
			$user_type = self::UT_VENDOR;
		} elseif($this->_class == 'broker'){
			$user_type = self::UT_BROKER;
		} else if($this->_class == 'realtor') {
			$user_type = self::UT_REALTOR;
		}
		$extra['cemail'] = $this->email;
		$payments = \Model\Order::forge()->load_last($this->id, $user_type);
		if ($payments->loaded()) {
			$extra['last_payment_amt'] = '$' . number_format($payments->sub_total, 2);
			$extra['last_payment_date'] = date_create($payments->order_date)->format(DATE_FORMAT);
			$extra['last_payment'] = $extra['last_payment_amt'] . ' on ' . $extra['last_payment_date'];
			$extra['history'] = '<button type="button" class="button" id="billing_history">'.__('past_billing').'</button>';
		} else {
			$extra['last_payment_amt'] = '-';
			$extra['last_payment_date'] = __('nprev_payment');
			$extra['last_payment'] = $extra['history'] = __('nbilling');
		}
		/*
		if ($user_type == self::UT_REALTOR || $user_type == self::UT_VENDOR) {
		$agreement = new \Model\Paypalrest\Agreement();
		$detail = $agreement->detail($this->paypal_sub_id);

		//if($PayPalResult['AMT'] && strtotime($PayPalResult['NEXTBILLINGDATE'])-strtotime($PayPalResult['PROFILESTARTDATE']) > 86400){
		if ($detail['state'] == $agreement->get_status(\Model\Paypalrest\Agreement::STATE_ACTIVE) && $detail['next_date'] != '') {
		$extra['next_payment_amt'] = '$' . number_format(($detail['amt']+$detail['tax_amt']), 2);
		$extra['next_payment_date'] = date_create($detail['next_date'])->format(DATE_FORMAT);
		$extra['next_payment'] = $extra['next_payment_amt'] . ' on ' . $extra['next_payment_date'];
		} else {
		$extra['next_payment_amt'] = '-';
		$extra['next_payment_date'] = $extra['next_payment'] = __('pending');
		}
		}
		*/
		$extra['alerts'] = '';
		if (isset($this->alerts)){
			$alerts = array(self::ALERT_DAILY => __('daily'), self::ALERT_WEEKLY => __('weekly'), self::ALERT_NONE => __('none'));
			$extra['alerts'] = \Form::radios('data[alerts]', $this->alerts, $alerts);
		}

		$extra['email_type'] = '';
		if(isset($this->email_type)){
			$email_type_label = array(self::EMAIL_SUBSCRIBE => __('subscribe'), self::EMAIL_UNSUBSCRIBE => __('unsubscribe'));
			$extra['email_type'] = \Form::radios('data[email_type]', $this->email_type, $email_type_label);
		}
		$extra['utype'] = $this->_class;
		return array_merge($this->as_array(), $extra);
	}

	public static function realtor_id() {
		if(\Session::get('auth_realtor')){
			return \Session::get('auth_realtor')->realtor_id;
		}
		return NULL;
	}

	public static function account_status(){
		$account_type = \Session::get('type');

		switch($account_type){
			case self::UT_BROKER:
				$table = 'brokers';
				$condition = 'broker_id';
				break;

			case self::UT_VENDOR:
				$table = 'directory_vendors';
				$condition = 'vendor_id';
				break;

			case self::UT_ADMIN:
				$table = 'users';
				$condition = 'realtor_id';
				break;

			default:
				$table = 'users';
				$condition = 'realtor_id';
				break;
		}

		$result = \DB::select_array()
		->from($table)
		->where($condition, self::realtor_id())
		->execute();

		return $result->current();
	}

	public function save_user($post){
		$this->set_fields(array_map('trim', $post));
		//		$this->nrds_id = $post['nrds_id'] == '' ? NULL : $post['nrds_id'];
		//		$this->crea_id = $post['crea_id'] == '' ? NULL : $post['crea_id'];
		//		$this->user_name = $post['email'];
		$this->password = \Authlite::instance('auth_' . $this->_class)->hash($post['password']);
		$this->account_status = self::AS_NEW;
		$this->activate = \Sysconfig::create_string('activate');
		$this->account_type = self::UT_REALTOR;
		return parent::save();
	}

	public function activate_user($key, $type){
		$response['status'] = 'ERROR';
		$response['email'] = '';
		$user = $this->load(\DB::select_array()->where('activate', '=', $key))->as_array();
		if($this->loaded()){
			$fields = array(
				'activate' => '',
				'logged_in' => self::STATUS_LOGGED_IN,
				'ip_address' => \Input::ip(),
				'account_status' => self::AS_NEW,
			);
			$save = \DB::update($this->_actual_table[$type])->set($fields)->where('activate', '=', $key)->execute();
			if ($save) {
				$response['status'] = 'OK';
				$response['email'] = $user['email'];
				// Send welcome email
				\Emails::forge()->welcome($user, $type);
				if ($type == 'realtor') {
					\Emails::forge()->new_agent($user);
				}
			}
		}
		return $response;
	}

	public function check_activate_user($email, $type){
		$response['redirect'] = \Uri::base(false);
		$response['status'] = 'ERROR';
		$response['type'] = $type;
		$response['message'] = __('login_to_continue');
		$response['email'] = '';
		$this->load(\DB::select_array()
			->where('email', '=', $email)
			->where('activate', '=', '')
			->where('account_status', '=', self::AS_NEW)
		);
		if($this->loaded()){
			$this->set_user_type($type);
			$response['redirect'] .= $type.'s/profile';
			$response['status'] = 'OK';
			$response['message'] = __('acc_activated').' '.__('login_to_continue');
			$response['email'] = $this->email;
		} else {
			$response['status'] = __('acc_already_activated').' '.__('login_to_continue');
		}
		return $response;
	}

	public function user_unsubscribe_email($email){
		$response = FALSE;
		$this->load(\DB::select_array()->where('email', '=', $email)
			->where('account_status', '=', self::AS_VALID)
			->where('email_type', '=', self::EMAIL_SUBSCRIBE)
		);
		if($this->loaded()){
			$this->set_fields(array(
				'email_type' => self::EMAIL_UNSUBSCRIBE
			));
			$response = $this->save();
		}
		return $response;
	}

	public function user_subscribe_email($email){
		$response = FALSE;
		$this->load(\DB::select_array()->where('email', '=', $email)->where('account_status', '=', self::AS_VALID)
			->where('email_type', '=', self::EMAIL_UNSUBSCRIBE));
		if($this->loaded()){
			$this->set_fields(array(
				'email_type' => self::EMAIL_SUBSCRIBE
			));
			$response = $this->save();
		}
		return $response;
	}

	/**
	* Reset password function, create temporary password and send an email to user instruction to reset
	*
	* @param array $post Requested user type id [realtor_id, broker_id or vendor_id]
	* @param String $account_type User account type [realtor, vendor, or broker]
	* @return array Response status
	*/
	public function admin_reset_password($post, $account_type){
		$model = $this->load(\DB::select_array()->where($account_type.'_id', '=', $post));
		$user = $model->as_array();
		$reset_code = \Sysconfig::create_string('activate');
		$temp_password = \Sysconfig::create_string('temp_pass');
		$data = array();
		$fields = array(
			'pass_reset' => $reset_code,
			'temp_pass' => $temp_password,
			'session_id' => '',
			'account_status' => self::AS_NEEDS_PASS
		);
		if($account_type == $this->get_types(self::UT_VENDOR)){
			$fields['password'] = \Authlite::instance('auth_' . $this->_class)->hash($temp_password);
			$data['link'] = 'dialog/'.$account_type;
			$data['temp_pass_txt'] = 'Temporary password: '.$temp_password;
		} else {
			$fields['link'] = 'pass_reset/'.$reset_code.'/'.$account_type;
			$data['temp_pass_txt'] = '';
		}
		$model->set_fields($fields);
		if($model->save()){
			$fields['to'] = $user['email'];
			$fields['type'] = $account_type;
			$response = \Emails::forge()->admin_reset_password(array_merge($data, $fields));
			$response['id'] = $user[$account_type.'_id'];
		} else {
			$response = array('status' => 'error', __('err_temp_pass'));
		}
		return $response;
	}

	/**
	* Suspend user action
	*
	* @param array $id User ID identifier
	* @param String $type User's type [vendor, broker, realtor]
	* @return array Response status
	*/
	public function suspend_user($id, $type){
		$response['status'] = 'error';
		$response['id'] = $id;
		$fields['account_status'] = self::AS_SUSPENDED;
		$fields['paypal_status'] = \Model\Gateway::PP_SUSPENDED;
		$model = $this->load(\DB::select_array()->where($type.'_id', '=', $id));
		$model->set_fields($fields);
		if($model->save()){
			$response['status'] = 'ok';
			$response['id'] = $this->id;
		}
		return $response;
	}

	/**
	* Unsuspend user action
	*
	* @param String $id User ID
	* @param String $type User type [realtor, vendor, or broker]
	* @return array Response status
	*/
	public function unsuspend_user($id, $type){
		$response['status'] = 'error';
		$response['id'] = $id;
		$fields['paypal_status'] = \Model\Gateway::PP_ACTIVE;
		$fields['account_status'] = self::AS_VALID;
		$model = $this->load(\DB::select_array()->where($type.'_id', '=', $id));
		$model->set_fields($fields);
		if($model->save()){
			$response['status'] = 'ok';
			$response['id'] = $this->id;
		}
		return $response;
	}


	/**
	* Approve user with pending verify status
	*
	* @param String $id user id identifier
	* @param string $type User type [vendor, realtor, broker]
	* @return array Response status
	*/
	public function approve_account($id){
		$response['status'] = 'error';
		$response['id'] = $id;
		$model = $this->load(\DB::select_array()->where('id', '=', $id));
		$user = $model->as_array();
		if($user['account_status'] == self::AS_PENDING_VERIFY){
			$field['account_status'] = self::AS_APPROVED;
		} else {
			$field['account_status'] = self::AS_VALID;
		}
		$model->set_fields($field);
		if($model->save()){
			$response['status'] = 'ok';
		}
		return $response;
	}

	public function update_last_active() {
		if ($this->loaded()) {
			$this->location = $_SERVER['REQUEST_URI'];
			$this->last_active = date_create()->format(DATETIME_FORMAT_DB);
			parent::save();
		}
	}


}