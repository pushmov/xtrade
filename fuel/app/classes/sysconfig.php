<?php
class Sysconfig {
	const ACRE_TO_FEET = 43560;
	const M2_TO_FEET = 107639;

	public static function get($key) {
		return \DB::select('value')->from('sys_config')->where('id', $key)->execute()->get('value');
	}
	public static function update($key, $value) {
		\DB::update('sys_config')->value('value', $value)->where('id', $key)->execute();
	}
	public static function logit($id, $email, $ip, $message) {
		$query = \DB::insert('login_log');
		$query->columns(array('user_id','user_name','ip','message','date'));
		$query->values(array($id,$email,$ip,$message,date_create()->format('Y-m-d H:i:s')))->execute();
	}
	public static function all(){
		$data = array();
		$settings = \DB::select_array()->from('sys_config')->execute()->as_array();
		foreach($settings as $row) {
			$data[$row['id']] = $row['value'];
		}
		return $data;
	}
	public static function get_map_key(){
		if (strpos($_SERVER['SERVER_NAME'], 'xtradehomes.com') !== FALSE) {
			$map_key = 'AIzaSyCKr-kv8hijKtcEeu6LsAnR62CSBWKNjj4';
		}else{
			$map_key = 'AIzaSyDxPX7prosx493ctfzfC77NqgL5WWM1eW4';
		}

		return $map_key;
	}
	public static function create_string($type, $length = 5, $rec = false) {
		switch($type) {
			case 'realtor':
				$characters = '0123456789';
				$string='xTH';
				break;
			case 'broker':
				$characters = '0123456789';
				$string='bxTH';
				break;
			case 'vendor':
				$characters = '0123456789';
				$string='vxTH';
				break;
			case 'egen':
				$length = 60;
				$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ';
				$string='exTH';
				break;
			case 'activate':
				$length = 60;
				$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ';
				$string='xTH';
				break;
			case 'temp_pass':
				$length = 8;
				$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ';
				$string='x';
				break;
			case 'key':
				$length = 14;
				$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ';
				$string='';
				break;
			case 'listing_id':
				$length = 6;
				$characters = '0123456789';
				$string='NA';
				break;
			case 'salt':
				$length = 8;
				$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ';
				$string = '$2a$07$';
		}

		$real_string_length = strlen($characters)-1;

		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, $real_string_length)];
		}

		return $string;
	}

	public static function format_phone_number($s) {
		$rx = "/
		(1)?\D*     # optional country code
		(\d{3})?\D* # optional area code
		(\d{3})\D*  # first three
		(\d{4})     # last four
		(?:\D+|$)   # extension delimiter or EOL
		(\d*)       # optional extension
		/x";
		preg_match($rx, $s, $matches);
		if(!isset($matches[0])) return false;

		$country = $matches[1];
		$area = $matches[2];
		$three = $matches[3];
		$four = $matches[4];
		$ext = $matches[5];

		$out = "$three-$four";
		if(!empty($area)) $out = "$area-$out";
		if(!empty($country)) $out = "+$country-$out";
		if(!empty($ext)) $out .= "x$ext";

		// check that no digits were truncated
		// if (preg_replace('/\D/', '', $s) != preg_replace('/\D/', '', $out)) return false;
		return $out;
	}

	public static function country_code($name = null){
		$name = trim($name);
		$data = array(
			'Canada' => 'CA',
			'United States' => 'US'
		);
		if (array_key_exists($name, $data)) {
			return $data[$name];
		}
		if ($name == 1) {
			return 'CA';
		} elseif ($name == 2) {
			return 'US';
		}
		return $data;
	}

	public static function country_name($country_code){
		if (is_int($country_code)) {
			$country_name = array(
				\Model\User::COUNTRY_CA => 'Canada',
				\Model\User::COUNTRY_US => 'United States'
			);
		} else {
			$country_name = array(
				'CA' => 'Canada',
				'US' => 'United States'
			);
		}
		return $country_name[$country_code];
	}

	public static function trim_text($input, $length = 100, $ellipses = true, $strip_html = true) {
		//strip tags, if desired
		if ($strip_html) {
			$input = strip_tags($input);
		}
		//no need to trim, already shorter than trim length
		if (strlen($input) <= $length) {
			return $input;
		}
		//find last space within length
		$last_space = strrpos(substr($input, 0, $length), ' ');
		$trimmed_text = substr($input, 0, $last_space);
		//add ellipses (...)
		if ($ellipses) {
			$trimmed_text .= '...';
		}
		return $trimmed_text;
	}

	/**
	* Save prefferred setting action
	*
	* @param array $post Setting data collection from admin setting form
	* @return array Response status
	*/
	public static function save_settings($post){
		\Lang::load('common');
		foreach($post as $id => $value){
			$row_exist = \DB::select_array()->from('sys_config')->where('id', '=', $id)->execute()->current();
			if(empty($row_exist)){
				\DB::insert('sys_config')->set(array('value' => $value, 'id' => $id))->execute();
			} else {
				\Sysconfig::update($id, $value);
			}
		}
		return array('message' => __('setting_saved'));
	}

	/**
	* Format display sqft number in search result, listing detail
	*
	* @param string $string unformatted number data
	* @return string formatted sqft number
	*
	* ex :
	* 2964 sqft => 2,964 sq ft
	* 932 => 932 sq ft
	* 4687.32 => 4,687.32 sq ft
	*/
	public static function format_sqft($string){
		if (is_numeric($string)) {
			return number_format(trim($string), 0) . ' sqft';
		}
		if (strpos($string, ' ') > 0) {
			list($num, $unit) = explode(' ', $string, 2);
			if (is_numeric($num)) {
				return number_format(trim($num), 1) . ' ' . $unit;
			}
		}
		return $string;
	}

	/**
	* Log system errors and send email
	*
	* @param string $file -  __FILE__ . ' : ' , __LINE__
	* @param mixed $msg - error message or array
	*/

	public static function logerror($file, $msg) {
		if (is_array($msg)) {
			\Log::Warning($file . "\n" . print_r($msg, true));
		} else {
			\Log::Warning($file . "\n" . $msg);
		}
	}
}