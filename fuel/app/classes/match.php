<?php

/**
* core match functions
*/

class Match {
	const SEARCH_LEVEL_HIGH = 0;
	const SEARCH_LEVEL_MEDIUM = 1;
	const SEARCH_LEVEL_LOW = 2;

	const KNOCK_EXACT = 0;
	const KNOCK_HIGH = 2;
	const KNOCK_MEDIUM = 1;

	public $inc = false;
	public $template = 'div';
	public $show_contact = true;

	public static function forge(){
		return new self;
	}

	public function reset_record($listing_id){
		\Model\Matches::forge()->two_way_batch($listing_id);
		\Model\Matches::forge()->three_way_batch($listing_id);
		\Model\Matches::forge()->four_way_batch($listing_id);
		\Model\Matches::forge()->five_way_batch($listing_id);
	}

	public function remove_matches($listing_id){
		\DB::delete('matches')->where('listing_id', '=', $listing_id)->where(\DB::expr('matched_listings IS NOT NULL'))->execute();
		\DB::delete('matches')->where('listing_id', '=', $listing_id)->where(\DB::expr('buyers_match IS NOT NULL'))->execute();

		return true;
	}

	public function get_current_listing_box($listing_id, $type_of_listing, $use_email_template = false, $from_cron = false){
		if($type_of_listing == \Model\Listing::TYPE_BUY_ONLY){
			$in_set = 'buyer';
		} else {
			$in_set = 'A';
		}

		if ($use_email_template) {
			$template = $this->setup_box_information_email($listing_id, $in_set, null, false, $from_cron);
		} else {
			$template = $this->setup_box_information($listing_id, $in_set, null);
		}

		return $template;
	}

	/**
	* match result process, setting up global x_2_match, x_3_match, x_4_match array
	*
	* @param Array $listing_ids Array of listing id collection
	* @return Array image link
	*/
	public function get_fsbo($listing_ids){

		$images = array();

		$get_fsbo = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_ids[0]))->as_array();
		if($get_fsbo['fsbo'] == 0){
			$images[0] = \Model\Photos\Xtrade::forge()->get_images($listing_ids[0], 0);
		}elseif($get_fsbo['imported'] == 1){
			$images[0] = \Model\Crea\Property::forge()->get_image($get_fsbo['external_id'], 0, false);
		}else{
			$images[0] = \Model\Photos\Xtrade::forge()->xtrade->get_images($listing_ids[0], 1);
		}

		$get_fsbo_1 = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_ids[1]))->as_array();
		if($get_fsbo_1['fsbo'] == 0){
			$images[1] = \Model\Photos\Xtrade::forge()->get_images($listing_ids[1], 0);
		}elseif($get_fsbo_1['imported'] == 1){
			$images[1] = \Model\Crea\Property::forge()->get_image($id, $pos = null, $thumb = true);
		}else{
			$images[1] = \Model\Photos\Xtrade::forge()->get_images($listing_ids[1], 1);
		}

		if(isset($listing_ids[2])){
			$get_fsbo_2 = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_ids[2]));
			if($get_fsbo_2['fsbo'] == 0){
				$images[2] = \Model\Photos\Xtrade::forge()->get_images($listing_ids[2], 0);
			}elseif($get_fsbo_2['imported'] == 1){
				$images[2] = \Model\Crea\Property::forge()->get_image($id, $pos = null, $thumb = true);
			}else{
				$images[2] = \Model\Photos\Xtrade::forge()->get_images($listing_ids[2], 1);
			}
		}

		if(isset($listing_ids[3])){
			$get_fsbo_3 = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_ids[3]));

			if($get_fsbo_3['fsbo'] == 0){
				$images[3] = \Model\Photos\Xtrade::forge()->get_images($listing_ids[3], 0);
			}elseif($get_fsbo_3['imported'] == 1){
				$images[3] = \Model\Crea\Property::forge()->get_image($id, $pos = null, $thumb = true);
			}else{
				$images[3] = \Model\Photos\Xtrade::forge()->get_images($listing_ids[3], 1);
			}
		}

		return $images;
	}

	/**
	* get listing_id collection based on search params info
	* @param string id_to_check, string checker_id
	* @return array
	*/
	public function search_results($ID2Check, $Checker_ID){
		$get_what_they_want = \Model\Client\Want::forge()->load(\DB::select_array()->where('listing_id', '=', $ID2Check));
		$d_list = \Listing::forge()->set_search_params_info($get_what_they_want, 0, $ID2Check);
		$d_list->where('listing_status', '=', \Model\Listing::LS_ACTIVE);
		$d_list->where('list_as_sold', '=', \Model\Listing::AS_NOT_SOLD);
		if($Checker_ID === false){
			$d_list->where('listing_id', 'NOT LIKE', $ID2Check);
		} else {
			$d_list->where('listing_id', 'LIKE', $Checker_ID);
		}
		$get_desires = \Model\Client\Has::forge()->load($d_list, null)->as_array();
		$all_search_results = array();
		//CHECK LATER IF YOU NEED TO CHANGE FETCH_ROW FUNCTION!!
		foreach ($get_desires as $ids){
			$all_search_results[] = $ids['listing_id'];
		}
		if(empty($all_search_results)){
			$all_search_results[0] = '';
		}
		return $all_search_results;
	}

	/**
	* setup formatted match box to be displayed in matches page / email
	* @param string listing_id, string in_set, boolean add, boolean grey_bg, boolean not_direct, boolean for_email, string $template
	* @return string
	*/
	public function setup_box_information($listing_id, $in_set, $add=NULL, $grey_bg = false, $not_direct = false, $for_email = false){
		\Lang::load('matches');
		$buyer = false;
		$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id))->as_array();
		$add_to = '';
		$client = \Model\Client\Information::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id))->as_array();
		list($city, $prov_state, $country) = \CityState::get_area_name($listing['city'], $listing['prov_state'], $listing['country'], TRUE);
		$ID_or_contact = __('listingid') . $listing_id;

		if($in_set != 'buyer'){
			\Lang::load('common');
			if($listing['imported'] == 1){
				$public_listing_id = '2_'.$listing['external_id'];
				$image_set = \Model\Crea\Property::forge()->get_image($listing['external_id'], 1, false);
			}elseif($listing['imported'] == 2){
				$public_listing_id = '3_'.$listing['external_id'];
				$image_set = \Model\Crea\Property::forge()->get_image($listing['external_id'], 1, false);
			}else{
				$public_listing_id = '0_'.$listing['listing_id'];
				$image_set = \Model\Listing::IMG_DIR_NAME . \Model\Photos\Xtrade::forge()->get_first_photo($listing_id, 0);
			}
			$price = '$'.number_format((double) $listing['listing_price'], 2);
			$top = 'top';
			$match_info = '';
			if($in_set == 'A'){
				$home_title = __('client_home');
				$client_or_view = __('client').$client['first_name'].' '.$client['last_name'];
			} else {
				$client_or_view = \Html::anchor('listing/detail/' . $public_listing_id, __('view_listing'));
				if ($this->show_contact === true) {
					$ID_or_contact = \Html::anchor('#', __('contact_agent'), array('class' => 'contact-agent no-link', 'id' => $public_listing_id));
				} else {
					$ID_or_contact = '';
				}
			}

			if($in_set == 'B'){
				$home_title = __('client_match');
			}
			if($in_set == 'C'){
				$home_title = __('b_match');
				if($add){
					$add_to = '<br>'.__('c_want');
				}
				if($grey_bg){
					$top = 'top_cd';
				}else{
					$top = 'top l_top';
				}
			}
			if($in_set == 'D'){
				$home_title = __('c_match');
				if($add){
					$add_to = '<br>'.__('d_want');
				}
				if($grey_bg){
					$top = 'top_cd';
				}else{
					$top = 'top l_top';
				}
			}
			if($in_set == 'E'){
				$top = 'top l_top';
				$home_title = __('d_match');
				$home_title .= '<br>'.__('e_want');
			}
			if($in_set == 'buyer_match'){
				$home_title = __('match_home');
				$in_set = '&nbsp;';
			}

		} else {
			$image_set = 'buyers.jpg';
			$price = __('buyer_only');
			$home_title = __('client_home');
			$client_or_view = __('client') . $client['first_name'].' '.$client['last_name'];
			$top = 'top';
			$in_set = '&nbsp;';
			$buyer = true;
		}
		$match_info_data = array(
			'in_set' => $in_set,
			'buyer' => $buyer,
			'not_direct' => $not_direct,
			'top' => $top,
			'home_title' => $home_title,
			'add_to' => $add_to,
			'image_set' => $image_set,
			'price' => $price,
			'street' => $listing['address'],
			'city' => $city,
			'prov' => $prov_state,
			'client_or_view' => $client_or_view,
			'ID_or_contact' => $ID_or_contact,
			'in_1' => $listing_id
		);

		$match_info = \Helper\Html::match_info($match_info_data, $this->template);
		if($for_email){
			if(is_array($client['first_name']) || $client['first_name'] != ''){
				$ret = 'at '. $listing['address'] .' '.$city;
			}else{
				$ret = $client['first_name'].' '.$client['last_name'];
			}
			$return = $ret;
		} else {
			$return = $match_info;
		}
		return $return;
	}

	public function universal_lookup($listing_id, $inc = false){

		$GLOBALS['all_ids'] = array();
		$GLOBALS['listing_IDs'] = $listing_id;
		$GLOBALS['Listing_ID'] = $listing_id;
		$GLOBALS['desire_array'] = array();
		$GLOBALS['have_array'] = array();

		$GLOBALS['Want_Each_Other_IDs_exact_high'] = array();
		$GLOBALS['Want_Each_Other_IDs_exact_med'] = array();
		$GLOBALS['Want_Each_Other_IDs_exact_low'] = array();

		$GLOBALS['Want_Each_Other_IDs_med_high'] = array();
		$GLOBALS['Want_Each_Other_IDs_med_med'] = array();
		$GLOBALS['Want_Each_Other_IDs_med_low'] = array();

		$GLOBALS['Want_Each_Other_IDs_high_high'] = array();
		$GLOBALS['Want_Each_Other_IDs_high_med'] = array();
		$GLOBALS['Want_Each_Other_IDs_high_low'] = array();

		$GLOBALS['global_matches'] = array();

		$desirable_listings_exact = array();
		$desirable_listings_medium = array();
		$desirable_listings_high = array();
		$desirable_all = array();
		$desire_list = NULL;
		///////////////////////////////////// End Universal lookups ////////////////////////////////////////
		$has = \Model\Client\Has::forge()->load_by_listing_id($listing_id);
		$listing = $has->as_array();
		////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////// Universal lookups ////////////////////////////////////////
		//This is THIS listings desire list
		$sql_query_my_desires = \Model\Client\Want::forge()->load(\DB::select_array()->where('listing_id', '=', $listing['listing_id']));
		$get_my_desires_search_results =  $sql_query_my_desires->as_array();
		$sql_query_my_haves = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing['listing_id'])
			->where('type_of_listing', '<>', \Model\Listing::TYPE_BUY_ONLY));
		$get_my_haves_search_results = $sql_query_my_haves->as_array();

		$GLOBALS['desire_array'] = $get_my_desires_search_results;
		$GLOBALS['have_array'] = $get_my_haves_search_results;

		$sql_query_ignores = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing['listing_id'])
			->where(\DB::expr('ignore_listings IS NOT NULL')), null)->as_array();
		foreach($sql_query_ignores as $row){
			$GLOBALS['ignore_array'] = $row['ignore_listings'];
		}
		///////////////////////////////////////////////////////////////////////////////
		$has->matches_found = 0;
		$has->save();

		//sorted list for searching through ALL listing information based on THIS desire list
		$my_desires_query_result_strings_exact = \Listing::forge()->set_search_params_desires($get_my_desires_search_results, 2, $listing['listing_id']);
		if($listing['type_of_listing'] == \Model\Listing::TYPE_XTRADED){
			$type_of_listing = \Model\Listing::TYPE_XTRADED;
		} else {
			$type_of_listing = \Model\Listing::TYPE_BUY_ONLY;
		}
		$my_desires_query_result_strings_exact->where('listing_id', '<>', $listing['listing_id']);
		$my_desires_query_result_strings_exact->where('type_of_listing', '=', $type_of_listing);
		$my_desires_query_result_strings_exact->where('list_as_sold', '=', \Model\Listing::AS_NOT_SOLD);
		$my_desires_query_result_strings_exact->where('listing_status', '=', \Model\Listing::LS_ACTIVE);
		$ddd = \Model\Client\Has::forge()->load($my_desires_query_result_strings_exact, null);
		$get_desirable_listings_exact = \Model\Client\Has::forge()->load($my_desires_query_result_strings_exact, null)->as_array();
		$number_of_exacts = sizeof($get_desirable_listings_exact);
		$number_of_mediums = $number_of_highs = 0;

		if($number_of_exacts > 0){
			foreach($get_desirable_listings_exact as $q){
				$desire_list = $q['listing_id'];
				if($inc === true){
					$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing['listing_id']));
				}else{
					$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing['listing_id'])
						->where_open()
						->where('ignore_listings', '=', $desire_list)
						->or_where('save_listings', '=', $desire_list)
						->where_close());
				}
				if(!$check_for_ignore->loaded()){
					//Array of all the listings THIS listing wants EXACTLY
					$desirable_listings_exact[] = $desire_list;
					$desirable_all[] = $desire_list;
				}
			}

			if($listing['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
				foreach($desirable_listings_exact as $round_1){
					$match = \Model\Matches::forge()->load(\DB::select_array()->from('matches')->where('listing_id', '=', $listing['listing_id'])
					->where_open()
					->where('matched_listings', '=', $round_1)
					->or_where('save_listings', '=', $round_1)
					->or_where('buyers_match', '=', $round_1)
					->where_close());
					if($match->loaded()){
						$match->match_potential = 'High';
						$match->save();
					} else {
						$fields = array(
							'realtor_id' => $listing['realtor_id'],
							'listing_id' => $listing['listing_id'],
							'buyers_match' => $round_1,
							'seen' => \Model\Matches::MATCH_UNSEEN,
							'match_potential' => 'High'
						);
						$match->set_fields($fields)->save();
					}
				}
			}else{
				foreach($desirable_listings_exact as $round_1){
					$exact_high = $this->check_matchs($round_1, 2, 0, $listing['listing_id'], 'Exact', $listing['realtor_id']);
					$exact_med = $this->check_matchs($round_1, 1, 0, $listing['listing_id'], 'Exact', $listing['realtor_id']);
					$exact_low = $this->check_matchs($round_1, 0, 0, $listing['listing_id'], 'Exact', $listing['realtor_id']);
				}
			}
		}

		$my_desires_query_result_strings_medium = \Listing::forge()->set_search_params_desires($get_my_desires_search_results, 1, $listing['listing_id']);
		if($number_of_exacts <= 10){
			if($listing['type_of_listing'] == \Model\Listing::TYPE_XTRADED){
				$type_of_listing = \Model\Listing::TYPE_XTRADED;
			}else{
				$type_of_listing = \Model\Listing::TYPE_BUY_ONLY;
			}
			$my_desires_query_result_strings_medium->where('listing_id', '<>', $listing['listing_id']);
			$my_desires_query_result_strings_medium->where('type_of_listing', '<>', $type_of_listing);
			$my_desires_query_result_strings_medium->where('list_as_sold', '=', \Model\Listing::AS_NOT_SOLD);
			$my_desires_query_result_strings_medium->where('listing_status', '=', \Model\Listing::LS_ACTIVE);
			$get_desirable_listings_medium = \Model\Client\Has::forge()->load($my_desires_query_result_strings_medium, null)->as_array();
			$number_of_mediums = sizeof($get_desirable_listings_medium);
			if($number_of_mediums > 0){
				foreach($get_desirable_listings_medium as $q){
					$desire_list = $q['listing_id'];
					if(isset($_GET['inc'])){
						$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()
							->where('listing_id', '=', $listing['listing_id'])
							->where('ignore_listings', '=', $desire_list));
					}else{
						$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()
							->where('listing_id', '=', $listing['listing_id'])
							->where_open()
							->where('ignore_listings', '=', $desire_list)
							->or_where('save_listings', '=', $desire_list)
							->where_close());
					}
					if(!$check_for_ignore->loaded()){
						//Array of all the listings THIS listing wants EXACTLY
						if(!in_array($desire_list, $desirable_listings_exact)){
							$desirable_listings_medium[] = $desire_list; //$q['Listing_ID'];
							$desirable_all[] = $desire_list;
						}
					}
				}

				if($listing['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
					foreach($desirable_listings_medium as $round_2){
						$match = \Model\Matches::forge()->load(\DB::select_array()->from('matches')->where('listing_id', '=', $listing['listing_id'])
						->where_open()
						->where('matched_listings', '=', $round_2)
						->or_where('save_listings', '=', $round_2)
						->or_where('buyers_match', '=', $round_2)
						->where_close());
						if ($match->loaded()){
							$match->match_potential = 'Medium';
							$match->save();
						} else {
							$fields = array(
								'realtor_id' => $listing['realtor_id'],
								'listing_id' => $listing['listing_id'],
								'buyers_match' => $round_2,
								'seen' => \Model\Matches::MATCH_UNSEEN,
								'match_potential' => 'Medium'
							);
							$match->set_fields($fields);
							$match->save();
						}
					}
				}else{
					foreach($desirable_listings_medium as $round_2){
						$med_high = $this->check_matchs($round_2, 2, 1, $listing['listing_id'], 'Medium', $listing['realtor_id']);
						$med_med = $this->check_matchs($round_2, 1, 1, $listing['listing_id'], 'Medium', $listing['realtor_id']);
						$med_low = $this->check_matchs($round_2, 0, 1, $listing['listing_id'], 'Medium', $listing['realtor_id']);
					}
				}
			}
		}

		$my_desires_query_result_strings_high = \Listing::forge()->set_search_params_desires($get_my_desires_search_results, 0, $listing['listing_id']);
		if($number_of_exacts+$number_of_mediums <= 10){

			if($listing['type_of_listing'] == \Model\Listing::TYPE_XTRADED){
				$type_of_listing = \Model\Listing::TYPE_XTRADED;
			}else{
				$type_of_listing = \Model\Listing::TYPE_BUY_ONLY;
			}

			$my_desires_query_result_strings_high->where('listing_id', '<>', $listing['listing_id']);
			$my_desires_query_result_strings_high->where('listing_status', '=', \Model\Listing::LS_ACTIVE);
			$my_desires_query_result_strings_high->where('list_as_sold', '=', \Model\Listing::AS_NOT_SOLD);
			$my_desires_query_result_strings_high->where('type_of_listing', '=', \Model\Listing::TYPE_XTRADED);
			$get_desirable_listings_high = \Model\Client\Has::forge()->load($my_desires_query_result_strings_high, null)->as_array();
			$number_of_highs = sizeof($get_desirable_listings_high);
			if($number_of_highs > 0){
				foreach($get_desirable_listings_high as $q){
					$desire_list = $q['listing_id'];
					if(isset($_GET['inc'])){
						$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing['listing_id'])
							->where('ignore_listings', '=', $desire_list));
					}else{
						$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing['listing_id'])
							->where_open()
							->where('ignore_listings', '=', $desire_list)
							->or_where('save_listings', '=', $desire_list)
							->where_close());
					}
					if(!$check_for_ignore->loaded()){
						if(!in_array($desire_list, $desirable_listings_exact) && !in_array($desire_list, $desirable_listings_medium)){
							$desirable_listings_high[] = $desire_list;
							$desirable_all[] = $desire_list;
						}
					}
				}

				if($listing['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY){
					foreach($desirable_listings_high as $round_3){
						$match = \Model\Matches::forge()->load(\DB::select_array()->from('matches')->where('listing_id', '=', $listing['listing_id'])
						->where_open()
						->where('matched_listings', '=', $round_3)
						->or_where('save_listings', '=', $round_3)
						->or_where('buyers_match', '=', $round_3)
						->where_close());
						if($match->loaded()){
							$match->match_potential = 'Low';
							$match->save();
						} else {
							$fields = array(
								'realtor_id' => $listing['realtor_id'],
								'listing_id' => $listing['listing_id'],
								'buyers_match' => $round_3,
								'seen' => \Model\Matches::MATCH_UNSEEN,
								'match_potential' => 'Low'
							);
							$match->set_fields($fields)->save();
						}
					}
				}else{
					foreach($desirable_listings_high as $round_3){
						$high_high = $this->check_matchs($round_3, 2, 2, $listing['listing_id'], 'High', $listing['realtor_id']);
						$high_med = $this->check_matchs($round_3, 1, 2, $listing['listing_id'], 'High', $listing['realtor_id']);
						$high_low = $this->check_matchs($round_3, 0, 2, $listing['listing_id'], 'High', $listing['realtor_id']);
					}
				}
			}
		}

		return $desirable_all;
	}

	public function find_all_way_matches($listing_id, $from_cron=false){
		$buyers_match = $x_1_match_high = $x_1_match_med = $x_1_match_low = $y_1_match_high =
		$y_1_match_med = $y_1_match_low = $z_1_match_high = $z_1_match_med = $z_1_match_low =
		$x_2_match = $x_3_match = $x_4_match = array();

		$max_number_of_matches = \Sysconfig::get('max_matches');
		$desirable_all = $this->universal_lookup($listing_id, false);
		$listing = \Model\Client\Has::forge()->load_by_listing_id($listing_id);
		if (!$listing->loaded()) {
			\Log::error(__FILE__.' : '.__LINE__.''.print_r($listing_id, true));
		}
		$Want_Each_Other_IDs = array();
		$total_matches = $x_old_matches = $x_found_matches = 0;

		//show not found matches data when its not ready to xtrade
		if(\Model\Listing::forge()->is_ready_for_xtrade($listing_id) === false){
			$return['buyers_match'] = $buyers_match;
			$return['x_1_match_high'] = $x_1_match_high;
			$return['x_1_match_med'] = $x_1_match_med;
			$return['x_1_match_low'] = $x_1_match_low;
			$return['y_1_match_high'] = $y_1_match_high;
			$return['y_1_match_med'] = $y_1_match_med;
			$return['y_1_match_low'] = $y_1_match_low;
			$return['z_1_match_high'] = $z_1_match_high;
			$return['z_1_match_med'] = $z_1_match_med;
			$return['z_1_match_low'] = $z_1_match_low;
			$return['desirable_all'] = $desirable_all;
			$return['x_2_match'] = $x_2_match;
			$return['x_3_match'] = $x_3_match;
			$return['x_4_match'] = $x_4_match;
			$return['found_matches'] = $total_matches;
			$return['x_found_matches'] = $x_found_matches;
			$return['x_old_matches'] = $x_old_matches;
			return $return;
		}

		$seen_matches = array();
		$check_what_match_exists = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing->listing_id)
			->where('seen', '=', \Model\Matches::MATCH_SEEN), null)->as_array();
		foreach($check_what_match_exists as $row){
			$seen_ID = $row['matched_listings'];
			if(!$seen_ID){
				$seen_ID = $row['save_listings'];
			}
			if(!$seen_ID){
				$seen_ID = $row['ignore_listings'];
			}
			if(!$seen_ID){
				$seen_ID = $row['buyers_match'];
			}
			if($seen_ID){
				array_push($seen_matches, $seen_ID);
			}
		}

		/** July 18-2016 : added condition to break the matches run when limit from admin.max_matches exceed */
		if($listing->type_of_listing == \Model\Listing::TYPE_BUY_ONLY){
			if(!empty($desirable_all)){
				foreach($desirable_all as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$buyers_match[] = $this->setup_box_information_email($value, 'buyer_match', null, false, $from_cron);
					} else {
						$buyers_match[] = $this->setup_box_information($value, 'buyer_match', null);
					}
					$set_match_found = 1;
					if(in_array($value, $seen_matches)){
						$x_old_matches++;
					}else{
						$x_found_matches++;
					}
					$total_matches++;
				}
			}
		} else {
			//exact-high
			if(!empty($GLOBALS["Want_Each_Other_IDs_exact_high"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_exact_high"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$x_1_match_high[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$x_1_match_high[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;
					if(in_array($value, $seen_matches)){
						$x_old_matches++;
					} else {
						$x_found_matches++;
					}

					$total_matches++;
				}
			}

			//exact-med
			if(!empty($GLOBALS["Want_Each_Other_IDs_exact_med"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_exact_med"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$x_1_match_med[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$x_1_match_med[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;

					$total_matches++;
				}
			}

			//exact-low
			if(!empty($GLOBALS["Want_Each_Other_IDs_exact_low"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_exact_low"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$x_1_match_low[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$x_1_match_low[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;

					$total_matches++;
				}
			}

			//high-high
			if(!empty($GLOBALS["Want_Each_Other_IDs_high_high"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_high_high"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$y_1_match_high[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$y_1_match_high[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;

					$total_matches++;
				}
			}

			//high-med
			if(!empty($GLOBALS["Want_Each_Other_IDs_high_med"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_high_med"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$y_1_match_med[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$y_1_match_med[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;

					$total_matches++;
				}
			}

			//high-low
			if(!empty($GLOBALS["Want_Each_Other_IDs_high_low"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_high_low"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$y_1_match_low[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$y_1_match_low[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;

					$total_matches++;
				}
			}

			//medium-high
			if(!empty($GLOBALS["Want_Each_Other_IDs_med_high"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_med_high"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$z_1_match_high[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$z_1_match_high[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;

					$total_matches++;
				}
			}

			//medium-medium
			if(!empty($GLOBALS["Want_Each_Other_IDs_med_med"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_med_med"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$z_1_match_med[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$z_1_match_med[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;

					$total_matches++;
				}
			}

			//medium-low
			if(!empty($GLOBALS["Want_Each_Other_IDs_med_low"]) && $total_matches < $max_number_of_matches){
				foreach($GLOBALS["Want_Each_Other_IDs_med_low"] as $value){
					if($total_matches >= $max_number_of_matches){
						break;
					}
					if($from_cron === true){
						$z_1_match_low[] = $this->setup_box_information_email($value, 'B', null, false, $from_cron);
					} else {
						$z_1_match_low[] = $this->setup_box_information($value, 'B', null);
					}
					$set_match_found = 1;

					$total_matches++;
				}
			}

		}

		$return['buyers_match'] = $buyers_match;
		$return['x_1_match_high'] = $x_1_match_high;
		$return['x_1_match_med'] = $x_1_match_med;
		$return['x_1_match_low'] = $x_1_match_low;
		$return['y_1_match_high'] = $y_1_match_high;
		$return['y_1_match_med'] = $y_1_match_med;
		$return['y_1_match_low'] = $y_1_match_low;
		$return['z_1_match_high'] = $z_1_match_high;
		$return['z_1_match_med'] = $z_1_match_med;
		$return['z_1_match_low'] = $z_1_match_low;
		if(\Sysconfig::get('match_inc_all') == 1){
			$diff_result = $desirable_all; //All matches including direct matches.
		}else{
			$diff_result = array_diff($desirable_all, $GLOBALS['global_matches']); //matches I want but are not direct matches.
		}

		//3-way
		if($listing->type_of_listing != \Model\Listing::TYPE_BUY_ONLY && $total_matches < $max_number_of_matches){
			$Want_Each_Other_IDs = array($listing->listing_id);

			foreach($diff_result as $value){
				$results = \Match::forge()->search_results($value, false);
				array_push($Want_Each_Other_IDs, $value);
				$results = array_diff($results, $Want_Each_Other_IDs);
				$results = array_filter(array_map('trim', $results));
				foreach($results as $v_n){
					if($v_n == $listing->listing_id || $v_n == $value){
						//						continue;
					}
					$results_1 = \Match::forge()->search_results($v_n, $listing->listing_id);
					if($results_1[0] == $listing->listing_id){
						if($total_matches >= $max_number_of_matches){
							break;
						}
						$insert_val = $value.','.$v_n;
						$this->insert_into_matches($insert_val, $listing->listing_id, \Model\Matches::MATCH_UNSEEN, '3-Way', $listing->realtor_id);
						$x_2_match[][$value] = $v_n;
						$set_match_found = 1;

						if(in_array($insert_val, $seen_matches)){
							$x_old_matches++;
						}else{
							$x_found_matches++;
						}


						$total_matches++;
					}
					$Want_Each_Other_IDs = array($listing->listing_id);
				}
			}
		}
		$return['x_2_match'] = $x_2_match;

		//4-way
		if($listing->type_of_listing != \Model\Listing::TYPE_BUY_ONLY && $total_matches < $max_number_of_matches){
			$Want_Each_Other_IDs = array($listing->listing_id);


			foreach($diff_result as $value){
				$results = \Match::forge()->search_results($value, false);
				array_push($Want_Each_Other_IDs, $value);
				$results = array_diff($results, $Want_Each_Other_IDs);
				$results = array_filter(array_map('trim', $results));


				foreach($results as $v_n){
					if($v_n == $listing->listing_id || $v_n == $value){
						continue;
					}
					$results_1 = \Match::forge()->search_results($v_n, false);
					array_push($Want_Each_Other_IDs, $v_n);
					$results_1 = array_diff($results_1, $Want_Each_Other_IDs);
					$results_1 = array_filter(array_map('trim', $results_1));

					foreach($results_1 as $v_n_1){
						if($v_n_1 == $listing->listing_id || $v_n_1 == $v_n || $v_n_1 == $value){
							continue;
						}
						$results_2 = \Match::forge()->search_results($v_n_1, $listing->listing_id);
						if($results_2[0] == $listing->listing_id){
							if($total_matches >= $max_number_of_matches){
								break;
							}
							$insert_val = $value.','.$v_n.','.$v_n_1;
							\Match::forge()->insert_into_matches($insert_val, $listing->listing_id, \Model\Matches::MATCH_UNSEEN, '4-Way', $listing->realtor_id);
							$x_3_match[][$value][$v_n] = $v_n_1;
							$set_match_found = 1;
							if(in_array($insert_val, $seen_matches)){
								$x_old_matches++;
							}else{
								$x_found_matches++;
							}
							$total_matches++;

						}
					}
				}
				$Want_Each_Other_IDs = array($listing->listing_id);
			}
		}
		$return['x_3_match'] = $x_3_match;

		//5-way
		if($listing->type_of_listing != \Model\Listing::TYPE_BUY_ONLY && $total_matches < $max_number_of_matches){

			$Want_Each_Other_IDs = array($listing->listing_id);
			foreach($diff_result as $value){
				$results = \Match::forge()->search_results($value, false);
				array_push($Want_Each_Other_IDs, $value);
				$results = array_diff($results, $Want_Each_Other_IDs);
				$results = array_filter(array_map('trim', $results));
				foreach($results as $v_n){
					if($v_n == $listing->listing_id || $v_n == $value){
						continue;
					}
					$results_1 = \Match::forge()->search_results($v_n, false);
					array_push($Want_Each_Other_IDs, $v_n);
					$results_1 = array_diff($results_1, $Want_Each_Other_IDs);
					$results_1 = array_filter(array_map('trim', $results_1));
					foreach($results_1 as $v_n_1){
						if($v_n_1 == $listing->listing_id || $v_n_1 == $v_n || $v_n_1 == $value){
							continue;
						}
						$results_2 = \Match::forge()->search_results($v_n_1, false);
						array_push($Want_Each_Other_IDs, $v_n_1);
						$results_2 = array_diff($results_2, $Want_Each_Other_IDs);
						$results_2 = array_filter(array_map('trim', $results_2));
						foreach($results_2 as $v_n_2){
							if($v_n_2 == $listing->listing_id || $v_n_2 == $v_n_1 || $v_n_2 == $v_n || $v_n_2 == $value){
								continue;
							}
							$results_3 = \Match::forge()->search_results($v_n_2, $listing_id);
							if($results_3[0] == $listing->listing_id){
								if($total_matches >= $max_number_of_matches){
									break;
								}
								$insert_val = $value.','.$v_n.','.$v_n_1.','.$v_n_2;
								\Match::forge()->insert_into_matches($insert_val, $listing->listing_id, \Model\Matches::MATCH_UNSEEN, '5-Way', $listing->realtor_id);
								$x_4_match[][$value][$v_n][$v_n_1] = $v_n_2;
								$set_match_found = 1;
								if(in_array($insert_val, $seen_matches)){
									$x_old_matches++;
								}else{
									$x_found_matches++;
								}
								$total_matches++;

							}
						}
					}
				}
				$Want_Each_Other_IDs = array($listing->listing_id);
			}
		}
		//denormalize matches to client_has.matches || client_has.matches_found (not sure which one is used)
		$has = \Model\Client\Has::forge()->load(\DB::select()->where('listing_id', '=', $listing_id));
		$has->matches_found = $total_matches;
		$has->matches = $total_matches;
		$has->save();

		$return['desirable_all'] = $desirable_all;
		$return['x_4_match'] = $x_4_match;
		$return['found_matches'] = $total_matches;
		$return['x_found_matches'] = $x_found_matches;
		$return['x_old_matches'] = $x_old_matches;
		return $return;
	}

	public function matches($listing_id){
		$listing = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id));
		if(!$listing->loaded()){
			$found_matches_txt = '<strong class="matches-link" data-target="">'.__('refine_wants').'</strong>';
			return $found_matches_txt;
		}
		$data = $this->find_all_way_matches($listing->listing_id);
		$matches = \Model\Matches::forge()->count_seen_unseen_matches($listing_id);
		$total_matches = $matches['seen'] + $matches['unseen'];
		if($total_matches <= 0) {
			$found_matches_txt = __('refine_wants');
		} else {
			$txt = 'Matches Found : ' . $total_matches . ' Matches ';
			if($matches['unseen'] > 0){
				$inner = '<strong>';
				$inner .= '(';
				$inner .= ($matches['unseen'] == 1) ? $matches['unseen'] . ' '. __('new_match') : $matches['unseen'] . ' '. __('new_matches');
				$inner .= ')';
				$inner .= '</strong>';
			} else {
				$inner = '';
			}
			$txt .= $inner;
			$found_matches_txt = '<strong class="matches-link" data-target="/realtors/match/id/'.$listing_id.'">'.$txt.'</strong>';
		}
		return $found_matches_txt;
	}

	public function notify_all_matched_listings($listing_id){
		$all_listing_id = array();
		$matched_listings = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id), null)->as_array();
		if(sizeof($matched_listings > 0)){
			foreach($matched_listings as $row){
				$all_listing_id[] = $row['matched_listings'];
			}
		}
		$s = join(',', $all_listing_id);
		$array_s = explode(',', $s);
		if(count($array_s) <= 0){
			return true;
		}
		$listings = array_unique($array_s);
		foreach($listings as $q){
			$this->find_all_way_matches($q);
		}
		return true;
	}

	public function check_matchs($search_ID, $s_type, $knock_knock, $lID, $match_pot, $RID = 0, $off_page = false){
		$inc_maybe = $this->inc;
		$get_what_they_want = \Model\Client\Want::forge()->load(\DB::select_array()->where('listing_id', '=', $search_ID))->as_array();
		$d_list = \Listing::forge()->set_search_params_info($get_what_they_want, $s_type, $search_ID);
		$d_list->where('listing_id', '=', $lID);
		$d_list->where('type_of_listing', '<>', \Model\Listing::TYPE_SELL_ONLY);
		$get_desires = \Model\Client\Has::forge()->load($d_list);
		if($get_desires->loaded()){
			if($inc_maybe){
				$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $lID)
					->where('ignore_listings', '=', $search_ID));
			}else{
				$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $lID)
					->where('ignore_listings', '=', $search_ID)
					->or_where('listing_id', '=', $lID)
					->where('save_listings', '=', $search_ID));
			}
			if(!$check_for_ignore->loaded()){
				$check_if_match_exists = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $lID)
					->where_open()
					->where('matched_listings', '=', $search_ID)
					->or_where('save_listings', '=', $search_ID)
					->or_where('buyers_match', '=', $search_ID)
					->where_close());
				if($off_page){
					if(!$check_if_match_exists->loaded()){
						$fields = array(
							'realtor_id' => $RID,
							'listing_id' => $lID,
							'matched_listings' => $search_ID,
							'seen' => \Model\Matches::MATCH_UNSEEN,
							'match_potential' => $match_pot,
						);
						$check_if_match_exists->set_fields($fields);
						$check_if_match_exists->save();
					}
				}else{
					if(!$check_if_match_exists->loaded()){
						$fields = array(
							'realtor_id' => $RID,
							'listing_id' => $lID,
							'matched_listings' => $search_ID,
							'seen' => \Model\Matches::MATCH_UNSEEN,
							'match_potential' => $match_pot,
						);
						$check_if_match_exists->set_fields($fields);
						$check_if_match_exists->save();
					}else{
						$check_if_match_exists->match_potential = $match_pot;
						$check_if_match_exists->save();
					}
				}

				if($knock_knock == 0){
					if(!in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_exact_high"]) && !in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_exact_med"]) && !in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_exact_low"])){
						if($s_type == 2){
							$GLOBALS["Want_Each_Other_IDs_exact_high"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}elseif($s_type == 1){
							$GLOBALS["Want_Each_Other_IDs_exact_med"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}elseif($s_type == 0){
							$GLOBALS["Want_Each_Other_IDs_exact_low"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}
					}
				}elseif($knock_knock == 1){
					if(!in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_med_high"]) && !in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_med_med"]) && !in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_med_low"])){
						if($s_type == 2){
							$GLOBALS["Want_Each_Other_IDs_med_high"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}elseif($s_type == 1){
							$GLOBALS["Want_Each_Other_IDs_med_med"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}elseif($s_type == 0){
							$GLOBALS["Want_Each_Other_IDs_med_low"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}
					}
				}elseif($knock_knock == 2){
					if(!in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_high_high"]) && !in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_high_med"]) && !in_array($search_ID, $GLOBALS["Want_Each_Other_IDs_high_low"])){
						if($s_type == 2){
							$GLOBALS["Want_Each_Other_IDs_high_high"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}elseif($s_type == 1){
							$GLOBALS["Want_Each_Other_IDs_high_med"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}elseif($s_type == 0){
							$GLOBALS["Want_Each_Other_IDs_high_low"][] = $search_ID;
							$GLOBALS["global_matches"][] = $search_ID;
						}
					}
				}
			}
		}
	}

	public function insert_into_matches($search_ID, $lID, $cron, $match_pot, $RID){
		// TODO: should not have to check this here
		if (empty($lID) || empty($RID)) {
			\Log::error(__FILE__.' : '.__LINE__.' - missing data' . \Debug::backtrace() . "\n-$search_ID\n--$lID\n---$RID");
			return false;
		}

		$check_for_ignore = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $lID)
			->where_open()
			->where('ignore_listings', '=', $search_ID)
			->or_where('save_listings', '=', $search_ID)
			->where_close());
		if(!$check_for_ignore->loaded()){
			$check_if_match_exists = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $lID)
				->where_open()
				->where('matched_listings', '=', $search_ID)
				->or_where('save_listings', '=', $search_ID)
				->or_where('buyers_match', '=', $search_ID)
				->where_close());
			if(!$check_if_match_exists->loaded()){
				$fields = array(
					'realtor_id' => $RID,
					'listing_id' => $lID,
					'matched_listings' => $search_ID,
					'seen' => $cron,
					'match_potential' => $match_pot
				);
				\DB::insert('matches')->set($fields)->execute();
			}elseif($check_if_match_exists['seen'] == \Model\Matches::MATCH_SEEN){
				$matches = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $lID)
					->where('matched_listings', '=', $search_ID));
				$matches->match_potential = $match_pot;
				$matches->save();
			}else{
				$matches = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $lID)
					->where('matched_listings', '=', $search_ID));
				$matches->seen = $cron;
				$matches->match_potential = $match_pot;
				$matches->save();
			}
		}
		return true;
	}

	public function setup_box_information_email($in_1, $in_set, $add, $grey_bg = false, $from_cron = false){
		\Lang::load('matches');
		$buyer = false;
		$has = \Model\Client\Has::forge()->load_by_listing_id($in_1);
		list($country, $prov_state, $city) = \CityState::get_area_name($has->country, $has->prov_state, $has->country, true);
		$price = '$'.number_format($has->listing_price, 2);
		$sqft = number_format($has->sq_footage);
		$street = $has->address;
		$top = 'top';
		$info = \Model\Client\Information::forge()->load_by_listing_id($has->listing_id);
		$add_to = $ID_or_contact = '';

		if($in_set != 'buyer'){
			if($has->imported == \Model\Listing::IMPORTED){
				$actual_ID = '2_'.$has->external_id;
				if($from_cron === true){
					$image_set = \Model\Photos\Xtrade::forge()->get_local_imported_photo($has->external_id, 1, false);
				} else {
					$image_set = \Model\Crea\Property::forge()->get_image($has->external_id, 1, false);
				}
			}elseif($has->imported == 2){
				$actual_ID = '3_'.$has->external_id;
				if($from_cron === true){
					$image_set = \Model\Photos\Xtrade::forge()->get_local_imported_photo($has->external_id, 1, false);
				} else {
					$image_set = \Model\Crea\Property::forge()->get_image($has->external_id, 1, false);
				}
			}else{
				$actual_ID = '0_'.$has->listing_id;
				$image_set = \Model\Photos\Xtrade::forge()->get_first_photo($in_1, $from_cron);
			}
			if($in_set == 'A'){
				$home_title = __('your_home');
				$client_or_view = __('client').$info->first_name.' '.$info->last_name;
				$ID_or_contact = __('listingid').$in_1;
			} else {
				$client_or_view = \Html::anchor(\Config::get('uri').'listing/detail/'.$actual_ID, __('view_listing'));
			}

			if($in_set == 'B'){
				$home_title = __('your_match');
			}
			if($in_set == 'C'){
				$home_title = __('b_match');
				if($add){
					$add_to = __('c_wants_your_home');
				}
				if($grey_bg){
					$top = 'top_cd';
				}
			}
			if($in_set == 'D'){
				$home_title = __('c_match');
				if($add){
					$add_to = __('d_wants_your_home');
				}
				if($grey_bg){
					$top = 'top_cd';
				}
			}

			if($in_set == 'E'){
				$home_title = __('d_match_e_wants_your_home');
			}

			if($in_set == 'buyer_match'){
				$home_title = __('match_your_wants');
				$in_set = "&nbsp;";
			}



			if($image_set == 'no-image.png'){
				if($from_cron === true){
					$image_set = \Config::get('uri').'assets/images/no-image.png';
				} else {
					$image_set = \Config::get('uri').'assets/images/no-image.png';
				}
			} else {
				if($has->imported == \Model\Listing::IMPORTED){
					$crea = 'crea/';
				} else {
					$crea = '';
				}
				if($from_cron === true){
					$image_set = \Config::get('uri').'assets/images/'.$image_set;
				} else {
					//					$image_set = \Config::get('uri').'assets/images/listings/'.$crea.$image_set;
					$image_set = \Config::get('uri').'assets/images/'.$image_set;
				}

			}

		}else{
			$image_set = DOCROOT . 'assets/images/buyers.jpg';
			$price = __('buyer_only');
			$sqft = \Sysconfig::format_sqft($has->sq_footage);
			$street = $has->address.' '. \CityState::format_zip($has->z_p_code);
			$home_title = __('your_home');
			$client_or_view = $info->first_name.' '.$info->last_name;
			$ID_or_contact = __('listingid') . $in_1;
			$top = 'top';
			$in_set = '&nbsp;';
			$buyer = true;
		}

		$match_info = '';
		$data = array(
			'in_set' => $in_set,
			'home_title' => $home_title,
			'add_to' => $add_to,
			'image_set' => $image_set,
			'price' => $price,
			'street' => $street,
			'city' => $city,
			'prov_state' => $prov_state,
			'client_or_view' => $client_or_view,
			'ID_or_contact' => $ID_or_contact
		);

		if($in_set != 'A' && !$buyer){
			$match_info .= \View::forge('email/matches_box', $data);
		}else{
			$match_info .= \View::forge('email/matches_box_a', $data);
		}

		return $match_info;
	}

	public function render_matches($listing_id, $realtor_id, $inc=false){

		if(\Session::get('type') == \Model\User::UT_BROKER){
			$broker = \Model\Broker::forge(\Authlite::instance('auth_broker')->get_user()->id);
			$client_has = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id));
			$office = \Model\Realtor::forge()->load(\DB::select_array()->where('realtor_id', '=', $client_has->realtor_id));
			if($office->office_id == $broker->id){
				$has = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id)->where('listing_status', '=', \Model\Listing::LS_ACTIVE));
			}
		} else {
			$has = \Model\Client\Has::forge()->load(\DB::select_array()
				->where('listing_id', '=', $listing_id)
				->where('realtor_id', '=', $realtor_id)
				->where('listing_status', '=', \Model\Listing::LS_ACTIVE));
		}
		if(!$has->loaded() && $has->listing_status == \Model\Listing::LS_XCOMPLETE){
			\Session::instance()->set_flash('error', 'empty data');
			\Response::redirect('/realtors');
		}

		$listing = $has->as_array();
		if($listing['imported'] == \Model\Listing::IMPORTED){
			$src = \Model\Crea\Property::forge()->get_image($listing['external_id'], 1, false);
			$listing['listing_id'] = \Model\Crea\Property::forge($listing['external_id'])->listingid;
		}else{
			if($listing['type_of_listing'] != \Model\Listing::TYPE_BUY_ONLY){
				$src = \Model\Listing::IMG_DIR_NAME . \Model\Photos\Xtrade::forge()->get_first_photo($listing_id);
			} else {
				$src = 'buyers.jpg';
			}
		}

		$location = \CityState::get_area_name($listing['city'], $listing['prov_state'], $listing['country']);
		$fulladdress = ($listing['type_of_listing'] != \Model\Listing::TYPE_BUY_ONLY) ? implode(', ', array_merge(array($listing['address']), array($location))) .' '. \CityState::format_zip($listing['z_p_code']) : 'Buyer Only Listing';
		$data['listing'] = $listing;
		$data['client_house_img'] = $src;
		$data['address'] = ($location == '') ? '' : $fulladdress;
		$data['location'] = $location;
		$client_info = \Model\Client\Information::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id))->as_array();

		$data['client']['listing'] = \Model\Client\Has::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id))->as_array();
		$data['client']['info'] = $client_info;

		if($data['client']['listing']['listing_status'] == \Model\Listing::LS_ACTIVE){
			$public_status = '<span class="active">'.__('active').'</span>';
		}elseif($data['client']['listing']['listing_status'] == \Model\Listing::LS_XCOMPLETE){
			$public_status = '<span class="incomplete">'.__('incomplete').'</span>';
		}else{
			$public_status = '<span class="inactive">'.__('inactive').'</span>';
		}
		$data['match_exists'] = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id)
			->where(\DB::expr('save_listings IS NOT NULL')),NULL)->as_array();
		$data['inc_maybe'] = $inc ? true : false;

		$ignore_array = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id)
			->where(\DB::expr('ignore_listings IS NOT NULL')), NULL)->as_array();
		$data['ignore_array'] = $ignore_array;

		//////////////////////////////matches original start//////////////////////////////////
		$preview_results_client = \Model\Client\Information::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id));
		$GLOBALS['fname'] = $preview_results_client['first_name'];
		$GLOBALS['lname'] = $preview_results_client['last_name'];
		$match_exists = \Model\Matches::forge()->load(\DB::select_array()->where('listing_id', '=', $listing_id)
			->where(\DB::expr('save_listings IS NOT NULL')));
		$data['match_exists'] = $match_exists->loaded();
		//reset this
		$data['how_it_works'] = '';
		$this->inc = $inc;
		$data['this_listing_box'] = $this->get_current_listing_box($listing_id, $listing['type_of_listing'], false);
		//		$this->remove_matches($listing['listing_id']);
		$matches = $this->find_all_way_matches($listing_id);
		$data['buyers_match'] = $matches['buyers_match'];

		/** 2-way trade */
		$data['x_1_match_high']	= $matches['x_1_match_high'];
		$data['x_1_match_med']	= $matches['x_1_match_med'];
		$data['x_1_match_low']	= $matches['x_1_match_low'];

		$data['y_1_match_high']	= $matches['y_1_match_high'];
		$data['y_1_match_med']	= $matches['y_1_match_med'];
		$data['y_1_match_low']	= $matches['y_1_match_low'];

		$data['z_1_match_high']	= $matches['z_1_match_high'];
		$data['z_1_match_med']	= $matches['z_1_match_med'];
		$data['z_1_match_low']	= $matches['z_1_match_low'];

		/** 3-way trade */
		$data['x_2_match'] = $matches['x_2_match'];

		/** 4-way trade */
		$data['x_3_match'] = $matches['x_3_match'];

		/** 5-way trade */
		$data['x_4_match'] = $matches['x_4_match'];
		$data['ignore_listing_exists'] = \Model\Matches::forge()->get_ignore_listings($listing_id);
		$data['found_matches'] = ($matches['found_matches'] == 1) ? ' '.__('match_found') : ' '.__('matches_found');
		$data['total_matches'] = $matches['found_matches'];
		$data['desirable_all'] = $matches['desirable_all'];
		$data['matches_data'] = $matches;

		$optimize_attr = array('class' => 'button match_button Optimize');
		$reset_attr = array('type' => 'button', 'class' => 'button reset match_button reset_button');
		$sendmatches_attr = array('type' => 'button', 'class' => 'button Send_It match_button reset_button');
		$btn_txt = __('return_home');
		if(\Session::get('type') == \Model\User::UT_BROKER){
			$optimize_attr['disabled'] = 'disabled';
			$reset_attr['disabled'] = 'disabled';
			$sendmatches_attr['disabled'] = 'disabled';
			$btn_txt = __('return_dashboard');
		}
		$data['button']['optimize'] = \Form::button('button', __('optimize_matches'), $optimize_attr);
		$data['button']['reset'] = \Form::button('reset_matches', __('reset_match'), $reset_attr);
		$data['button']['send_matches'] = \Form::button('send_matches', __('send_matches'), $sendmatches_attr);

		$data_type = \Session::get('type') == '' ? \Model\User::UT_ADMIN : \Session::get('type');
		$data['button']['return_home'] = \Form::button('return_home', $btn_txt, array(
			'class' => 'wants_buttons return_home',
			'id' => 'return_home',
			'type' => 'button',
			'data-type' => \Model\User::forge()->get_types($data_type).'s'
		));
		if (empty($listing['external_id'])) {
			$data['header']['listing_num'] = '<em>xTradeHomes ID#:</em> <strong> ' . $listing['listing_id'] . '</strong>';
		} else {
			$data['header']['listing_num'] = '<em>MLS&reg; #:</em> <strong> ' . $listing['listing_id'] . '</strong>';
		}
		$data['header']['address'] = ($location == '') ? '' : $fulladdress;
		if ($client_info['first_name'] == '' && $client_info['last_name'] == ''){
			$data['header']['client'] = '<em>Client:</em> <strong>-</strong>';
		} else {
			$data['header']['client'] = '<em>Client:</em> <strong>'.$client_info['first_name'].' '.$client_info['last_name'].'</strong>';
		}
		$data['header']['public_status'] = '<em>Public Status:</em> '.$public_status;
		$data['header']['xtrade_status'] = '<em>xTrade Status:</em> <strong>'.\Model\Listing::forge()->get_xtrade_status($listing['list_as_sold']).'</strong>';
		return $data;
	}

}