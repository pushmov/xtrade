<?php
/**
* Common email functions
*
*/
class Emails {
	protected $_email;
	protected $_types = array('realtor', 'broker', 'vendor', 'admin');
	protected $_member;
	public function __construct($reply = null) {
		\Package::load('email');
		$this->_email = \Email::forge();
		if ($reply !== null) {
			$this->_email->from($reply);
		}
		$this->_member = new stdClass();
		$this->_member->email_type = \Model\User::EMAIL_SUBSCRIBE;
		\Lang::load('public');
		\Asset::add_path('assets/images/email/', 'img');
	}

	public function send(){
		if($this->_member->email_type == \Model\User::EMAIL_SUBSCRIBE){
			return $this->_email->send();
		}
	}

	public function set_member($data){
		$this->_member = $data;
	}

	public static function forge() {
		return new self;
	}

	public function forgot($data, $pass, $type) {
		$vars['url'] = \Uri::base(false) . 'pass_reset/' . $data['pass_reset'] . '/' . $type;
		$vars['pass'] = $pass;
		$footer['unsubscribe_link'] = \Html::anchor('stop_mail/' . $data['email'] . '/' . $type, 'here');
		$footer['gst_num'] = '';
		$vars['email_footer'] = \View::forge('email/footer', $footer);
		$this->_email->to($data['email']);
		$this->_email->subject(__('password_reset'));
		$this->_email->html_body(\View::forge('email/forgot', $vars));
		try	{
			$this->send();
			$this->track_email('', $data['email'],'', 'System Email', __('password_reset'));
			return true;
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
		}
		return false;
	}

	public function activate($data, $type) {
		$this->_email->to($data['email']);
		if (($bcc = \Sysconfig::get('email_bcc')) != '') {
			$this->_email->bcc($bcc);
		}
		$this->_email->subject('Verify Email Address');
		$this->_email->html_body(\View::forge('email/activate_' . $type, $data));
		try	{
			$this->send();
			return true;
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
		}
		return false;
	}

	public function welcome($data, $type) {
		$footer['unsubscribe_link'] = \Html::anchor('stop_mail/' . $data['email'] . '/' . $type, 'here');
		$vars['email_footer'] = \View::forge('email/footer', $footer);
		$this->_email->to($data['email']);
		if (($bcc = \Sysconfig::get('email_bcc')) != '') {
			$this->_email->bcc($bcc);
		}
		$this->_email->subject('Welcome to xTradeHomes!');
		$this->_email->html_body(\View::forge('email/welcome_' . $type, $data));
		try	{
			$this->send();
			return true;
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
		}
		return false;
	}

	public function track_email($name_to, $email_to, $name_from = '', $email_from = 'System Email', $subject = '', $id = '', $cc = '', $bcc = ''){
		if($this->_member->email_type == \Model\User::EMAIL_SUBSCRIBE){
			$query = \DB::insert('email_log');
			$query->columns(array('email_from_name','email_from','email_to_name','email_to','cc','bcc','subject', 'homebase_id'));
			$query->values(array($name_from,$email_from,$name_to,$email_to,$cc,$bcc,$subject,$id))->execute();
		}
	}

	public static function email_template_header(){
		$view = \View::forge('email/header');
		return $view;
	}

	public static function email_template_footer($unsubscribe_link=NULL){
		$data['unsubscribe_link'] = $unsubscribe_link;
		$view = \View::forge('email/footer', $data);
		return $view;
	}

	public static function email_us($switch, $params){
		switch ($switch){
			case 'listing_upgrade':
				$view = \View::forge('email/email_us/listing_upgrade', $params);
				break;
		}

		return $view;
	}

	public function upgrade_listing_recurring($data){
		$subject = 'Listing Upgrade at xTradeHomes!';
		//email receipt
		$this->_email->to($data['email']);
		$this->_email->subject($subject);
		$data['for_url'] = ($data['imported'] == \Model\Listing::IMPORTED) ? 'listing/detail/2_'.$data['external_id'] : 'listing/detail/0_'.$data['listing_id'];
		$this->_email->html_body(\View::forge('email/upgraded_listing', $data));
		try {
			$this->send();
			$this->track_email($data['first_name'] .' '. $data['last_name'], $data['email'],'', 'System Email', $subject);
			$e_response = array('status' => 'OK', 'message' => 'OK');
		} catch(\EmailValidationFailedException $e) {
			$e_response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$e_response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $e_response;
	}

	public function ipn_report($textreport, $subject){
		$this->_email->to('paypal@xtradehomes.com');
		$this->_email->subject($subject);
		$this->_email->body($textreport);
		try {
			$resp = $this->send();
		}
		catch(\EmailValidationFailedException $e) {
			$resp = $this->response(array('status' => $e->getMessage()));
		}
		catch(\EmailSendingFailedException $e) {
			$resp = $this->response(array('status' => $e->getMessage()));
		}

		return $resp;
	}

	public function ipn_failed_suspended($type, $data){
		if($type == 'Missed_Payment'){
			$view = \View::forge('email/missed_payment', $data);
		} elseif($type == 'Payments_Failed') {
			$view = \View::forge('email/payment_failed', $data);
		}

		$this->_email->to($data['mailto']);
		$this->_email->subject('Message From xTradeHomes');
		$this->_email->html_body($view);

		try {
			$this->send();
			$response = array('status' => 'OK', 'message' => 'OK');
		}
		catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
	}

	public function ipn_mail_after($param){
		$this->_email->from($param['sender_name']['from'], $param['sender_name']['name']);
		$this->_email->to('paypal@xtradehomes.com');
		$this->_email->subject($param['subject']);
		$this->_email->html_body(\View::forge('email/ipn_mailer', $param));

		try {
			$this->send();
			$response = array('status' => 'OK', 'message' => 'OK');
		}
		catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}

		return $response;
	}

	public function vendor_downgrade($param){
		$this->_email->to('paypal@xtradehomes.com');
		$this->_email->subject('Listing Upgrade at xTradeHomes!');
		$this->_email->html_body(\View::forge('email/vendor_upgrade', $param));

		try {
			$this->send();
			$this->track_email('paypal@xtradehomes.com', 'paypal@xtradehomes.com','', 'System Email', 'Listing Upgrade at xTradeHomes!');
			$response = array('status' => 'OK', 'message' => 'OK');
		}
		catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}

		return $response;

	}

	public function email_matches_cron($data){
		$this->_email->to($data['mailto']);
		$this->_email->subject('xTradeHomes Matches!');
		$this->_email->html_body(\View::forge('email/matches', $data));
		try {
			$this->send();
			$this->track_email($data['mailtoname'], $data['mailto'],'', 'System Email', 'xTradeHomes Matches!');
			$response = array('status' => 'OK', 'message' => 'OK');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}

		return $response;
	}

	public function request_client_wants($data){
		$this->_email->to($data['emailto']);
		$this->_email->subject('xTradeHomes Wants Form Request');
		$this->_email->html_body(\View::forge('email/wants_request', $data));
		try {
			$this->send();
			$this->track_email($data['r_name'], $data['emailto'],'', 'System Email', 'xTradeHomes Wants Form Request');
			$response = array('status' => 'OK', 'message' => 'Request Sent!<br>You will be notified when they complete the form.');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function send_matches($data){
		$this->_email->to($data['mailto'], $data['mailtoname']);
		$this->_email->subject('xTradeHomes Matches!');
		$this->_email->html_body(\View::forge('email/send_matches', $data));
		try {
			$this->send();
			$this->track_email($data['mailtoname'], $data['mailto'],'', 'System Email', 'xTradeHomes Matches!');
			$response = array('status' => 'OK', 'message' => 'Matches successfully sent to '.$data['mailtoname']);

		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function upgradevendor($data){
		$this->_email->to($data['email'], $data['company_name']);
		$this->_email->subject('xTradeHomes Advertisement Upgrade!');
		$this->_email->html_body(\View::forge('email/upgrade_vendor', $data));
		try {
			$this->send();
			$this->track_email($data['first_name'].' '.$data['last_name'], $data['email'],'', 'System Email', 'xTradeHomes Advertisement Upgrade!', $data['vendor_id'], '', \Sysconfig::get('email_bcc'));
			$response = array('status' => 'OK', 'message' => 'OK');
		}
		catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function invite_realtor($data){
		\Lang::load('common');
		$this->_email->to($data['to'], $data['first_name'] . ' ' . $data['last_name']);
		$this->_email->subject('Special Invitation to Join xTradeHomes');

		$http_build_query = array(
			'broker' => $data['broker_id'],
			'email' => $data['to'],
			'fname' => $data['first_name'],
			'lname' => $data['last_name'],
			'id' => $data['realtor_id'],
			'bref' => 'true'
		);
		$data['url'] = http_build_query($http_build_query);
		$this->_email->html_body(\View::forge('email/new_realtor_from_broker', $data));

		try {
			$this->send();
			//$this->track_email($data['first_name'] . ' ' . $data['last_name'], $data['to'],'', 'System Email', 'Message From Your Broker Regarding xTradeHomes!');
			$response = array('status' => 'OK', 'message' => __('realtor_added'));
		}
		catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		}
		catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		}

		return $response;
	}

	public function new_realtor_from_broker($data){
		\Lang::load('common');
		$this->_email->to($data['to'], $data['first_name'] . ' ' . $data['last_name']);
		$this->_email->subject('Special Invitation to Join xTradeHomes');

		$data['data'] = $data;

		$url = array(
			'broker' => $data['broker_id'],
			'email' => $data['email'],
			'fname' => $data['first_name'],
			'lname' => $data['last_name'],
			'id' => $data['realtor_id']
		);
		$data['url'] = http_build_query($url);
		$this->_email->html_body(\View::forge('email/new_realtor_from_broker', $data));
		try {
			$this->send();
			$response = array('status' => 'ok', 'message' => 'Realtor Added! They Have Been Notified!');
			//$this->track_email($data['first_name'].' '.$data['last_name'], $data['email'],'', 'System Email', 'Message From Your Broker Regarding xTradeHomes!');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function contact_agent($data){
		\Lang::load('agent');
		$ToSubject 	= 'Potential Lead From xTradeHomes';
		switch ($data['direction']){
			case \Model\Realtor::VIEW_MEMBER_TO_MEMBER:
				$data['view_file'] = 'email/contact_agent/member_to_member';
				break;
			case \Model\Realtor::VIEW_MEMBER_TO_NON_MEMBER:
				$data['view_file'] = 'email/contact_agent/to_non_member';
				break;
			default :
				$data['view_file'] = 'email/contact_agent/default';
				break;
		}
		//listing not found in client_has view
		if($data['not_found_flag'] === true){
			$data['view_file'] = 'email/contact_agent/not_found_listing';
		}

		$body = \View::forge($data['view_file']);
		if (isset($data['unsubscribe_link']) && ($data['unsubscribe_link'] != '')) {
			$body->set_global('unsubscribe_link', $data['unsubscribe_link']);
		}
		$body->data = $data;

		$v = new \vCard();
		$v->setPhoneNumber($data['phone'], 'PREF;HOME;VOICE');
		$split_name = explode(' ', $data['name']);
		$v->setFormattedName($data['name']);
		$v->setName($split_name[0], isset($split_name[1]) ? $split_name[1] : '');
		$v->setEmail($data['email']);
		$output = $v->getVCard();
		$filename = $v->getFileName();
		$this->_email->string_attach($output, $filename);
		$message = 'Thank you for the request, ';
		if (empty($data['agent_email'])) {
			$this->_email->to(\Sysconfig::get('email_contact'));
			$message .= 'our xTrading member '.$data['agent']['name'].' should contact you shortly. Or you can phone them at '.$data['agent']['phone'];
		} else {
			$this->_email->to($data['agent_email']);
			$message .= 'our xTrading member '.$data['agent_name'].' should contact you shortly. Or you can phone them at '.\Num::format_phone($data['agent_phone']);
		}
		$this->_email->subject($ToSubject);
		$this->_email->html_body($body);
		try {
			$this->_email->send();
			$this->track_email($data['name'], $data['agent_email'],'', 'System Email', $ToSubject);
			$response = array('status' => 'OK', 'message' => $message);
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function admin_reset_password($data){
		$this->_email->to($data['to']);
		$this->_email->subject('xTradeHomes Password Reset');
		$this->_email->html_body(\View::forge('email/admin_reset_password', $data));
		try {
			$this->send();
			$response = array('status' => 'ok');
			$this->track_email('', $data['to'],'', 'System Email', 'xTradeHomes Password Reset');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function contact_us($data){
		$this->_email->to(\Sysconfig::get('email_contact'));
		$this->_email->subject('Contact Us');
		$this->_email->html_body(\View::forge('email/contact_us', $data));
		try {
			$this->send();
			$response = array('status' => 'ok');
			$this->track_email('', $data['email_to'], '', 'System Email', 'Contact Us');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		}
		return $response;
	}

	/**
	* Used for admin to contact a system user.
	*
	* @param mixed $data
	*/
	public function admin_contact($data){
		$this->_email->to($data['email']);
		$this->_email->subject('xTradeHomes Message');
		$this->_email->html_body(\View::forge('email/admin/general', $data));
		try {
			$this->send();
			$response = array('status' => 'ok');
			$this->track_email('', $data['email'], '', 'System Email', 'Admin Contact');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function complete_client_wants($data){
		$this->_email->to($data['realtor']['email']);
		$this->_email->subject('Completed Wants Form at xTradeHomes');
		$this->_email->html_body(\View::forge('email/complete_client_wants', $data));
		try {
			$this->send();
			$response = array('status' => 'ok', 'message' => '');
			$this->track_email($data['realtor']['first_name'].' '.$data['realtor']['last_name'], $data['realtor']['email'],'', 'System Email', 'Completed Wants Form at xTradeHomes');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'error', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function listing_expired_notify($data){
		$this->_email->to($data['email']);
		$this->_email->subject('xTradeHomes Listings Expiry Alert!');
		$this->_email->html_body(\View::forge('email/listing_expire', $data));
		try {
			$this->send();
			$response = array('status' => 'OK', 'message' => '');
			$this->track_email($data['first_name'].' '.$data['last_name'], $data['email'],'', 'System Email', 'xTradeHomes Listings Expiry Alert!');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function listing_featured_expire_notify($data){
		$this->_email->to($data['email']);
		$this->_email->subject('xTradeHomes Featured Listing Alert!');
		$this->_email->html_body(\View::forge('email/listing_feature_expire', $data));
		try {
			$this->send();
			$response = array('status' => 'OK', 'message' => '');
			$this->track_email($data['first_name'].' '.$data['last_name'], $data['email'],'', 'System Email', 'xTradeHomes Featured Listing Alert!');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function inactive_listing_reminder($data){
		$this->_email->to($data['email']);
		$this->_email->subject('xTradeHomes Inactive Listing Alert!');
		$this->_email->html_body(\View::forge('email/inactive_listing_reminder', $data));
		try {
			$this->send();
			$response = array('status' => 'OK', 'message' => '');
			$this->track_email($data['first_name'].' '.$data['last_name'], $data['email'],'', 'System Email', 'xTradeHomes Inactive Listing Alert!');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function broker_request_new($post){
		$this->_email->from('no-reply@xtradehomes.com', 'xTradeHomes Customer Service');
		\Asset::add_path('assets/images/email/', 'img');
		$this->_email->to($post['invite_email']);
		if (($bcc = \Sysconfig::get('email_bcc')) != '') {
			$this->_email->bcc($bcc);
		}
		$this->_email->subject("Message From ".$post['first_name']." ".$post['last_name']." Regarding xTradeHomes!");
		$vars['cname'] = $post['invite_name'];
		$vars['msg'][1] = $post['first_name']." ".$post['last_name'].", has requested that you join xTradeHomes;";
		$vars['msg'][2] = \Html::anchor("signup/broker/?rid=".$post['id']."&rref=true&email=".$post['invite_email'], 'Click here');
		$vars['msg'][2] .= ' to signup today to take advantage of all the things xTradeHomes has to offer!';
		$vars['msg'][3] = 'If you cannot click on the link, copy and paste this url ' . \Uri::base(false) . 'signup/broker/?rid='.$post['id'].'&rref=true&email='.$post['invite_email'];
		$this->_email->html_body(\View::forge('email/broker_request', $vars));
		try	{
			$this->_email->send();
			$response = array('status' => 'OK', 'message' => '');
			$this->track_email('', $post['invite_email'], 'xTradeHomes Customer Service', 'no-reply@xtradehomes.com', 'invite request');
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	/**
	* Agent request broker association to existing broker account
	*
	*/
	public function broker_request_assoc($broker, $realtor){
		$this->_email->from('no-reply@xtradehomes.com', 'xTradeHomes Customer Service');
		\Lang::load('public');
		\Asset::add_path('assets/images/email/', 'img');
		$this->_email->to($broker->email);
		if (($bcc = \Sysconfig::get('email_bcc')) != '') {
			$this->_email->bcc($bcc);
		}
		$this->_email->subject('xTradeHomes Add REALTOR® Request');
		$view = \View::forge('email/broker_assoc_request');
		$view->agent = "{$realtor->first_name} {$realtor->last_name}";
		$view->broker = $broker->first_name;
		$this->_email->html_body($view);
		try	{
			$this->_email->send();
			$response = array('status' => 'OK', 'message' => '');
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function broker_approve_request($realtor){
		if(($bcc = \Sysconfig::get('email_bcc')) != ''){
			$this->_email->bcc($bcc);
		}
		$this->_email->to($realtor->email);
		$this->_email->subject('Message Regarding Broker Request on xTradeHomes!');
		$vars['cname'] = $this->name;
		$vars['first_name'] = $realtor->first_name;
		$vars['last_name'] = $realtor->last_name;
		$vars['url'] = \Uri::base(false) . "signup.php?id={$realtor->id}&bref=true";
		$this->_email->html_body(\View::forge('email/broker_realtor_approved', $vars));
		try	{
			$this->_email->send();
			$this->track_email('', $this->email, 'xTradeHomes Customer Service', 'no-reply@xtradehomes.com', 'realtor approved');
			$response = array('status' => 'OK', 'message' => '');
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function broker_approve_request_existing($realtor, $broker){
		if(($bcc = \Sysconfig::get('email_bcc')) != ''){
			$this->_email->bcc($bcc);
		}
		$this->_email->to($realtor->email);
		$this->_email->subject('Message Regarding Broker Request on xTradeHomes!');
		$vars['company_name'] = $broker->company_name;
		$vars['first_name'] = $realtor->first_name;
		$vars['last_name'] = $realtor->last_name;
		$vars['url'] = \Uri::base(false) . "realtors/profile";
		$this->_email->html_body(\View::forge('email/broker_realtor_approved_existing', $vars));
		try	{
			$this->_email->send();
			$this->track_email('', $realtor->email, 'xTradeHomes Customer Service', 'no-reply@xtradehomes.com', 'realtor approved');
			$response = array('status' => 'OK', 'message' => '');
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function broker_decline_request($data){
		if(($bcc = \Sysconfig::get('email_bcc')) != ''){
			$this->_email->bcc($bcc);
		}
		$this->_email->to($data['email']);
		$this->_email->subject('Message Regarding Broker Request on xTradeHomes!');
		$vars['company_name'] = $data->company_name;
		$vars['first_name'] = $data->first_name;
		$vars['last_name'] = $data->last_name;
		$vars['url'] = \Uri::base(false).'realtors';
		$this->_email->html_body(\View::forge('email/broker_realtor_declined', $vars));
		try	{
			$this->_email->send();
			$this->track_email('', $data->email, 'xTradeHomes Customer Service', 'no-reply@xtradehomes.com', 'realtor declined');
			$response = array('status' => 'OK', 'message' => '');
		} catch(\EmailValidationFailedException $e)	{
			// The validation failed
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			// The driver could not send the email
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}
		return $response;
	}

	public function new_crea_office($data) {
		if (is_array($data) && !empty($data)) {
			$this->_email->to(explode(',', \Sysconfig::get('email_new_office')));
			$this->_email->subject('CREA Feed New Offices');
			$this->_email->html_body(\View::forge('email/admin/new_crea_office', array('data' => $data)));
			try {
				$this->send();
				\Log::error(__FILE__.' : '.__LINE__.'New Offices');
			} catch(\EmailValidationFailedException $e) {
				\Log::error(__FILE__.' : '.__LINE__.''.print_r($e, true));
			} catch(\EmailSendingFailedException $e) {
				\Log::error(__FILE__.' : '.__LINE__.''.print_r($e, true));
			}
		}
	}

	public function new_agent($data) {
		if (is_array($data) && !empty($data)) {
			$this->_email->to(explode(',', \Sysconfig::get('email_new_office')));
			$this->_email->subject('New xTrade Agent Registration');
			$this->_email->html_body(\View::forge('email/admin/new_agent', $data));
			try {
				$this->send();
				\Log::error(__FILE__.' : '.__LINE__.'New Registration');
			} catch(\EmailValidationFailedException $e) {
				\Log::error(__FILE__.' : '.__LINE__.''.print_r($e, true));
			} catch(\EmailSendingFailedException $e) {
				\Log::error(__FILE__.' : '.__LINE__.''.print_r($e, true));
			}
		}
	}

	public function agent_first_download($data){
		$this->_email->to($data['email']);
		$this->_email->subject('xTradeHomes Listings Downloading');
		$this->_email->html_body(\View::forge('email/agent/first_download', $data));
		try {
			$this->send();
			$response = array('status' => 'OK', 'message' => 'OK');
		} catch(\EmailValidationFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		} catch(\EmailSendingFailedException $e) {
			$response = array('status' => 'ERROR', 'message' => $e->getMessage());
		}

		return $response;
	}


}
