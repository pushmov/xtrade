<?php
/**
* Other validation rules
*/
class Rules {
	public static function _validation_valid_domain($val) {
		if ($val == '') {
			return true;
		}
		return (bool)preg_match('/(?:https?:\/\/)?(?:[a-zA-Z0-9.-]+?\.(?:[a-zA-Z])|\d+\.\d+\.\d+\.\d+)/', $val);
	}
	/**
	* Checks for valid realtor promo code
	*
	* @param string $val
	* @return bool
	*/
	public static function _validation_valid_promo_r($val) {
		if ($val == '') {
			return true;
		}
		$result = \DB::select()->from('plans')->where('status_id', 'IN', array(1,2))
			->where('type_id', \Model\Plan::PT_REALTOR)->where('promo_code',$val)->execute();
		return (bool)$result->count();
	}
	/**
	* Checks for valid vendor promo code
	*
	* @param string $val
	* @return bool
	*/
	public static function _validation_valid_promo_v($val) {
		if ($val == '') {
			return true;
		}
		$result = \DB::select()->from('plans')->where('status_id', 'IN', array(1,2))
			->where('type_id', \Model\Plan::PT_VENDOR)->where('promo_code',$val)->execute();
		return (bool)$result->count();
	}
}