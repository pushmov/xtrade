<?php

class Pdf {
	const AUTHOR = 'xTradeHomes';
	const PDF_MARGIN_BOTTOM = 0;
	const PDF_PAGE_ORIENTATION = ''; //[empty string] = automatic orientation

	const MATCHES_BOX = 41;
	const MATCHES_BOX_SHUFFLE = 13;

	const BORDER = 1;
	const NO_BORDER = 0;
	const NO_PAGE_BREAK = 0;
	const PAGE_BREAK = 1;
	const TOC_PAGE = true;

	protected static $_pdf;

	protected $_two_way_width;
	protected $_three_way_width;
	protected $_four_way_width;
	protected $_five_way_width;

	public function __construct() {
		$this->_two_way_width = (2 * self::MATCHES_BOX) + self::MATCHES_BOX_SHUFFLE;
		$this->_three_way_width = (3 * self::MATCHES_BOX) + (2 * self::MATCHES_BOX_SHUFFLE);
		$this->_four_way_width = (4 * self::MATCHES_BOX) + (3 * self::MATCHES_BOX_SHUFFLE);
		$this->_five_way_width = (5 * self::MATCHES_BOX) + (4 * self::MATCHES_BOX_SHUFFLE);
	}

	public static function forge($title='', $orientation='P') {
		\Package::load('tcpdf');
		self::$_pdf = new \TCPDF($orientation, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		self::$_pdf->SetCreator(PDF_CREATOR);
		self::$_pdf->SetTitle($title);
		self::$_pdf->SetAuthor(self::AUTHOR);
		self::$_pdf->setPrintFooter(false);
		self::$_pdf->setPrintHeader(false);
		self::$_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		self::$_pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		self::$_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		self::$_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		self::$_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		self::$_pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
		self::$_pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		self::$_pdf->SetFont('dejavusans', '', 10);
		self::$_pdf->AddPage('', '', false, false);
		return new self;
	}

	public function SetAutoPageBreak($flag){
		self::$_pdf->SetAutoPageBreak($flag, self::PDF_MARGIN_BOTTOM);
	}


	public function WriteMatchesPage(\Model\Client\Has $listing, $inc = false) {
		$pdf = self::$_pdf;
		$pdf->SetAutoPageBreak(TRUE, self::PDF_MARGIN_BOTTOM);
		$match_class = \Match::forge();
		$match_class->template = 'table';
		$m = \Match::forge();
		$m->template = 'table';
		$m->show_contact = false;
		$data = $m->render_matches($listing->listing_id, $listing->realtor_id, $inc);
		$data['m'] = $m;

		$view = \View::forge('matches/matches_pdf');
		$view->data = $data;
		$view->this_listing_box = $match_class->get_current_listing_box($listing->listing_id, $listing->type_of_listing, false);

		/* template with native table */
		$pdf->setPageOrientation('L');
		$pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);

		$shuffle_break = '<br><br><br><br><br><br><br><br><br><br><br>';

		$info_right = $data['address'].'<br>';
		$info_right .= 'Client : <b>'.$data['client']['info']['first_name'].' '.$data['client']['info']['last_name'].'</b><br>';
		$info_right .= 'Listing ID : <b>'.$data['listing']['listing_id'].'</b><br>';
		//$info_right .= 'Public Status : <b>'.$data['public_status'].'</b><br>';
		$info_right .= 'xTrade Status : <b>'. \Model\Listing::forge()->get_xtrade_status($data['listing']['list_as_sold']) .'</b><br>';

		$pdf->writeHTMLCell(200, '', '', $pdf->getY(), 'xTradeHomes Matches to '.$data['listing']['address'].' '.$data['listing']['listing_id'], self::NO_BORDER, 1, false, true, 'L', true);
		$pdf->Ln(5);
		//$pdf->writeHTMLCell(50, '', '', $pdf->getY(), \Asset::img($data['client_house_img']), self::NO_BORDER, 0, false, true, 'L', true);
		$pdf->writeHTMLCell(200, '', '', '', $info_right, self::NO_BORDER, 0, false, true, 'L', true);

		if (!empty($data['buyers_match'])) {
			foreach ($data['buyers_match'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('direct_match').':</b> '.__('buyers_match'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}

		//X- area
		if (!empty($data['x_1_match_high'])) {
			foreach ($data['x_1_match_high'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}


		if (!empty($data['x_1_match_med'])) {
			foreach($data['x_1_match_med'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}

		if (!empty($data['x_1_match_low'])) {
			foreach($data['x_1_match_low'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}

		//Y- area
		if (!empty($data['y_1_match_high'])) {
			foreach ($data['y_1_match_high'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}


		if (!empty($data['y_1_match_med'])) {
			foreach($data['y_1_match_med'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}

		if (!empty($data['y_1_match_low'])) {
			foreach($data['y_1_match_low'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}

		//Z- area
		if (!empty($data['z_1_match_high'])) {
			foreach ($data['z_1_match_high'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}


		if (!empty($data['z_1_match_med'])) {
			foreach($data['z_1_match_med'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}

		if (!empty($data['z_1_match_low'])) {
			foreach($data['z_1_match_low'] as $match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('two_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_two_way_width, 0, '', '', \Asset::img('email/2_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $match, self::NO_BORDER, self::PAGE_BREAK, false, true, 'C');
				$pdf->Ln(5);
			}
		}

		/** x-2 match 3-way */
		if (!empty($data['x_2_match'])) {
			foreach ($data['x_2_match'] as $next_match) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_three_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('three_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_three_way_width, 0, '', '', \Asset::img('email/3_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				foreach ($next_match as $match_1 => $match_2) {
					$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($match_1, 'B', null, false, true), self::NO_BORDER, 0, false, true, 'C');
					$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
					$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($match_2, 'C', self::PAGE_BREAK, false, true), self::NO_BORDER, 2, false, true, 'C');
					$pdf->Ln(5);
				}
			}
		}

		/** x-3 match / 4-way*/
		if (!empty($data['x_3_match'])) {
			foreach ($data['x_3_match'] as $next_match_1) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_four_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('four_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_four_way_width, 0, '', '', \Asset::img('email/4_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				foreach ($next_match_1 as $match_1 => $next_match_2) {
					$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($match_1, 'B', null, false, true), self::NO_BORDER, 0, false, true, 'C');
					$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
					foreach ($next_match_2 as $next_match_3 => $next_match_4) {
						$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($next_match_3, 'C', null, false, true), self::NO_BORDER, 0, false, true, 'C');
						$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
						$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($next_match_4, 'D', self::PAGE_BREAK, false, true), self::NO_BORDER, 2, false, true, 'C');
					}
				}
			}
		}

		/** x-4 match / 5-way */
		if (!empty($data['x_4_match'])) {
			foreach ($data['x_4_match'] as $match => $next_match_1) {
				$pdf->AddPage('', '', false, self::TOC_PAGE);
				$pdf->writeHTMLCell($this->_five_way_width, 0, '', $pdf->getY(), '<b>'.__('xtrade_match').':</b> '.__('five_way_trade'), self::NO_BORDER, 1, false, true, 'L');
				$pdf->Ln(6);
				$pdf->writeHTMLCell($this->_five_way_width, 0, '', '', \Asset::img('email/5_way_arrow_r.png'), self::NO_BORDER, 1, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $view->this_listing_box, self::NO_BORDER, 0, false, true, 'C');
				$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
				foreach ($next_match_1 as $next_match_2 => $next_match_3) {
					$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($next_match_2, 'B', null, false, true), self::NO_BORDER, 0, false, true, 'C');
					$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
					foreach ($next_match_3 as $next_match_4 => $next_match_5) {
						$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($next_match_4, 'C', null, false, true), self::NO_BORDER, 0, false, true, 'C');
						$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
						foreach ($next_match_5 as $next_match_6 => $next_match_7) {
							$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($next_match_6, 'D', null, false, true), self::NO_BORDER, 0, false, true, 'C');
							$pdf->writeHTMLCell(self::MATCHES_BOX_SHUFFLE, 0, '', '', $shuffle_break.\Asset::img('email/shuffle.png'), self::NO_BORDER, 0, false, true, 'C');
							$pdf->writeHTMLCell(self::MATCHES_BOX, 0, '', '', $data['m']->setup_box_information($next_match_7, 'E', self::PAGE_BREAK, false, true), self::NO_BORDER, 2, false, true, 'C');
							$pdf->Ln(5);
						}
					}
				}
			}
		}

	}

	public function writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align=''){
		return self::$_pdf->writeHtml($html, $ln, $fill, $reseth, $cell, $align);
	}

	public function Output($name = 'doc.pdf', $dest = 'I'){
		return self::$_pdf->Output($name, $dest);
	}

}