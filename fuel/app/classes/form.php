<?php

class Form extends Fuel\Core\Form {
	/**
	* Takes an array of options and creates a radio buton list
	*
	* @param string $name
	* @param array $options
	* @param string $selected
	* @param string $separator
	* @param string $extras
	* @return string
	*/
	public static function radios($name, $selected = NULL, $options, array $attributes = NULL, $separator = '&nbsp;&nbsp;') {
		if (!is_array($options)) {
			return;
		}

		$output = '';

		foreach($options as $v => $t) {
			$output .= '<input type="radio" name="' . $name . '" value="' . $v . '" ';

			if ($v == $selected) {
				$output .= 'checked="checked" ';
			}
			$output .= array_to_attr($attributes) . '/> ' . $t . $separator;
		}
		return $output;
	}

	/**
	* Takes an array of options and creates a check box list
	*
	* @param string $name
	* @param array $options
	* @param array $selected
	* @param string $separator
	* @param string $extras
	* @return string
	*/
	public static function checkboxes($name, $selected = array(), $options, array $attributes = NULL, $separator = '&nbsp;') {
		if (!is_array($options)) {
			return;
		}
		$_output = '';

		foreach($options as $v => $t) {
			$_output .= '<input type="checkbox" name="' . $name . '[]" value="' . $v . '" ';

			if (in_array((string)$v, $selected)) {
				$_output .= 'checked="checked"';
			}
			$_output .= array_to_attr($attributes) . ' /> ' . $t . $separator;
		}

		return $_output;
	}

	public static function err_msg($data, $id, $class = '') {
		if (is_array($data)) {
			// Multi array
			if (($pos = strpos($id, '.')) !== FAlSE) {
				$key = substr($id, 0, $pos);
				$id = substr($id, $pos + 1);
			}

			if (isset($key) && array_key_exists($key, $data)) {
				$data = $data[$key];
			}

			if (array_key_exists($id, $data)) {
				echo '<span class="' . $class . '">' . \Security::htmlentities($data[$id]) . '</span>';
			}
		}
	}
}