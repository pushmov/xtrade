<?php

namespace Helper;

class Html {
  
    public static function buyers_match_found_header(){
        $html = '<table width="360" border="0" cellspacing="5px">'
          . '<tbody><tr><td><span>Direct Match:</span> &nbsp;&nbsp;&nbsp; '
          . '<span class="matches_blue_text" style="color: rgba(36,157,219,1);font-weight: 600;">Buyers Match</span><br><br>';

        return $html;
    }

    public static function buyers_match_found_body($data){

        $html = self::buyers_match_found_header();
        $html .= '<table width="360" border="0" cellspacing="5px"><tbody><tr><td>'.$data['listing_box'].'</td><td>'
          . '<div class="shuffle_image" style="background: url(../images/shuffle.png) no-repeat 50%;width: 30px;height: 280px;"></div></td>'
          . '<td>'.$data['match'].'</td></tr></tbody></table><br>';

        $html .= self::buyers_match_found_footer();
        return $html;
    }

    public static function buyers_match_found_footer(){
        $html = '</td></tr></tbody></table>';
        return $html;
    }
  
    public static function match_info($data, $template='div'){
        $data['listing_id'] = (isset($data['in_1']['listing_id'])) ? $data['in_1']['listing_id'] : $data['in_1'];
		if ($template == 'table') {
			$view = \View::forge('matches/matches_box_table');
		} else {
			$view = \View::forge('matches/matches_box');
		}
		$view->data = $data;
        return $view;
    }
	
  
}