<?php
namespace Fuel\Tasks;

class Update {
    public function run(){
    	echo 'Start Time: ' . date_create()->format('H:m:i') . "\n";
		$crea = new \Model\Crea\Update;
		$crea->do_update();
    	echo 'Stop Time: ' . date_create()->format('H:m:i') . "\n";
	}
}