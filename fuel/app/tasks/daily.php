<?php
namespace Fuel\Tasks;

class Daily {
    public function run(){
    	echo 'Start Time: ' . date_create()->format('H:m:i') . "\n";
    	// Remove Featured Listing Status
		$sql = "UPDATE client_has SET featured_listing = 0 WHERE featured_listing = 1 AND featured_end <= CURDATE()";
		\DB::query($sql, \DB::UPDATE)->execute();

		$curdate = date_create();
		$now = $curdate->format('Y-m-d');
		/** sql curdate comparison not working */
		//$sql = "SELECT listing_id, client_has.address, date_expire, imported, users.realtor_id, first_name, last_name, email
		//FROM client_has
		//JOIN users ON users.realtor_id = client_has.realtor_id
		//WHERE date_expire BETWEEN CURDATE() AND CURDATE() - INTERVAL 3 DAY
		//AND type_of_listing REGEXP '0|1' AND expiry_notified REGEXP '0|2'
		//AND listing_status REGEXP '0|10|11'";

		/* Notify Soon To Expire Listing */
		$curdate = date_create();
		date_add($curdate, date_interval_create_from_date_string('3 days'));
		$result = \DB::select('client_has.listing_id','client_has.address','date_expire','imported','expiry_notified','users.realtor_id',
				'first_name','last_name','email','external_id')
				->from('client_has')->join('users', 'inner')
				->on('users.realtor_id', '=', 'client_has.realtor_id')
				->where('date_expire', 'between', array($now, date_format($curdate, 'Y-m-d')))
				->where(\DB::expr('type_of_listing REGEXP \'0|1\''))
				->where(\DB::expr('expiry_notified REGEXP \'0|2\''))
				->where(\DB::expr('listing_status REGEXP \'0|10|11\''))
				->execute();
		foreach($result->as_array() as $row) {
			if($row['imported'] == \Model\Listing::IMPORTED){
				$import_status = '2';
				$id = $row['external_id'];
			}else{
				$import_status = '0';
				$id = $row['listing_id'];
			}
			$row['email_info'] = 'Listing ID: '.\Html::anchor(\Config::get('uri').'listing/detail/'.$import_status.'_'.$id, $row['listing_id']).'<br>
			Address: '.$row['address'].'<br>
			Expiry Date: '.date_create($row['date_expire'])->format(DATETIME_FORMAT).'<br><br>';

			if($row['expiry_notified'] == 0){
				$expiry_notified = 1;
			} else {
				$expiry_notified = 3;
			}
			$response = \Emails::forge()->listing_expired_notify($row);
			if($response['status'] == 'OK'){
				$has = \Model\Client\Has::forge()->load_by_listing_id($row['listing_id']);
				$has->expiry_notified = $expiry_notified;
				$has->save();
			}
		}


		/* Notify Soon To Expire Featured Listings */
		$curdate = date_create();
		date_add($curdate, date_interval_create_from_date_string('3 days'));
		$listings_feat_exp = \DB::select('client_has.listing_id','client_has.address','date_expire','imported','expiry_notified','featured_end','users.realtor_id',
				'first_name','last_name','email','external_id')
				->from('client_has')->join('users', 'inner')
				->on('client_has.realtor_id', '=', 'users.realtor_id')
				->where('featured_end', 'between', array($now, date_format($curdate, 'Y-m-d')))
				->where(\DB::expr('type_of_listing REGEXP \'0|1\''))
				->where(\DB::expr('expiry_notified REGEXP \'0|1\''))
				->where('featured_listing', '=', \Model\Listing::AS_FEATURED)->execute();
		foreach($listings_feat_exp->as_array() as $row){
			if($row['imported'] == \Model\Listing::IMPORTED){
				$import_status = '2';
				$id = $row['external_id'];
			}else{
				$import_status = '0';
				$id = $row['listing_id'];
			}
			$row['email_info'] = 'Listing ID: '.\Html::anchor(\Config::get('uri').'listing/detail/'.$import_status.'_'.$id, $row['listing_id']).'<br>
			Address: '.$row['address'].'<br>
			Expiry Date: '.date_create($row['featured_end'])->format(DATETIME_FORMAT).'<br>
			<br>';
			$response = \Emails::forge()->listing_featured_expire_notify($row);
			if($row['expiry_notified'] == 0){
				$expiry_notified = 2;
			} else {
				$expiry_notified = 3;
			}
			if($response['status'] == 'OK'){
				$has = \Model\Client\Has::forge()->load_by_listing_id($row['listing_id']);
				$has->expiry_notified = $expiry_notified;
				$has->save();
			}
		}

		/* Notify 5 Day Old Unfinished Listings */
		$curdate = date_create();
		date_add($curdate, date_interval_create_from_date_string('1 days'));
		$listings = \DB::select('client_has.imported', 'client_has.external_id', 'client_has.listing_id', 'client_has.address', 'client_has.date_listed',
				'users.email', 'users.first_name', 'users.last_name')
				->from('client_has')
				->join('users', 'inner')->on('users.realtor_id', '=', 'client_has.realtor_id')
				->where('date_created', '>=', date_format($curdate, 'Y-m-d'))
				->where(\DB::expr('listing_status REGEXP \'8|9\''))
				->where('incomplete_notified', '=', 0)->execute();
		foreach($listings->as_array() as $row){
			if($row['imported'] == \Model\Listing::IMPORTED){
				$import_status = '2';
				$id = $row['external_id'];
			}else{
				$import_status = '0';
				$id = $row['listing_id'];
			}
			$row['email_info'] = $row['address'].', Added: '.date_create($row['date_listed'])->format(DATETIME_FORMAT).', '.\Html::anchor(\Config::get('uri').'listing/detail/'.$import_status.'_'.$row['listing_id']).' '
			. 'If you\'d like to complete the Clients Wants, '.\Html::anchor(\Config::get('uri').'realtors/listing/edit/'.$row['listing_id'].'?pos=client_wants', 'click here').' '
			. 'or if you wish to exclude this listing from xTrading at this time, '
			. \Html::anchor(\Config::get('uri').'realtors?action=listing_type&listing_id='.$row['listing_id'], 'click here').'<br><br>';
			$response = \Emails::forge()->inactive_listing_reminder($row);
			if($response['status'] == 'OK'){
				$has = \Model\Client\Has::forge()->load_by_listing_id($row['listing_id']);
				$has->incomplete_notified = 1;
				$has->save();
			}
		}

		/* Unlist Expired Listings */
		$unlisted = \DB::select('listing_id')->from('client_has')
				->where('date_expire', '<=', $now)
				->where('type_of_listing', '=', \Model\Listing::TYPE_XTRADED)
				->where('imported', '=', \Model\Listing::NON_IMPORTED)
				->execute();
		foreach($unlisted->as_array() as $row){
			$has = \Model\Client\Has::forge()->load_by_listing_id($row['listing_id']);
			$has->listing_status = \Model\Listing::LS_EXPIRED;
			$has->save();
		}

		/* Unlist Externally Expired Listings */
		$curdate = date_create();
		date_add($curdate, date_interval_create_from_date_string('-5 days'));
		$unlisted_external = \DB::select('id')->from('crea_property')
				->where('listing_status', '=', \Model\Listing::LS_SOLD_EX)
				->where('lastupdated', '>=', date_format($curdate, 'Y-m-d'))
				->execute();
		foreach($unlisted_external as $row){
			$prop = \Model\Crea\Property::forge($row['id']);
			$prop->active = \Model\Listing::LS_ACTIVE;
			$prop->save();
		}

		/* Delete Unfinished Listings Older Than 7 Days */
		$curdate = date_create();
		date_add($curdate, date_interval_create_from_date_string('-7 days'));
		$unfinished = \DB::select('listing_id')->from('client_has')
				->where('listing_status', '=', \Model\Listing::LS_XCOMPLETE)
				->where('date_created', '<=', date_format($curdate, 'Y-m-d'))
				->where('imported', '=', \Model\Listing::NON_IMPORTED)
				->execute();
		foreach($unfinished->as_array() as $row){
			\DB::delete('client_has')->where('listing_id', '=', $row['listing_id'])->execute();
			\DB::delete('client_wants')->where('listing_id', '=', $row['listing_id'])->execute();
			\DB::delete('client_information')->where('listing_id', '=', $row['listing_id'])->execute();
		}

		/* Delete Orphaned Wants/Info */
		$want_orphans = \DB::select('listing_id')->from('client_wants')->execute();
		foreach($want_orphans->as_array() as $row){
			$has = \Model\Client\Has::forge()->load_by_listing_id($row['listing_id']);
			if(!$has->loaded()){
				\DB::delete('client_wants')->where('listing_id', '=', $row['listing_id'])->execute();
			}
		}
		$info_orphans = \DB::select('listing_id')->from('client_information')->execute();
		foreach($info_orphans->as_array() as $row){
			$has = \Model\Client\Has::forge()->load_by_listing_id($row['listing_id']);
			if(!$has->loaded()){
				\DB::delete('client_information')->where('listing_id', '=', $row['listing_id'])->execute();
			}
		}

		/* Delete Orphaned Matches */
		$match_orphans = \DB::select('listing_id')->from('matches')->execute();
		foreach($match_orphans->as_array() as $row){
			$has = \Model\Client\Has::forge()->load_by_listing_id($row['listing_id']);
			if(!$has->loaded()){
				\DB::delete('matches')->where('listing_id', '=', $row['listing_id'])->execute();
			}
		}

		/* Cancel Accounts of Those Who Failed to Confirm Email within 24 hours */
		$curdate = date_create();
		date_add($curdate, date_interval_create_from_date_string('1 days'));
		$users = \DB::select()->from('users')->where('account_creation', '<=', date_format($curdate, 'Y-m-d')) //fixme
				->where('activate', '>', '')
				->where('account_status', '=', \Model\User::AS_PENDING_VERIFY)
				->execute();
		foreach($users->as_array() as $row){
			if($row['paypal_sub_id'] != ''){
				$realtor = \Model\Realtor::forge()->load(\DB::select()->where('realtor_id', '=', $row['realtor_id']));
				$realtor->account_status = \Model\User::AS_PAST_CLOSED;
				\Model\Gateway::forge()->cancel_payment($row['paypal_sub_id']);
			}
		}

		/* Cancel Accounts of Those Who Requested Cancellation */
		$cancelled_realtors = \DB::select()->from('users')->where('account_status', '=', \Model\User::AS_PENDING_CANCEL)->execute();
		foreach($cancelled_realtors->as_array() as $row){
			if($row['cancelled_on'] != '0000-00-00 00:00:00'){
				$realtor = \Model\Realtor::forge()->load(\DB::select()->where('realtor_id', '=', $row['realtor_id']));
				$realtor->account_status = \Model\User::AS_USER_CLOSED;
				\Model\Gateway::forge()->cancel_payment($row['paypal_sub_id']);
			}
		}
		// Brokers are handled manually
		/*
		$cancelled_brokers = \DB::select()->from('brokers')->where('account_status', '=', \Model\User::AS_PENDING_CANCEL)->execute();
		foreach($cancelled_brokers->as_array() as $row){
			if($row['cancelled_on'] != '0000-00-00 00:00:00'){
				$broker = \Model\Broker::forge()->load(\DB::select()->where('broker_id', '=', $row['broker_id']));
				$broker->account_status = \Model\User::AS_USER_CLOSED;
				\Model\Gateway::forge()->cancel_payment($row['paypal_sub_id']);
			}
		}
		*/
		$cancelled_vendors = \DB::select()->from('directory_vendors')->where('account_status', '=', \Model\User::AS_PENDING_CANCEL)->execute();
		foreach($cancelled_vendors->as_array() as $row){
			if($row['cancelled_on'] != '0000-00-00 00:00:00'){
				$vendor = \Model\Vendor::forge()->load(\DB::select()->where('vendor_id', '=', $row['vendor_id']));
				$vendor->account_status = \Model\User::AS_USER_CLOSED;
				\Model\Gateway::forge()->cancel_payment($row['paypal_sub_id']);
			}
		}

		/* Remove Empty CREA Listings */
		$empty_listings = \Model\Crea\Property::forge()->load(\DB::select('id')
				->where(\DB::expr('city_txt IS NULL'))
				->where(\DB::expr('prov_state IS NULL'))
				->where(\DB::expr('streetaddress IS NULL'))
				->where(\DB::expr('price IS NULL')), null);
		foreach($empty_listings->as_array() as $row){
			$crea = \Model\Crea\Property::forge()->load(\DB::select()->where('id', '=', $row['id']));
			$crea->active = \Model\Listing::LS_ACTIVE;
			$crea->save();
		}

		/* Update CREA Listing Cities with 0 values */
		$empty_crea_listings = \Model\Crea\Property::forge()->load(\DB::select()
				->where('city', '=', 0)
				->or_where(\DB::expr('city IS NULL'))
				->where('active', '=', \Model\Listing::LS_ACTIVE), null);
		foreach($empty_crea_listings->as_array() as $row){

			if($row['country'] == \Model\User::COUNTRY_CA){
				$prov = $row['province'];
				$city = $row['city_txt'];
				$city = preg_replace('/\s+/', ' ', $city);
				$city = preg_replace('/\s*,\s*/', ',', $city);
				$city = explode(",", $city);
				$city = $city[0];
				$city = explode(" (", $city);
				$city = preg_replace('/Ã©/', 'e', $city[0]);
				$city = preg_replace('/Ã¢/', 'a', $city);
				$city = preg_replace('/Ã/', 'o', $city);
				$lat = $row['latitude'];
				$long = $row['longitude'];
				if($prov == 'Newfoundland & Labrador'){
					$prov = 'Newfoundland';
				}
				if(stripos($city, '[Not Specified]') !== FALSE){
					$city = preg_replace("/(\\[Not Specified\\])/mi", "", $city);
				}
				if(strpos($city, "'")){
					$city = preg_replace("/'/mi", "`", $city);
				}
				if(($city == '' || !$city) && ($lat && $long)){
					$lat = $row['latitude'];
					$long = $row['longitude'];
					$city = \CityState::reverse_lat_lng($lat, $long);
				}

				if($city == '' || $city == 'Canada' || ($city == 'Alberta') || ($city == 'British Columbia')
						|| ($city == 'Ontario') || ($city == 'Saskatchewan')){
					$crea = \Model\Crea\Property::forge()->load(\DB::select()->where('id', '=', $row['id']));
					$crea->active = \Model\Listing::LS_ACTIVE;
					$crea->save();
					continue;
				}


				$province = \Model\Provstate\Canada::forge()->load(\DB::select('id')->where('province', '=', $prov));
				$cities = \Model\City\Canada::forge()->load(\DB::select()
					->where_open()
					->where('name', '=', $city)
					->or_where('alias', '=', $city)
					->where_close()
					->where('province_id', '=', $province->id)
				);
				if($cities->loaded()){
					$city_obj = \Model\City\Canada::forge()->load(\DB::select()
						->where('province_id', '=', $province->id)
						->where('name', '=', $city));
					$crea = \Model\Crea\Property::forge()->load(\DB::select_array()->where('id', '=', $row['id']));
					$crea->prov_state = $province->id;
					$crea->city = $city_obj->id;
					$crea->save();

				} else {
					$fields = array(
						'province_id' => $province->id,
						'name' => $city
					);
					$insert = \DB::insert('ca_cities')->set($fields)->execute();
					if($insert){
						$city_obj = \Model\City\Canada::forge()->load(\DB::select()->where('province_id', '=', $province->id)
								->where('name', '=', $city));
						$crea = \Model\Crea\Property::forge()->load(\DB::select()->where('id', '=', $row['id']));
						$crea->prov_state = $province->id;
						$crea->city = $city_obj->id;
						$crea->save();
					}
				}

			}

		}

		/* Add New Neighbourhoods */
		/*
		$neighbourhoods = \Model\Crea\Property::forge()->load(\DB::select()
				->where('neigh_added', '=', 0)
				->where('city', '<>', 0)
				->where('neighbourhood' ,'<>', ''), null);
		foreach($neighbourhoods->as_array() as $row){
			if($row['country'] == \Model\User::COUNTRY_CA){
				$neighbourhood = \Model\Country\Canada::forge()->load(\DB::select()
						->where('neigh_name', 'like', '%'.$row['neighbourhood'].'%')
						->where('city_id', '=', $row['city'])
						->where('province_id', '=', $row['prov_state']));
				if(!$neighbourhood->loaded()){
					$fields = array(
						'neigh_name' => $row['neighbourhood'],
						'city_id' => $row['city'],
						'province_id' => $row['prov_state']
					);
					\DB::insert('ca_neighbourhoods')->set($fields)->execute();
				}
				$crea = \Model\Crea\Property::forge($row['id']);
				$crea->neigh_added = 1;
				$crea->save();
			}
		}
		*/

		/* Check CREA Listings For Low Value Price, Beds, Bath */
		$crea_empty = \DB::select('crea_property.id')->from('crea_property')
				->join('crea_building', 'inner')->on('crea_property.id', '=', 'crea_building.id')
				->where('bathroomtotal', '=', 0)
				->where('bedroomstotal', '=', 0)
				->execute();
		foreach($crea_empty as $row){
			$crea = \Model\Crea\Property::forge($row['id']);
			$crea->active = \Model\Listing::LS_ACTIVE;
			$crea->save();
		}

		/* Email Newly Added Realtors */
		/*
		$agents = \Model\Crea\Agent::forge()->load(\DB::select()->where('notified_added_listings', '=', 2), null);
		foreach($agents->as_array() as $row){
			$agents->notified_added_listings = 1;
			$agents->save();
		}
*/
		echo "Cron Jobs Complete.";

	}
}