<?php
return array(
	'broker_request' => 'Broker Request',
	'req_assoc' => 'Request Association',
	'change_broker' => 'Change Broker',
	'current_broker' => 'You are currently with: Broker name here',
	'req_approve' => 'is requesting approval',
	'approve' => 'Approve',
	'decline' => 'Decline',
	'realtor_removed' => 'REALTOR&reg; Removed From Your Account.',
	'realtor_rem_error' => 'Error Removing REALTOR&reg;.',
	'realtor_not_found' => 'REALTOR&reg; not found',
	'not_yet_activated' => 'Not Yet Activated',
	'not_yet_login' => 'Not Yet Logged In'
);