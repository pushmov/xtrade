<?php
return array(
	'title' => 'Terms and Conditions of Use<br>TradeHomes by iREX Inc.',
	'subtitle' => '(TradeHomes or &quot;we, our or us&quot;)',
	'text' => '
	<h3>No warranties or Representations</h3>
	<p>This website is provided “as is” without any representations or warranties, express or implied. <strong>TradeHomes</strong> makes no representations or warranties in relation to this website or the information and materials provided on this website. By using this website, you implicitly agree to the following. The information contained in this website has been “user provided”, and while we endeavor to keep the information up to date and correct, <strong>TradeHomes</strong>  makes no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>
	<p>Without limiting the generality of the foregoing paragraph, TradeHomes does not warrant that:</p>
	<ul>
	<li>this website will be constantly available, or available at all; or</li>
	<li>the information on this website is complete, true, accurate or non-misleading.</li>
	<li>Every effort is made to keep the website up and running smoothly. However, <strong>TradeHomes</strong> takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.</li>
	</ul>
	<p>Nothing on this website constitutes, or is meant to constitute, advice of any kind. If you require advice in relation to any [legal, financial or medical] matter you should consult an appropriate professional.</p>
	<h3>Limitations of liability</h3>
	<p><strong>TradeHomes</strong> will not be liable to you (whether under the law of contact, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:</p>
	<p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.</p>
	<ul>
	<li>to the extent that the website is provided at a nominal user-fee, for any direct loss;</li>
	<li>for any indirect, special or consequential loss; or</li>
	<li>for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.</li>
	<li>Through this website if you are able to link to other websites which are not under the control of <strong>TradeHomes</strong>. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.</li>
	</ul>
	<p>These limitations of liability apply even if <strong>TradeHomes</strong> has been expressly advised of the potential loss.</p>
	<h3>Exceptions</h3>
	<p>Nothing in this website disclaimer will exclude or limit any warranty implied by law that it would be unlawful to exclude or limit; and nothing in this website disclaimer will exclude or limit our liability in respect of any:</p>
	<ul>
	<li>death or personal injury caused by our negligence;</li>
	<li>fraud or fraudulent misrepresentation on the part of us; or</li>
	<li>matter which it would be illegal or unlawful for <strong>TradeHomes</strong> to exclude or limit, or to attempt or purport to exclude or limit, its liability.</li>
	</ul>
	<h3>Reasonableness</h3>
	<p>By using this website, you agree that the exclusions and limitations of liability set out in this website disclaimer are reasonable.</p>
	<p>If you do not think they are reasonable, you must not use this website.</p>
	<h3>Other parties</h3>
	<p>You accept that, as a limited liability entity, <strong>TradeHomes</strong>  has an interest in limiting the personal liability of its officers and employees. You agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website.</p>
	<p>Without prejudice to the foregoing paragraph, you agree that the limitations of warranties and liability set out in this website disclaimer will protect our officers, employees, agents, subsidiaries, successors, assigns and sub-contractors as well as <strong>TradeHomes</strong>.</p>
	',
);
