<?php
/**
*	Common public page text
*/
return array(
	'login_failed' => 'We don\'t recognize this email or password',
	'login_error' => 'There was a login error',
	'email_not_found' => 'Email address not found',
	'email_sent' => 'Email Sent. You will receive it shortly',
	'password_reset' => 'xTradeHomes Password Reset',
	'login_realtor' => 'Login to xTradeHomes',
	'login_broker' => 'Login as Broker',
	'login_vendor' => 'Login as Advertiser to xTradeHomes',
	'login_admin' => 'Login as Admin',
	'log_in' => 'Log In',
	'remember' => 'Remember Me',
	'forgot' => 'Forgot Password',
	'signup' => 'Account Sign Up',
	'back_login' => 'Back to Login',
	'submit_email' => 'Submit Email',
	'account_suspended' => 'Your account is temporarily suspended'
);
