<?php
/**
*	Common public page text
*/
return array(
	'realtor_added' => 'REALTOR&reg; Added To Your Account.',
	'realtor_added_error' => 'Error Adding REALTOR&reg;.',
	'edit_promo' => 'Edit Promo',
	'add_promo' => 'Add Promo',
	'valid_from' => 'Date from',
	'valid_till' => 'Date untill',
	'promo_detail' => 'Promo detail',
	'promo_terms' => 'Promo terms',
	'promo_fine_print' => 'Promo fine print'
);
