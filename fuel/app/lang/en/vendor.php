<?php

return array(
	'downgrade_adv' => 'Downgrade Advertisement',
	'upgrade_adv' => 'Upgrade Advertisement',
	'downgrade_success' => 'Success. Your vendor account has been downgraded',
	'upgrade_success' => 'Success. Your vendor account has been upgraded',
	'success' => 'Success',
	'featured' => 'YES',
	'nfeatured' => 'NO'
);