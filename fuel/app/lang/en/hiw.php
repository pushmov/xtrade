<?php
return array(
	'title' => 'xTradeHomes - How Matching Works',
	'content_title' => 'How xTrading (Matching) Works',
	'content_top' => '<p>xTradeHomes makes two-way xTrades&trade; to match Sellers with the right properties, and easily converts them into Buyers as well. The agent simply makes a checklist of the seller’s must‐haves with a simple online form. This generates the listings that match the sellers’ desires as a buyer.</p>
	<p>Once an agent is signed on, the powerful xTradeHomes application quickly goes into overdrive to identify and locate appropriate matches, at both ends.</p>
	<p>xTrades&trade; then makes two-way, and/or, up to five-way xTrades&trade; to match the Sellers with the right properties, anywhere in Canada or the U.S. and convert them into Buyers faster and easier than conventional real estate transactions.  Some xTrades™ are two‐way matches. But they can involve three, (or up to,) even five properties at one time.</p>
	<p>For example, if a seller in Vancouver wants to move to Phoenix, the program ﬁnds sellers in Phoenix who want to move to Vancouver.</p>
	<p>From there it is a matter of coordinating the trading of one house for another in a way that works for the clients, just as a REALTOR&reg; normally does—saving time, hassle and money for both.</p>',
	'content_top_under' => 'xTradeHomes has the approval of the Canadian Real Estate Association (CREA) to use its millions of listings.',
	'content_second_title' => 'Make xTrades work for you',
	'content_second_1' => '<p>It\'s usually less than 10 minutes to list a property. A Property Search on the main site shows all listings on xTradeHomes.  Buyers or their agents can both access this information. Agents are charged a small fee to join xTradeHomes site.  But there is no charge for leads provided by xTradeHomes. Nor is there a charge to buyers or sellers.<p>',
	'content_second_2' => '<p>Anyone can take advantage of the xTrade&trade; system, and anyone can view our website. However, xTradeHomes is intended for real estate professionals to act as agents. Owner sellers can perform property searches but a licensed REALTOR&reg; is required to use the system to execute the transaction. If a seller wants to relocate in the same locale or another, the program ﬁnds the right xTrade(s)&trade;</p>',
	'content_second_brokers_title' => 'Benefits to Brokers',
	'content_second_brokers_l' => '
	<ul>
	<li>Recruitment & retention tool<br></li>
	<li>Creates a new lead source<br></li>
	<li>Increases value of the REALTOR&reg;<br></li>
	<li>Agent centric tool<br></li>
	<li>Stimulates / escalates sales<br></li>
	</ul>
	',
	'content_second_brokers_r' => '
	<ul>
	<li>Cost of sales goes down (shorter sales cycle)</li>
	<li>Income to REALTOR&reg; goes up (two commissions)</li>
	<li>Income to broker goes up</li>
	<li>Increases the Broker’s bottom line<br></li>
	</ul>
	',
	'content_second_agents_title' => 'Benefits to Agents',
	'content_second_agents_l' => '
	<ul>
	<li><span title="xTRADE&trade;: a computerized identification process exclusively created by xTradeHomes to assist a REALTOR&reg; and their seller in locating another seller to trade properties with, often involving multiple sellers, each of whom obtains exactly the property they desire and at the same time - selling their property simultaneously to another seller.">xTRADE&trade;</span> is an Agent centric technology tool<br></li>
	<li>Increases value of the REALTOR&reg;<br></li>
	<li>Allows REALTOR&reg; to provide a higher level of service to clients, by bringing more potential buyers / options to the table.<br></li>
	<li>Also allows the agent to capitalize on their creative and negotiation skills.<br></li>
	<li>Increases speed to sale, which is very important in a slow market, or in circumstances where the seller needs to sell & buy quickly (ie: job transfer).<br></li>
	<li>Ability to retain seller (ie: if the listing is not sold within the contracted time period, the agent risks losing the client.  With trading, the agent has a higher likelihood of finding a solution for his client within the contractual timeframe.)<br></li>
	<li>Allows the agent to manage common concerns, eg:<br></li>
	<ul>
	<li>Transactions are not affected by fluctuating market conditions, because the client is selling and buying at the same time.<br></li>
	<li>Ensures their client will not be stuck having to do a short or long term rental between the sale of their home, and finding their next home. This concern is valid in a slow market AND a fast market where inventory is low.<br></li>
	</ul>
	</ul>
	',
	'content_second_agents_r' => '
	<ul>
	<li>Expand your pool of buyers.  Find buyers where sellers are.  The buyers are the sellers.<br></li>
	<li>Both REALTORS&reg; are guaranteed two commissions (assuming 1:1 trade) When there is a multiple xTrade (up to 5 properties involved) each of the 5 REALTORS&reg; should receive 2 commissions, one listing commission and one selling commission.<br></li>
	<li>Cost of sales goes down<br></li>' .
	//<li>ROI: one deal, one agent, assuming two sale prices $350,000 involved in the <span title="xTRADE&trade;: a computerized identification process exclusively created by xTradeHomes to assist a REALTOR&reg; and their seller in locating another seller to trade properties with, often involving multiple sellers, each of whom obtains exactly the property they desire and at the same time - selling their property simultaneously to another seller.">xTRADE&trade;</span>, $15,000 average commission, less $49.95 subscription cost.  ROI - $14,950. If a REALTOR&reg; only does one xTrade&trade; per annum the ROI would be $14,400. No matter how you look at it it\'s a win win for the REALTOR&reg; and the homeowner!!<br></li>
	'<li>Any other leads to the REALTOR&reg; is a free service of xTradeHomes<br></li>
	</ul>
	',
	'content_second_consumers_title' => 'Benefits to Consumers',
	'content_second_consumers_l' => '
	<ul>
	<li>Increases speed to sale – good in a slow market, or when a quick sale is required (job relocation, or in the U.S. to avoid short sale or foreclosure)<br></li>
	<li>Alleviates concerns about fluctuating market conditions, because the client is selling and buying at the same time.<br></li>
	<ul>
	<li>Avoid chasing the market down, which happens when the client lists during a downward trending market.<br></li>
	<li>Avoid stress of an upward trending market, where prices rise between the time the consumer sells home, and finds a new home to buy.<br></li>
	</ul>
	</ul>
	',
	'content_second_consumers_r' => '
	<ul>
	<li>Ensures the seller has their new home in waiting, prior to closing the sale of their existing home.  This avoids a common situation where consumers have to rent until they are able to find their new home.  This concern is valid in slow markets AND in fast markets with low inventory.<br></li>
	<li>Consumers are more likely to receive better values for their homes.<br></li>
	<li>No need for bridge financing.<br></li>
	<li>No worries about potentially having to carry two mortgages<br></li>
	<li>An easier sales process.  Fewer open houses, and fewer times when the seller has to clean their home and vacate, to allow a showing.<br></li>
	<li>Easier negotiations between the <span title="xTRADE&trade;: a computerized identification process exclusively created by xTradeHomes to assist a REALTOR&reg; and their seller in locating another seller to trade properties with, often involving multiple sellers, each of whom obtains exactly the property they desire and at the same time - selling their property simultaneously to another seller.">xTrading&trade;</span> parties.</li>
	</ul>
	',
	'content_third_title' => 'Sample xTrade Scenarios',
	'content_third_s1' => '
	Anne and Bob are empty nesters looking to downsize from their 3-bedroom home to a condo in the city. Joe and Ashley have a condo in the city and are looking to buy a large home for their growing family. Both couples want to sell their current properties, and each has what the other wants. This is a 2-WAY xTRADE&trade;.
	',
	'content_third_s2' => '
	Todd has a beach house and would like to move closer to his family in the suburbs. James and Amanda have a townhouse in the suburbs and want to move to a city condo to be closer to work. David is tired of the city hustle and would like to move from his condo to the beach for peace and quiet. All of these clients are sellers who simultaneously want to buy new homes. Todd sells his home to David, and buys James and Amanda’s home. James and Amanda sell their townhouse to Todd and buy David’s condo. It’s a win-win-win 3-WAY match.
	',
	'content_fourth_title' => 'The xTrade Home Match Advantage',
	'content_fourth_subtitle' => 'Most home sellers will buy another home in the same general area and in the traditional process they can’t buy their next homes until they sell their current homes.',
	'content_fourth_l' => '
	<ul class="small-8 column">
	<li>Faster</li>
	<li>Easier</li>
	<li>Quicker relocation</li>
	<li>Saves money on renting</li>
	<li>No need to sell ﬁrst, buy later</li>
	</ul>
	',
	'content_fourth_r' => '
	<ul class="small-8 column">
	<li>Less energy on no‐go properties</li>
	<li>Selective viewings</li>
	<li>Eliminates ‘subject to sale’ clause</li>
	<li>Makes matching automatic</li>
	<li>Continent wide listings</li>
	</ul>
	',
);
