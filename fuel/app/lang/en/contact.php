<?php
return array(
	'title' => 'xTradeHomes Contact Us',
	'subtitle' => 'xTradeHomes Contact Form',
	'gi' => 'General Inquiries',
	'ai' => 'Advertising Inquiries',
	'wri' => 'Website Related Issues',
	'mr' => 'Media Relations',
	'ac' => 'Account Questions',
	'bi' => 'Billing Questions',
	'who' => 'Department: ',
	'name' => 'Name: ',
	'phone' => 'Phone Number: ',
	'email' => 'Email: ',
	'comment' => 'Comment:',
	'address_1' => 'Address',
	'address_2' => ' Address 2',
	'submit' => 'Submit',
);
