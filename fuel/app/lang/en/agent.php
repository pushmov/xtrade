<?php
return array(
	'name' => 'Name',
	'email' => 'Email',
	'message' => 'Message',
	'working_realtor' => 'Working REALTOR&reg; question',
	'trade' => 'Trading your home question',
	'are' => 'are',
	'arent' => 'are not',
	'officename' => 'Company Name'
);