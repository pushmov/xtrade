<div>
	<form id="client_form" name="client_form" method="post" action="/realtors/profile" autocomplete="off" >
		<input name="data[id]" id="id" type="hidden" value="<?=$data['id'];?>">
		<input name="data[account_status]" type="hidden" id="account_status" value="<?=$data['account_status'];?>">
		<input name="data[utype]" type="hidden" id="usertype" value="<?=$data['utype']?>">
		<div class="profile-wrapper clearfix">
			<div class="small-12 medium-12 large-6 column profile-info">
				<div class="padded-sides">
					<br />
					<h2 class="text-center"><?=$title;?></h2>

					<?php if($data['listing_btn'] != '') : ?>
						<section class="profile-side profile-listings">
							<dl class="clearfix">
								<dt>Number of Listings:</dt>
								<dd><?=$data['total'];?> Total</dd>
							</dl>
							<div class="small-12 medium-12 large-12"><?=$data['listing_btn']?></div>
						</section>
					<?php endif; ?>

					<?php if (isset($data['broker_info']) && is_array($data['broker_info'])):?>
						<section class="profile-side profile-association">
							<dl class="clearfix">
								<dt>Broker:</dt>
								<dd id="broker_assoc">
									<?=$data['broker_info']['info'];?>	<?=$data['broker_info']['button']?>
								</dd>
							</dl>
						</section>
					<?php endif;?>

					<section class="profile-side profile-actions">
						<button class="button" type="button" id="advanced_info">Account Information</button>
						<div id="advanced_info_options" class="advanced-info-options hide">
							<button class="button" type="button" id="account_activity">Account Activity</button>
							<?=$data['history'];?>
							<button id="change_billing" type="button" class="button">Update Billing Details</button>
							<button id="close_account" type="button" class="button">Close Account</button>
						</div>
					</section>
					<section class="profile-side profile-advanced">
						<dl class="clearfix">
							<dt>Receive Email Alerts:</dt>
							<dd><?=$data['alerts'];?></dd>
						</dl>
					</section>

					<section class="profile-side profile-advanced">
					<div class="small-12 medium-12 large-12">
						<button id="advanced" type="button" class="button">Additional Options</button>
						<div id="advanced_options" class="advanced-info-options hide">
							<button id="change_pass" type="button" class="button">Update Password</button>
							<button id="listing_forms" type="button" class="button">Listing Forms</button>
						</div>
					</div>
				</div>
			</div>
			<div class="small-12 medium-12 large-6 column">
				<div class="padded">
					<div class="row">
						<div class="small-12 medium-6 column">
							<label for="first_name"><?=__('first'); ?><span class="astrict">*</span></label>
							<input name="data[first_name]" type="text" id="first_name" value="<?=$data['first_name'];?>">
						</div>
						<div class="small-12 medium-6 column">
							<label for="last_name"><?=__('last'); ?><span class="astrict">*</span></label>
							<input name="data[last_name]" type="text" id="last_name" value="<?=$data['last_name'];?>">
						</div>
					</div>
					<label for="company_name"><?=__('company_name'); ?><span class="astrict">*</span></label>
					<input name="data[company_name]" type="text" id="company_name" value="<?=$data['company_name'];?>">
					<?php if (isset($data['toll_free'])): ?>
						<label for='toll_free'><?=__('toll_free');?></label>
						<input name="data[toll_free]" type="text" id="toll_free" value="<?=$data['toll_free'];?>">
					<?php endif;?>
					<div class="row">
						<div class="small-12 medium-6 column">
							<label for="phone"><?=__('phone'); ?><span class="astrict">*</span></label>
							<input name="data[phone]" type="tel" id="phone" placeholder="555-555-5555" value="<?=$data['phone'];?>" maxlength="14" title="Enter Numbers Only Please" class="tooltip">
						</div>
						<div class="small-12 medium-6 column">
							<label for="cell"><?=__('cell'); ?></label>
							<input name="data[cell]" type="tel" class="input_field" id="cell" placeholder="555-555-5555" value="<?=$data['cell'];?>" maxlength="14" title="Enter Numbers Only Please">
						</div>
					</div>
					<label for="web_site"><?=__('site'); ?></label>
					<input name="data[web_site]" type="text" placeholder="http://www.yoursite.com" id="web_site" value="<?=$data['web_site']?>" size="24" />
					<div class="row">
						<div class="small-12 medium-6 column">
							<label for="email"><?=__('email'); ?><span class="astrict">*</span></label>
							<input name="data[email]" type="email" placeholder="your@emailaddress.com" id="email" value="<?=$data['email'];?>">
						</div>
						<div class="small-12 medium-6 column">
							<label for="cemail">Confirm <?=__('email'); ?><span class="astrict">*</span></label>
							<input name="cemail" type="email" placeholder="your@emailaddress.com" id="cemail" value="<?=$data['cemail'];?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-6 column">
							<label for="nrb_id" class="tooltip-crea"><?=$nrb_txt;?> <span class="tooltip1 underline">(Look Up)</span></label>
							<div id="crea_exp" class="hide tooltiptxt"><?=$nrb_tip;?></div>
							<input name="data[nrb_id]" id="nrb_id" value="<?=$data['nrb_id'];?>" type="text">
						</div>
					</div>

					<?php if(\Authlite::instance('auth_admin')->logged_in()) :?>
						<label for="account_status">Account Status</label>
						<?=\Form::select('data[account_status]', $data['account_status'], \Model\User::forge()->get_status(), array('id' => 'account_status'));?>
					<?php endif; ?>


					<label for="plan_id">Discount Promo Code For REALTORS&reg;:</label>
					<?php if(\Authlite::instance('auth_admin')->logged_in() && ($data['utype'] == 'broker')):?>
						<?=\Form::select('data[plan_id]', $data['plan_id'], \Model\Plan::forge()->promo_list($data['country']), array('id' => 'plan_id'));?>
					<?php else: ?>
						<input name="promo_code" id="plan_id" value="<?=$promo_code?>" type="text" disabled="disabled">
					<?php endif; ?>

				</div>
			</div>
		</div>
		<div id="submit_div" class="text-center padded-top">
			<button type="button" id="submit" class="button">Submit Changes</button>
		</div>
	</form>
</div>
<div id="page_dialog" class="reveal" data-reveal></div>
<div id="billing_dialog" class="reveal large" data-reveal></div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
<script>
	$(document).ready(function() {
		new autoComplete({
			selector: '#cpc_address',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
			}
		});
	});
</script>