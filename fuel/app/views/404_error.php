<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Page Not Found</title>
	<link href="/assets/css/app.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<header>
		<div class="row">
			<a href="/"><img src="/assets/images/hd_logo.png" alt="Xtrade Homes" class="left"></a>
		</div>
	</header>
	<div class="container">
		<div class="row">
			<div class="small-12 column">
				<h1><small>We can't find that!</small></h1>
				<hr>
				<p><?=\Html::anchor('/', 'Click here');?> for our home page.</p>
			</div>
		</div>
		<footer>
		</footer>
	</div>
</body>
</html>
