<div class="row content">
	<form id="client" name="client" method="post" action="/signup" >
		<div class="small-12 medium-12 large-6 column">
			<div class="row">
				<div class="small-12 column signup-left-top">
					<h2>Not a Broker member yet?</h2>
					<h1>Join xTradeHomes!</h1>
					<h2>Need more information? Learn more about us under &quot;<a href="/hiw" class="text-black">How Matching Works</a>&quot; and see our <a href="/faq" class="text-black">FAQ</a> page.</h2>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 column">
					<div class="padded-sides">
						<h3>Pricing for Brokers and Offices Starts At</h3>
						<h3 class="pricing-style">$<? //=$fee;?></h3> <strong>Monthly<br>Per Agent**</strong>
						<hr>
						<div class="fine-print">
							Minimum 10 agents.<br>
							Plus applicable taxes.<br>
							Billed at the beginning of the month.<br>
							No setup fee.<br>
							Unlimited number of listings.<br>
							Automatic renewal.<br>
						</div>
					</div>
				</div>
				<div class="small-12 medium-6 column signup-left-bottom padded-top">
					<div class="padded-sides">
						<p class="text-center">Contact us to get your special pricing, fill out the form and someone will contact you.</p>
						<span class="fine-print">**All pricing subject to change</span>
					</div>
				</div>
			</div>
		</div>
		<div class="small-12 medium-12 large-6 column">
			<div class="padded">
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="first_name"><?=__('first'); ?><span class="astrict">*</span></label>
						<input name="data[first_name]" type="text" id="first_name" value="<?=$data['first_name'];?>">
					</div>
					<div class="small-12 medium-6 column">
						<label for="last_name"><?=__('last'); ?><span class="astrict">*</span></label>
						<input name="data[last_name]" type="text" id="last_name" value="<?=$data['last_name'];?>">
					</div>
				</div>
				<label for="company_name"><?=__('company_name'); ?><span class="astrict">*</span></label>
				<input name="data[company_name]" type="text" id="company_name" value="<?=$data['company_name'];?>">
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="phone"><?=__('phone'); ?><span class="astrict">*</span></label>
						<input name="data[phone]" type="tel" id="phone" placeholder="555-555-5555" value="<?=$data['phone'];?>" maxlength="14" title="Enter Numbers Only Please" class="tooltip">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cell"><?=__('cell'); ?></label>
						<input name="data[cell]" type="tel" class="input_field tooltip" id="cell" placeholder="555-555-5555" value="<?=$data['cell'];?>" maxlength="14" title="Enter Numbers Only Please">
					</div>
				</div>
				<label for="web_site"><?=__('site'); ?></label>
				<input name="data[web_site]" type="text" placeholder="http://www.yoursite.com" id="web_site" value="<?=$data['web_site']?>">
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="email"><?=__('email'); ?><span class="astrict">*</span></label>
						<input name="data[email]" type="email" placeholder="your@emailaddress.com" id="email" value="<?=$data['email'];?>">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cemail">Confirm <?=__('email'); ?><span class="astrict">*</span></label>
						<input name="data[cemail]" type="email" placeholder="your@emailaddress.com" id="cemail" value="<?=$data['cemail'];?>">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="password"><?=__('password'); ?><span class="astrict">*</span></label>
						<input name="data[password]" id="password" value="<?=$data['password'];?>" type="password" autocomplete="off"  title="<?=__('pass_rules'); ?>" class="tooltip">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cpassword"><?=__('re_enter_password'); ?><span class="astrict">*</span></label>
						<input name="data[cpassword]" id="cpassword" value="<?=$data['cpassword']?>" type="password" autocomplete="off"  title="<?=__('pass_rules'); ?>" class="tooltip">
					</div>
				</div>
				<div id="show_pass">Show Password</div>
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="crea_id" class="tooltip-crea">CREA ID <sup class="tooltip1">?</sup></label>
						<input name="data[crea_id]" id="crea_id" type="text" class="tooltip-crea">
					</div>
					<div class="small-12 medium-6 column">
						<label for="nrds_id" class="tooltip-nrds">NRDS ID <sup class="tooltip2">?</sup></label>
						<input name="data[nrds_id]" id="nrds_id" type="text" class="tooltip-nrds">

					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-6 end column">
						<label for="number_agents">Number of Agents</label>
						<?=\Form::select('data[number_agents]', '', $cboAgents);?>
					</div>
				</div>
				<div class="row padded-top">
					<h5>Terms &amp; Conditions</h5>
					<div class="scroll-div" name="terms"> <?=__('members_body'); ?> </div>
					<div class="padded-top">
						<?=\Form::checkbox('tos', '1', '', array('class' => 'left', 'id' => 'tos'));?>
						<label for="tos" class="right tos-agreement"><?=__('agree'); ?><span class="astrict">*</span></label>
					</div>
				</div>

				<div class="row">
				<hr>
					<div class="small-12 column text-center">
						<input name="user_type" id="user_type" type="hidden" value="broker">
						<input name="promo_code" id="promo_code" type="hidden" value="">
						<input name="broker_id" id="broker_id" type="hidden" value="">
						<?php if($data['rref'] == 'true'):?>
						<input name="data[rref]" id="rref" type="hidden" value="<?=$data['rref']?>">
						<input name="data[realtor_id]" id="realtor_id" type="hidden" value="<?=(isset($data['realtor_id'])) ? $data['realtor_id'] : '';?>">
						<?php endif; ?>
						<button class="button large" type="button" id="btn_join">Sign Up</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div id="page_dialog" class="reveal large" data-reveal></div>
<div id="crea_exp" class="hide"><span>Look up your CREA ID by clicking <a href="http://mms.realtorlink.ca/indDetails.aspx" target="_blank">here</a></span></div>
<div id="nrds_exp" class="hide"><span>Look up your NRDS ID by clicking <a href="https://reg.realtor.org/roreg.nsf/retrieveID?OpenForm" target="_blank">here</a></span></div>
