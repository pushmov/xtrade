<div class="row content">
	<h1 class="text-green text-center">xTradeHomes SiteMap</h1>
	<div class="small-12 column">
		<ul>
			<li><a href="/" title="xTradeHomes">xTradeHomes</a>
				<ul>
					<li><a href="/signup" title="xTradeHomes Sign Up">xTradeHomes Sign Up</a></li>
					<li><a href="/about" title="xTradeHomes About Us">xTradeHomes About Us</a></li>
					<li><a href="/hiw" title="xTradeHomes - How Matching Works">xTradeHomes - How Matching Works</a></li>
					<li><a href="/faq" title="xTradeHomes FAQ">xTradeHomes FAQ</a></li>
					<li><a href="/vendor/location" title="xTradeHomes Directory Results">xTradeHomes Advertiser Results</a></li>
					<li><a href="/signup/vendor" title="xTradeHomes Advertiser Sign Up">xTradeHomes Advertiser Sign Up</a></li>
					<li><a href="/privacy" title="xTradeHomes Privacy Policy">xTradeHomes Privacy Policy</a></li>
					<li><a href="/terms" title="xTradeHomes Terms &amp; Conditions">xTradeHomes Terms &amp; Conditions</a></li>
					<li><a href="/contact" title="xTradeHomes Contact">xTradeHomes Contact</a></li>
				</ul>
			</li>
		</ul>
			</div>
</div>