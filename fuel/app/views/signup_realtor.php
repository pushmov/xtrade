<div class="row content">
	<form id="client" name="client" method="post" action="/signup">
		<div class="small-12 medium-12 large-6 column">
			<div class="row">
				<div class="small-12 column signup-left-top">
					<div class="padded-left">
						<h2>Join xTradeHomes Today!</h2>
						<h1>xTradeHomes is a real estate matchmaker for motivated sellers.</h1>
						<p>With xTradeHomes you can help your clients avoid the painful prospect of having to sell their home and bear the cost and hassle of an interim rental while they find their new home. You can also ensure they don’t get stuck in a situation where they’ve purchased their new home before selling their old home, thus having to carry two mortgages for a period of time.</p>
						<p>With xTradeHomes you can quickly identify real estate matches, helping your client find their new home, and a buyer for their current home, all at the same time. With both transactions happening simultaneously, your clients will have a much better experience, they’ll save money, and you’ll be able to facilitate both sides of the transaction.</p>
						<p>Sign up today and let xTradeHomes powerful patent pending matching algorithm help your clients find their real estate match. </p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 column padded-top">
					<div class="padded-sides">
						<h3>Pricing for REALTORS&reg; and Buyers' Agents</h3>
						<h3 class="pricing-style">$<?=$fee; ?></h3> <strong>Monthly Membership**</strong>
						<hr>
						<div class="fine-print">
							<strong>Setup fee complementary for a limited time.</strong><br>
							Plus applicable taxes.<br>
							Billed monthly.<br>
							Unlimited number of listings.<br>
							Automatic renewal.<br>
						</div>
					</div>
				</div>
				<div class="small-12 medium-6 column signup-left-bottom padded-top">
					<div class="padded-sides">
						<?php if(!empty($data['broker_id'])): ?>
							<h3 class="text-green">Your Broker:</h3>
							<strong><?=$data['company_name']; ?></strong>
							<?php if ($data['promo_code'] != ''): ?>
								offers a special promotion by using the Promo Code <strong><?=$data['promo_code'];?></strong>
							<?php endif; ?>
						<?php else: ?>
							<h4>Limited time offer:</h4>
							First 3 Months complementary<br>then $<?=$fee;?> per month thereafter, minimum 6 months subscription.
							<br>
						<?php endif; ?>
						<div class="padded-top">
							<label for="promo_code">Promo Code</label>
							<input name="promo_code" type="text" id="promo_code" value="<?=$promo_code ?>">
						</div>
						<span class="fine-print">**All pricing subject to change</span>
					</div>
				</div>
			</div>
			<div class="row padded-top">
				<div class="text-center">
					<button class="button" onClick="javascript: window.location.assign('/signup/broker');" type="button">Broker/Office Managers Click Here</button>
				</div>
			</div>
		</div>
		<div class="small-12 medium-12 large-6 column">
			<div class="padded">
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="_name"><?=__('first'); ?><span class="astrict">*</span></label>
						<input name="data[first_name]" type="text" id="first_name" value="<?=$data['first_name'];?>" title="Please fill out this field">
					</div>
					<div class="small-12 medium-6 column">
						<label for="last_name"><?=__('last'); ?><span class="astrict">*</span></label>
						<input name="data[last_name]" type="text" id="last_name" value="<?=$data['last_name'];?>" title="Please fill out this field">
					</div>
				</div>
				<label for="company_name"><?=__('company_name'); ?><span class="astrict">*</span></label>
				<input name="data[company_name]" type="text" id="company_name" value="<?=$data['company_name'];?>" title="Please fill out this field">
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="phone"><?=__('phone'); ?><span class="astrict">*</span></label>
						<input name="data[phone]" type="tel" id="phone" placeholder="555-555-5555" value="<?=$data['phone'];?>" maxlength="14" title="Enter Numbers Only Please">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cell"><?=__('cell'); ?></label>
						<input name="data[cell]" type="tel" id="cell" placeholder="555-555-5555" value="<?=$data['cell'];?>" maxlength="14" title="Enter Numbers Only Please">
					</div>
				</div>
				<label for="web_site"><?=__('site'); ?></label>
				<input name="data[web_site]" type="text" placeholder="http://www.yoursite.com" id="web_site" value="<?=$data['web_site']?>">
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="email"><?=__('email'); ?><span class="astrict">*</span></label>
						<input name="data[email]" type="email" placeholder="your@emailaddress.com" id="email" value="<?=$data['email'];?>">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cemail">Confirm <?=__('email'); ?><span class="astrict">*</span></label>
						<input name="data[cemail]" type="email" placeholder="your@emailaddress.com" id="cemail" value="<?=$data['cemail'];?>">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="password"><?=__('password'); ?><span class="astrict">*</span></label>
						<input name="data[password]" id="password" value="<?=$data['password'];?>" type="password" autocomplete="off"  title="<?=__('pass_rules'); ?>">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cpassword"><?=__('re_enter_password'); ?><span class="astrict">*</span></label>
						<input name="data[cpassword]" id="cpassword" value="<?=$data['cpassword']?>" type="password" autocomplete="off"  title="<?=__('pass_rules'); ?>">
					</div>
				</div>
				<div id="show_pass">Show Password</div>
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="crea_id" class="tooltip-crea">CREA ID <sup class="tooltip1">?</sup></label>
						<input name="data[crea_id]" id="crea_id" value="" type="text">
					</div>
					<div class="small-12 medium-6 column">
						<label for="nrds_id" class="tooltip-nrds">NRDS ID <sup class="tooltip2">?</sup></label>
						<input name="data[nrds_id]" id="nrds_id" value="" type="text">

					</div>
				</div>
				<div class="row padded-top">
					<h5>Terms &amp; Conditions</h5>
					<div class="scroll-div" name="terms"> <?=__('members_body'); ?> </div>
					<div class="padded-top">
						<?=\Form::checkbox('tos', '1', '', array('class' => 'left', 'id' => 'tos'));?>
						<label for="tos" class="right tos-agreement"><?=__('agree'); ?><span class="astrict">*</span></label>
					</div>
				</div>
				<div class="row">
					<hr>
					<div class="small-12 column text-center">
						<input name="user_type" id="user_type" type="hidden" value="realtor">
						<input name="changes" id="changes" type="hidden" value="0">
						<input name="data[broker_id]" id="broker" type="hidden" value="<?=$data['broker_id'];?>">
						<input name="data[id]" id="id" type="hidden" value="<?=$data['id'];?>">
						<?php if($data['broker_ref']): ?>
							<input name="broker_ref" id="broker_ref" type="hidden" value="1">
						<?php endif; ?>
						<button id="btn_join" class="button large" type="button">Sign Up</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div id="page_dialog" class="reveal" data-reveal></div>
<div id="payment_dialog" class="reveal large payment-dialog" data-reveal></div>
<div id="crea_exp" class="hide"><span>Look up your CREA ID by clicking <a href="http://mms.realtorlink.ca/indDetails.aspx" target="_blank">here</a></span></div>
<div id="nrds_exp" class="hide"><span>Look up your NRDS ID by clicking <a href="https://reg.realtor.org/roreg.nsf/retrieveID?OpenForm" target="_blank">here</a></span></div>
