<div class="content column">
	<h1 class="text-green text-center"><?=__('title'); ?></h1>
	<div class="row column">
		<ul class="accordion" data-accordion role="tablist">
			<li class="accordion-item is-active">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">What is xTradeHomes?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>xTradeHomes is a new and innovative Internet based lead generation service that allows you to find real estate matches, also known as xTrades, for your listing clients.  An xTrade is found when your Selling client’s wants match what another Sellerbuyer has.  xTrades can involve two to five parties, with each party having a property for sale that another party in the xTrade desires to buy.</p>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_2" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">How does xTradeHomes make a match?</a>
				<div id="panel_2" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>This is our secret sauce!  Our patent pending matching algorithm uses multiple data points to make real estate matches.</p>
					<p>It’s the only service of its kind; it opens up a whole new source of leads for you, helping you find your client their next home while simultaneously finding a buyer for their current house.</p>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">What happens when a match is found?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>Once a match is found, and you’re notified of the match, the transaction proceeds as usual.  You contact the other agent and verify the properties are the right match for your respective clients.  If so, you set up showings, and proceed to contract with offers to purchase that are subject to each respective client’s property selling. </p>
					<p>This subject must appear on both offers and have the same removal date.  This prevents an owner from being in the unexpected position of owning two properties, if one of the parties fails to act.</p>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">How will the service benefit me?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>The bottom line xTradeHomes will help you find more listings, sell homes more efficiently, and delight your clients in the process!  Specifically, here’s what xTradeHomes can do for you:</p>
					<ol>
						<li>EXPANDS THE BUYERS POOL. Allows you to bring more potential buyers / options to the table. Buyers are found where sellers are.</li>
						<li>INCREASES SPEED TO SALE. Very important when your client needs to sell & buy quickly.</li>
						<li>REDUCES COST OF SALES. Less advertising, fewer open houses, less administrative work.</li>
						<li>GENERATES NEW LISTINGS. Can help you attract a homeowner back into the market, who might otherwise be hesitant to list their house.</li>
						<li>ELIMINATES UNCERTAINTIES. Ensures your clients will not face some of the complications that occur with typical real estate transactions such as:
							<ul>
								<li>Having to secure an interim rental while looking for their next home. A real bonus, particularly when rentals are expensive and vacancies are hard to find.</li>
								<li>Being impacted by fluctuating market conditions such as rising interest rates and/or rising property values that can quickly price a buyer out of a neighborhood they desire during the time between selling and buying.</li>
								<li>Having to deal with the financial difficulties involved when buying first, such as financing approval, carrying two mortgages for an unknown period of time, and not knowing what their financial outcome will be.</li>
							</ul>
						</li>
						<li>DUAL COMMISSIONS. With an xTrade you’re guaranteed two commissions one listing, and one selling.</li>
						<li>CUSTOMER SATISFACTION - Imagine how happy your client will be when you present them with the opportunity to sell and buy at the same time, get the home they really want, and avoid all the potential hassles and worries of the traditional real estate process.</li>
					</ol>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">What type of client does this work best for?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<ol>
						<li>Sellers who are also buyers (Sellerbuyers).</li>
						<li>Clients who are concerned about the uncertainty of buying first.  Eg: How long will it take to sell my house?  What will I get for it?  Will the bank approve the financing for my new home while I’m still carrying the mortgage for my current house?  What mortgage will I be left with once both transactions are completed?</li>
						<li>Clients who are concerned about the uncertainty of selling first. Eg: Will I have to find an interim rental?  How easy will it be to find a rental and what will it cost me?  How long will it take to find my next home?  Will prices and/or interest rates go up while I’m looking?</li>
						<li>Clients who want to expedite the selling-buying process.</li>
						<li>Clients who want to avoid the disruptions of multiple open houses and having to keep their home show ready.</li>
					</ol>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Is the service easy to use?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>Once a match is found, and you’re notified of the match, the transaction proceeds as usual. You contact the other agent and verify the properties are the right match for your respective clients.  If so, you set up showings, and proceed to contract with offers to purchase that are subject to each respective client’s property selling. </p>
					<p>Once your listings are prepared for xTrades, the system will instantly go to work finding and presenting matches for you to review and act upon.</p>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">Is xTradeHomes available in all market areas?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>xTradeHomes will be available soon across Canada, and we’re very excited about it!  Before we do a national release, however, we’re conducting a pilot test in the Vancouver and Lower Mainland market area.  Through our special arrangement with your Broker, you can have access to our service, as a pilot tester, for FREE.  </p>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">What’s involved with pilot testing?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>Pilot testing is a way for us to make our service available to a select group of brokers and agents in Vancouver and the Lower Mainland, so we can gather feedback on the service before a national rollout.</p>
					<p>As a pilot tester, you’ll have early adopter access to xTradeHomes. You’ll be part of a unique community of agents that will have a voice in shaping future enhancements and new features to the service that will make it even more valuable to you.</p>
					<p>In exchange for your free access to the service during the pilot period, we ask that you provide us with your valuable feedback.  Tell us what you think!  Your comments, suggestions, and any information you pass along regarding issues you may encounter, will be sincerely appreciated and most certainly taken into consideration.</p>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">How can I get started?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>Here’s a few easy steps to help you set up your account and start reaping the benefits of the xTradeHomes service.</p>
					<ol>
						<li>Create your account
							<ul>
								<li>Look for an invitation email from your Broker with a link to a signup page, or go to <a href="http://stage.xtradehomes.com">http://stage.xtradehomes.com</a> and sign up for an account.</li>
								<li>When signing up, be sure to include your CREA ID.  This is a required field and will ensure your listings are automatically imported into xTradeHomes.</li>
								<li>Once you’re logged in, click on &quot;My Profile&quot; and associate your account with your Broker.  This will ensure that in future, you’ll be entitled to any special pricing that may be available to your Broker.</li>
							</ul>
						</li>
						<li>Prepare your listings for xTrading
							<ul>
								<li>Click on My Listings</li>
								<li>Click on Client Has to open a form where you’ll find most of the data is already conveniently populated for you.  You may find that some required information (such as Sq. Footage) is not pre-populated if it’s not available through the CREA data feed.  Simply add the information and click Save Now.  You can also add additional information that will make for an even better match.</li>
								<li>Add your client’s contact information at the top of the form so that you can easily use certain email features within the app to email your client.</li>
								<li>Add your Client’s Wants.  You can enter the information yourself, or use the built in email feature to &quot;client complete their Wants&quot;.</li>
								<li>When you’ve completed the form, the xTrade Status will indicate &quot;Ready for xTrading&quot;. You’re all set, and ready for xTradeHomes to work its magic!</li>
							</ul>
						</li>
						<li>Ensure match notifications are turned on
							<ul>
								<li>Click on My Profile</li>
								<li>Subscribe to Match Notifications to receive email alerts of new matches.</li>
							</ul>
						</li>
						<li>View your matches
							<ul>
								<li>Click on My Matches</li>
								<li>Under the Actions menu, click &quot;View Matches&quot;.</li>
								<li>Click &quot;Save For Later&quot; if you want to keep the match.</li>
								<li>Click &quot;Ignore Match&quot; to remove the listing from your matches.</li>
							</ul>
						</li>
						<li>Take action on your matches
							<ul>
								<li>When viewing your matches, click &quot;Send Matches to Client&quot; to send your client an email notification of a possible match that you’d like them to review.</li>
								<li>If you’d like to create a PDF file of all matches for a particular listing, click &quot;Export Matches&quot;.  The file will open in your browser, allowing you to save it to your computer.</li>
								<li>If you have successfully completed a property trade, update the listing by clicking on &quot;Mark As xTraded&quot;.</li>
							</ul>
						</li>
					</ol>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">How can I submit product feedback?</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>During your pilot testing, we’ll periodically email you an online survey to check in with you and ask for your feedback.  In addition, we encourage you to submit comments, suggestions, and reports of any issues you find at any time to <a href="mailto:info@xtradehomes.com">info@xtradehomes.com</a>.  We want to hear from you and welcome all your feedback!</p>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d">The road ahead</a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<p>While using the xTradeHomes service you may notice a few features that, while not available during the pilot period, will be rolled out with our national launch.  We welcome your thoughts and comments on these as well!</p>

					<ul>
						<li><strong>Matches based on unlisted properties</strong><br>
							If you’re working with a prospect who hasn’t listed yet, you’ll be able to enter the details of their current home as a prospect listing in xTradeHomes, along with their wants for their next home.  xTradeHomes will show you what matches are available, without any notifications going out to other agents representing the other listing(s) in the match.  That’s a powerful tool for you, giving you some insightful market knowledge that you can provide to your prospective client, which will help encourage them to move forward and list!</li>
						<li><strong>Buyer searches and featured listings</strong><br>
							Buyers will be able to search for homes and indicate their interest in trading.  You’ll be able to upgrade your listings to a Featured Home that will get prominent display in buyer searches.</li>
						<li><strong>Local services directory</strong><br>
							Buyers and agents will be able to search from a list of local service providers, all specializing in services related to real estate.</li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
</div>