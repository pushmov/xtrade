<h2 class="small-12 column">Vendor's Home</h2>
<div class="small-6 column">
	<div class="padded">
		<h6>Information</h6>
		<dl>
			<dt>xTradeHomes ID:</dt>
			<dd><?=$data['vendor_id'];?></dd>

			<dt>Company Name:</dt>
			<dd><?=$data['company_name']?></dd>

			<dt>Name:</dt>
			<dd><?=$data['first_name'].' '.$data['last_name']?></dd>

			<dt>Address:</dt>
			<dd><?=$data['address'].' '.$data['location_address'].' '.$data['z_p_code']?></dd>

			<dt>Email:</dt>
			<dd><?=$data['email']?></dd>

			<dt>Public Email:</dt>
			<dd><?=$data['public_email']?></dd>

			<dt>Phone:</dt>
			<dd><?=  \Sysconfig::format_phone_number($data['phone'])?></dd>

			<dt>Toll Free:</dt>
			<dd><?=  \Sysconfig::format_phone_number($data['toll_free'])?></dd>

			<dt>Cell:</dt>
			<dd><?=  \Sysconfig::format_phone_number($data['cell'])?></dd>

			<dt>Site:</dt>
			<dd><?=$data['web_site']?></dd>

			<dt>Advertising In:</dt>
			<dd>
				<ul><li><?=str_replace(';', '</li><li>', $data['locale'])?></li></ul>
			</dd>

			<dt>Services Offered:</dt>
			<dd>
				<ul><li><?=str_replace(';', '</li><li>', $data['type']);?></li></ul>
			</dd>

			<dt>Service Renews On:</dt>
			<dd><?=$payment['next_payment']?></dd>

			<dt>Featured Status</dt>
			<dd><?=$featured_txt?></dd>
		</dl>
	</div>
</div>
<div class="small-6 column detail-div">
	<h2>Update Information</h2>
	<?=$gradeBtn?>
    <button type="button" class="button preview_ad">Preview Advertisement</button>
</div>

<div id="page_dialog" class="reveal large" data-reveal></div>
<div id="dialog" class="reveal tiny" data-reveal></div>