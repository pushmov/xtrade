<? if($_SESSION['SESS_ACCOUNT_TYPE'] == "Realtor"){ ?>
<div id="members_menu">
    <a href="members_home.php" class="buttons" style="margin-left: 10px; position: relative;" id="member_home">Member's Home</a> <button class="html_button" id='new_listing' style='font-size: 1em;'>Start New Listing</button> <button class="buttons" id="resource_button">Forms</button> <a href="profile.php" class="buttons" id="profile" style="position: relative;">My Profile</a> <a href="members_home.php?tutorial=true" class="buttons" id="profile" style="position: relative;">Tutorial</a>
    <span style="float: right;">
        <?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?>
        <?=\Html::anchor('vendor/location', 'Local Services Directory', array('class' => 'buttons', 'style' => 'margin-left: 0px;'));?>
    </span>
    <div id="resources" class="resources" style="display: none;">
        <div class="container">
        	<div style="background-color: #39B54A; color: white; width: 103px; height: 20px; margin-bottom: 5px;">Download</div>
            <a href="/assets/pdf/xtrade_form.pdf" target="_blank" style="margin-left: 0px;"><strong>xTrade Form</strong></a><br>
        	<a href="/assets/pdf/seller_form.pdf" target="form_blank" style="margin-left: 0px;"><strong>Seller Form</strong></a><br>
        	<a href="/assets/pdf/buyer_form.pdf" target="_blank" style="margin-left: 0px;"><strong>Buyer Form</strong></a><br>
        </div>
    </div>
</div>
<? }elseif($_SESSION['SESS_ACCOUNT_TYPE'] == "Broker"){ ?>
<div id="members_menu">
    <a href="members_home.php" class="buttons" style="margin-left: 10px; position: relative;" id="member_home">Broker's Home</a> <button class="html_button" id='add_user' style='font-size: 1em;'>Add REALTOR&reg;</button> <button class="buttons" id="resource_button">Forms</button> <a href="profile.php" class="buttons" id="profile" style="position: relative;">My Profile</a> <? if(count($pending_list) >= 1){ ?><a href="members_home.php" class="buttons" style="margin-left: 30px; position: relative;" id="member_home">Review REALTOR&reg; Requests</a> <? } ?>
    <span style="float: right;">
        <?=\Asset::img('dir_image.png', array('width' => 14, 'height' => '14', 'alt' => 'directory services'));?>
        <?=\Html::anchor('vendor/location', 'Local Services Directory', array('class' => 'buttons', 'style' => 'margin-left: 0px;'));?>
    </span>
    <div id="resources" class="resources_b" style="display: none;">
        <div class="container">
        	<div style="background-color: #39B54A; color: white; width: 103px; height: 20px; margin-bottom: 5px;">Download</div>
            <a href="images/pdf/xtrade_form.pdf" target="_blank" style="margin-left: 0px;"><strong>xTrade Form</strong></a><br>
        	<a href="images/pdf/seller_form.pdf" target="form_blank" style="margin-left: 0px;"><strong>Seller Form</strong></a><br>
        	<a href="images/pdf/buyer_form.pdf" target="_blank" style="margin-left: 0px;"><strong>Buyer Form</strong></a><br>
        </div>
    </div>
</div>
<? }elseif($_SESSION['SESS_ACCOUNT_TYPE'] == "Vendor"){ ?>
<div id="members_menu">
    <a href="members_home.php" class="buttons" style="margin-left: 10px; position: relative;" id="member_home">Advertiser's Home</a> <a href="profile.php" class="buttons" id="profile" style="position: relative;">My Profile</a>
    <span style="float: right;">
        <?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?>
        <?=\Html::anchor('vendor/location', 'Local Services Directory', array('class' => 'buttons', 'style' => 'margin-left: 0px;'));?>
    </span>
</div>
<? }elseif($_SESSION['SESS_ACCOUNT_TYPE'] == "Admin"){ ?>
<div id="members_menu">
    <a href="members_home.php" class="buttons" style="margin-left: 10px; position: relative; padding-left: 16px;" id="member_home">Admin Home</a> <button class="html_button" id='new_listing' style='font-size: 1em;'>Start New Listing</button> <button class="buttons" id="resource_button">Forms</button>
    <span style="float: right;">
        <?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?>
        <?=\Html::anchor('vendor/location', 'Local Services Directory', array('class' => 'buttons', 'style' => 'margin-left: 0px;'));?>
    </span>
    <div id="resources" class="resources" style="display: none;">
        <div class="container">
        	<div style="background-color: #39B54A; color: white; width: 103px; height: 20px; margin-bottom: 5px;">Download</div>
            <a href="images/pdf/xtrade_form.pdf" target="_blank" style="margin-left: 0px;"><strong>xTrade Form</strong></a><br>
        	<a href="images/pdf/seller_form.pdf" target="form_blank" style="margin-left: 0px;"><strong>Seller Form</strong></a><br>
        	<a href="images/pdf/buyer_form.pdf" target="_blank" style="margin-left: 0px;"><strong>Buyer Form</strong></a><br>
        </div>
    </div>
    | Admin:
    <select name="Select" id="admin_select">
    	<option value="">Choose Admin Area</option>
      <option value="users">Members</option>
      <option value="brokers">Brokers</option>
      <option value="vendors">Advertisers</option>
      <option value="settings">Settings</option>
      <option value="promos">Promotions</option>
      <option value="p2">CREA Agents and Offices</option>
      <option value="email_log">Email Log</option>
      <option value="login_log">Login Log</option>
    </select>
</div>
<? } ?>