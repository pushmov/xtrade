<div class="directory-results">
    <?php if(!empty($result)) : ?>
	<div class="clearfix">
		<?=$pagination;?>
	</div>
    <?php foreach($result as $row) : ?>
    <div class="row">
		<div class="clearfix padded-vertical vendor-result-item">
			<?php if($row['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED) : ?>
				<div class="directory-results-featured"></div>
			<?php endif; ?>
			<div class="small-12 medium-4 large-4 column">
				<!--<img src="homes/vendors/<?=$row['vendor_id'];?>" >-->

				<?= \Asset::img(\Model\Vendor::forge()->get_vendor_logo($row['vendor_id']), array('width' => 220, 'height' => 220));?>
			</div>
			<div class="small-12 medium-4 large-4 column">
				<ul class="unstyled option-list">
					<li>Service: <strong><?= \Model\Vendor::forge()->typeid_to_services($row['type']);?></strong></li>
					<li><a href="<?=($row['web_site'] == '') ? '#' : $row['web_site'];?>" target="_blank"><h3><?=$row['company_name']?></h3></a></li>
					<li><p><?=$row['address'].'<br>'.$row['location_address'].'<br>'.$row['z_p_code']?></p></li>
					<li><span><em>Telephone</em></span> : <strong><?=\Sysconfig::format_phone_number($row['phone']);?></strong></li>
					<li><span><em>Email</em></span> : <strong><?=\Html::mail_to($row['public_email'], $row['public_email'], NULL, array('style' => 'font-size: 0.9em;'));?></strong></li>
					<?php if($row['web_site'] != '') : ?>
					<li><span><em>Website</em></span> : <strong><?=\Html::anchor($row['web_site'], $row['web_site'], array('target' => '_blank'));?></strong></li>
					<?php endif; ?>

				</ul>
			</div>
			<div class="small-12 medium-4 large-4 column"><?=$row['comments']?></div>
		</div>
    </div>
    <hr>
    <?php endforeach; ?>
    <?php else :?>
    <div class="no_results">No results found for local services in <br><?=$location;?><br> <?= \Model\Vendor::forge()->typeid_to_services($type);?><br>Please type in another city.</div>
    <?php endif;?>
</div>