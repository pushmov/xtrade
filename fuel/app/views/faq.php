<div class="content column">
	<h1 class="text-green text-center"><?=__('title'); ?></h1>
	<div class="row">
		<div class="small-12 medium-8 small-centered column">
			<?php if($this->member !== null): ?>
				<p>When property owners, your clients, are ready to sell, most of them are also ready to buy.</p>
				<p>And they have a very good idea of what they want in their next home right down to a dozen details including price, location, type of home, and size.</p>
				<p>That presents you with the golden opportunity to be involved in at least two sales: selling your client’s current home and helping your clients find the ideal new home in an xTRADE™ involving as many as four other properties.</p>
				<?php else: ?>
				<p>When your client is ready to sell, they're usually prepared to buy as well.</p>
				<p>Since you already know what they want, right down to dozens of details such as price, location, size and type of home, doesn't it make more sense to both sell and buy at the same time?</p>
				<p>xTradeHomes.com - The Evolution in Real Estate</p>
				<p>xTradeHomes presents you with the perfect tool for matching up with another seller. Out there, somewhere, is your seller's future dream home!</p>
				<p>You'll wonder why you ever did this the traditional way. All you have to do now is contact your client and help them to add their house to xTradeHomes.</p>
				<?php endif; ?>
		</div>
	</div>
	<div class="row column">
		<ul class="accordion" data-accordion role="tablist">
			<?php foreach($data as $k): ?>
				<?php echo ($k == 1) ? '<li class="accordion-item is-active">' : '<li class="accordion-item">'; ?>
				<a href="#panel_<?=$k;?>" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d"><?=__('q' . $k);?></a>
				<div id="panel_<?=$k;?>" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<?=__('a' . $k);?>
				</div>
				</li>
				<?php endforeach; ?>
		</ul>
	</div>
</div>