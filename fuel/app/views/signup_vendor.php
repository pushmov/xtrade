<div class="row content">
	<form id="client" name="client" method="post" action="/signup">
		<div class="small-12 medium-12 large-6 column">
			<div class="row">
				<div class="small-12 column signup-left-top">
					<div class="padded-sides">
						<h2>Advertise here!</h2>
						<h1>Local Services Advertising Directory</h1>
						<p>xTradeHomes offers a local community focused advertising opportunity related to real estate trades and services. Your company's name and contact details will display alongside property search results where the visitor can conveniently contact you. A Featured Listing results in your ad displaying prominently at the top of any list.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 column">
					<div class="padded-sides">
						<h3>Pricing for Advertisers</h3>
						<h3 class="pricing-style">$<?=$fee; ?></h3> <strong>Monthly<br>Fee**</strong>
						<hr>
						<div class="fine-print">
							Plus applicable taxes.<br>
							Billed monthly.<br>
							No setup fee.<br>
							Automatic renewal.<br>
						</div>
					</div>
				</div>
				<div class="small-12 medium-6 column signup-left-bottom">
					<div class="padded-sides">
						<h4>Upgrade Your Ad To A Featured Ad!</h4>
						<p>for only <?=$feat_fee; ?> per month extra!</p>
						<input type="radio" name="featured" value="1"> Yes <input type="radio" name="featured" value="0" checked> No Thanks
						<br>
						<br>
						Promo Code<br>
						<input name="promo_code" type="text" id="promo_code" value="<?= $promo_code ?>" size="24" />
						<br>
						<br>
						<span class="fine-print">**All pricing subject to change</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 column">
					<div class="padded-sides">
						<h5>Please provide the following information for your directory listing:</h5>
						<label for="company_name"><?=__('company_name'); ?><span class="astrict">*</span></label>
						<input name="data[company_name]" type="text" id="company_name" value="<?=$data['company_name'];?>">

						<label for="type"><?=__('types_service'); ?><span class="astrict">*</span></label>
						<?= \Form::select('data[type][]', '', \Model\Vendor::forge()->all_services(), array('multiple' => 'multiple', 'id' => 'type'));?>

						<label for="locale"><?=__('advert_address'); ?><span class="astrict">*</span></label>
						<input name="data[locale]" type="text" id="locale" value="" placeholder="Start Typing City">
					</div>
				</div>
			</div>
			<div class="row padded-top">
				<div class="small-12 column">
					<div class="padded-sides">
						<h5>Terms &amp; Conditions</h5>
						<div class="scroll-div" name="terms"> <?=__('members_body'); ?> </div>
						<?=\Form::checkbox('tos', '1', '', array('class' => 'left', 'id' => 'tos'));?>
						<label for="tos" class="right tos-agreement"><?=__('agree'); ?><span class="astrict">*</span></label>
					</div>
				</div>
			</div>
		</div>

		<div class="small-12 medium-12 large-6 column">
			<div class="padded">
				<h5>Company Contact Information</h5>
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="first_name"><?=__('first'); ?><span class="astrict">*</span></label>
						<input name="data[first_name]" type="text" id="first_name" value="<?=$data['first_name'];?>">
					</div>
					<div class="small-12 medium-6 column">
						<label for="last_name"><?=__('last'); ?><span class="astrict">*</span></label>
						<input name="data[last_name]" type="text" id="last_name" value="<?=$data['last_name'];?>">
					</div>
				</div>
				<label for="address"><?=__('address');?><span class="astrict">*</span></label>
				<input name="data[address]" type="text" id="address" value="<?=$data['address'];?>">
				<label for="location_address"><?=__('city_prov_state_country');?><span class="astrict">*</span></label>
				<input name="data[location_address]" type="text" id="location_address" value="" placeholder="Start Typing City">
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="z_p_code"><?=__('zip_postal_code');?><span class="astrict">*</span></label>
						<input name="data[z_p_code]" type="text" id="z_p_code" value="<?=$data['z_p_code'];?>">
					</div>
					<div class="small-12 medium-6 column">
						<label for="toll_free"><?=__('toll_free_number');?></label>
						<input name="data[toll_free]" type="tel" id="toll_free" placeholder="1-800-555-5555" value="<?=$data['toll_free'];?>">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="phone"><?=__('phone'); ?><span class="astrict">*</span></label>
						<input name="data[phone]" type="tel" id="phone" placeholder="555-555-5555" value="<?=$data['phone'];?>" maxlength="14" title="Enter Numbers Only Please">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cell"><?=__('cell'); ?></label>
						<input name="data[cell]" type="tel" class="input_field" id="cell" placeholder="555-555-5555" value="<?=$data['cell'];?>" maxlength="14" title="Enter Numbers Only Please">
					</div>
				</div>
				<label for="web_site"><?=__('site'); ?></label>
				<input name="data[web_site]" type="text" placeholder="http://www.yoursite.com" id="web_site" value="<?=$data['web_site']?>">
				<div class="row">
					<div class="small-12 medium-6 end column">
						<label for="public_email"><?=__('public_facing_email'); ?><span class="astrict">*</span></label>
						<input name="data[public_email]" type="email" placeholder="your@emailaddress.com" id="public_email" value="<?=$data['public_email'];?>">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="email"><?=__('internal_email'); ?><span class="astrict">*</span></label>
						<input name="data[email]" type="email" placeholder="your@emailaddress.com" id="email" value="<?=$data['email'];?>">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cemail">Confirm <?=__('email'); ?><span class="astrict">*</span></label>
						<input name="data[cemail]" type="email" placeholder="your@emailaddress.com" id="cemail" value="<?=$data['cemail'];?>">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-6 column">
						<label for="password"><?=__('password'); ?><span class="astrict">*</span></label>
						<input name="data[password]" id="password" value="<?=$data['password'];?>" type="password" autocomplete="off"  title="<?=__('pass_rules'); ?>">
					</div>
					<div class="small-12 medium-6 column">
						<label for="cpassword"><?=__('re_enter_password'); ?><span class="astrict">*</span></label>
						<input name="data[cpassword]" id="cpassword" value="<?=$data['cpassword']?>" type="password" autocomplete="off"  title="<?=__('pass_rules'); ?>">
					</div>
				</div>
				<div id="show_pass">Show Password</div>
				<div class="row column">
					<label for="comments">Service Description<span class="astrict">*</span> (800 characters max) (Characters Left: <span id='limit_count'></span>)</label>
					<textarea name="data[comments]" id="comments"></textarea>
				</div>
				<div class="row">
					<div class="small-12 medium-7 small-centered column text-center">
						<small class="padded">Drop Images In The Box Below Or Click Upload.<br>
						Image will be resized to 140px by 140px.</small>
						<div class="padded-top"  id="file_upload">Select File To Upload</div>
					</div>
				</div>

				<div class="row padded-top">
				<hr>
					<div class="small-12 column text-center">
						<input name="user_type" id="user_type" type="hidden" value="vendor">
						<input name="data[logo]" id="img_file" type="hidden">
						<button type="button" class="button large" id="btn_join">Sign Up</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div id="page_dialog" class="reveal large" data-reveal></div>
<div id="payment_dialog" class="reveal large payment-dialog" data-reveal></div>
<script>
	$(document).ready(function() {
		new autoComplete({
			selector: '#location_address',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
			}
		});
/*
		new autoComplete({
			selector: '#types_service',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/tos.json', { q: term }, function(data){ response(data); });
			}
		});

		new autoComplete({
			selector: '#advert_address',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
			}
		});
*/

 		$('#locale').tokenInput(baseUrl + 'autocomplete/cpc.json?from=tokeninput', {
 			minChars: 3,
 			tokenDelimiter: ';',
 			preventDuplicates: true
		});

		$('#comments').jqte();

		var limitCount = 800;
		var limitNum = 800;
		var obj;
		var cur_val;
		var re = /(<\/*[\s\S]*?>)/g;
		var re2 = /(&nbsp;)/g;
		var re_sp = /(&nbsp;)|(<\/*[\s\S]*?>)/g;

		$("#limit_count").html(limitCount);
		$('.jqte_editor').on("keydown", function(e){
			if(e.which == 8 || e.which == 46){
				return true;
			}else{
				cur_var = $('#comments').val();
				cur_var_stripped = cur_var.replace(re, "");
				cur_var_stripped = cur_var_stripped.replace(re2, " ");
				obj = cur_var_stripped.length;

				if(obj >= limitNum){
					return false;
				}
			}
		});

		$('.jqte_editor').on("keyup", function(){
			cur_var = $('#comments').val();
			cur_var_stripped = cur_var.replace(re, "");
			cur_var_stripped = cur_var_stripped.replace(re2, " ");
			obj = cur_var_stripped.length;

			limitCount = limitNum - obj;
			$("#limit_count").html(limitCount);
		});

		$('#file_upload').uploadFile({
			url: baseUrl + 'signup/doupload.json',
			fileName: 'myfile',
			allowedTypes: 'jpg,jpeg,png',
			returnType: 'json',
			showPreview: true,
			previewWidth: '75%',
			onSuccess: function(files,data,xhr) {
				$('#img_file').val(data[0]);
			}
		});
		$('#type').SumoSelect();
	});

	function limitText(limitField, limitCount, limitNum) {
		if (limitField.value.length > limitNum) {
			limitField.value = limitField.value.substring(0, limitNum);
		} else {
			limitCount = limitNum - limitField.value.length;
		}
	}
</script>