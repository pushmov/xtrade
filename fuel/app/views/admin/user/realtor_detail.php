<h3>REALTOR&reg;'s Details</h3>
<?php if(empty($data)):?>
	Unable to retrieve details
	<?php else: ?>
	<dl id="<?=$data['realtor_id']?>" class="row">
		<dt>Name:</dt>
		<dd><?=$data['first_name'].' '.$data['last_name']?></dd>
		<dt>Email:</dt>
		<dd><?=\Html::mail_to($data['email'], $data['email']);?></dd>
		<dt>xTradeHomes ID:</dt>
		<dd><?=$data['realtor_id'];?></dd>

		<?php if($data['office_id'] != '0') : ?>
			<dt>Office ID:</dt>
			<dd><?=$data['office']['company_name'];?></dd>
			<?php endif; ?>

	<?php if($data['nrb_id'] != '') : ?>
		<dt>CREA/NRDS ID: </dt>
		<dd><?=$data['nrb_id']?></dd>
		<?php endif; ?>
	<dt>Account Created On:</dt>
	<dd><?=date('F d, Y', strtotime($data['account_creation']));?></dd>
	<dt>Last Logged In:</dt>
	<dd><?=date('F d, Y H:i:s', strtotime($data['last_login']));?></dd>
	<dt>Alert Status:</dt>
	<dd><?=$data['alerts'];?></dd>
	<dt>Membership Renews On:</dt>
	<dd><?=$data['renewal'];?></dd>
	<dt>Last Active:</dt>
	<dd><?=$data['last_active'];?></dd>
	<dt>Last Known Page:</dt>
	<dd><?=$data['location'];?></dd>

	<dt><?=\Form::button('profile', 'View/Modify Profile', array('type' => 'button', 'class' => 'button view_profile', 'name' => 'profile', 'id' => $data['realtor_id'], 'data-name' => 'realtor', 'data-id' => $data['id']));?></dt>
	<dd></dd>

	<dt><?=\Form::button('change_broker', 'Add to/Change Broker', array('type' => 'button', 'class' => 'button change_broker', 'id' => $data['realtor_id']));?></dt>
	<dd></dd>

	<dt><?=\Form::button('reset', 'Reset Password', array('type' => 'button', 'class' => 'button reset_password', 'id' => $data['realtor_id']));?></dt>
	<dd></dd>

	<dt><?=$data['suspend_or_not']?></dt>
	<dd></dd>

	<dt><?=\Form::button('delete', 'Delete Account', array('type' => 'button', 'class' => 'button delete_account', 'id' => $data['realtor_id']));?></dt>
	<dd></dd>

</dl>
<?php endif;?>
