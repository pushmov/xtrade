<div class="content">
	<div class="column admin-page">
		<div class="small-12 medium-6 column">
			<h4>Members</h4>
		</div>
	</div>
	<div class="padded">
		<table id="users" class="display">
			<thead>
				<tr>
					<th>Realtor ID</th>
					<th>Name</th>
					<th>Status</th>
					<th>Created</th>
					<th>Exclusive</th>
					<th>Downloaded</th>
					<th>X Ready</th>
					<th>Matches</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
<div id="contact_agent_dialog" class="reveal" data-reveal data-close-on-click="false"></div>
<script>
	$(document).ready(function() {
		var dt = $('#users').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			order: [[2, 'asc'],[3, 'desc']],
			ajax: {
				url: '/admins/user/list.json'
			},
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 4,5,6,7,8 ] }
			]
		});

		//exclusive link
		$('#users').on('click', '.realtor-exclusive', function(){
			var l = $(this);
			var txt = l.html();
			var tr = $(this).closest('tr');
			var row = dt.row(tr);
			if(row.child.isShown()){
				row.child.hide();
				tr.removeClass('shown');
			} else {
				l.html(loader);
				l.prop('disabled', true);
				$.post('/admins/user/exclusive.html', {realtor_id: $(this).attr('id')}, function(html){
					row.child(html).show();
					tr.addClass('shown');
					l.html(txt);
					l.prop('disabled', false);
				});
			}
		});

		//downloaded link
		$('#users').on('click', '.realtor-downloaded', function(){
			var l = $(this);
			var txt = l.html();
			var tr = $(this).closest('tr');
			var row = dt.row(tr);
			if(row.child.isShown()){
				row.child.hide();
				tr.removeClass('shown');
			} else {
				l.html(loader);
				l.prop('disabled', true);
				$.post('/admins/user/downloaded.html', {realtor_id: $(this).attr('id')}, function(html){
					row.child(html).show();
					tr.addClass('shown');
					l.html(txt);
					l.prop('disabled', false);
				});
			}
		});

		//downloaded link
		$('#users').on('click', '.realtor-ready', function(){
			var l = $(this);
			var txt = l.html();
			var tr = $(this).closest('tr');
			var row = dt.row(tr);
			if(row.child.isShown()){
				row.child.hide();
				tr.removeClass('shown');
			} else {
				l.html(loader);
				l.prop('disabled', true);
				$.post('/admins/user/ready.html', {realtor_id: $(this).attr('id')}, function(html){
					row.child(html).show();
					tr.addClass('shown');
					l.html(txt);
					l.prop('disabled', false);
				});
			}
		});

		//matches link
		$('#users').on('click', '.realtor-matches', function(){
			var l = $(this);
			var txt = l.html();
			var tr = $(this).closest('tr');
			var row = dt.row(tr);
			if(row.child.isShown()){
				row.child.hide();
				tr.removeClass('shown');
			} else {
				l.html(loader);
				l.prop('disabled', true);
				$.post('/admins/user/matches.html', {realtor_id: $(this).attr('id')}, function(html){
					row.child(html).show();
					tr.addClass('shown');
					l.html(txt);
					l.prop('disabled', false);
				});
			}
		});
		// Email member
		$('#users').on('click', '.email-user', function(e){
			e.preventDefault();
			$.get('/admins/contact/agent.html', {id : $(this).attr('id')},function(html) {
				$('#contact_agent_dialog').html(html);
				$('#contact_agent_dialog').foundation('open');
			});
		});

		$('.reveal').on('click', '#submit', function(e){
			e.preventDefault();
			var that = $(this);
			var currTxt = that.text();
			that.html('<i class="fa fa-spinner fa-spin"></i>Processing...');
			that.prop('disabled', true);
			$('.alert-box,.input-error').remove();
			var form = $(this).closest('form');
			$.post(form.attr('action'), form.serialize(), function(json) {
				$('.callout').remove();
				if (json.errors) {
					$.each(json.errors, function(k,v) {
						$('#c_' + k).after('<div class="input-error">' + v + '</div>');
					});
					that.prop('disabled', false);
				} else if(json.status === 'OK'){
					form.before(json.message);
				}
				that.html(currTxt);
			}).fail( function(xhr, textStatus, errorThrown) {
				$('.notifier#sending').hide();
				$('.notifier#error').show();
				alert(xhr.responseText);
			});
			return false;
		});

		// Delete member
		$('#users').on('click', '.remove', function(e){
			e.preventDefault();
			$.get('/admins/contact/delete.html', {id : $(this).attr('id')},function(html) {
				$('#dialog').html(html);
				$('#dialog').foundation('open');
			});
		});

		$('.reveal').on('click', '#btn_delete_yes', function(e){
			$.post('/admins/contact/delete.html', {id : $('#del_agent_id').val()}, function(json) {
				dt.ajax.reload();
				$('#dialog').foundation('close');
			});
		});

		//impersonate btn
		$('#users').on('click', '.impersonate', function(e){
			e.preventDefault();
			window.location = '/admins/user/impersonate/?id='+$(this).attr('id');
		});
	});
</script>
