<table width="100%" class="responsive">
	<thead>
		<tr>
			<th>xTrade ID</th>
			<th>Address</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($data)) : ?>
		<?php foreach($data as $row):?>
		<tr>
			<td><?=$row['listing']?></td>
			<td><?=$row['address']?></td>
		</tr>
		<?php endforeach; ?>
		<?php else :?>
		<tr>
			<td colspan="3" align="center">No xTradeHomes Listings Found For This REALTOR&reg;.</td>
		</tr>
		<?php endif; ?>
	</tbody>
</table>