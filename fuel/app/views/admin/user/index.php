<div class="columns admin-page">
	<div class="clearfix">
		<div class="small-7 column realtor-list">
			<h2>Members</h2>
			<div><?=$pagination?></div>
			<?php if(!empty($users)) : ?>
			<?php foreach($users as $row) : ?>
			<div class="small-12 columns">
				<dl id="<?=$row['id']?>" class="row">
					<dt>REALTOR&reg; Name:</dt>
					<dd><?=$row['first_name'].' '.$row['last_name']?> <button class="button realtor-detail">REALTOR&reg; Details</button></dd>
					<dt>REALTOR&reg; ID:</dt>
					<dd><?=$row['realtor_id']?></dd>
					<dt>Account Status:</dt>
					<dd><?=$row['button']['status'].' '.$row['button']['approve'];?></dd>
					<dt>Number of Listings:</dt>
					<dd><?=$row['listing']['valid'];?> Valid | <?=$row['listing']['total']?> Total</dd>
					<dt>
						<button type="button" class="button exclusive"><?=$row['xlisting']?></button>
						<button type="button" data-id="<?=$row['ext_id']?>" class="button downloaded"><?=$row['olisting']?></button>
					</dt>
					<dd></dd>
					<dt>Current Number of Matches:</dt>
					<dd>
						<button type="button" class="button matching">Matching Listings <?=($row['listing']['matches'] > 0) ? ' ('.$row['listing']['matches'].')' : ''?></button>
					</dd>
				</dl>
			</div>
			<hr>
			<?php endforeach; ?>
			<div><?=$pagination?></div>
			<?php else : ?>
			<p>No record found</p>
			<?php endif; ?>
		</div>
		<div class="small-5 column" id="details_div">
			<h2>REALTOR&reg; Details</h2>
			<p>Click On A Link On The Left To See More Details.</p>
		</div>
	</div>
	<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
</div>