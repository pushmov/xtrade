<table width="100%" class="responsive">
    <thead>
        <tr>
            <th>Agent ID</th>
			<th>Agent Name</th>
            <th>Office</th>
            <th>Number of Listings</th>
        </tr>
    </thead>
    <tbody>
        <?php if(empty($data)) :?>
        <tr>
            <td colspan="3">No record found</td>
        </tr>
        <?php else :?>
        <?php foreach($data as $row):?>
        <tr>
            <td><?=$row['realtor_id']?></td>
			<td><?=$row['name']?></td>
            <td><?=$row['office_id']?></td>
            <td><?=$row['listings']?></td>
        </tr>
        <?php endforeach;?>
        <?php endif; ?>
    </tbody>
</table>