<li class="accordion-item is-active client-has" id="client_has">
	<?php if ($has['type_of_listing'] == \Model\Listing::TYPE_XTRADED || $has['type_of_listing'] == \Model\Listing::TYPE_SELL_ONLY):?>
	<a href="#panel_2" role="tab" class="accordion-title" id="panel2d-heading" aria-controls="panel2d">Client &quot;Has&quot;
		<?php if($has['external_id'] != ''):?>
			<span>MLS#<?=$has['external_id'];?></span>
		<?php endif; ?>
	</a>
	<div id="panel_2" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel2d-heading">
		<input name="has[id]" id="id" type="hidden" value="<?=$has['id'];?>">
		<input name="has[listing_id]" id="id" type="hidden" value="<?=$edit['listing_id'];?>">
		<div class="row column text-right attention">
			<?php if ( $has['completed_section'] == \Model\Client::STATUS_INCOMPLETE): ?>
			Please Complete All Required Fields <?=\Asset::img('attention.png', array('width' => 35, 'height' => 35, 'alt' => __('attention')));?>
			<?php else : ?>
			Information Up To Date <?=\Asset::img('up-to-date.png');?>
			<?php endif; ?>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<label for="p_address" class="req-text"><?=__('paddress');?><span class="astrict">*</span></label>
				<input name="has[address]" type="text" class="input_field_ext req" id="p_address" value="<?=$has['address'];?>" data-field="address"/>
			</div>
			<div class="large-6 columns">
				<label for="address" class="req-text"><?=__('cpsc');?><span class="astrict">*</span></label>
				<input name="has[c_address]" type="text" class="input_field_ext req" id="address" value="<?=$has['c_address'];?>" placeholder="Start Typing City" data-field="c_address"/>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns" id="neighbourhood_h_wrapper">
				<label for="has[neighbourhood]" class="req-text" id="neighbourhood_h_label"><?=__('neigh');?>
					<span class="astrict">*</span>
					<?=\Form::select('has[neighbourhood]', $has['neighbourhood'], array(), array('id' => 'neighbourhood_h','class' => 'no_multi req','title' => __('neigh_title'), 'data-field' => 'neighbourhood'));?>
				</label>
			</div>
			<div class="large-4 columns">
				<label for="has_z_p_code" class="req-text"><?=__('zip');?><span class="astrict">*</span></label>
				<input name="has[z_p_code]" type="text" class="input_box_required req" id="has_z_p_code" value="<?=$has['z_p_code'];?>" data-field="z_p_code" />
			</div>
			<div class="large-4 columns">
				<div class="row collapse">
					<label for="has_listing_price" class="req-text"><?=__('price');?><span class="astrict">*</span></label>
					<span class="prefix inline left">$</span>
					<input type="text" name="has[listing_price]" id="has_listing_price" value="<?=$has['listing_price'];?>" class="input_box_required req prefixed right" data-field="listing_price" />
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label for="has_sq_footage" class="req-text"><?=__('sqfeet');?><span class="astrict">*</span></label>
				<input type="text" name="has[sq_footage]" id="has_sq_footage" value="<?=$has['sq_footage'] ?>" class="input_box_required req" data-field="sq_footage"/>
			</div>
			<div class="large-4 columns">
				<label for="prop_type_h" class="req-text"><?=__('property');?><span class="astrict">*</span></label>
				<?=\Form::select('has[prop_type]', $has['prop_type'], \Model\Client\Has::forge()->prop_type(), array('id' => 'prop_type_h','class' => 'req10 no_multi req', 'data-field' => 'prop_type'));?>
			</div>
			<div class="large-4 columns">
				<label for="has_bedrooms" class="req-text"><?=__('bed');?><span class="astrict">*</span></label>
				<input type="text" name="has[bedrooms]" id="has_bedrooms" value="<?=$has['bedrooms'] ?>" class="req" data-field="bedrooms" />
			</div>
		</div>

		<div class="row">
			<div class="large-4 columns">
				<label for="has_bathrooms" class="req-text"><?=__('bath'); ?><span class="astrict">*</span></label>
				<input type="text" name="has[bathrooms]" id="has_bathrooms" value="<?=$has['bathrooms'] ?>" class="req" data-field="bathrooms"/>
			</div>
			<div class="large-4 columns p_title_h_wrapper">
				<label for="p_title_h" class="req-text" id="p_title_h_label"><?=__('title'); ?><span class="astrict">*</span></label>
				<?=\Form::select('has[p_title]', $has['p_title'], \Model\Client\Has::forge()->p_title(), array('id' => 'p_title_h','class' => 'no_multi req dropdown_required', 'data-field' => 'p_title'));?>
			</div>
			<div class="large-4 columns">
				<label for="prop_style_h" class="req-text"><?=__('storeys'); ?><span class="astrict">*</span></label>
				<?=\Form::select('has[prop_style]', $has['prop_style'], \Model\Client\Has::forge()->prop_style(), array('id' => 'prop_style_h','class' => 'no_multi req dropdown_required', 'data-field' => 'prop_style'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label><?=__('lotsize'); ?></label>
				<input type="text" name="has[lot_size]" id="lot_size" value="<?=$has['lot_size'];?>" class="input_boxs" data-field="lot_size"/>
			</div>
			<div class="large-4 columns has-total-acreage">
				<label><?=__('acreage'); ?></label>
				<input type="text" name="has[total_acreage]" id="total_acreage" value="<?=$has['total_acreage'];?>" class="input_boxs" data-field="total_acreage"/>
			</div>
			<div class="large-4 columns">
				<label for="siding_type_h"><?=__('siding_type'); ?></label>
				<?=\Form::select('has[siding_type][]', str_split($has['siding_type']), \Model\Client\Has::forge()->siding_type(), array('id' => 'siding_type_h','class' => 'multi dropdown_required','multiple' => 'multiple', 'data-field' => 'siding_type'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns finished_basement_h_wrapper">
				<label for="finished_basement_h" class="req-text" id="basement_h_label"><?=__('basement'); ?><span class="astrict">*</span></label>
				<?=\Form::select('has[finished_basement]', $has['finished_basement'], \Model\Client\Has::forge()->finished_basement(), array('id' => 'finished_basement_h','class' => 'no_multi req dropdown_required', 'data-field' => 'finished_basement'));?>
			</div>
			<div class="large-4 columns garage_type_h_wrapper">
				<label for="garage_type_h" class="req-text" id="garage_h_label"><?=__('garage'); ?><span class="astrict">*</span></label>
				<?=\Form::select('has[garage_type]', $has['garage_type'], \Model\Client\Has::forge()->garage_type(), array('id' => 'garage_type_h','class' => 'no_multi req dropdown_required', 'data-field' => 'garage_type'));?>
			</div>
			<div class="large-4 columns">
				<label for="view_h"><?=__('view'); ?></label>
				<?=\Form::select('has[view]', $has['view'], \Model\Client\Has::forge()->view(), array('id' => 'view_h','class' => 'no_multi dropdown_required', 'data-field' => 'view'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label for="heat_source_h"><?=__('heat_source');?></label>
				<?=\Form::select('has[heat_source]', $has['heat_source'], \Model\Client\Has::forge()->heat_source(), array('id' => 'heat_source_h','class' => 'no_multi dropdown_required', 'data-field' => 'heat_source'));?>
			</div>
			<div class="large-4 columns">
				<label for="heating_h"><?=__('heat_type');?></label>
				<?=\Form::select('has[heating]', $has['heating'], \Model\Client\Has::forge()->heating(), array('id' => 'heating_h','class' => 'no_multi dropdown_required', 'data-field' => 'heating'));?>
			</div>
			<div class="large-4 columns">
				<label for="cooling_h"><?=__('cool_type');?></label>
				<?=\Form::select('has[cooling]', $has['cooling'], \Model\Client\Has::forge()->cooling(), array('id' => 'cooling_h','class' => 'no_multi dropdown_required', 'data-field' => 'cooling'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label for="water_h"><?=__('water_type');?></label>
				<?=\Form::select('has[water]', $has['water'], \Model\Client\Has::forge()->water(), array('id' => 'water_h','class' => 'no_multi dropdown_required', 'data-field' => 'water'));?>
			</div>
			<div class="large-4 columns">
				<label for="sewer_h"><?=__('sewer_type');?></label>
				<?=\Form::select('has[sewer]', $has['sewer'], \Model\Client\Has::forge()->sewer(), array('id' => 'sewer_h','class' => 'no_multi dropdown_required', 'data-field' => 'sewer'));?>
			</div>
			<div class="large-4 columns">
				<label for="floor_type_h"><?=__('floor_type');?></label>
				<?=\Form::select('has[floor_type][]', str_split($has['floor_type']), \Model\Client\Has::forge()->floor_type(), array('id' => 'floor_type_h','class' => 'multi dropdown_required','multiple' => 'multiple', 'data-field' => 'floor_type'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label for="roof_type_h"><?=__('roof_type');?></label>
				<?=\Form::select('has[roof_type][]', str_split($has['roof_type']), \Model\Client\Has::forge()->roof_type(), array('id' => 'roof_type_h','class' => 'multi dropdown_required','multiple' => 'multiple', 'data-field' => 'roof_type'));?>
			</div>
			<div class="large-4 columns">
				<label for="year_built_h"><?=__('ybuilt');?></label>
				<?=\Form::select('has[year_built]', $has['year_built'], \Model\Client\Has::forge()->year_built(), array('id' => 'year_built_h','class' => 'no_multi dropdown_required', 'data-field' => 'year_built'));?>
			</div>
			<div class="large-4 columns">
				<div class="row collapse">
					<label><?=__('ptax');?></label>
					<span class="prefix inline left">$</span>
					<input type="text" name="has[prop_tax]" id="prop_tax" value="<?=$has['prop_tax'];?>" class="prefixed right" data-field="prop_tax"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<label><?=__('lsd'); ?></label>
				<input type="text" name="has[school_info]" id="school_info" value="<?=$has['school_info'];?>" class="input_boxs" data-field="school_info"/>
			</div>
			<div class="large-4 columns">
				<label><?=__('youtube_link');?></label>
				<input type="text" name="has[youtube_link]" id="youtube_link" value="<?=$has['youtube_link'] ?>" class="input_boxs" data-field="youtube_link">
			</div>
			<div class="large-4 columns">
				<label><?=__('vt_link');?></label>
				<input type="text" name="has[virtual_tour_link]" id="virtual_tour_link" value="<?=$has['virtual_tour_link'];?>" class="input_boxs" data-field="virtual_tour_link">
			</div>
		</div>

		<div class="row small-up-1 medium-up-3 large-up-5 features-box">
			<div class="column">
				<h6><strong><?=__('ifeature');?></strong></h6>
				<?php if(!empty($has['interior_features'])) :?>
					<div class="features-check-all">(<a href="#" class="no-link">check all</a>)</div>
					<?php foreach($has['interior_features'] as $index => $value) :?>
						<label for="has[interior_features][<?=$index?>]">
							<input type="hidden" value="<?=$unchecked;?>" name="has[interior_features][<?=$index?>]">
							<?=\Form::checkbox('has[interior_features]['.$index.']', $checked, $value, array('id' => 'has[interior_features]['.$index.']', 'class' => 'customized-checkbox', 'data-field' => 'interior_features', 'data-index' => $index, 'data-name' => 'interior_features'));?>
							<?= \Model\Client\Has::forge()->interior_features($index); ?>
						</label>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="column">
				<h6><strong><?=__('kfeature');?></strong></h6>
				<?php if(!empty($has['kitchen'])) :?>
					<div class="features-check-all">(<a href="#" class="no-link">check all</a>)</div>
					<?php foreach($has['kitchen'] as $index => $value) :?>
						<label for="has[kitchen][<?=$index?>]">
							<input type="hidden" name="has[kitchen][<?=$index?>]" value="<?=$unchecked?>">
							<?=\Form::checkbox('has[kitchen]['.$index.']', $checked, $value, array('id' => 'has[kitchen]['.$index.']', 'class' => 'customized-checkbox', 'data-field' => 'kitchen', 'data-index' => $index, 'data-name' => 'kitchen'));?>
							<?= \Model\Client\Has::forge()->kitchen($index);?>
						</label>
					<?php endforeach; ?>
				<?php endif;?>
			</div>
			<div class="column">
				<h6><strong><?=__('lfeatures');?></strong></h6>
				<?php if(!empty($has['laundry'])) :?>
					<div class="features-check-all">(<a href="#" class="no-link">check all</a>)</div>
					<?php foreach($has['laundry'] as $index => $value) :?>
						<label for="has[laundry][<?=$index?>]">
							<input type="hidden" name="has[laundry][<?=$index?>]" value="<?=$unchecked?>">
							<?=\Form::checkbox('has[laundry]['.$index.']', $checked, $value, array('id' => 'has[laundry]['.$index.']', 'class' => 'customized-checkbox', 'data-field' => 'laundry_features', 'data-index' => $index, 'data-name' => 'laundry'));?>
							<?= \Model\Client\Has::forge()->laundry($index);?>
						</label>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="column">
				<h6><strong><?=__('pfeature');?></strong></h6>
				<?php if($has['property_features']) :?>
					<div class="features-check-all">(<a href="#" class="no-link">check all</a>)</div>
					<?php foreach($has['property_features'] as $index => $value) :?>
						<label for="has[property_features][<?=$index?>]">
							<input type="hidden" name="has[property_features][<?=$index?>]" value="<?=$unchecked?>">
							<?=\Form::checkbox('has[property_features]['.$index.']', $checked, $value, array('id' => 'has[property_features]['.$index.']', 'class' => 'customized-checkbox', 'data-field' => 'property_features', 'data-index' => $index, 'data-name' => 'property_features'));?>
							<?= \Model\Client\Has::forge()->property_features($index);?>
						</label>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="column">
				<?php if(!empty($has['neighbourhood_features'])) :?>
					<h6><strong><?=__('nfeature');?></strong></h6>
					<div class="features-check-all">(<a href="#" class="no-link">check all</a>)</div>
					<?php foreach($has['neighbourhood_features'] as $index => $value) :?>
						<label for="has[neighbourhood_features][<?=$index?>]">
							<input type="hidden" name="has[neighbourhood_features][<?=$index?>]" value="<?=$unchecked?>">
							<?=\Form::checkbox('has[neighbourhood_features]['.$index.']', $checked, $value, array('id' => 'has[neighbourhood_features]['.$index.']', 'class' => 'customized-checkbox', 'data-index' => $index, 'data-name' => 'neighbourhood_features'));?>
							<?= \Model\Client\Has::forge()->neighbourhood_features($index);?>
						</label>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>

		<div class="has_comments">
			<?=__('has_add_comments');?><br />
			<textarea name="has[comments]" id="comments" rows="12" <?=$has['imported'] == \Model\Listing::IMPORTED ? 'disabled' : '';?> data-field="comments"><?=$has['comments']?></textarea>
			<br />
		</div>

	</div>
	<?php else :?>
	<a href="#panel_2" role="tab" class="accordion-title" id="panel2d-heading" aria-controls="panel2d">Client &quot;Has&quot;<i class="fa fa-lock fa-lg"></i></a>
	<div id="panel_2" class="accordion-content accordion-closed" role="tabpanel" data-tab-content aria-labelledby="panel2d-heading"></div>
	<?php endif; ?>
</li>