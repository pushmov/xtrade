<li class="accordion-item is-active client-image-info" id="image_info">
	<?php if ($has['type_of_listing'] == \Model\Listing::TYPE_XTRADED || $has['type_of_listing'] == \Model\Listing::TYPE_SELL_ONLY):?>
	<a href="#panel_3" role="tab" class="accordion-title" id="panel3d-heading" aria-controls="panel3d">Property Photo's 
		<?php if($has['external_id'] != ''):?>
			<span>MLS#<?=$has['external_id'];?></span>
		<?php endif; ?>
	</a>
	<div id="panel_3" class="accordion-content" role="tabpane3" data-tab-content aria-labelledby="panel3d-heading">
		<input name="wants[id]" id="id" type="hidden" value="<?=$wants['id'];?>">

		<div class="row column text-right attention">
			<?php if($has['imported'] == \Model\Listing::NON_IMPORTED) : ?>
				<?php if ( $has['current_images'] == \Model\Client::STATUS_INCOMPLETE): ?>
					Please Complete All Required Fields <?=\Asset::img('attention.png', array('width' => 35, 'height' => 35, 'alt' => __('attention')));?>
					<?php else : ?>
					Information Up To Date <?=\Asset::img('up-to-date.png');?>
					<?php endif; ?>
				<?php endif; ?>
		</div>
		<div class="row">
			<div class="clearfix padded-sides">
				<div class="image-info-wrapper" id="image_info_wrapper"></div>
			</div>
			<?php if($has['imported'] == \Model\Listing::NON_IMPORTED) : ?>
				<div class="clearfix row collapse padded-top">
					<div class="small-12 medium-push-1 medium-2 column" id="dropzone">
						Drop Photo<br>Here<br>To Delete
					</div>
					<div class="small-12 medium-8 medium-centered column text-center padded-top">
						<small>Drop Images In The Box Below Or Click Upload. Image will be resized to 700px maximum width</small>
						<div class="padded-top" id="file_upload">Select File To Upload</div>
					</div>
				</div>
				<?php endif; ?>
		</div>
		<input name="img_file" id="img_file" type="hidden">
	</div>
	<?php else : ?>
	<a href="#panel_3" role="tab" class="accordion-title" id="panel3d-heading" aria-controls="panel3d">Property Photo's <i class="fa fa-lock fa-lg"></i></a>
	<div id="panel_3" class="accordion-content accordion-closed" role="tabpanel" data-tab-content aria-labelledby="panel3d-heading"></div>
	<?php endif; ?>
</li>