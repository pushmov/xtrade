<div class="content">
	<div class="column admin-page">
		<div class="small-12 medium-6 column">
			<h4>Active CREA Brokers and Agents</h4>
		</div>
		<div class="column">
			CREA Listings: <?=number_format($listings, 0);?> &nbsp; CREA Brokers: <?=number_format($brokers, 0);?> &nbsp; CREA Agents: <?=number_format($agents, 0);?>
		</div>
		<div class="small-12 medium-6 column text-right">
			<button class="button small" id="btn_agent_export">Export</button>
		</div>
	</div>
	<div class="padded">
		<table id="agents" class="display">
			<thead>
				<tr>
					<th>CREA ID</th>
					<th>Name</th>
					<th>City</th>
					<th>Province</th>
					<th>Phone</th>
					<th>Total Listings</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>