<div class="content">
	<div class="column admin-page">
		<div class="small-12 medium-6 column">
			<strong>Login Log</strong>
		</div>
	</div>
	<div id="admin_page" class="padded">
		<table id="logins" class="display">
			<thead>
				<tr>
					<th>Date</th>
					<th>User ID</th>
					<th>User Name Used</th>
					<th>IP Address</th>
					<th>Response</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
<script>
	$(document).ready(function() {
		$('#logins').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: '/admins/log/login_log.json'
			},
			responsive: true
		});
	} );
</script>