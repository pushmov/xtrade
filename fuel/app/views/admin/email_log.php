<div class="content">
	<div class="column admin-page">
		<div class="small-12 medium-6 column">
			<strong>Email Log</strong>
		</div>
	</div>
	<div id="admin_page" class="padded">
		<table id="email" class="display">
			<thead>
				<tr>
					<th>Date</th>
					<th>To</th>
					<th>From</th>
					<th>Subject</th>
					<th>CC</th>
					<th>BCC</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
<script>
	$(document).ready(function() {
		$('#email').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: '/admins/log/email_log.json'
			},
			responsive: true
		});
	} );
</script>