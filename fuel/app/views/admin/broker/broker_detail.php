<div class="padded">
	<h2>Broker Details</h2>
	<?php if(empty($data)) : ?>
		Unable to retrieve details
		<?php else:?>
		<dl id="<?=$data['broker_id']?>" class="row">
			<dt>xTradeHomes ID:</dt>
			<dd><?=$data['broker_id']?></dd>
			<dt><?=$data['company_name']?></dt>
			<dd></dd>
			<dt><?=$data['first_name'].' '.$data['last_name']?></dt>
			<dd></dd>
			<dt>Office ID:</dt>
			<dd><?=$data['broker_id']?></dd>
			<dt>Membership Renews On:</dt>
			<dd><?=$renewal?></dd>
			<dt><?=\Form::button('profile', 'View/Modify Profile', array('type' => 'button', 'id' => $data['broker_id'], 'class' => 'button view_profile', 'data-name' => 'broker', 'data-id' => $data['id']));?></dt>
			<dd></dd>
			<dt><?=\Form::button('profile', 'Add New REALTOR&reg; To Broker', array('type' => 'button', 'id' => $data['broker_id'], 'class' => 'button add_realtor'));?></dt>
			<dd></dd>
			<dt><?=\Form::button('profile', 'Reset Password', array('type' => 'button', 'id' => $data['broker_id'], 'class' => 'button broker_reset_password'));?></dt>
			<dd></dd>
			<dt><?=$suspend_or_not?></dt>
			<dd></dd>
			<dt><?=\Form::button('profile', 'Delete Account', array('type' => 'button', 'id' => $data['broker_id'], 'class' => 'button delete_broker'));?></dt>
			<dd></dd>
		</dl>

		<?php endif; ?>
</div>