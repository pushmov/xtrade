<div class="content">
	<div class="column admin-page">
		<div class="small-12 medium-6 column">
			<h4>Brokers</h4>
		</div>
	</div>
	<div class="padded">
		<table id="admin_brokers" class="display">
			<thead>
				<tr>
					<th>Broker ID</th>
					<th>Company Name</th>
					<th>Name</th>
					<th>City</th>
					<th>State</th>
					<th>Status</th>
					<th>Realtors</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>