<table width="100%" class="responsive">
	<thead>
		<tr>
			<th>Realtor ID</th>
			<th>Name</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($data)) : ?>
		<tr>
			<td colspan="3" align="center">These Are The REALTORS&reg; Within <em><strong><?=$broker['company_name']?></strong></em> Brokers Office.</td>
		</tr>
		<?php foreach($data as $row):?>
		<tr>
			<td><?=$row['realtor_id'];?></td>
			<td><?=$row['first_name'].' '.$row['last_name']?></td>
			<td>
				<?=\Form::button('profile', 'View/Modify Profile', array('type' => 'button', 'class' => 'button view_profile small', 'id' => $row['realtor_id'], 'name' => 'profile', 'data-id' => $row['id'], 'data-name' => 'realtor'));?>
				<?=\Form::button('remove_realtor', 'Remove REALTOR&reg;', array('type' => 'button', 'class' => 'button remove_realtor small alert', 'id' => $row['realtor_id']));?>
			</td>
		</tr>
		<?php endforeach; ?>
		<?php else :?>
		<tr>
			<td colspan="3" align="center">No REALTORS&reg; Found For <em><strong><?=$broker['company_name']?></strong></em> Broker.</td>
		</tr>
		<?php endif; ?>
	</tbody>
</table>