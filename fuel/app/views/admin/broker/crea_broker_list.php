<div class="content">
	<div class="column admin-page">
		<div class="small-12 medium-6 column">
			<h4>CREA Brokers</h4>
		</div>
	</div>
	<div class="padded">
		<table id="crea_admin_brokers" class="display">
			<thead>
				<tr>
					<th>CREA id</th>
					<th>Name</th>
					<th>City</th>
					<th>Province</th>
					<th>Postal</th>
					<th>Phone</th>
					<th>Listings</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<script>
	$(document).ready(function() {
		$('#crea_admin_brokers').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			//order: [[ 3, 'asc' ], [4, 'asc']],
			ajax: {
				url: '/admins/broker/crea_list.json'
			}
		});
	});
</script>