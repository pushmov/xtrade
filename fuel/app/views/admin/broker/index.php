<div class="columns admin-page">
	<div class="clearfix">
		<div class="small-7 column realtor-list">
			<h2>Brokers</h2>
			<div class="padded">

			<?php if(!empty($brokers)) : ?>
				<?php foreach($brokers as $row) :?>
				<dl id="<?=$row['id']?>" class="row">
					<dt>Brokers Name:</dt>
					<dd>
						<?=$row['first_name'].' '.$row['last_name'].' of '.$row['company_name'];?>
						<?=\Form::button('details', 'Broker Details', array('class' => 'button broker_detail', 'id' => $row['broker_id']));?>
					</dd>
					<dt>Discount For REALTORS&reg;:</dt>
					<dd>
					<input name="realtor_discount" class="change_field inline" value="<?=$row['realtor_discount'];?>"> % &nbsp;
					<button name="update_discount" type="button" class="button small change_discount">Change</button>
					</dd>
					<?php if($row['paypal_plan'] != '') : ?>
					<dd>
						<a href="/admins/paypal/view/<?=$row['paypal_plan']?>" class="inline-action">View Plan</a>
						<a href="#" class="inline-action red disable-discount" data-id="<?=$row['id']?>">Disable Discount</a>
					</dd>
					<?php endif; ?>
					<dt>Account Status:</dt>
					<dd><?=$row['button_account_status']['status'].' '.$row['button_account_status']['approve'];?></dd>
					<dt>CREA/NRDS ID:</dt>
					<dd><?=$row['ext_id'];?></dd>
					<dt>Account Created On:</dt>
					<dd><?=$row['account_creation'];?></dd>
					<dt>Last Logged In:</dt>
					<dd><?=$row['last_login'];?></dd>
					<dt>Number of REALTORS&reg;:</dt>
					<dd><?=$row['number_realtors']?> <?=\Form::button('realtors', 'See REALTOR&reg; List', array('type' => 'button', 'class' => 'button num_realtors', 'id' => $row['broker_id']));?></dd>
				</dl>
				<hr>
				<?php endforeach; ?>
			<?php else :?>
				No record found
			<?php endif; ?>
			</div>
		</div>
		<div class="small-5 column" id="details_div">

		</div>
	</div>
</div>

<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>