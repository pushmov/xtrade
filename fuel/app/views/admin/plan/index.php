<div class="columns admin-page">
	<h3 class="float-left">Plans</h3>
	<a href="/admins/plans/edit" class="button float-right" type="button">Add Plan</a>
	<div class="small-12 column">
		<div class="row">
			<div class="small-12 column">
				<?=\Session::get_flash('message');?>
			</div>
		</div>

		<div class="row">
			<table id="promo" class="display">
				<thead>
					<tr>
						<th>Type</th>
						<th>Description</th>
						<th>Country</th>
						<th>State</th>
						<th>Paypal Name</th>
						<th>Promo Code</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
