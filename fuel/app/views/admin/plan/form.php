<div class="content">
	<div class="small-12 column">
		<h3><?=$page_title;?></h3>
		<form action="/admins/plans/submit.json" method="post" name="plan">
			<div class="row">
				<div class="small-12 column">
					<?=\Session::get_flash('message');?>
				</div>
			</div>
			<div class="row">
				<div class="small-4 column">
					<label for="type_id" class="right inline">Type</label>
				</div>
				<div class="small-8 column">
					<?=\Form::select('data[type_id]', $data['type_id'], \Model\Plan::forge()->get_type(), array('id' => 'type_id', $readonly => $readonly));?>
				</div>
			</div>
			<div class="row">
				<div class="small-4 column">
					<label for="country_id" class="right inline">Country</label>
				</div>
				<div class="small-2 column">
					<?=\Form::select('data[country_id]', $data['country_id'], \CityState::get_country(), array('id' => 'country_id', $readonly => $readonly));?>
				</div>
				<div class="small-2 column text-right">
					<label for="state_id" class="right inline">State</label>
				</div>
				<div class="small-3 column end">
					<?=\Form::select('data[state_id]', $data['state_id'], \CityState::get_state($data['country_id']), array('id' => 'state_id', $readonly => $readonly));?>
				</div>
			</div>

			<div class="row">
				<div class="small-4 column">
					<label for="name" class="right inline">Paypal Name  <span class="astrict">*</span></label>
				</div>
				<div class="small-8 column">
					<input type="text" name="data[name]" id="name" value="<?=$data['name']?>" <?=$readonly?>>
				</div>
			</div>

			<div class="row">
				<div class="small-4 column">
					<label for="info" class="right inline">Description <span class="astrict">*</span></label>
				</div>
				<div class="small-8 column">
					<input type="text" name="data[info]" id="info" value="<?=$data['info']?>" <?=$readonly?>>
				</div>
			</div>

			<div class="row">
				<div class="small-4 column">
					<label for="details" class="right inline">Details for the User</label>
				</div>
				<div class="small-8 column">
					<textarea name="data[details]" class="editor" id="details" <?=$readonly?>><?=$data['details']?></textarea>
				</div>
			</div>

			<div class="row">
				<div class="small-4 column">
					<label for="rate" class="right inline">Monthly Rate <span class="astrict">*</span></label>
				</div>
				<div class="small-2 column">
					<input type="text" name="data[rate]" id="rate" value="<?=$data['rate']?>" <?=$readonly?>>
				</div>
				<div class="small-2 column text-right">
					<label for="rate" class="right inline">Monthly Tax Amount</label>
				</div>
				<div class="small-2 column end">
					<input type="text" name="data[rate_tax]" id="rate_tax" value="<?=$data['rate_tax']?>" <?=$readonly?>>
				</div>
			</div>


			<div class="row">
				<div class="small-4 column">
					<label for="promo_code" class="right inline">Promo Code</label>
				</div>
				<div class="small-2 column">
					<input type="text" name="data[promo_code]" id="promo_code" value="<?=$data['promo_code']?>" <?=$readonly?>>
				</div>
				<div class="small-2 column text-right">
					<label for="promo_len" class="right inline">Promo Length (month)</label>
				</div>
				<div class="small-2 column end">
					<input type="text" name="data[promo_len]" id="promo_len" value="<?=$data['promo_len']?>" <?=$readonly?>>
				</div>
			</div>

			<div class="row">
				<div class="small-4 column">
					<label for="promo_rate" class="right inline">Promo Monthly Rate</label>
				</div>
				<div class="small-2 column">
					<input type="text" name="data[promo_rate]" id="promo_rate" value="<?=$data['promo_rate']?>" <?=$readonly?>>
				</div>
				<div class="small-2 column text-right">
					<label for="promo_tax" class="right inline">Promo Tax Amount</label>
				</div>
				<div class="small-2 column end">
					<input type="text" name="data[promo_tax]" id="promo_tax" value="<?=$data['promo_tax']?>" <?=$readonly?>>
				</div>
			</div>

			<div class="row">
				<div class="small-4 column">
					<label for="plan_id" class="right inline">Paypal Plan ID</label>
				</div>
				<div class="small-8 column">
					<input type="text" name="data[plan_id]" id="plan_id" value="<?=$data['plan_id']?>" readonly="readonly">
				</div>
			</div>

			<div class="row column">
				<?php if($data['status_id'] == \Model\Plan::S_NEW || $data['status_id'] == \Model\Plan::S_ACTIVE):?>
				<button type="button" class="button" id="btn_save">Save</button>
				<?php endif; ?>

				<?php if($data['status_id'] == \Model\Plan::S_NEW && $data['id'] != ''):?>
				<button type="button" class="button" id="btn_active">Activate Plan</button>
				<button type="button" class="button" id="btn_delete">Delete</button>
				<?php endif; ?>

			</div>
			<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
		</form>
	</div>
</div>
<div id="dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>