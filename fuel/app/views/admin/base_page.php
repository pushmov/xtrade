<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link href="/assets/css/auto-complete.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/app.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/admin.css" rel="stylesheet" type="text/css" media="screen" />
		<?php echo \Asset::css($styles, array(), null, false); ?>
		<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="/assets/js/modernizr.js"></script>
		<script src="/assets/js/foundation.min.js" type="text/javascript"></script>
        <script src="/assets/js/jquery.mask.js" type="text/javascript"></script>
		<script src="/assets/js/admin.js" type="text/javascript"></script>
		<?php echo \Asset::js($scripts, array(), null, false); ?>
		<script>
			var baseUrl = '<?=Uri::base(false);?>';
			<?php if (isset($js_vars)) {foreach ($js_vars as $k => $v) echo "var $k = '$v';", PHP_EOL;} ?>
		</script>
	</head>
	<body>
		<div id="header">
			<div class="row"></div>
		</div>
		<div class="row content" id="content">
			<div class="column"><?=$header;?></div>
			<?=$content;?>
		</div>
		<div id="footer">
			<div class="row">
				<div class="small-12 medium-2 column"><a href="/"><img src="/assets/images/ft_logo.png" width="152" height="52" alt="Xtrade Homes"></a></div>
				<div class='small-12 medium-8 column b-nav'>
					<a href="/about">About Us</a>
					<a href="/contact">Contact Us</a>
					<a href="/privacy">Privacy Policy</a>
					<a href="/terms">Terms & Conditions</a>
					<a href="/sitemap">Sitemap</a>
				</div>
				<div class="small-12 medium-2 column">
					<span title="The trademarks REALTOR&reg;, REALTORS&reg; and the REALTOR&reg; logo are controlled by The Canadian Real Estate Association (CREA) and identify real estate professionals who are members of CREA" style="font-weight:bold;" class='ft_img'>REALTOR&reg;</span> <img src="/assets/images/ddf.jpg" width="45" height="25" alt="CREA" class='ft_img' title='The trademark DDF® is owned by The Canadian Real Estate Association (CREA) and identifies CREA’s Data Distribution Facility (DDF®)'>
				</div>
			</div>
		</div>
		<div id="login_dialog" class="reveal tiny" data-reveal data-close-on-click="false">
		</div>
		<script>
			$(document).foundation();
		</script>
	</body>
</html>