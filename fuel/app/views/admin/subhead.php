<div class="small-5 column padded-vertical">
<h5 class="text-green">xTradeHomes Administration - <?=$name;?></h5>
<div><?=$breadcrumbs?></div>
</div>
<div class="small-5 column padded-vertical">
	<select name="Select" id="admin_select">
		<option value="">Choose Admin Area</option>
		<option value="">Listings</option>
		<option value="user">Members</option>
		<option value="broker">Brokers</option>
		<option value="vendor">Vendors</option>
		<option value="setting">Settings</option>
		<option value="plans">Plans</option>
		<option value="agent">CREA Brokers and Agents</option>
		<option value="log/email">Email Log</option>
		<option value="log/login">Login Log</option>
	</select>

</div>
<div class="small-2 column padded-vertical text-center">
	<button id="logout" class="button tiny">Logout</button>
</div>
<hr>