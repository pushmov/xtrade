<div class="column padded"><h3>Administration Login</h3></div>
<div class="row">
	<div class="small-4 small-centered column">
		<div id="login_error"><?=$message;?></div>
		<form action="/admins/login" method="post" name="login_form" id="login_form">
			<input name="login_email" type="email" class="" id="login_email" value="<?=$email; ?>" placeholder="Email Address"  /><br>
			<input name="login_pass" type="password" class="" id="login_pass" value="" placeholder="Password" /><br>
			<button type="submit" class="button expanded" id="login_sbutton" name="login_button">Login</button><br>
		</form>
	</div>
</div>