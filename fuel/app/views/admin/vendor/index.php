<div class="columns admin-page">
	<div class="clearfix">
		
		<div class="small-7 column realtor-list">
			<h2>Vendors</h2>
			<?php if(!empty($vendors)) : ?>
			<?php foreach($vendors as $row) : ?>
			<dl id="<?=$row['id']?>" class="row">
				<dt>Vendor Name:</dt>
				<dd><?=$row['first_name'].' '.$row['last_name']?>
					<div><button class="button vendor-detail">Vendor Detail</button></div></dd>
				<dt>Account Status:</dt>
				<dd><?=$row['button']['status'].' <div>'.$row['button']['approve'].'</div>'?></dd>
				<dt>Phone Number</dt>
				<dd><?=\Sysconfig::format_phone_number($row['phone']);?></dd>
				<dt>Cell Number</dt>
				<dd><?=\Sysconfig::format_phone_number($row['cell']);?></dd>
				<dt>Toll Free Number</dt>
				<dd><?=\Sysconfig::format_phone_number($row['toll_free']);?></dd>
				<dt>Email</dt>
				<dd><?=$row['internal_email']?></dd>
				<dt>Featured Status</dt>
				<dd><?=$row['featured']?></dd>
				<dt>Account Created On</dt>
				<dd><?=date_create($row['account_creation'])->format(DATETIME_FORMAT)?></dd>

				<dt>Next Renewal Date</dt>
				<dd><?=$row['active_till']?></dd>

			</dl>
			<?php endforeach; ?>

			<?php else : ?>
			<p>No record found</p>
			<?php endif; ?>
		</div>
		<div class="small-5 column" id="details_div">

		</div>
	</div>
</div>