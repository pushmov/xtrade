<div class="padded">
	<h3>Vendor's Details</h3>
	<?php if(empty($data)):?>
		Unable to retrieve details
		<?php else: ?>
		<dl id="<?=$data['vendor_id']?>" class="row">
			<dt>xTradeHomes ID:</dt>
			<dd><?=$data['vendor_id']?></dd>

			<dt>Company:</dt>
			<dd><?=$data['company_name']?></dd>

			<dt>Name:</dt>
			<dd><?=$data['first_name'].' '.$data['last_name']?></dd>
			<dt>Address:</dt>
			<dd><?=$data['address'].'<br>'.$data['location_address'].'<br>'.$data['z_p_code']?></dd>
			<dt>Email:</dt>
			<dd><?=$data['email']?></dd>
			<dt>Internal Email:</dt>
			<dd><?=$data['internal_email']?></dd>
			<dt>Phone:</dt>
			<dd><?=\Sysconfig::format_phone_number($data['phone']);?></dd>
			<dt>Toll Free:</dt>
			<dd><?=\Sysconfig::format_phone_number($data['toll_free'])?></dd>
			<dt>Cell:</dt>
			<dd><?=\Sysconfig::format_phone_number($data['cell']);?></dd>
			<dt>Site:</dt>
			<dd><?=$data['web_site']?></dd>

			<dt>Advertising In:</dt>
			<dd><?=$advertising?></dd>

			<dt>Services Offered:</dt>
			<dd><?=$services?></dd>

			<dt>Membership Renews On:</dt>
			<dd><?=$renewal?></dd>

			<dt><?=\Form::button('profile', 'View/Modify Profile', array('type' => 'button', 'class' => 'button view_profile', 'data-name' => 'vendor', 'id' => $data['vendor_id'], 'data-id' => $data['id']));?></dt>
			<dd></dd>
			<dt><?=\Form::button('preview', 'Preview Advertisement', array('type' => 'button', 'class' => 'button vendor_preview'));?></dt>
			<dd></dd>
			<dt><?=\Form::button('reset', 'Reset Password', array('type' => 'button', 'class' => 'button vendor_reset'));?></dt>
			<dd></dd>
			<dt><?=$suspend_or_not;?></dt>
			<dd></dd>
			<dt><?=\Form::button('delete', 'Delete Account', array('type' => 'button', 'class' => 'button vendor_delete'));?></dt>
			<dd></dd>

		</dl>
	<?php endif;?>
</div>