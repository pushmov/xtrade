<div class="content column">
	<div class="row about-top">
		<div class="small-12 medium-11 large-10 small-centered column">
			<?=\Asset::img('hiw_q.png', array('alt' => 'xTrade'));?>
			<h1><?=__('content_title');?></h1>
			<?=__('content_top');?>
		</div>
	</div>
	<div class="row about-top-under">
		<div class="small-12 medium-11 large-10 small-centered column">
			<?=__('content_top_under');?>
		</div>
	</div>
	<div class="row column hiw-second">
		<h2><?=__('content_second_title');?></h2>
	</div>
	<div class="row hiw-second">
		<div class="small-9 column"><?=__('content_second_1');?></div>
		<div class="small-3 column text-center"><img src="/assets/images/hiw_second_image1.png"></div>
	</div>
	<div class="row hiw-second">
		<div class="small-3 column text-center"><img src="/assets/images/hiw_second_image2.png"></div>
		<div class="small-9 column"><?=__('content_second_2');?></div>
	</div>
	<div class="row hiw-acc">
		<ul class="accordion" data-accordion role="tablist">
			<li class="accordion-item is-active">
				<a href="#panel_1" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d"><?=__('content_second_brokers_title');?></a>
				<div id="panel_1" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<ul class="row">
						<li class="small-12 medium-6 column"><?=__('content_second_brokers_l');?></li>
						<li class="small-12 medium-6 column"><?=__('content_second_brokers_r');?></li>
					</ul>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_2" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d"><?=__('content_second_agents_title');?></a>
				<div id="panel_2" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<ul class="row">
						<li class="small-12 medium-6 column"><?=__('content_second_agents_l');?></li>
						<li class="small-12 medium-6 column"><?=__('content_second_agents_r');?></li>
					</ul>
				</div>
			</li>
			<li class="accordion-item">
				<a href="#panel_3" role="tab" class="accordion-title" id="panel1d-heading" aria-controls="panel1d"><?=__('content_second_consumers_title');?></a>
				<div id="panel_3" class="accordion-content" role="tabpanel" data-tab-content aria-labelledby="panel1d-heading">
					<ul class="row">
						<li class="small-12 medium-6 column"><?=__('content_second_consumers_l');?></li>
						<li class="small-12 medium-6 column"><?=__('content_second_consumers_r');?></li>
					</ul>
				</div>
			</li>
		</ul>
	</div>
	<div class="row column hiw-third">
		<h2><?=__('content_third_title');?></h2>
	</div>
	<div class="row column hiw-third">
		<p><?=__('content_third_s1');?></p>
	</div>
	<div class="row column hiw-third text-center">
		<div class="matches-matches">
			<div class="match-box sample two-way-match-box matches-margin clearfix">
				<div data-target="small" class="sided-arrow-area">
					<canvas></canvas>
					<i class="fa fa-random fa-lg"></i>
				</div>
				<div class="row padded-sides matches-wrapper">
					<i class="fa fa-random fa-lg"></i>
						<canvas class="canvas-arrow" width="314" height="80"></canvas>
						<div class="list clearfix">
						<!-- Single House Box Start -->
							<div class="matches-column matches-container">
								<div class="matches-outer-box">
									<div class="letter">A</div>
									<div class="matches-box">
										<div class="top">Your Client's Home</div>
										<?=\Asset::img('3bed_ex.jpg', array('class' => 'img-responsive'));?>
										<div class="text padded-top padded-bottom">
											<h3>$450,000</h3>
											<p>1353 Maple Rd</p>
											<p>Vancouver, British Columbia</p>
											<span>Client: George Furgeson<br>Listing ID: NA1246533</span>
										</div>
									</div>
								</div>
							</div>
							<!-- Single House Box End -->
							<div class="matches-column matches-container matches-hiw">
								<a href="#" class="no-link" title=""><i class="fa fa-random fa-lg"></i></a>
							</div>
							<div class="matches-column matches-container sided-random">
								<div class="matches-outer-box">
									<a class="no-link" href="#">
										<i class="fa fa-random fa-lg"></i>
									</a>
								</div>
							</div>
							<!-- Single House Box Start -->
							<div class="matches-column matches-container">
								<div class="matches-outer-box">
									<div class="letter">B</div>
									<div class="matches-box">
										<div class="top">Your Client's Match</div>
										<?=\Asset::img('condo_ex.jpg', array('class' => 'img-responsive'));?>
										<div class="text padded-top padded-bottom">
											<h3>$375,000</h3>
											<p>100 White Ave</p>
											<p>Abbotsford, British Columbia</p>
											<span>
												<a href="#" class="no-link">View Listing</a><br>
												<a href="#" class="contact-agent no-link">Contact Agent</a>
											</span>
										</div>
									</div>
								</div>
							</div>
							<!-- Single House Box End -->
						</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="row column hiw-third">
		<p><?=__('content_third_s2');?></p>
	</div>
	<div class="row column hiw-third text-center">
		<div class="matches-matches">
			<div class="match-box sample three-way-match-box matches-margin clearfix">
				<div data-target="medium" class="sided-arrow-area">
					<canvas></canvas>
					<i class="fa fa-random fa-lg"></i>
				</div>
				<div class="row padded-sides matches-wrapper">
					<i class="fa fa-random fa-lg"></i>
					<canvas class="canvas-arrow" width="510" height="80"></canvas>
					<div class="list clearfix">
						<div class="matches-column matches-container">
							<div class="matches-outer-box">
								<div class="letter">A</div>
								<div class="matches-box">
									<div class="top">Your Client's Home </div>
									<?=\Asset::img('beach_ex.jpg', array('class' => 'img-responsive'));?>
									<div class="text padded-top padded-bottom">
										<h3>$490,000</h3>
										<p>53 Calle Rd</p>
										<p>Beverly Hills, California</p>
										<span>Client: George Furgeson<br>Listing ID: NA1246533</span>
									</div>
								</div>
							</div>
						</div>
						<div class="matches-column matches-container matches-hiw">
							<a href="#" class="no-link" target="_blank">
								<i class="fa fa-random fa-lg"></i>
							</a>
						</div>
						<div class="matches-column matches-container sided-random">
							<div class="matches-outer-box">
								<a class="tooltip tooltipstered" target="_blank" href="#">
									<i class="fa fa-random fa-lg"></i>
								</a>
							</div>
						</div>

						<div class="matches-column matches-container">
							<div class="matches-outer-box">
								<div class="letter">B</div>
								<div class="matches-box">
									<div class="top">Your Client's Match </div>
									<?=\Asset::img('town_ex.jpg', array('class' => 'img-responsive'));?>
									<div class="text padded-top padded-bottom">
										<h3>$400,000</h3>
										<p>1000 Main St</p>
										<p>Calgary, Alberta</p>
										<span>
											<a href="#" class="no-link">View Listing</a><br>
											<a href="#" class="no-link">Contact Agent</a></span>
									</div>
								</div>
							</div>
						</div>

						<div class="matches-column matches-container matches-hiw">
							<a href="#" class="no-link" target="_blank">
								<i class="fa fa-random fa-lg"></i>
							</a>
						</div>
						<div class="matches-column matches-container sided-random">
							<div class="matches-outer-box">
								<a class="tooltip tooltipstered" target="_blank" href="#">
									<i class="fa fa-random fa-lg"></i>
								</a>
							</div>
						</div>
						<div class="matches-column matches-container">
							<div class="matches-outer-box">
								<div class="letter">C</div>
								<div class="matches-box">
									<div class="top l_top"><div>B's Match <br>C Wants Your Client's Home</div></div>
									<?=\Asset::img('example_listing.jpg', array('class' => 'img-responsive'));?>
									<div class="text padded-top padded-bottom">
										<h3>$395,000</h3>
										<p>120 De Maisonneuve St</p>
										<p>Montreal, Quebec</p>
										<span>
											<a href="#" class="no-link">View Listing</a><br>
											<a href="#" class="no-link">Contact Agent</a>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row about-fifth">
		<div class="small-12 column text-center">
			<?=\Asset::img('large_shuffle.png');?>
			<h2><?=__('content_fourth_title');?></h2>
		</div>
		<p class="row column"><?=__('content_fourth_subtitle');?></p>
		<div class="small-12 medium-6 column">
			<?=__('content_fourth_l');?>
			<div class="small-3 end column">
				<?=\Asset::img('note.png');?>
				<br>
				<?=\Asset::img('l_mag.png');?>
		</div></div>
		<div class="small-12 medium-6 column">
			<?=__('content_fourth_r');?>
			<div class="small-3 end column">
				<?=\Asset::img('world.png');?>
				<br>
				<?=\Asset::img('bolt.png');?>
			</div>
		</div>
		<div class="row column text-center padded">
			<a href="/signup"><button div class="button large">Get Started Now</button></a>
		</div>
	</div>
</div>