<?=\View::forge('email/header');?>
<p>Thank you for signing up and joining our growing community of agents who are using xTradeHomes to find real estate matches for their listing and buyer clients.</p>
<p>Ready to get started?</p>
<ol>
	<li>Set up your account profile.
		<ul>
			<li>Select My Profile, and make sure your account is associated with your broker.</li>
			<li>Subscribe to Match Notifications.</li>
		</ul>
	</li>
	<li>Prepare your listings for xTrading.
		<ul>
			<li>Select My Listings, and verify what your client HAS.</li>
			<li>Enter in what your client WANTS, or if you prefer, use the handy built-in email feature to request your client completes their Wants.</li>
			<li>Once you’ve prepped your listings, the status will indicate Ready for xTrading, and the magic will start happening!</li>
		</ul>
	</li>
	<li>Review your matches.
		<ul>
			<li>Select My Matches, then click &quot;View Matches&quot;.</li>
			<li>Save For Later if you want to keep the match.</li>
			<li>Ignore Match if you don’t want to see the match again.</li>
		</ul>
	</li>
	<li>Send matches to your client for their review.
		<ul>
			<li>When viewing matches, click &quot;Send Matches to Client&quot;.</li>
		</ul>
	</li>
</ol>
<p>Have any questions? We’re here to help! Email us at <a href='mailto:info@xtradehomes.com'>info@xtradehomes.com</a>.</p>
<p>Sincerely,</p>
<p>xTradeHomes Customer Service</p>
<?=\View::forge('email/footer');?>