<?=\View::forge('email/header');?>
Hello <?=$data['first_name'] . ' ' . $data['last_name'];?>!
<br>
Your broker, <?=$data['company_name'];?>, has approved your request to be added to their account;<br>
<?=\Html::anchor($url, 'Click here');?> to sign in and take advantage of the discount!
<br>
<br>
If you cannot click on the link, copy and paste this url: <?=\Html::anchor($url, $url);?>
<br>
<br>
Thank You
<?=\View::forge('email/footer');?>