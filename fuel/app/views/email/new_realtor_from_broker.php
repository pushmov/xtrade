<?=\View::forge('email/header');?>
<p>Hello <?=$first_name;?>,</p>
<p>Your broker, <?=$company_name;?>, has invited you to join xTradeHomes, a powerful real estate matching service and listing tool designed exclusively for real estate agents.</p>
<p>With xTradeHomes you can:</p>
<ul>
<li><strong>Help your listing clients sell and buy faster.</strong> As soon as you join xTradeHomes, the service instantly starts identifying other sellers that your client can trade properties with.  Each party gets the property they desire and all transactions occur simultaneously, eliminating all the uncertainties and hassles involved with traditional real estate transactions such as interim rentals and bridge financing.</li>
<li><strong>Secure new listing clients.</strong> In a hot market with low inventory and low rental vacancy rates, prospective sellers may be hesitant to list their house.  xTradeHomes will identify potential xTrades for your Exclusive Listings, so your client never has to be concerned about finding their next home.</li>
</ul>
<p>Ready to give it a try?  It takes just minutes to set up your account, and there is no charge at this time to use it.</p>
<p style="center"><strong><?=\Html::anchor('signup/realtor?'.$url, 'Join xTradeHomes Now!');?></strong></p>
<p>To learn more about xTradeHomes and how it can help you, visit our <?=\Html::anchor('faq?agent=faq', 'FAQ');?>.</p>
<p>Thanks, and we look forward to having you on board!</p>
<p>xTradeHomes Customer Service</p>
<?=\View::forge('email/footer');?>