<?=\View::forge('email/header');?>
<h4>Contact Form Submission</h4>
 	Name: <?=$name;?>
	<br />
	Phone: <?=$phone;?>
	<br />
	Email: <?=$email;?>
	<br />
	<br />
	In relation to: <?=$email_to;?>
	<br />
	<br />
	Comment: <?=$comment;?>
	<br />
	<br />
	Submission Time: <?=date_create()->format('Y-m-d H:m:i');?>
	<br />
	User IP: <?=\Input::ip();?>
	<br />
<?=\View::forge('email/footer');?>