<?=\View::forge('email/header');?>
Instructions for Canadian member or Broker to approve xTradeHomes download from CREA/Point2<br>
<br>
1. Login to RealtorLink <a href='http://www.realtorlink.ca'>www.realtorlink.ca</a><br>
<br>
2. Look for the DDF Dashboard or click <a href='https://secure.realtorlink.ca/naflogin/naflogin.aspx?SiteDomain=login.point2.com&targetURL=https%3A%2F%2Flogin.point2.com%2FCreaLogin'>here</a><br>
<br>
<?=\Asset::img('email/cnd_sign/img_1.png');?>
<br>
3. Then the Notifications/Manage Destinations<br>
<br>
<?=\Asset::img('email/cnd_sign/img_2.png');?>
<br>
4. And under Approvals select either My Listings or if you’re a Broker, Brokerage Listings<br>
<br>
<?=\Asset::img('email/cnd_sign/img_3.png');?>
<br>
5. Scroll to xTradehomes.com and turn on the datafeed<br>
<br>
<?=\Asset::img('email/cnd_sign/img_4.png');?>
<br>
6. Log out of RealtorLink and within 24 hours, your active MLS® listings will start to appear on the xTradeHomes.com website.<br>
<?=\View::forge('email/footer');?>