<?=\View::forge('email/header');?>
Hello <?=$fname?> <?=$lname?>,
<br>
Your account has been suspended due to too many payments failing;<br>
Please <a href="mailto:account@xtradehomes.com">email us</a> or call us at 1-844-7XTRADE (1-844-798-7233) to resolve this issue.
<br>
<br>
Thank You
<?=\View::forge('email/footer');?>