<?=\View::forge('email/header');?>
<p>Hello <?=$first_name;?>,</p>
<p>This email is to notify you that your CREA listing are being imported in to your xTradeHomes dashboard.</p>
<p>Sincerely,</p>
<p>xTradeHomes Customer Service<br>
<?=\Html::mail_to('info@xtradehomes.com');?><p>
<?=\View::forge('email/footer');?>