<?=\View::forge('email/header');?>
<p>Thank you for signing up and joining our growing community of brokers who are making xTradeHomes available to their agents.</p>
<p>Ready to get started?</p>
<ol>
	<li>Invite your agents to start using xTradeHomes.
		<ul>
			<li>Log in to xTradeHomes and use the convenient built-in invitation feature to Add REALTOR&reg;.</li>
			<li>Simply enter the agent’s name and email address and an invitation will be sent to their inbox.</li>
		</ul>
	</li>
	<li>Get real-time insight into match activity.
		<ul>
			<li>From your Dashboard, view which agents have created an account.</li>
			<li>Get metrics on downloaded listings, and number of matches presented for the agent’s review.</li>
			<li>View match details. Click on the listing and you’ll see the details of all matches that are available for that particular listing.</li>
		</ul>
	</li>
</ol>
<p>Have any questions?  We’re here to help!  Email us at <a href='mailto:info@xtradehomes.com'>info@xtradehomes.com</a>.</p>
<p>Sincerely,</p>
<p>xTradeHomes Customer Service</p>
<?=\View::forge('email/footer');?>