<?=\View::forge('email/header');?>
Your broker has discontinued their special pricing;<br>
Therefore as of 30 days after <?=$next_date;?>, you will be charged $<?=$price;?>.
<br>
<br>
To discontinue your subscription with xTradeHomes, log in and cancel your account from your profile.
<br>
<br>
Thank You
<?=\View::forge('email/footer');?>