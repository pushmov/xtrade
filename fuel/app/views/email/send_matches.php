<?=\View::forge('email/header');?>
<?=$rname?> at <?=$cname?> wanted to notify you of these xTrade matches:<br>
<br>
<br>
<?=$match_info?>
<br>
<br>
Contact <?=$rname?> at <?=$phone_number?> or email them at <?=$remail?> to discuss these potential new homes!
<br>
<br>
<?=\View::forge('email/footer');?>