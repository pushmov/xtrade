<?=\View::forge('email/header');?>
Hello <?=$data['agent_name']?><br>
<br>
Regarding Listing <?=\Html::anchor('realtors/listing/detail/'.$data['feed'] . '_'. $data['listing_id'], $data['listing_id']);?> 
at <b><?=$data['address']?></b>, <?=$data['user_contacted']['name']?> 

<?php if($data['user_contacted']['officename'] != '') : ?>
from <?=$data['user_contacted']['officename']?> 
<?php endif; ?>

has a question.<br><br>
<i><?=$data['message']?></i><br>
<br>
Call <?=$data['name']?> at <?=$data['phone']?> or email <?=\Html::mail_to($data['email'], $data['email']);?> to respond.<br>
<br>
Please contact them at your earliest convenience.
<br>
Good luck from<br>
xTradeHomes
<br>
<br>
<br />If you run into any problems using our service we'd be glad to help. Please email <?=\Html::mail_to('helpdesk@xtradehomes.com', 'helpdesk@xtradehomes.com');?>
to open a support ticket and a member of our customer support staff will respond as soon as possible.
<?=\View::forge('email/footer');?>