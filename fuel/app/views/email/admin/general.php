<?=\View::forge('email/header');?>
<p>Hello <?=$name;?></p>
<p><?=$message;?></p>
<p>Have any questions? We’re here to help! Email us at <a href='mailto:info@xtradehomes.com'>info@xtradehomes.com</a>.</p>
<p>Sincerely,</p>
<p>xTradeHomes Customer Service</p>
<?=\View::forge('email/footer');?>