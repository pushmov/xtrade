<table width="960" border="0" cellspacing="5px">
	<tr>
		<td>
			<span>xTrade Match:</span> &nbsp;&nbsp;&nbsp; 
			<span style="color: rgba(36,157,219,1);font-weight: 600;">5-way trade</span>
			<br>
			<br>
			<?php foreach($x_4_match as $match => $next_match_1): ?>
				<table width="960" border="0" cellspacing="5px" style="padding-top: 10px;">
					<tr>
						<td align="center" colspan="9">
							<img src="<?=\Config::get('uri').'assets/images/email/5_way_arrow_r.png'?>" alt="" />
						</td>
					</tr>
					<tr>
						<td align="center"><?=$this_listing_box?></td>
						<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
						<?php foreach($next_match_1 as $next_match_2 => $next_match_3): ?>
						<td align="center"><?=  \Match::forge()->setup_box_information_email($next_match_2, 'B', null, false, true);?></td>
						<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
						<?php foreach($next_match_3 as $next_match_4 => $next_match_5): ?>
							<td align="center"><?=  \Match::forge()->setup_box_information_email($next_match_4, 'C', 1, false, true); ?></td>
							<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
							<?php foreach($next_match_5 as $next_match_6 => $next_match_7): ?>
							<td align="center"><?=  \Match::forge()->setup_box_information_email($next_match_6, 'D', 1, false, true); ?></td>
							<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
							<td align="center"><?=  \Match::forge()->setup_box_information_email($next_match_7, 'E', 1, false, true); ?></td>
						  <?php endforeach; ?>
					  <?php endforeach; ?>
					<?php endforeach; ?>
					</tr>
				</table>
				<br>
			<?php endforeach; ?>
		</td>
	</tr>
</table>