<?=\View::forge('email/header');?>
Thank you for advertising with xTradeHomes, <?=$first_name.' '.$last_name?> at <?=$company_name?>!<br>
<br>
Account Number: <?=$vendor_id?><br>
We will review your application and once complete, your ad will appear on the site usually within 48 hours.<br>
<br>
The information you provided to us is below.<br>
<br>
Contact: <?=$first_name.' '.$last_name?><br>
Company Name: <?=$company_name?><br>
Address: <?=join(' ', array($address, $location_address, $z_p_code))?><br>
Email: <?=$email?><br>
<?php if($web_site != '') : ?>
Website: <?=\Html::anchor($web_site, $web_site);?><br>
<?php endif;?>

<?php if($featured == \Model\Vendor::VENDOR_STATUS_FEATURED) :?>
Featured : Yes<br>
<?php endif;?>

Areas you wish to advertise in:<br>
<ul>
	<li><?=str_replace(';', '</li><li>', $locale)?></li>
</ul>
<br>
Categories you are advertising in:
<ul>
	<li><?=str_replace(';', '</li><li>', $type)?></li>
</ul>
<br>
Listing Description: <?=$comments?>
<br>
<br>
Please click <?=\Html::anchor('user/activation?key='.$activate.'&type=vendor', 'here');?> to confirm everything is correct.

<br />If you run into any problems using our service we'd be glad to help. Please email <a href='mailto:helpdesk@xtradehomes.com'>helpdesk@xtradehomes.com</a> to open a support ticket and a member of our customer support staff will respond as soon as possible.
<?=\View::forge('email/footer');?>