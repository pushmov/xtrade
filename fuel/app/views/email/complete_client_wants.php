<?=\View::forge('email/header');?>
<p>Hello <?=$realtor['first_name'];?> <?=$realtor['last_name']?> from <?=$realtor['company_name']?>,</p>
<p>Your client <?=$client['first_name'].' '.$client['last_name']?> at <?=$client['address']?> (Listing <?=\Html::anchor('realtors/listing/edit/'.$client_has['listing_id'].'?pos=client_wants');?>), has filled out their 'wants' information!</p>
<p>Please review the information for accuracy by <?=\Html::anchor('realtors/listing/edit/'.$client_has['listing_id'].'?pos=client_wants', 'clicking here');?> at which point the listing will be added to our matching system.</p>
<p>Thank you.</p>
<?=\View::forge('email/footer');?>