<?=\View::forge('email/header');?>
<p>Hello <?=$broker;?>,</p>
<p><?=$agent;?> has requested to be added to your Broker account on xTradeHomes.</p>
<p>To approve the request and ensure your agent is able to download their listings into xTradeHomes, <?=\Html::anchor('/brokers', 'log in to your Broker account');?> and click &quot;Approve&quot;.</p>
<p>Sincerely,</p>
<p>xTradeHomes Customer Service<br>
<?=\Html::mail_to('info@xtradehomes.com');?><p>
<?=\View::forge('email/footer');?>