<?=\View::forge('email/header');?>
Hello <?=$fname?> <?=$lname?>,
<br>
A payment on your account has failed, We attempted to charge your credit card number on $date but were unable to receive confirmation.<br>
If you wish to discuss this issue please <a href="mailto:account@xtradehomes.com">email us</a> or call us at 1-844-7XTRADE (1-844-798-7233).
<br>
If we don't hear from you, we may need to suspend your membership.
<br>
Thank You
<?=\View::forge('email/footer');?>