<?=\View::forge('email/header');?>
<p>Hi <?=$first_name;?>!</p>
<p>Thank you for signing up for xTradeHomes.</p>
<p>Please click on the following link to verify your email address and complete the registration process.</p>
<p><?=\Html::anchor('user/activation?key='.$activate.'&type=realtor', 'Verify Email Address');?></p>
<p>Sincerely,</p>
<p>xTradeHomes Customer Service<br>
<?=\Html::mail_to('info@xtradehomes.com');?><p>
<?=\View::forge('email/footer');?>