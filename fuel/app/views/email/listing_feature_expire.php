<br>
The featured status for the following listing(s) on xTradeHomes are about to expire:
<br>
<?=$email_info?>
<br>
This is a friendly reminder that if you want to maintain your featured status, renew now!
<br>
If you will be renewing your listing; please login and do so from your Dashboard.
<br>
Thank you. 
<br>