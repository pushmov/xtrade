<?=\View::forge('email/header');?>
This Listing Was Upgraded: <?=$listing_id?>
<br>
The information they provided to us is below.
<br>
Listing Number: <?=$listing_id?><br>
<br>
Address: <?=$address?>
<br>
<br>
Their Receipt is <a href="<?=$receipt_url?>">here</a>.
<br>
<?=\View::forge('email/footer');?>