<?=\View::forge('email/header');?>
Thank you for advertising with xTradeHomes, <?=$first_name.' '.$last_name;?> at <?=stripslashes($company_name)?>!<br>
<br>
Account Number: <?=$vendor_id?><br>
<br>
Your advertisement has been upgraded!<br>
<br>
The information you provided to us is below.<br>
<br>
<br>
Please click <?=\Html::anchor('receipt/?id='.$receipt, 'here')?> for a copy of your receipt.
<br>
<br />
If you run into any problems using our service we'd be glad to help. Please email <?=\Html::mail_to('helpdesk@xtradehomes.com','helpdesk@xtradehomes.com');?> to open a support ticket and a member of our customer support staff will respond as soon as possible.
<?=\View::forge('email/footer');?>