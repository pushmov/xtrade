<?=\View::forge('email/header');?>
<p><?=$first_name.' '.$last_name?>, your listing has been upgraded!</p>
<p>Listing Number: <?=\Html::anchor($for_url, $listing_id);?></p>
<p>Address: <?=$address.' '.$cpc_address.' '.$z_p_code?></p>
<p>Please click <?=\Html::anchor('receipt/?id=' . $order_id, 'here')?> for a copy of your receipt.</p>
<p>If you run into any problems using our service we'd be glad to help. Please email <?=\Html::mail_to('helpdesk@xtradehomes.com', 'helpdesk@xtradehomes.com');?> to open a support ticket and a member of our customer support staff will respond as soon as possible.</p>
<?=\View::forge('email/footer');?>