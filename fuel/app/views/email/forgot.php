<?=\View::forge('email/header');?>
You must use the link provided within this email to login again.
<br />
<a href="<?=$url;?>">Click here to login and create a new password.</a>
<br />
Temporary password: <?=$pass;?>
<br />
If you cannot click on the link please copy and paste this link into your browsers address bar:
<br />
<?=$url;?>
<br /><br />
<br />If you have received this email and did not reset your password, please inform xTradeHomes by iRex at <a href="mailto:accounts@xtradehomes.com">this email</a>.<br>
<?=\View::forge('email/footer');?>