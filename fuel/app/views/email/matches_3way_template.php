<table width="555" border="0" cellspacing="5px">
	<tr>
		<td>
			<span>xTrade Match:</span> &nbsp;&nbsp;&nbsp; 
			<span style="color: rgba(36,157,219,1);font-weight: 600;">3-way trade</span>
			<br>
			<br>
			<?php foreach($x_2_match as $match => $next_match): ?>		
				<table width="555" border="0" cellspacing="5px" style="padding-top: 10px;">
					<tr>
						<td align="center" colspan="5">
							<img src="<?=\Config::get('uri').'assets/images/email/3_way_arrow_r.png'?>" alt="" />
						</td>
					</tr>
					<tr>
						<td align="center"><?=$this_listing_box?></td>
						<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
						<?php foreach($next_match as $match_1 => $match_2):?>
						<td align="center"><?=  \Match::forge()->setup_box_information_email($match_1, 'B', null, false, true)?></td>
						<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
						<td align="center"><?=  \Match::forge()->setup_box_information_email($match_2, 'C', 1, false, true)?></td>
						<?php endforeach; ?>
					</tr>
				</table>
				<br>
			<?php endforeach; ?>
		</td>
	</tr>
</table>