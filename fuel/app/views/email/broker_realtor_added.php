<?=\View::forge('email/header');?>
<p>Hello <?=$data['first_name']?>,</p>
<p>Your broker, <?=$data['company_name'];?>, has invited you to join xTradeHomes, a powerful patent pending matching tool designed exclusively for real estate agents.</p>
<p>With xTradeHomes you can help your listing clients find their new home, and a buyer for their current home, all at the same time.</p>
<p>Ready to give it a try?  It takes just minutes to set up your account, and there is no charge at this time to use it.</p>
<p style="center"><?=\Html::anchor('signup/realtor?' . $data['url'], 'Activate My Account Now!');?></p>
<p>To learn more about xTradeHomes, visit our FAQ.</p>
<p>Thanks, and we look forward to having you on board!</p>
<p>The xTradeHomes Team</p>
<?=\View::forge('email/footer');?>