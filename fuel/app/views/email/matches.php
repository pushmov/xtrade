<html>
	<head>
	  <link href="//fonts.googleapis.com/css?family=Exo:200,500,700|Open+Sans:400,700" rel="stylesheet" type="text/css">
	</head>
	<body style="background-color: #fff; font-family: \'Open Sans\', sans-serif; margin: 0 auto;">
	<div style="width: 800px; margin: 0 auto;">
	<br>
	<br>
	<?=$mailtoname?>, here is your latest match information!
	<br>
	<br>
	<?=$match_info?>
	<br>
	<br />If you want to change the frequency of how often you receive these emails, you can login and change the settings in your profile.
	<br>
	<br>
	<div align="center">
		<?php if(isset($unsubscribe_link)) : ?>
			<span style="color: grey;">
				xTradeHomes values your privacy.
				If you wish to stop receiving these emails, click <?=\Html::anchor(\Config::get('uri').$unsubscribe_link, 'here');?> to unsubscribe.
			</span>
			<?php endif; ?>
		<br>
		<br>
		TradeHomes by iRex, Inc - <span style="color: navy;">#370 - 425 Carrall Street,	Vancouver, BC V6B 6E3</span>
		<br>
		<?=isset($gst_num) ? $gst_num : '';?>
		<br>
	</div>
	</div>
</body>
</html>

