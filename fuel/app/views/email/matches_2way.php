<table width="360" border="0" cellspacing="5px">
	<tbody>
		<tr>
			<td>
				<span>xTrade Match:</span> &nbsp;&nbsp;&nbsp; 
				<span style="color: rgba(36,157,219,1);font-weight: 600;">2-way trade</span>
				<br>
				<br>
				<?php foreach($data as $match): ?>
					<table width="360" border="0" cellspacing="5px">
						<tbody>
							<tr>
								<td align="center" colspan="3">
									<img src="<?=\Config::get('uri').'assets/images/email/2_way_arrow_r.png'?>" alt="" />
								</td>
							</tr>
							<tr>
								<td align="center"><?=$this_listing_box?></td>
								<td align="center"><img src="<?=\Config::get('uri').'assets/images/email/shuffle.png'?>" alt="" /></td>
								<td align="center"><?=$match?></td>
							</tr>
						</tbody>
					</table>
					<br>
				<?php endforeach; ?>
			</td>
		</tr>
	</tbody>
</table>