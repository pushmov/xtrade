<?=\View::forge('email/header');?>
<?=$r_name;?> at <?=$company;?> has requested that you fill out your &quot;Wants&quot; form. Please click on the link below. Once you have completed that form,
<?=$r_name;?> will be automatically notified.
<br>
<?=\Html::anchor($form_link , 'Wants Form Link');?>
<br>
<br>
If you cannot click on the link please copy and paste this link into your browser's address bar:
<br>
<br>
<?=\Uri::create($form_link); ?>
<br>
<?=\View::forge('email/footer');?>