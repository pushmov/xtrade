<div class="clearfix filter members-dashboard">
	<div class="small-12 medium-8 column">
		<label for="view_select">View:</label>
		<?=\Form::select('members_view', $view, $filters['view'], array('id' => 'view_select'));?>
		<label for="view_order">Sort by:</label>
		<?=\Form::select('members_order', '', $filters['order'], array('id' => 'view_order'));?>
	</div>
	<div class="small-12 medium-4 column text-right">
		<div class="dashboard-search-input-wrapper">
			<i class="fa fa-search fa-lg"></i>
			<input name="search" id="search" type="text" value="" placeholder="Search by Keyword" maxlength='20' class="members-search-input" />
		</div>
	</div>
</div>
<div id="all_listings"></div>
<div id="page_dialog" class="reveal" data-reveal></div>
<div id="tutorial_dialog" class="reveal medium" data-reveal data-close-on-click="false">
	<div class="modal-header">Welcome To xTrade Homes!
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
	</div>
	<div class="modal-body text-center">
		<p><i class="fa fa-spinner fa-spin"></i>Processing...</p>
	</div>
</div>