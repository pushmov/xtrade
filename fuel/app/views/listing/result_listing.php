<div class="row">
	<div class="small-12 medium-12 large-6 column" id="pagination"><?=$paginate?></div>
	<div class="small-12 medium-12 large-6 column">
		<div id="results_listing" data-filter="<?=sizeof($filter) > 1 ? http_build_query($filter) : '';?>"></div>
		<div id="t_results" class="right">Total Results: <?=($records_num > 0) ? $records_num . ' Listings' :  $records_num ?></div>
	</div>
</div>
<?php if(!empty($result)) : ?>
	<?php foreach($result as $row) : ?>
		<div class="columns listing-result-details members-listing padded">
			<?php if($row['featured_listing'] == \Model\Listing::AS_FEATURED) : ?>
			<div  class="clearfix featured"></div>
			<?php endif; ?>
			<div class="clearfix">
				<div class="small-12 medium-12 large-3 column">
					<?=\Asset::img($row['image'], array('title' => $row['address'], 'alt' => $row['address']));?>
				</div>
				<div class="small-12 medium-12 large-3 column">
					<h3>$<?=number_format($row['listing_price']); ?></h3>
					<p class="address"><?=$row['full_address']?></p>
					<?=$row['btn_detail']?>
					<?=\Form::button('contact', 'Contact xTrade Agent', array('class' => 'button contact_agent', 'type' => 'button', 'data-id' => $row['feed'].'_'.$row['id']));?>
				</div>
				<div class="small-12 medium-12 large-6 column">
					<div class="row">
						<div class="small-12 medium-6 column">
							<?=$row['left']?>
						</div>
						<div class="small-12 medium-6 column">
							<?=$row['right']?>
						</div>
					</div>
				</div>
			</div>
			<?php if($row['broker'] != '') :?>
			<div class="padded-bottom">
			<?php if(strstr($row['listing_id'], '_', true) != 0) :?>
				<div>Listing Courtesy of
			<?php else: ?>
				<div class="small-5 callout success">Exclusively by
			<?php endif; ?>
				<?=$row['broker'];?>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<?php endforeach; ?>
	<div class="row">
		<div class="small-12 column end padded-top" id="pagination">
			<?=$paginate?>
		</div>
	</div>
	<?php else: ?>
	<div class="column">
		<p>No search results found</p>
	</div>
	<?php endif; ?>