<?php if(!empty($result)) : ?>
	<?=$pagination;?>
	<?php foreach($result as $row) : ?>
		<?=$row['data']['heading'];?>
		<?php if($row['imported'] == \Model\Listing::IMPORTED && $row['listing_status'] == \Model\Listing::LS_XCOMPLETE): ?>
			<div class="finish-listing">
				<?=\Html::anchor('realtors/listing/edit/'.$row['listing_id'].'?pos=client_info', 'Finish Listing', array('class' => 'button'));?>
			</div>
			<?php elseif($row['imported'] == \Model\Listing::IMPORTED && $row['listing_status'] == \Model\Listing::LS_ACTIVE): ?>
			<div class="date-added<?=($row['featured_listing'] == \Model\Listing::AS_FEATURED)?' date-added-featured' : ''?>"><span class="right">Date Added: <?=date_create($row['date_created'])->format(DATE_FORMAT);?></span></div>
			<?php endif; ?>

		<div class="small-12 medium-3 large-4 column padded">
			<div class="picture-box">
				<?=$row['picture'];?>

				<?php if($row['imported'] == \Model\Listing::NON_IMPORTED) : ?>
					<div class="exclusive text-right">
						<span title="This listing is exclusive to xTradeHomes and will not show up on any listing service.">Exclusive</span><br>
						<span ><?=$row['data']['expiry']?></span>
					</div>
					<?php endif; ?>
			</div>
		</div>

		<!-- details -->
		<div class="small-12 medium-5 large-4 column padded">
			<?php if($row['type_of_listing'] != \Model\Listing::TYPE_BUY_ONLY) : ?>
				<h3>$<?=number_format($row['listing_price'])?> <span class="members_type"><?=$row['data']['extra_stats']?></span></h3>
				<div class="address">
					<?php if($row['address'] != ''):?>
						<p><?=$row['address']?></p>
						<?php endif; ?>

					<?php if($row['data']['location'] != ''):?>
						<p><?=$row['data']['location']?></p>
						<?php endif; ?>
				</div>
				<?php else: ?>
				<h3>Buyer Only<?=$row['data']['buyer_add']?> <span class="members_type"><?=$row['data']['extra_stats']?></span></h3>
				<?php endif; ?>

			<ul class="unstyled option-list">
				<li><span>Client</span> : <strong><?=$row['data']['clients_name']?></strong></li>
				<li><span>Listing ID</span> : <strong><?=$row['data']['details_url']?></strong></li>
				<li><span>Public Status</span> : <strong><?=$row['data']['pstatus']?></strong></li>
				<?php if($row['type_of_listing'] == \Model\Listing::TYPE_XTRADED || $row['type_of_listing'] == \Model\Listing::TYPE_SELL_ONLY): ?>
					<li><span>xTrade Status</span> : <strong><?=$row['data']['xstatus']?></strong></li>
					<?php endif; ?>
				<li><span>Type</span> : <strong><?=$row['data']['type_of_listing']?> <?=\Form::button($row['imported'], 'Change', array('type' => 'button', 'id' => $row['listing_id'], 'class' => 'azin_button change_listing_type', 'style' => 'margin-left: 0px;'))?></strong></li>
			</ul>
		</div>
		<!-- end details -->

		<!-- actions part -->
		<div class="small-12 medium-4 large-4 column padded">
			<h3>Actions</h3>
			<?php if($row['listing_status'] != \Model\Listing::LS_PENDING_EX): ?>
				<ul class="unstyled actions">
					<?php if($row['type_of_listing'] == \Model\Listing::TYPE_XTRADED): ?>
						<li class="<?=$row['data']['has_incomp_class'];?>" title="<?=$row['data']['has_incomp'];?>">
							<?=\Html::anchor('realtors/listing/edit/'.$row['listing_id'].'?pos=client_has'.$row['data']['tut_url'], '<i class="fa fa-list fa-lg"></i>Client HAS', array('class' => 'buttons '));?>
						</li>
						<li class="<?=$row['data']['wants_incomp_class'];?>" title="<?=$row['data']['wants_incomp'];?>">
							<?=\Html::anchor('realtors/listing/edit/'.$row['listing_id'].'?pos=client_wants'.$row['data']['tut_url'], '<i class="fa fa-list fa-lg"></i>Client WANTS'.$row['data']['w_req'], array('class' => 'buttons edit_listing_wants'));?>
						</li>

						<?php if(!$row['imported']): ?>
							<li class="<?=$row['data']['image_incomp_class'];?>" title="<?=$row['data']['image_incomp'];?>">
								<?=\Html::anchor('realtors/listing/edit/'.$row['listing_id'].'?pos=image_info'.$row['data']['tut_url'], '<i class="fa fa-file-image-o fa-lg"></i>Listing Images', array('class' => 'buttons edit_listing_images '));?>
							</li>
							<?php endif; ?>

						<?php elseif($row['type_of_listing'] == \Model\Listing::TYPE_SELL_ONLY): ?>
						<li>
							<?=\Html::anchor('realtors/listing/edit/'.$row['listing_id'].'?pos=client_has'.$row['data']['tut_url'], '<i class="fa fa-list fa-lg"></i>Client HAS', array('class' => 'buttons edit_listing_has '.$row['data']['has_incomp_class'], 'title' => $row['data']['has_incomp']));?>
						</li>
						<?php if(!$row['imported']): ?>
							<li>
								<?=\Html::anchor('realtors/listing/edit/'.$row['listing_id'].'?pos=image_info'.$row['data']['tut_url'], '<i class="fa fa-file-image-o fa-lg"></i>Listing Images', array('class' => 'buttons edit_listing_images '.$row['data']['image_incomp_class'], 'title' => $row['data']['image_incomp']));?>
							</li>
							<?php endif; ?>
						<?php elseif($row['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY): ?>
						<li>
							<?=\Html::anchor('realtors/listing/edit/'.$row['listing_id'].'?pos=client_wants'.$row['data']['tut_url'], '<i class="fa fa-list fa-lg"></i>Client WANTS'.$row['data']['w_req'], array('class' => 'buttons edit_listing_wants '.$row['data']['wants_incomp_class'], 'title' => $row['data']['wants_incomp']));?>
						</li>
						<?php endif; ?>
				</ul>
				<?php endif; ?>

			<?php if($row['listing_status'] != \Model\Listing::LS_XCOMPLETE && $row['listing_status'] != \Model\Listing::LS_XCOMPLETE_IMPORT && $row['listing_status'] != \Model\Listing::LS_PENDING_EX): ?>
				<ul class="unstyled actions">
					<?php if($row['listing_status'] != \Model\Listing::LS_XTRADED): ?>
						<li <?=$row['data']['new_matches_class'];?>>

							<a href="/realtors/match/id/<?=$row['listing_id']?>" class="buttons btn-matches">
								<?php if($row['data']['total_matches'] == 1): ?>
									<i class="fa fa-eye fa-lg"></i>View <?=($row['data']['total_matches'])?> Match
									<?php elseif($row['data']['total_matches'] > 1): ?>
									<i class="fa fa-eye fa-lg"></i>View <?=($row['data']['total_matches'])?> Matches
									<?php else: ?>
									<i class="fa fa-eye fa-lg"></i>View Matches <strong>(No Matches Yet)</strong>
									<?php endif; ?>

								<!-- only display new matches notification when total matches less than max matches dbconfig -->
								<!-- show remaining item left (new matches total minus max-matches allowed) -->
								<?php if($row['data']['unseen_matches'] == 1): ?>
									<strong>(<span><?=$row['data']['unseen_matches']?></span> New Match!)</strong>
									<?php elseif($row['data']['unseen_matches'] > 1): ?>
									<strong>(<span><?=$row['data']['unseen_matches']?></span> New Matches!)</strong>
									<?php endif; ?>

							</a>
						</li>
						<?php endif; ?>
					<?php if($row['imported'] == \Model\Listing::NON_IMPORTED || $row['imported'] == \Model\Listing::IMPORTED): ?>
						<?php if($row['listing_status'] != \Model\Listing::LS_XTRADED && $row['featured_listing'] == \Model\Listing::AS_NON_FEATURED): ?>
							<li>
								<a href="#" id="<?=$row['listing_id']?>" class="no-link upgrade tooltip" title="This is a one time payment that features your listing on the front page of our site. The featured status will last for 90 days from payment confirmation or until the listing goes offmarket."><i class="fa fa-level-up fa-lg"></i>Feature Listing</a>
							</li>
							<?php endif; ?>

						<?php if($row['list_as_sold'] == '0'): ?>
							<li>
								<a href="#" id="<?=$row['listing_id']?>" class="no-link xtraded tooltip" title="If you have successfully completed a property trade with another member, tell us about it!"><i class="fa fa-random fa-lg"></i>Mark As xTraded</a>
							</li>
							<?php else: ?>
							<li>
								<a href="#" class="no-link not_xtraded" id="<?=$row['listing_id']?>"><i class="fa fa-random fa-lg"></i>Unmark As xTraded</a>
							</li>
							<?php endif; ?>
						<?php endif; ?>
					<?php if($row['listing_status'] != \Model\Listing::LS_XTRADED): ?>
						<?php if($row['listing_status'] == \Model\Listing::LS_ACTIVE): ?>
							<li>
								<a href="#" class="no-link deactivate tooltip" title="Remove this exclusive listing from displaying and matching." id="<?=$row['listing_id']?>"><i class="fa fa-minus-circle fa-lg"></i>Deactivate Listing</a>
								<?php else: ?>
								<a href="#" id="<?=$row['listing_id']?>" class="no-link activate"><i class="fa fa-check-circle fa-lg"></i>Activate Listing</a>
							</li>
							<?php endif; ?>
						<?php endif; ?>
				</ul>
				<?php endif; ?>
			<?php if($row['imported'] == \Model\Listing::NON_IMPORTED): ?>
				<ul class="unstyled actions">
					<li>
						<?=\Html::anchor('javascript:void(0)', '<i class="fa fa-trash-o fa-lg"></i>Delete Listing', array('id' => $row['listing_id'], 'title' => 'Completely remove this listing information from the xTradehomes database.', 'class' => 'delete tooltip'));?>
					</li>
				</ul>
				<?php endif; ?>
		</div>
		<!-- end actions part -->

		</div>

		<!--end wrapper-->
		<hr />
		<?php endforeach; ?>

	<?php echo $pagination; ?>
	<?=\Asset::js('all_listing_ajax.js')?>
	<?php else :?>
	<div class="content column">
		<p>No Record Found</p>
	</div>
	<?php endif;