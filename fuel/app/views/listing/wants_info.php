<?php if($show_request_button === TRUE):?>
	<li class="accordion-item is-active client-wants" id="client_wants">
		<?php if ($has['type_of_listing'] == \Model\Listing::TYPE_XTRADED || $has['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY):?>
			<a href="#panel_4" role="tab" class="accordion-title" id="panel4d-heading" aria-controls="panel4d">Client &quot;Wants&quot;
			<span><?=$listing_num;?></span>
			</a>
			<div id="panel_4" class="accordion-content" role="tabpane4" data-tab-content aria-labelledby="panel4d-heading">
		<?php else: ?>
			<a href="#panel_4" role="tab" class="accordion-title" id="panel4d-heading" aria-controls="panel4d">Client &quot;Wants&quot;<i class="fa fa-lock fa-lg"></i></a>
			<div id="panel_4" class="accordion-content accordion-closed" role="tabpane4" data-tab-content aria-labelledby="panel4d-heading"></div>
		<?php endif; else: ?>
			<h3>Client Form</h3>
		<?php endif; ?>


		<?php if ($has['type_of_listing'] == \Model\Listing::TYPE_XTRADED || $has['type_of_listing'] == \Model\Listing::TYPE_BUY_ONLY):?>
		<div class="row">
			<div class="large-12 columns text-right">
				<div class="matches-notification"><?=$matches_notification['matches']?></div>
			</div>
		</div>
		<div class="row">
			<div class="clearfix">
				<input name="wants[id]" id="id" type="hidden" value="<?=$wants['id'];?>">
				<input name="wants[listing_id]" id="id" type="hidden" value="<?=$wants['listing_id'];?>">
				<div class="small-12 medium-6 column">
					<?php if($show_request_button === TRUE):?>
						<button type="button" class="button" id="wants_request">Request client complete their Wants</button>
						<?php endif; ?>
				</div>
				<div class="small-12 medium-6 column text-right attention">
					<?php if ( $wants['completed_section'] == \Model\Client::STATUS_INCOMPLETE): ?>
						Please Complete All Required Fields <?=\Asset::img('attention.png', array('width' => 35, 'height' => 35, 'alt' => __('attention')));?>
						<?php else : ?>
						Information Up To Date <?=\Asset::img('up-to-date.png');?>
						<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns">
				<label for="wants_address" class="req-text"><?=__('cpsc');?><span class="astrict">*</span></label>
				<input name="wants[address]" type="text" class="input_field_ext req" id="wants_address" value="<?=$wants['address'];?>" placeholder="Start Typing City" data-field="address"/>
			</div>
			<div class="large-6 columns" id="neighbourhood_w_wrapper">
				<label for="wants[neighbourhood][]" class="req-text" id='neighbourhood_w_label'><?=__('wants_req') . " " . __('neigh'); ?><span class="astrict">*</span></label>
				<?=\Form::select('wants[neighbourhood][]', $wants['neighbourhood'], array(), array('class' => 'req', 'id' => 'neighbourhood_w', 'title' => __('neigh_title'), 'disabled' => 'disabled', 'data-field' => 'neighbourhood'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-9 columns">
				<label for="listing_price_low" class="req-text"><?=__('wants_req') . " " . __('price'); ?><span class="astrict">*</span></label>
				<?=__('search_between'); ?>
				<div class="row">
					<div class="columns small-5 listing-price-low-wrapper">
						<?=\Form::select('wants[listing_price_low]', $wants['listing_price_low'], \Model\Client\Want::forge()->listing_price_low(), array('id' => 'listing_price_low', 'class' => 'no_multi req dropdown_required', 'data-field' => 'listing_price_low' ));?>
					</div>
					<div class="columns small-1">
						<?=__('and'); ?>
					</div>
					<div class="columns small-6 listing-price-high-wrapper">
						<?=\Form::select('wants[listing_price_high]', $wants['listing_price_high'], \Model\Client\Want::forge()->listing_price_high(), array('id' => 'listing_price_high', 'class' => 'no_multi req dropdown_required', 'data-field' => 'listing_price_high'));?>
					</div>
				</div>
			</div>
			<div class="large-3 columns sq-footage-w-wrapper">
				<label for="sq_footage" class="req-text"><?=__('wants_req') . ' ' . __('sqfeet'); ?><span class="astrict">*</span></label>
				<?=\Form::select('wants[sq_footage]', $wants['sq_footage'], \Model\Client\Want::forge()->sq_footage(), array('id' => 'sq_footage', 'class' => 'no_multi req dropdown_required', 'data-field' => 'sq_footage'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns prop-type-w-wrapper">
				<label for="prop_type_w" class="req-text"><?=__('wants_req') . " " . __('property'); ?><span class="astrict">*</span></label>
				<?=\Form::select('wants[prop_type]', $wants['prop_type'], \Model\Client\Want::forge()->prop_type(), array('id' => 'prop_type_w', 'class' => 'no_multi req dropdown_required', 'data-field' => 'prop_type'));?>
			</div>
			<div class="large-4 columns bedrooms-w-wrapper">
				<label for="bedrooms_w" class="req-text"><?=__('wants_req') . " " . __('beds'); ?><span class="astrict">*</span></label>
				<?=\Form::select('wants[bedrooms]', $wants['bedrooms'], \Model\Client\Want::forge()->bedrooms(), array('id' => 'bedrooms_w','class' => 'no_multi req dropdown_required', 'data-field' => 'bedrooms'));?>
			</div>
			<div class="large-4 columns bathrooms-w-wrapper">
				<label for="bathrooms_w" class="req-text"><?=__('wants_req') . " " . __('baths'); ?><span class="astrict">*</span></label>
				<?=\Form::select('wants[bathrooms]', $wants['bathrooms'], \Model\Client\Want::forge()->bathrooms(), array('id' => 'bathrooms_w','class' => 'no_multi req dropdown_required', 'data-field' => 'bathrooms'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns p-title-w-wrapper">
				<label for="p_title_w" class="req-text"><?=__('wants_req') . " " . __('title'); ?><span class="astrict">*</span></label>
				<?=\Form::select('wants[p_title]', $wants['p_title'], \Model\Client\Want::forge()->p_title(), array('id' => 'p_title_w','class' => 'no_multi req dropdown_required', 'data-field' => 'p_title'));?>
			</div>
			<div class="large-4 columns prop-style-w-wrapper">
				<label for="prop_style_w" class="req_text req-text-done"><?=__('wants_req') . " " . __('storeys'); ?><span class="astrict">*</span></label>
				<?=\Form::select('wants[prop_style][]', $wants['prop_style'], \Model\Client\Want::forge()->prop_style(), array('id' => 'prop_style_w', 'class' => 'multi req','multiple' => 'multiple', 'data-field' => 'prop_style'));?>
			</div>
			<div class="large-4 columns total-lot-size-wrapper">
				<label for="total_lot_size_w"><?=__('lotsize'); ?></label>
				<?=\Form::select('wants[total_lot_size]', $wants['total_lot_size'], \Model\Client\Want::forge()->total_lot_size(), array('id' => 'total_lot_size_w','class' => 'no_multi', 'data-field' => 'total_lot_size'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns total-acreage-w-wrapper">
				<label for="total_acreage_w"><?=__('acreage'); ?></label>
				<?=\Form::select('wants[total_acreage]', $wants['total_acreage'], \Model\Client\Want::forge()->total_acreage(), array('id' => 'total_acreage_w','class' => 'no_multi', 'data-field' => 'total_acreage'));?>
			</div>
			<div class="large-4 columns siding-type-w-wrapper">
				<label for="siding_type_w"><?=__('siding_type'); ?></label>
				<?=\Form::select('wants[siding_type][]', $wants['siding_type'], \Model\Client\Want::forge()->siding_type(), array('id' => 'siding_type_w','class' => 'multi dropdown_required','multiple' => 'multiple', 'data-field' => 'siding_type'));?>
			</div>
			<div class="large-4 columns finished-basement-w-wrapper">
				<label for="finished_basement_w"><?=__('basement_type'); ?></label>
				<?=\Form::select('wants[finished_basement]', $wants['finished_basement'], \Model\Client\Want::forge()->finished_basement(), array('id' => 'finished_basement_w','class' => 'no_multi dropdown_required', 'data-field' => 'finished_basement'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns garage-type-w-wrapper">
				<label for="garage_type_w"><?=__('garage'); ?></label>
				<?=\Form::select('wants[garage_type]', $wants['garage_type'], \Model\Client\Want::forge()->garage_type(), array('id' => 'garage_type_w','class' => 'multi','multiple' => 'multiple', 'data-field' => 'garage_type'));?>
			</div>
			<div class="large-4 columns view-w-wrapper">
				<label for="view_w"><?=__('view'); ?></label>
				<?=\Form::select('wants[view]', $wants['view'], \Model\Client\Want::forge()->view(), array('id' => 'view_w','class' => 'no_multi dropdown_required', 'data-field' => 'view'));?>
			</div>
			<div class="large-4 columns heat-source-type-w-wrapper">
				<label for="heat_source_w"><?=__('heat_source'); ?></label>
				<?=\Form::select('wants[heat_source]', $wants['heat_source'], \Model\Client\Want::forge()->heat_source(), array('id' => 'heat_source_w','class' => 'no_multi dropdown_required', 'data-field' => 'heat_source'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns heat-type-w-wrapper">
				<label for="heating_w"><?=__('heat_type'); ?></label>
				<?=\Form::select('wants[heating]', $wants['heating'], \Model\Client\Want::forge()->heating(), array('id' => 'heating_w','class' => 'no_multi dropdown_required', 'data-field' => 'heating'));?>
			</div>
			<div class="large-4 columns cool-type-w-wrapper">
				<label for="cooling_w"><?=__('cool_type'); ?></label>
				<?=\Form::select('wants[cooling]', $wants['cooling'], \Model\Client\Want::forge()->cooling(), array('id' => 'cooling_w','class' => 'no_multi dropdown_required', 'data-field' => 'cooling'));?>
			</div>
			<div class="large-4 columns water-type-w-wrapper">
				<label for="water_w"><?=__('water_type')?></label>
				<?=\Form::select('wants[water]', $wants['water'], \Model\Client\Want::forge()->water(), array('id' => 'water_w', 'class' => 'dropdown_required', 'data-field' => 'water'));?>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns sewer-type-w-wrapper">
				<label for="sewer_w"><?=__('sewer_type'); ?></label>
				<?=\Form::select('wants[sewer]', $wants['sewer'], \Model\Client\Want::forge()->sewer(), array('id' => 'sewer_w','class' => 'no_multi dropdown_required', 'data-field' => 'sewer'));?>
			</div>
			<div class="large-4 columns floor-type-w-wrapper">
				<label for="floor_type_w"><?=__('floor_type'); ?></label>
				<?=\Form::select('wants[floor_type][]', $wants['floor_type'], \Model\Client\Want::forge()->floor_type(), array('id' => 'floor_type_w', 'class' => 'multi dropdown_required','multiple' => 'multiple', 'data-field' => 'floor_type'));?>
			</div>
			<div class="large-4 columns roof-type-w-wrapper">
				<label for="roof_type_w"><?=__('roof_type'); ?></label>
				<?=\Form::select('wants[roof_type][]', $wants['roof_type'], \Model\Client\Want::forge()->roof_type(), array('id' => 'roof_type_w','class' => 'multi dropdown_required','multiple' => 'multiple', 'data-field' => 'roof_type'));?>
			</div>
		</div>
		<?php endif; ?>

		<?php if($show_request_button === FALSE):?>
			<input type="hidden" name="wants[listing_id]" value="<?=$listing_id?>">
			<input type="hidden" name="wants[key]" value="<?=$key?>">
		<?php endif; ?>
	</div>
	<?php if($show_request_button === TRUE):?>
	</li>
	<?php endif; ?>
