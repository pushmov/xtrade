<label for="<?=$label_for?>" class="req-text-done" id="neighbourhood_h_label"><?=__('neigh')?>
	<span class="astrict">*</span>
	<?=$select;?>
</label>
<?php if($type == \Model\Neighbourhoods::NEIGHBOURHOOD_TYPE_WANTS) : ?>
<script>
	$(document).ready(function(){
		$('#neighbourhood_w').SumoSelect();
		$('#neighbourhood_w_wrapper').find('.options > li').on('click', function(){
			if($(this).hasClass('selected') && $(this).attr('data-val') === 'none'){
				$('#neighbourhood_w')[0].sumo.unSelectAll();
				$('#neighbourhood_w')[0].sumo.selectItem(0);
			} else {
				$('#neighbourhood_w')[0].sumo.unSelectItem(0);
			}
		});
	});
</script>
<?php elseif($type == \Model\Neighbourhoods::NEIGHBOURHOOD_TYPE_HAS):?>
<script>
	$(document).ready(function(){
		$('#neighbourhood_h').SumoSelect();
	});
</script>
<?php endif; ?>
