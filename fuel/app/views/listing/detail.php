<script src="//maps.googleapis.com/maps/api/js?v=3&key=<?=$map_key;?>" type="text/javascript"></script>
<script type="text/javascript" src="/assets/js/buttons.js"></script>

<div class="content column">
	<div class="row detail-top">
		<div class="small-12 medium-6 large-6 columns text-left">
			<h3>xTradeHomes Listing Details</h3>
		</div>
		<div class="small-12 medium-6 large-6 columns text-right">
			<div class="padded-top">
				<?=\Asset::img('dir_image.png', array('width' => 14, 'height' => 14, 'alt' => 'directory services'));?>
				<?=\Html::anchor('vendor/location/'.$location, 'Local Services Directory', array('style' => 'margin-left: 0px;'));?>
			</div>
		</div>
	</div>

	<div class="details-content padded-top padded-bottom">
		<?php if($listing_status == \Model\Listing::LS_SOLD_EX): ?>
			<div class="listing_sold"></div>
			<?php elseif($listing_status == \Model\Listing::LS_PENDING_EX): ?>
			<div class="listing_pending"></div>
			<?php endif; ?>

		<div class="fine-print">
		<?=$listing_num;?>
		</div>
		<div class="row">
			<div class="small-12 large-6 column">
				<ul id="light_slider" class="listing">
					<?=$photos['images_list'];?>
				</ul>
			</div>
			<div class="small-12 large-6 column listing-details">
				<div>
					<div class="price-nav details-price clearfix">
						<div class="nav-items"><?=$nav;?></div>
						<h2>$<?=number_format($listing_price, 0);?></h2>
					</div>
					<p class="clear address"><?=preg_replace("/,/", ", ", $address); ?></p>
					<em>Listing Information</em>
					<div class="row">
						<div class="small-12 medium-6 column">
							<?= $left_options; ?>
						</div>
						<div class="small-12 medium-6 column">
							<?= $right_options; ?>
						</div>
					</div>
					<div class="listing-details-center">
						<div class="row">
							<div class="listing-details-column column small-12 medium-6 view-map">
								<?=\Asset::img('map_image.png', array('width' => 25, 'height' => 25, 'alt' => 'View Map'));?>
								View Map
							</div>

							<div class="listing-details-column column small-12 medium-6">
								<div class="addthis_custom_sharing">
									<span class="st_email_large" displayText="Email"></span>
									<span class="st_facebook_large" displayText="Facebook"></span>
									<span class="st_twitter_large" displayText="Tweet"></span>
									<span class="st_linkedin_large" displayText="LinkedIn"></span>
									<span class="st_pinterest_large" displayText="Pinterest"></span>
									<span class="st_googleplus_large" st_url="<?=\Uri::current();?>" st_title="<?=preg_replace("/,/", ", ", $address); ?>" st_summary="<?=preg_replace("/,/", ", ", $address); ?>"></span>
								</div>
							</div>
						</div>
					</div>

					<?php if($agent['name'] != '') : ?>
						<em class="clear">Listing Agent</em>
						<div class="row">
							<div class="small-12 medium-6 column">
								<h5><?=$agent['name'];?></h5>
							</div>

							<div class="small-12 medium-6 column">
								<?php if($agent['phone'] != '' && $agent['phone'] != '0') : ?>
									<div class="small-12">Office: <?= \Sysconfig::format_phone_number($agent['phone']);?></div>
									<?php if($agent['mobile'] != '' && $agent['mobile'] != '0'):?>
										<div class="small-12">Cell: <?= \Sysconfig::format_phone_number($agent['mobile']);?></div>
										<?php endif;?>
									<?php if(isset($data['DetailViewUrl'])): ?>
										<div class="small-12"><a href="<?=$data['DetailViewUrl'];?>" target="_blank">Listing's Website</a></div>
										<?php endif; ?>
									<?php endif; ?>
							</div>

						</div>
						<?php endif; ?>


					<?php if($agent['broker'] != '') : ?>
						<h6 class="clear"><?=$agent['broker'];?></h6>
						<?php endif; ?>

					<div class="small-12">
						<?=\Form::button('button', 'Contact xTrade Agent', array('type' => 'button', 'class' => 'button contact_agent', 'data-id' => $param))?>
						<?php if($has_wants && $is_logged_in): ?>
							<?=\Form::button('button', 'See What This Client Wants', array('type' => 'button', 'class' => 'button client_wants'))?>
							<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php if(!empty($extra_options)) :?>
		<div class="row small-up-1 medium-up-2 large-up-3 extra-options">
		<?=$extra_options;?>
		</div>
		<?php endif;?>

		<?php if(!empty($features)) :?>
			<div class="row">
				<div class="column">
					<?php foreach($features as $title => $list): ?>
						<div class="small-12 medium-4 large-3 features_list">
							<div class="padded-top">
								<em><?=__($title);?></em>
								<?=$list?>
							</div>
						</div>
						<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>

		<div class="padded-bottom padded-top">
			<?php if($comments != '') : ?>
			<h6>Additional Comments</h6>
			<p><?=preg_replace("/\\|\\|/", "<br><br>", $comments);?></p>
			<?php endif; ?>
		</div>
	</div>
	<div class="details-added text-right padded-bottom">Date Added: <?=date_create($date_created)->format(DATE_FORMAT);?></div>
</div>
<div id="contact_agent_dialog" data-listing="<?=$listing_id?>" data-type="<?=$type_of_listing?>" class="reveal medium" data-reveal></div>
<div id="client_want_dialog" data-id="<?=$param?>" class="reveal medium" data-reveal></div>
<div id="map_dialog" data-lat="<?=$lat?>" data-lon="<?=$lng?>" class="reveal medium" data-reveal></div>
<?php if (isset($analyticsview)) echo $analyticsview; ?>
<script>
	$(document).ready(function() {
		$("#light_slider").lightSlider({
			gallery:true,
			item:1,
			loop:true,
			thumbItem:8,
			slideMargin:0,
			enableDrag: false,
		});
		stButtons.locateElements();
	});
</script>