<div class="content column padded-top">
	<?=\Form::open(array('id' => 'main_form', 'name' => 'main_form', 'method' => 'post', 'action' => '/listing/client_update.json'));?>
	<input name="Listing_ID" type="hidden" value="<?=$listing_id; ?>">
	<?=$wants_info; ?>
	<?=\Form::button('submit_content', 'Send Request', array('class' => 'button submit_content', 'id' => 'submit_content', 'type' => 'button', 'style' => 'display:none;'))?>
	<a href="/" class="button">Close</a>
	<?=\Form::close(); ?>
	<div id="edit_listing_dialog" class="reveal tiny" data-reveal data-close-on-click="false"></div>
</div>