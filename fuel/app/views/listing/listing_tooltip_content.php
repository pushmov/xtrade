<div class="column clearfix">
	<div class="col-left">
		<?=\Asset::img($data['picture'])?>
	</div>
	<div class="col-right">
		<h3><?=$data['location']?></h3>
		<ul class="unstyled">
			<li><?=$data['sqft']?></li>
			<li><?=$data['style']?></li>
			<li><?=$data['bedrooms']?> <?=$data['bathrooms']?></li>
		</ul>
	</div>
</div>