<ul class="unstyled" id="sub_menu">
	<li><a href="/brokers">Dashboard</a></li>
	<li><a id="add_realtor" class="new_realtor">Add REALTOR&reg;</a></li>
	<li><a href="/brokers/profile" id="profile">My Profile</a></li>
	<li class="float-right"><a href="/vendor/location/<?=$location?>" id="tutorial">Local Services Directory</a></li>
</ul>
<hr>
<script>
$(document).ready(function() {
	$('#add_realtor').on('click', function() {
		$.get(baseUrl + 'brokers/dashboard/add_realtor_dialog.html', function(html) {
			$('#page_dialog').html(html);
			$('#page_dialog').foundation('open');
		});
	});

	$('#page_dialog').on('click', '#btn_send_invite', function(){
		var form = $('#invite_form');
		var that = $(this);
		var currTxt = that.text();
		that.html('<i class="fa fa-spinner fa-spin"></i>Processing...');
		$('.input-error').remove();
		$.post(form.attr('action'), form.serialize(), function(json){
			if (json.status === 'OK') {
				$('#invite_form').after('<div class="callout success small">' + json.message + '</div>');
				that.html(currTxt);
				$('#btn_send_invite').remove();
				$('#btn_close').html('Close');
			} else {
				$.each(json.errors, function(k,v) {
					$('#' + k).after('<div class="input-error">' + v + '</div>');
				});
				that.html(currTxt);
			}
		});
	});
});
</script>

