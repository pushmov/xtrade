<h3>REALTOR&reg;'s Details</h3>
<?php if(empty($data)):?>
	Unable to retrieve details
	<?php else: ?>
	<dl class="row">
		<dt>xTradeHomes ID:</dt>
		<dd><?=$data['realtor_id']?></dd>
		<dt>REALTOR&reg; Name:</dt>
		<dd><?="{$data['first_name']} {$data['last_name']}"?></dd>
		<dt>xTrade Office ID:</dt>
		<dd><?=$data['office_id'];?></dd>
		<dt>Membership Renews On:</dt>
		<dd><?=$data['next_date'];?></dd>
	</dl>
	<button type="button" class="button btn-remove-realtor" id="rr_<?=$data['id'];?>">Remove REALTOR&reg;</button>
	<?php endif;?>
