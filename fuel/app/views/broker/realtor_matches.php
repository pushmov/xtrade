<h2><?=$data['name']?> Matches</h2>
<?php if(empty($data['listing'])):?>
	No Matches Found For This REALTOR&reg;.
	<?php else: ?>

        <?php foreach($data['listing'] as $row) : ?>
        <dl class="row">
            <dt>xTradeHomes ID:</dt>
            <dd>
                <?=\Html::anchor('brokers/realtor/match/'.$row['listing_id'].'?realtor_id='.$row['realtor_id'], $row['address']);?>
				<h5 class="matches"><strong><?=$row['num_total'].' Total matches, '.$row['num_ignore'].' Saved/Ignored';?></strong></h5>
                <br>
                <hr>
            </dd>
        </dl>
        <?php endforeach; ?>

	<?php endif;?>
