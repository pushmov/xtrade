<h2><?=$data['name']?> Listings</h2>
<?php if(empty($data['listing'])):?>
	No xTradeHomes Listings Found For This REALTOR&reg;.
<?php else: ?>
    <?php foreach($data['listing'] as $row) : ?>
    <dl class="row">
        <dt>These Listings Are Within The xTradeHomes Matching System.</dt>
        <dd>
            <?=\Html::anchor('listing/detail/0_'.$row['listing_id'], $row['address']);?>
            <br>
            <hr>
        </dd>
    </dl>
    <?php endforeach; ?>
<?php endif;?>