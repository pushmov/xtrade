<div class="row column">
	<div class="padded">
		<h2>Dashboard</h2>
		<?php if($requests != ''): ?>
			<div class="panel callout">
				<h3>Add REALTOR&reg;</h3>
				The following REALTOR&reg;’s have requested to be added to your Broker account.
				<ul><?=$requests;?></ul>
			</div>
			<?php endif;?>
	</div>
</div>
<div class="small-7 column realtor-list">
	<div class="padded">
		<h3>REALTORS&reg;</h3>
		<?php foreach($realtors as $row): ?>
			<dl class="row" id="<?=$row['id'];?>">
				<dt>REALTOR&reg; Name:</dt>
				<dd><?=\Html::anchor('#', $row['first_name'] . ' ' . $row['last_name'], array('class' => 'details'));?> </dd>
				<dt>Account Created On:</dt>
				<dd><?=$row['account_creation'];?></dd>
				<dt>Last Logged In:</dt>
				<dd><?=$row['last_login'];?></dd>
				<dt>Alert Status:</dt>
				<dd><?=$row['alerts'];?></dd>
				<dt>Number of Listings:</dt>
				<dd><button class="button small xlistings">Exclusive Listing<?=($row['xlistings'] > 0) ? ' ('.$row['xlistings'].')' : ''; ?></button>
				<?php if ($row['cn_id'] != ''):?>
				<button class="button small olistings">Downloaded Listing<?=$row['olistings'] > 0 ? ' ('.$row['olistings'].')' : ''?></button>
				<?php endif; ?>
				<button class="button small matches">Matching Listing<?=($row['matches'] > 0) ? ' ('.$row['matches'].')' : ''?></button></dd>
			</dl>
			<hr>
			<?php endforeach;?>
	</div>
</div>
<div class="small-5 column" id="details_div">
</div>
<div id="page_dialog" class="reveal" data-reveal></div>