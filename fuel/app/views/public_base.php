<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link href="/assets/css/auto-complete.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/slicknav.min.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/app.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" media="screen" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<?php echo \Asset::css($styles, array(), null, false); ?>
		<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="/assets/js/modernizr.js"></script>
		<script src="/assets/js/foundation.min.js" type="text/javascript"></script>
		<script src="/assets/js/jquery.slicknav.min.js" type="text/javascript"></script>
		<script src="/assets/js/auto-complete.min.js" type="text/javascript"></script>
		<script src="/assets/js/common.js" type="text/javascript"></script>
		<?php echo \Asset::js($scripts, array(), null, false); ?>
		<script>
			var baseUrl = '<?=Uri::base(false);?>';
			<?php if (isset($js_vars)) {foreach ($js_vars as $k => $v) echo "var $k = $v;", PHP_EOL;} ?>
			<?=$tracking_code;?>
		</script>
	</head>
	<body>
		<div id="ajax_loading"></div>
		<div id="header">
			<div class="row"><?=$header;?></div>
		</div>
		<div class="row" id="content">
			<?=$content;?>
		</div>
		<div id="footer">
		<div class="row">
			<div class="small-12 medium-2 column text-center"><a href="/">
				<img src="/assets/images/ft_logo.png" alt="Xtrade Homes"></a>
			</div>
			<div class='small-12 medium-8 column b-nav'>
				<a href="/about">About Us</a>
					<?php if(!isset($_SESSION['LOGGED_IN'])): ?>
						<button class='azin_button' id="broker_login">Brokers</button>
						<button class='azin_button' id="vendor_login">Advertisers</button>
						<?php endif; ?>
				<a href="/contact">Contact Us</a>
				<a href="/privacy">Privacy Policy</a>
				<a href="/terms">Terms &amp; Conditions</a>
			</div>
			<div class="small-12 medium-2 column text-center padded-top">
				<span title="The trademarks REALTOR&reg;, REALTORS&reg; and the REALTOR&reg; logo are controlled by The	Canadian Real Estate Association (CREA) and identify real estate professionals who are members of CREA" >
				REALTOR&reg;</span> <img src="/assets/images/ddf.jpg" title='The trademark DDF&reg; is owned by The Canadian Real Estate Association (CREA) and identifies CREA’s Data Distribution Facility (DDF&reg;)'>
			</div>
		</div>
		</div>
		<div id="login_dialog" class="reveal" data-reveal data-close-on-click="false">
		</div>
		<script>
			$(document).foundation();
		</script>
	</body>
</html>