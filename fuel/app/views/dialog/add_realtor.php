<div class="modal-header">Add REALTOR&reg;
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>Invite Your Agents to Join xTradeHomes.</p>
    <?=\Form::open(array('name' => 'invite_form', 'id' => 'invite_form', 'method' => 'post', 'action' => '/brokers/dashboard/addrealtor.json'));?>
		<div class="row">
			<div class="medium-12 column">
				<div class="row">
					<div class="small-12 medium-4 column">
						<label><?=__('first')?><span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input name="first_name" type="text" id="first_name" value="<?=$data['first_name']?>" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 column">
				<div class="row">
					<div class="small-12 medium-4 column">
						<label><?=__('last')?><span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input name="last_name" type="text" id="last_name" value="<?=$data['last_name']?>" />
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 column">
				<div class="row">
					<div class="small-12 medium-4 column">
						<label><?=__('email')?><span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input name="email" type="text" id="email" value="<?=$data['email']?>" />
					</div>
				</div>
			</div>
		</div>
		<button type="button" class="button" id="btn_send_invite">Send Invite</button>
		<button type="button" class="button dismiss" id="btn_close">Cancel</button>
        <p></p>
	<?=\Form::close();?>
</div>
