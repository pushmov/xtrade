<div class="modal-header">Delete Agent
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>You are about to delete the agent: <?="{$first_name} {$last_name}";?></p>
	<p>Are you sure? All xTrade information will be deleted.<br>This can not be undone.</p>
	<button type="button" class="button" id="btn_delete_yes">Yes</button>
	<button type="button" class="button" class="other_buttons" id="btn_close" name="btn_close" data-close>No</button>
	<input type="hidden" id="del_agent_id" value="<?=$id;?>"/>
</div>
