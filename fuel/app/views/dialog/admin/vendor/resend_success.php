<div class="modal-header"><span>Resend Activation Success!</span>
    <a class="close-reveal-modal"><img alt="Close" class="modal-close" src="/assets/images/close_image.png"></a>
</div>
<div class="modal-body text-center">
    <p>Activation link successfully sent</p>
    <button class="button close_button" type="button">Close</button>
</div>