<div class="modal-header">Delete User
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>This is PERMANENT!!, ONLY do this if you are 100% positive you want this account out of the system COMPLETELY. It is IRREVERSABLE! Are you sure?</p>
    <?=\Form::open(array('name' => 'delete_account', 'id' => 'delete_account', 'method' => 'post', 'action' => '/admins/broker/delbroker.json'));?>
        <input name="data[broker_id]" type="hidden" value="<?=$broker_id;?>">
        <?=\Form::button('submit', 'Submit', array('class' => 'button submit_delbroker', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
    <span class="er"></span>
</div>