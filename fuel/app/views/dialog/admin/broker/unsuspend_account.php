<div class="modal-header">Unsuspend Users Account
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>This will unsuspend the users account. Making it accessible to them again. Are you sure?</p>
    <?=\Form::open(array('name' => 'unsuspend_broker', 'id' => 'unsuspend_broker', 'method' => 'post', 'action' => '/admins/broker/unsuspend.json'));?>
        <input name="data[broker_id]" type="hidden" value="<?=$broker_id;?>">
        <?=\Form::button('submit', 'Submit', array('class' => 'button submit_unsuspend_broker', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
    <span class="er"></span>
</div>