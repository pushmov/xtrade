<div class="modal-header">Email Agent
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body">
	<form id="contact_agent_form" name="contact_agent_form" method="post" action="/admins/contact/send.json">
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-5 columns">
						<label>Name<span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-12 large-7 columns">
						<input type="text" class="input_field req" name="contact[name]" id="c_name" value="<?="{$data['first_name']} {$data['last_name']}";?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-5 columns">
						<label>Email Address<span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-12 large-7 columns">
						<input type="text" class="input_field req" name="contact[email]" id="c_email" value="<?=$data['email']?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-12 large-5 columns">
						<label>Message<span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-12 large-7 columns">
						<textarea name="contact[message]" rows="5" class="input_field req" id="c_message"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="text-center" id="submit_button">
			<button type="submit" class="button" id="submit" name="submit">Submit</button>
			<button type="button" class="button" id="btn_close" name="btn_close" data-close>Cancel</button>
		</div>
	</form>
</div>
