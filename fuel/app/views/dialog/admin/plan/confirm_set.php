<div class="modal-header">Confirm Set Plan
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>This will set the plan and all fields cannot be changed. Are you sure?</p>
	<button type="button" class="button" id="btn_set_submit">Set</button>
	<button type="button" class="button close_button">Cancel</button>
    <span class="er"></span>
</div>