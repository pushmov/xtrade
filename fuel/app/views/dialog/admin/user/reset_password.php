<div class="modal-header">Add To/Change Broker
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => '/admins/user/reset.json', 'method' => 'post', 'name' => 'reset_password'));?>
	<p>This will reset this users password, continue?</p>
    <input type="hidden" name="realtor_id" value="<?=$realtor_id?>">
    <?=\Form::button('reset', 'Yes', array('type' => 'button', 'id' => 'reset', 'class' => 'button submit_reset'));?>
    <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
    <?=\Form::close();?>
    <span class="er"></span>
</div>