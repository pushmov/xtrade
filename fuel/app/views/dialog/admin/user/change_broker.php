<div class="modal-header">Add To/Change Broker
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>Choose a new broker. If this REALTOR&reg; is already associated with a broker, it will be selected.</p>
    <?=\Form::open(array('name' => 'change_broker', 'id' => 'change_broker', 'method' => 'post', 'action' => '/admins/user/change.json'));?>
        <?=\Form::select('data[broker]', $office_id, \Model\Admin::forge()->select_broker());?>
        <input name="data[realtor_id]" type="hidden" value="<?=$realtor_id;?>">
        <?=\Form::button('submit', 'Submit', array('class' => 'button submit_change_broker', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
    <span class="er"></span>
</div>

