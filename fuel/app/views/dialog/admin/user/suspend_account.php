<div class="modal-header">Suspend Users Account
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>This will suspend the users account. Making it inaccessible to them. Are you sure?</p>
    <?=\Form::open(array('name' => 'change_broker', 'id' => 'change_broker', 'method' => 'post', 'action' => '/admins/user/suspend.json'));?>
        <input name="data[realtor_id]" type="hidden" value="<?=$realtor_id;?>">
        <?=\Form::button('submit', 'Submit', array('class' => 'button submit_suspend', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
    <span class="er"></span>
</div>