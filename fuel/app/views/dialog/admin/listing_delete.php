<div class="modal-header">Remove Listing Record
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>This will permanently delete this listing, it is irreversable. Are You Sure?</p>
    <?=\Form::open(array('name' => 'remove_listing', 'id' => 'remove_listing', 'method' => 'post', 'action' => '/admins/listing/delete.json'));?>
		<input type="hidden" name="data[listing_id]" value="<?=$id?>">
        <?=\Form::button('submit', 'Yes', array('class' => 'button submit-form', 'type' => 'button'));?>
        <?=\Form::button('close', 'No', array('type' => 'button', 'id' => 'reset', 'class' => 'button close_button'));?>
	<?=\Form::close();?>
</div>