<div class="modal-header">Ad Preview
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <div class="row small-12 ad-preview">
        <?php if($data['featured'] == \Model\Vendor::VENDOR_STATUS_FEATURED) : ?>
        <div class="directory-results-featured"></div>
        <?php endif; ?>
        <div class="small-12 medium-4 column vendor-logo">

        </div>
        <div class="small-12 medium-4 column text-left">
            <p>Service: <strong><ul><li><?=str_replace(';', '</li><li>', $data['type'])?></li></ul></strong></p>
            <?=\Html::anchor($data['web_site'], '<h3>'.$data['first_name'].' '.$data['last_name'].'</h3>');?>
            <p class="address"><?=$data['address']?></p>
            <p><?=$data['location_address']?></p>
            <em>Telephone:</em> <strong><?=\Sysconfig::format_phone_number($data['phone']);?></strong><br>
            <em>Email:</em> <strong><?=\Html::mail_to($data['public_email'], $data['public_email']);?></strong><br>
            <em>Website:</em> <strong><?=\Html::anchor($data['web_site'], $data['web_site']);?></strong><br>
        </div>
        <div class="small-12 medium-4 column"><?=$data['comments']?></div>
    </div>
    <hr>
</div>
