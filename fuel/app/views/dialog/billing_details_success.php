<div class="modal-header">Success
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>Thank You! Your Billing Details Have Been Changed</p>
	<button type="button" class="button continue">Close</button>
</div>
