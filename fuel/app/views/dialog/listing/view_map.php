<script>
	$(document).ready(function(){
		initialize();
	});
	function initialize() {
		var mapOptions = {
			center: { lat: <?=$lat?>, lng: <?=$lon?>},
			mapTypeId: google.maps.MapTypeId.HYBRID,
			zoom: 15
		};
		var map = new google.maps.Map(document.getElementById('map_content'), mapOptions);
		var image = new google.maps.MarkerImage('/assets/images/house.png', new google.maps.Size(129, 42), 
			new google.maps.Point(0,0),
			new google.maps.Point(18, 42)
		);

		var marker1 = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			map: map,
			icon: image // This path is the custom pin to be shown. Remove this line and the proceeding comma to use default pin
		});
	}
</script>

<div class="modal-header"><span>Listing Map</span>
    <a class="close-reveal-modal"><img alt="Close" class="modal-close" src="/assets/images/close_image.png"></a>
</div>
<div class="modal-body text-center">
    <div class="map-content" id="map_content"></div>
</div>