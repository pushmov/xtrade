<div class="modal-header"><span>Your Wants Updated!</span>
    <a class="close-reveal-modal"><img alt="Close" class="modal-close" src="/assets/images/close_image.png"></a>
</div>
<div class="modal-body text-center">
    <p>Your listing wants are complete! Thank you!</p>
    <button id="Home" class="button" onclick="window.location='/'">Dismiss</button>
</div>