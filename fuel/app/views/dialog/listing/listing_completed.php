<div class="modal-header"><span>Listing Complete!</span>
    <a class="close-reveal-modal"><img alt="Close" class="modal-close" src="/assets/images/close_image.png"></a>
</div>
<div class="modal-body text-center">
    Your listing details are complete
    <br><br>
    <button id="Home" class="button my_listing" onclick="window.location='/realtors'">Go Back To<br>'My Listings'</button>
    <br><br>
    <button id="No" class="button keep_edit">Keep Editing</button>
</div>