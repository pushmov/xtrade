<div class="modal-header"><span>Send Request</span>
    <a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<div class="row">
		<?=\Form::open(array('action' => '/listing/wantrequest.json', 'method' => 'post', 'name' => 'wantrequest'));?>
		<p>Please enter your client's email address.<br>They will receive a link to fill out their wants section.</p>
		<div class="columns small-3">
			<label>Email:</label>
		</div>
		<div class="columns small-9">
			<input type="text" placeholder="Email Address" value="<?=$data['email']?>" id="req_email" class="" name="email">
		</div>
		<input type="hidden" name="listing_id" value="<?=$data['listing_id']?>">
		<button type="button" class="button" id="submit_request">Send Request</button>
		<button type="button" class="button" id="close_button">Close</button>
		<?=\Form::close();?>
	</div>
</div>