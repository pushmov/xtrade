<div class="modal-header"><span>Upgrade Success!</span>
    <a class="close-reveal-modal"><img alt="Close" class="modal-close" src="/assets/images/close_image.png"></a>
</div>
<div class="modal-body text-center">
    <p><?=$txt?></p>
    <button type="button" class="button modal-close">Continue</button>
</div>