<div class="modal-header">Save Listing
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('method' => 'post', 'action' => 'realtors/match/save.json', 'name' => 'save_later'));?>
	<p>This will save this match for later.</p>
    <p>Are You Sure?</p>
    <button type="button" class="button later_submit" id="<?php echo $listing_id; ?>">Yes</button>
	<button type="button" class="button dismiss">No</button>
    <?=\Form::close();?>
</div>
