<div class="modal-header">Send Matches To Client
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/match/send.json', 'method' => 'post', 'name' => 'send'));?>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-3 large-3 columns">
						<label for="name">Client's Name:<span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-9 large-9 columns">
						<input type="text" class="input_field req" name="data[name]" id="name" value="<?= $data['first_name'] . ' '.$data['last_name']; ?>"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 large-12 columns">
				<div class="row">
					<div class="small-12 medium-3 large-3 columns">
						<label for="name">Client's Email:<span class="astrict">*</span></label>
					</div>
					<div class="small-12 medium-9 large-9 columns">
						<input type="text" class="input_field req" name="data[email]" id="email" value="<?= $data['email']; ?>"/>
					</div>
				</div>
			</div>
		</div>
        <button type="button" class="button send_submit">Submit</button>
		<button type="button" class="button dismiss">Close</button>
    <?=\Form::close();?>
</div>
