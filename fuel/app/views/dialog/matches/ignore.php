<div class="modal-header">Ignore Listing
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/match/ignore.json', 'method' => 'post', 'name' => 'ignore'));?>
	<p>This will remove this listing from your matches.</p>
    <p>Are You Sure?</p>
    <button type="button" class="button ignore_submit" id="<?=$listing_id;?>">Yes</button>
	<button type="button" class="button dismiss">No</button>
    <?=\Form::close();?>
</div>
