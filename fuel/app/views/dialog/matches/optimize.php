<div class="modal-header">Optimize Your Matches
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <p>Having done hundreds of xTrades&trade;, we know from experience, that most sellers' &quot;wants&quot; are not all carved in stone.</p>
    <p><em>When completing the "What My Client Wants" form, two things will become evident.</em></p>
    <p class="text-red"><em><strong>More information and requirements entered in the &quot;Wants&quot; section, will reduce the number of matches.</strong></em></p>
    <p class="text-green"><em><strong>Less information entered in the &quot;Wants&quot; section, will increase the number of matches.</strong></em></p>
    <p><em><strong>The number of possible matches can be adjusted by increasing or decreasing these search parameters.</strong></em></p>
    <p>Depending on various factors (length of time their property has been on the market, supply and demand) most sellers become more flexible to compromise if most of their criteria are met. You, the REALTOR&reg;, are in control and will guide them through this process to optimize your results and choices.</p>
   	<button type="button" class="button dismiss">Close</button>

</div>
