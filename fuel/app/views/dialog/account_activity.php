<div class="modal-header">Account Activity
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body">
	<p><strong>Account</strong> # : <?=$data['user_id']?></p>
	<table class="account-activity">
		<tbody>
			<tr>
				<td>Creation Date:</td>
				<td><?=$data['account_creation']?></td>
			</tr>
			<tr>
				<td>Last Logged In:</td>
				<td><?=date_create($data['login_log']['date'])->format(DATE_FORMAT);?></td>
			</tr>
			<tr>
				<td>Last IP Address:</td>
				<td><?=$data['login_log']['ip']?></td>
			</tr>
			<tr>
				<td>Last Payment Date:</td>
				<td><?=$data['last_payment_date']?></td>
			</tr>
			<tr>
				<td>Last Payment Amount:</td>
				<td><?=$data['last_payment_amt']?></td>
			</tr>
			<?php /*
			<tr>
				<td>Next Payment Date:</td>
				<td><?=$data['next_payment_date']?></td>
			</tr>
			<tr>
				<td>Next Payment Amount:</td>
				<td><?=$data['next_payment_amt']?></td>
			</tr>
			*/ ?>
		</tbody>
	</table>
</div>
