<div class="modal-header"><?=__('forgot');?>
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<div id="login_error"></div>
	<form action="" method="post" name="login_form" id="login_form">
		<input name="login_type" id="login_type" type="hidden" value="<?=$type;?>"><br>
		<input name="login_email" type="text" class="" id="login_email" value="<?=$saved_email; ?>" placeholder="Email Address"  /><br>
		<button type="button" class="button" id="login_sforgot" name="login_sforgot"><?=__('submit_email');?></button><br>
	</form>
	<a href="#" id="back_login" name="back_login"><?=__('back_login');?></a> <br>
	<a href="/signup"><?=__('signup');?></a>
</div>
