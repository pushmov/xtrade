<div class="modal-header">Refine Search
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body">
	<?=\Form::open(array('action' => 'listing/search/'.$location.'.html', 'method' => 'get', 'id' => 'refine_more_form'));?>
	<div class="row">
		<div class="small-12 medium-12 columns">
			<div class="row">
				<div class="small-12 medium-3 large-3 columns">
					<label for="right-label" class="right inline">Neighbourhood</label>
				</div>
				<div class="small-12 medium-9 large-9 columns neighbourhood-select">
					<?=\Form::select('filter[neighbourhood][]', isset($filter['neighbourhood']) ? $filter['neighbourhood'] : array(''), \Model\Neighbourhoods::forge()->get_neighbourhood($location, false), array('multiple' => 'multiple', 'id' => 'neighbourhood_select'));?>
				</div>
			</div>
		</div>
		<?php /* TODO: removed this option for now
		<div class="small-12 columns">
			<div class="row">
				<div class="small-12 medium-3 large-3 columns">
					<label for="right-label" class="right inline">Property Style</label>
				</div>
				<div class="small-12 medium-9 large-9 columns propstyle-select">
					<?=\Form::select('filter[prop_style][]', isset($filter['prop_style']) ? $filter['prop_style'] : array(''), \Model\Listing::forge()->filter_prop_style_checkbox(), array('multiple' => 'multiple', 'id' => 'dlg_prop_style'));?>
				</div>
			</div>
		</div>
		*/ ?>
		<div class="small-12 medium-12 columns">
			<div class="row">
				<div class="small-12 medium-3 large-3 columns">
					<label for="right-label" class="right inline">Price Between</label>
				</div>
				<div class="small-12 medium-9 large-9 columns">
					<?=\Form::select('filter[price_l]', isset($filter['price_l']) ? $filter['price_l'] : 1, \Model\Listing::forge()->filter_min_price(), array('style' => 'width:120px', 'class' => 'input_field inline', 'id' => 'price_l'));?>
					<label class="inline">and</label>
					<?=\Form::select('filter[price_h]', isset($filter['price_h']) ? $filter['price_h'] : 9000000000, \Model\Listing::forge()->filter_max_price(), array('style' => 'width: 120px;', 'class' => 'input_field inline', 'id' => 'price_h'));?>
				</div>
			</div>
		</div>

		<div class="small-12 medium-12 columns">
			<div class="row">
				<div class="small-12 medium-3 large-3 columns">
					<label for="right-label" class="right inline">Bedrooms</label>
				</div>
				<div class="small-12 medium-9 large-9 columns">
					<?=\Form::select('filter[bedrooms]', isset($filter['bedrooms']) ? $filter['bedrooms'] : null, \Model\Listing::forge()->filter_beds(), array('id' => 'refine_bedrooms'));?>
				</div>
			</div>
		</div>

		<div class="small-12 medium-12 columns">
			<div class="row">
				<div class="small-12 medium-3 large-3 columns">
					<label for="right-label" class="right inline">Bathrooms</label>
				</div>
				<div class="small-12 medium-9 large-9 columns">
					<?=\Form::select('filter[bathrooms]', isset($filter['bathrooms']) ? $filter['bathrooms'] : null, \Model\Listing::forge()->filter_baths(), array('id' => 'refine_bathrooms'));?>
				</div>
			</div>
		</div>

		<div class="small-12 medium-12 columns">
			<div class="row">
				<div class="small-12 medium-3 large-3 columns">
					<label for="right-label" class="right inline">Property Type</label>
				</div>
				<div class="small-12 medium-9 large-9 columns">
					<?=\Form::select('filter[prop_type]', isset($filter['prop_type']) ? $filter['prop_type'] : null, \Model\Listing::forge()->filter_prop_type_checkbox(), array('id' => 'refine_prop_type'));?>
				</div>
			</div>
		</div>


	</div>

	<div class="small-12 columns text-center">
		<button id="refine_more_submit" type="button" class="button">Submit</button>
		<button id="refine_more_reset" type="button" class="button">Reset</button>
		<button id="refine_more_close" type="button" class="button">Close</button>
	</div>
	<?=\Form::close();?>
</div>
<script>
	$(document).ready(function(){
		$('#neighbourhood_select').SumoSelect();
		$('.neighbourhood-select .SumoSelect .options > li').on('click', function(){
			if($(this).hasClass('selected') && $(this).attr('data-val') === ''){
				$('#neighbourhood_select')[0].sumo.unSelectAll();
				$('#neighbourhood_select')[0].sumo.selectItem(0);
			} else {
				$('#neighbourhood_select')[0].sumo.unSelectItem(0);
			}
		});

		$('#dlg_prop_style').SumoSelect();
		$('.propstyle-select .SumoSelect .options > li').on('click', function(){
			if($(this).hasClass('selected') && $(this).attr('data-val') === ''){
				$('#dlg_prop_style')[0].sumo.unSelectAll();
				$('#dlg_prop_style')[0].sumo.selectItem(0);
			} else {
				$('#dlg_prop_style')[0].sumo.unSelectItem(0);
			}
		});

	});
</script>