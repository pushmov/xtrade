<div class="modal-header">Renew Listing
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/listing/renew.json', 'method' => 'post', 'name' => 'renew'));?>
	<p>This will Reenable this listing, making it visible to the general public.</p>
	<p class="double-check">Are You Sure?</p>
	<button type="button" class="button btn-renew" data-id="<?=$listing_id?>">Yes</button>
	<button type="button" class="button" id="btn_close" name="btn_close">No</button>
    <?=\Form::close();?>
</div>