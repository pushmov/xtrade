<div class="modal-header">Activate Listing
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/listing/activate.json', 'method' => 'post', 'name' => 'activate'));?>
	<p>This will relist your listing, making it an xTradable listing again.</p>
	<p class="double-check">Are You Sure?</p>
	<button type="button" class="button btn-activate" data-id="<?=$listing_id?>">Yes</button>
	<button type="button" class="button" id="btn_close" name="btn_close">No</button>
    <?=\Form::close();?>
</div>
