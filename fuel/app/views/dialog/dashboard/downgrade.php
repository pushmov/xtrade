<div class="modal-header">Downgrade Listing
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/listing/downgrade.json', 'name' => 'downgrade', 'method' => 'post'));?>
	<p>This will stop your listing from being featured on xTradeHomes, it is irreversable.</p>
	<p class="double-check">Are You Sure?</p>
	<button type="button" class="button btn-downgrade" data-id="<?=$listing_id?>">Yes</button>
	<button type="button" class="button" id="btn_close" name="btn_close">No</button>
    <?=\Form::close();?>
</div>
