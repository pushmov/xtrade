<div class="modal-header">xTrade Status
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/listing/xtraded.json', 'name' => 'mark_as_xtrade', 'method' => 'post'));?>
	<p>This will mark your listing as xTraded and take it out of the xTrade matching system.</p>
	<p class="double-check">Are You Sure?</p>
	<button type="button" class="button mark-as-xtraded" data-id="<?=$listing_id?>">Yes</button>
	<button type="button" class="button" id="btn_close" name="btn_close">No</button>
    <?=\Form::close();?>
</div>