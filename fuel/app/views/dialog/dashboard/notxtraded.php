<div class="modal-header">xTrade Status
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
    <?=\Form::open(array('action' => 'realtors/listing/notxtraded.json', 'method' => 'post', 'name' => 'notxtraded'));?>
	<p>This will unmark your listing as xTraded and put it back in the xTrade matching system.</p>
	<p class="double-check">Are You Sure?</p>
	<button type="button" class="button mark-notxtraded" data-id="<?=$listing_id?>">Yes</button>
	<button type="button" class="button" id="btn_close" name="btn_close">No</button>
    <?=\Form::close();?>
</div>
