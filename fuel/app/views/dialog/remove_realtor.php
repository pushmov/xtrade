<div class="modal-header">Remove REALTOR&reg;'s Account
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>This will remove the REALTOR&reg; from your under your Brokers account.<br>Are You Sure?</p>
	<button type="button" class="button" id="btn_remove_realtor_yes" name="<?=$id;?>">yes</button>
	<button type="button" class="button" id="btn_close">No! Go Back!</button><br>
</div>
