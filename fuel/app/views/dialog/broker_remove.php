<div class="modal-header">Change Broker
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>You are currently with: <?=$broker;?></p>
	<p>Are you sure you want to leave this broker account? You will have to request to be added to their account again, once this is done.</p>
	<button type="button" class="button" id="btn_remove_broker_yes">Yes</button>
	<button type="button" class="button" class="other_buttons" id="btn_close" name="btn_close">No</button>
</div>
