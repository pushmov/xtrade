<div class="modal-header"><?=$title;?>
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<div id="login_error"></div>
	<form action="" method="post" name="login_form" id="login_form">
		<input name="login_type" id="login_type" type="hidden" value="<?=$type;?>"><br>
		<input name="login_email" type="email" class="" id="login_email" value="<?=$email; ?>" placeholder="Email Address"  /><br>
		<input name="login_pass" type="password" class="" id="login_pass" value="" placeholder="Password" /><br>
		<button type="button" class="button expanded" id="login_sbutton" name="login_sbutton"><?=__('log_in');?></button><br>
		<div><?=__('remember');?> <?=\Form::checkbox('remember_me', 1, $remember, array('title' => 'This will keep you logged in for 30 days'));?></div>
	</form>
	
	<?php if($type != \Model\User::forge()->get_types(\Model\User::UT_ADMIN)) :?>
	<a href="#" id="forgot_signin" name="forgot_signin"><?=__('forgot');?></a> <br>
	<a href="/signup/<?=$type;?>"><?=__('signup');?></a>
	<?php endif; ?>
</div>
