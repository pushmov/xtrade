<div class="modal-header">Forms Download
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<ul class="unstyled">
		<li><a href="/assets/pdf/xtrade_form.pdf" target="_blank">xTrade Form</a></li>
		<li><a href="/assets/pdf/seller_form.pdf" target="_blank">Seller Form</a><li>
		<li><a href="/assets/pdf/buyer_form.pdf" target="_blank">Buyer Form</a><li>
	</ul>
	<div class="padded-vertical"><button type="button" class="button modal-close" id="btn_close">Go Back</button></div>
</div>