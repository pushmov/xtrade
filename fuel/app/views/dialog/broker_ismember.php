<div class="modal-header">Broker Request
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>Is the broker a member of xTradeHomes?</p>
	<button type="button" class="button" id="btn_broker_member_yes">Yes</button>
	<button type="button" class="button" id="btn_broker_member_no">No</button>
</div>
