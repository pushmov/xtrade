<div class="modal-header">Welcome to xTradeHomes
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p>Welcome to xTradeHomes!<br>
	Please review all the information on this page then click My Listings to complete your Client's Wants, or update your imported listings.</p>
	<button type="button" class="button" id="btn_close" name="btn_close">Review Account</button>
</div>
