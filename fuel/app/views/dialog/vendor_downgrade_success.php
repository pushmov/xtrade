<div class="modal-header"><?=__('success')?>
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body text-center">
	<p><?=$txt?></p>
	<button type="button" class="button continue">Continue</button>
</div>