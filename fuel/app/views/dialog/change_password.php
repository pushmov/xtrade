<div class="modal-header">Change Password
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body">
	<form action="" method="post" name="change_form" id="change_form">
		<input name="login_type" id="login_type" type="hidden" value="<?=$type;?>">
		<input name="id" id="id" type="hidden" value="<?=$id;?>">
		<label for="password">Password <span class="astrict">*</span></label>
		<input name="password" type="text" id="password" value="" /><br>
		<label for="cpassword">Re-enter Password <span class="astrict">*</span></label>
		<input name="cpassword" type="text" id="cpassword" value="" /><br>
		<div class="text-center">
			<button type="button" class="button" id="btn_change_pass">Submit</button> <button type="button" class="button" id="btn_close">Cancel</button>
		</div>
	</form>
</div>
