<div class="modal-header">xTradeHomes Tutorial</div>
<div class="modal-body text-center">
	<p>The images associated with your properties are handled by your multiple listing service. If you wish to update photos, you must do so through the service you use.</p>
	<button class="button image-info-next" type="button">Next</button>
	<button class="button cancel" type="button">Cancel Tutorial</button>
</div>
