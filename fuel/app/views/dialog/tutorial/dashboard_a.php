<div class="modal-header">Welcome To xTrade Homes!</div>
<div class="modal-body text-center">
    <p>This tutorial will take you through completing your existing properties so that they can be matched up to other properties on our site.</p>
    <p>Lets get started xTrading.</p>
    <button class="button next" type="button" data-val="b">Lets Go!</button>
    <button class="button cancel" type="button">Cancel Tutorial</button>
</div>
