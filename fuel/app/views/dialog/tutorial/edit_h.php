<div class="modal-header">xTradeHomes Tutorial</div>
<div class="modal-body text-center">
	<p>In this section we will complete what your client &quot;Wants&quot;. The more information you provide here, the greater the potential for matches!</p>
	<button class="button client-wants-fill-required" type="button">Next</button>
	<button class="button cancel" type="button">Cancel Tutorial</button>
</div>
