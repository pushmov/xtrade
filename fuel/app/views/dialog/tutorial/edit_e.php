<div class="modal-header">xTradeHomes Tutorial</div>
<div class="modal-body text-center">
	<p>It's best to confirm that the information xTradeHomes has gathered is accurate. Also, we need to ensure that required fields <span class="astrict">(those with red asterisk headings)</span> are complete before we can move forward.</p>
	<button class="button client-has-fill-required" type="button">Next</button>
	<button class="button cancel" type="button">Cancel Tutorial</button>
</div>