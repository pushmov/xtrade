<div class="modal-header">xTradeHomes Tutorial</div>
<div class="modal-body text-center">
	<p>Only the fields in red are required, however, the more information you are able to fill in, the better the matching potential! Once you are satisfied with the &quot;Has&quot; area, click on &quot;Save Now&quot; to proceed to the next area.</p>
	<button class="button client-has-save-now" data-next="g" type="button">Next</button>
	<button class="button cancel" type="button">Cancel Tutorial</button>
</div>