<div class="modal-header"><?=__('billing_details');?>
	<a class="close-reveal-modal"><img src="/assets/images/close_image.png" class="modal-close" alt="Close" data-close /></a>
</div>
<div class="modal-body">
	<h5>Billing Information on File</h5>
	<form id="cc_form" name="cc_form" method="post" action="" >
		<div class="row">
			<div class="small-12 medium-6 column">
				<label for='first_name'><?=__('first');?><span class="astrict">*</span></label><input name="data[first_name]" id="first_name" value="<?=$data['first_name'];?>" type="text" />
				<label for='last_name'><?=__('last');?><span class="astrict">*</span></label><input name="data[last_name]" id="last_name" value="<?=$data['last_name'];?>" type="text" />
				<label for='address'><?=__('billing_address');?><span class="astrict">*</span></label><input name="data[address]" type="text" id="address" value="<?=$data['address'];?>" />
				<label for='cpc_address'><?=__('city_state_country');?><span class="astrict">*</span></label><input name="data[cpc_address]" type="text" id="cpc_address" value="<?=$data['cpc_address'];?>" placeholder="Start Typing City" />
				<label for='z_p_code'><?=__('postal');?><span class="astrict">*</span></label><input name="data[z_p_code]" id="z_p_code" value="<?=$data['z_p_code'];?>" type="text" />
			</div>
			<div class="small-12 medium-6 column">
				<label for='cc_type'><?=__('cc_type');?><span class="astrict">*</span></label><?=\Form::select('data[cc_type]', $data['cc_type'], $cboCardTypes);?>
				<label for='cc_number'><?=__('cc_number');?><span class="astrict">*</span></label>
				<input name="data[cc_number]" id="cc_number" value="<?=$data['cc_number']?>" type="text" />
				<div class="row">
					<div class="small-12 medium-4 column">
						<label for='cc_expiry'><?=__('cc_expiry_month');?><span class="astrict">*</span></label><?=\Form::select('data[exp_month]', $data['exp_month'], $cboMonths); ?>
					</div>
					<div class="small-12 medium-4 column">
						<label for='cc_expiry'><?=__('cc_expiry_year');?><span class="astrict">*</span></label><?=\Form::select('data[exp_year]', $data['exp_year'], $cboYears, array('id' => 'exp_year', 'required' => 'required'));?>
					</div>
					<div class="small-12 medium-4 column">
						<label for='cc_cvv'><?=__('cc_ccv');?><span class="astrict">*</span></label><input name="data[cc_cvv]" id="cc_cvv" value="" size="6" type="text" />
					</div>
				</div>
				<input type="hidden" name="data[user_xid]" value="<?=$data['user_xid']?>">
				<input type="hidden" name="data[user_id]" value="<?=$data['user_id']?>">
				<div class="row">
					<div class="billing-update-status"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="text-center padded-top">
				<button type="button" class="button" name="btn_update" id="btn_update"><?=$btnTxt?></button> <button type="button" class="button" id="btn_close" data-close>Cancel</button>
			</div>
		</div>
	</form>
</div>
<script>
	$(document).ready(function() {
		new autoComplete({
			selector: '#cpc_address',
			minChars: 3,
			source: function(term, response){
				$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
			}
		});
	});
</script>