<!DOCTYPE html>
<html>
	<head>
		
	</head>
	<body>
		<div style="text-align: center; width: 100%">
			<?php if(!empty($data['buyers_match'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td align="center">
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">Direct Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>Buyers Match</b></span>
							</div>
							<?php foreach($data['buyers_match'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle"><img src="/assets/images/email/shuffle.png" alt="" /></td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<!-- x section -->
			<?php if(!empty($data['x_1_match_high'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td align="center">
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">xTrade Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['x_1_match_high'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle"><img src="/assets/images/email/shuffle.png" alt="" /></td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<?php if(!empty($data['x_1_match_med'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tbody>
					<tr>
						<td>
							<div style="margin-bottom: 15px">
								<span>xTrade Match:</span>
								<span style="color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['x_1_match_med'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center" valign="top" width="140"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png"/>
											</td>
											<td align="center" valign="top" width="140"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<?php if(!empty($data['x_1_match_low'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">xTrade Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['x_1_match_low'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png" alt="" />
											</td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<!-- y section -->
			<?php if(!empty($data['y_1_match_high'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td align="center">
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">xTrade Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['y_1_match_high'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png" alt="" />
											</td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<?php if(!empty($data['y_1_match_med'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">xTrade Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['y_1_match_med'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png" alt="" />
											</td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<?php if(!empty($data['y_1_match_low'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">xTrade Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['y_1_match_low'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png" alt="" />
											</td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<!-- z section -->
			<?php if(!empty($data['z_1_match_high'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td align="center">
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">xTrade Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['z_1_match_high'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png" alt="" />
											</td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<?php if(!empty($data['z_1_match_med'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">xTrade Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['z_1_match_med'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png" alt="" />
											</td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<?php if(!empty($data['z_1_match_low'])) : ?>
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<div style="margin-bottom: 15px">
								<span style="font-size:11px;">xTrade Match:</span>
								<span style="font-size:11px;color: #249DDB;"><b>2-way trade</b></span>
							</div>
							<?php foreach($data['z_1_match_low'] as $match): ?>
								<table width="360" border="0" cellspacing="5px" align="center">
									<tbody>
										<tr>
											<td align="center" colspan="3">
												<img src="/assets/images/email/2_way_arrow_r.png" alt="" />
											</td>
										</tr>
										<tr>
											<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png" alt="" />
											</td>
											<td align="center"><?=$match?></td>
										</tr>
									</tbody>
								</table>
								<br>
							<?php endforeach; ?>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>

			<!-- 3-way -->
			<?php if(!empty($data['x_2_match'])):?>
			<table width="555" border="0" cellspacing="5px">
				<tr>
					<td>
						<div style="margin-bottom: 15px">
							<span style="font-size:11px;">xTrade Match:</span>
							<span style="font-size:11px;color: #249ddb;font-weight: 600;">3-way trade</span>
						</div>
						<?php foreach($data['x_2_match'] as $match => $next_match): ?>		
							<table width="555" border="0" cellspacing="5px">
								<tr>
									<td align="center" colspan="5">
										<img src="/assets/images/email/3_way_arrow_r.png" alt="" />
									</td>
								</tr>
								<tr>
									<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
									<td align="center" valign="middle">
										<img src="/assets/images/email/shuffle.png" alt="" />
									</td>
									<?php foreach($next_match as $match_1 => $match_2):?>
									<td align="center"><?=$data['m']->setup_box_information($match_1, 'B', null, false, true)?></td>
									<td align="center" valign="middle">
										<img src="/assets/images/email/shuffle.png" alt="" />
									</td>
									<td align="center"><?=$data['m']->setup_box_information($match_2, 'C', 1, false, true)?></td>
									<?php endforeach; ?>
								</tr>
							</table>
							<br>
						<?php endforeach; ?>
					</td>
				</tr>
			</table>
			<?php endif; ?>

			<!-- 4-way -->
			<?php if(!empty($data['x_3_match'])):?>
			<table width="756" border="0" cellspacing="5px">
				<tr>
					<td>
						<div style="margin-bottom: 15px">
							<span style="font-size:11px;">xTrade Match:</span>
							<span style="font-size:11px;color: #249ddb;font-weight: 600;">4-way trade</span>
						</div>
						<?php foreach($data['x_3_match'] as $match => $next_match_1): ?>
							<table width="756" border="0" cellspacing="5px">
								<tr>
									<td align="center" colspan="7">
										<img src="/assets/images/email/4_way_arrow_r.png" alt="" />
									</td>
								</tr>
								<tr>
									<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
									<td align="center" valign="middle">
										<img src="/assets/images/email/shuffle.png" alt="" />
									</td>
									<?php foreach($next_match_1 as $match_1 => $next_match_2): ?>
									<td align="center"><?=$data['m']->setup_box_information($match_1, 'B', null, false, true)?></td>
										<td align="center" valign="middle">
											<img src="/assets/images/email/shuffle.png" alt="" />
										</td>
										<?php foreach($next_match_2 as $next_match_3 => $next_match_4): ?>
											<td align="center"><?=$data['m']->setup_box_information($next_match_3, 'C', 1, false, true); ?></td>
											<td align="center" valign="middle">
												<img src="/assets/images/email/shuffle.png" alt="" />
											</td>
											<td align="center"><?=$data['m']->setup_box_information($next_match_4, 'D', 1, false, true); ?></td>
										<?php endforeach; ?>
									<?php endforeach; ?>
								</tr>
							</table>
							<br>
						<?php endforeach; ?>
					</td>
				</tr>
			</table>
			<?php endif; ?>


			<!-- 5-way -->
			<?php if(!empty($data['x_4_match'])):?>
			<table width="960" border="0" cellspacing="5px">
				<tr>
					<td>
						<div style="margin-bottom: 15px">
							<span style="font-size:11px;">xTrade Match:</span>
							<span style="font-size:11px;color: #249ddb;font-weight: 600;">5-way trade</span>
						</div>
						<?php foreach($data['x_4_match'] as $match => $next_match_1): ?>
							<table width="960" border="0" cellspacing="5px">
								<tr>
									<td align="center" colspan="9">
										<img src="/assets/images/email/5_way_arrow_r.png" alt="" />
									</td>
								</tr>
								<tr>
									<td align="center"><?=str_replace(\Config::get('uri'), '/', $this_listing_box);?></td>
									<td align="center" valign="middle">
										<img src="/assets/images/email/shuffle.png" alt="" />
									</td>
									<?php foreach($next_match_1 as $next_match_2 => $next_match_3): ?>
									<td align="center"><?=$data['m']->setup_box_information($next_match_2, 'B', null, false, true);?></td>
									<td align="center" valign="middle">
										<img src="/assets/images/email/shuffle.png" alt="" />
									</td>
									<?php foreach($next_match_3 as $next_match_4 => $next_match_5): ?>
										<td align="center"><?=$data['m']->setup_box_information($next_match_4, 'C', 1, false, true); ?></td>
										<td align="center" valign="middle">
											<img src="/assets/images/email/shuffle.png" alt="" />
										</td>
										<?php foreach($next_match_5 as $next_match_6 => $next_match_7): ?>
										<td align="center"><?=$data['m']->setup_box_information($next_match_6, 'D', 1, false, true); ?></td>
										<td align="center" valign="middle">
											<br><br><br><br><br><br><br><br><br>
											<img src="/assets/images/email/shuffle.png" alt="" style="margin-left: 25px;" />
										</td>
										<td align="center"><?=$data['m']->setup_box_information($next_match_7, 'E', 1, false, true); ?></td>
									  <?php endforeach; ?>
								  <?php endforeach; ?>
								<?php endforeach; ?>
								</tr>
							</table>
							<br>
						<?php endforeach; ?>
					</td>
				</tr>
			</table>
			<?php endif; ?>
		</div>
	</body>
</html>
