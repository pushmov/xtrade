<?php
\Autoloader::add_core_namespace('Tcpdf');

\Autoloader::add_classes(array(
	'Tcpdf\\Tcpdf' => __DIR__.'/tcpdf.php',
));
