$(document).ready(function(){
    $('.pagination li a').click(function(e){
      e.preventDefault();
      submit_form(getParameterByName($(this).attr('href'), 'page'));
      return false;
    });
    
    $('.no-link').click(function(e){
        e.preventDefault();
    });
    
    function getParameterByName(url, name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
});