$(function(){
    function Line(p1,p2,p3,p4){
        this.x1=p1[0];
        this.y1=p1[1];
        this.x2=p2[0];
        this.y2=p2[1];
		this.x3=p3[0];
        this.y3=p3[1];
		this.x4=p4[0];
        this.y4=p4[1];
    }
    Line.prototype.drawWithArrowheads=function(ctx){

        // arbitrary styling
        ctx.strokeStyle="#249DDB";
        ctx.fillStyle="#249DDB";
        ctx.lineWidth=1;

        // draw the line
        ctx.beginPath();
        ctx.moveTo(this.x1,this.y1);
        ctx.lineTo(this.x2,this.y2);
		ctx.lineTo(this.x3,this.y3);
		ctx.lineTo(this.x4,this.y4);
        ctx.stroke();

        // draw the starting arrowhead
        var startRadians=Math.atan((this.y2-this.y1)/(this.x2-this.x1));
        startRadians+=((this.x2>this.x1)?90:-90)*Math.PI/180;
        this.drawArrowhead(ctx,this.x1,this.y1,startRadians);
        // draw the ending arrowhead

    };
    Line.prototype.drawWithArrowheadsSide=function(ctx){

        // arbitrary styling
        ctx.strokeStyle="#249DDB";
        ctx.fillStyle="#249DDB";
        ctx.lineWidth=1;

        // draw the line
        ctx.beginPath();
        ctx.moveTo(this.x1,this.y1);
        ctx.lineTo(this.x2,this.y2);
		ctx.lineTo(this.x3,this.y3);
		ctx.lineTo(this.x4,this.y4);
        ctx.stroke();

        // draw the starting arrowhead
        var startRadians=Math.atan((this.y2-this.y1)/(this.x2-this.x1));
        startRadians+=((this.x2>this.x1)?-90:90)*Math.PI/180;
        this.drawArrowhead(ctx,this.x1,this.y1,startRadians);

    };
    Line.prototype.drawArrowhead=function(ctx,x,y,radians){
        ctx.save();
        ctx.beginPath();
        ctx.translate(x,y);
        ctx.rotate(radians);
        ctx.moveTo(0,0);
        ctx.lineTo(5,20);
        ctx.lineTo(-5,20);
        ctx.closePath();
        ctx.restore();
        ctx.fill();
    };
    //init
    resizeCanvas();
    adjustSideArrowArea();
    function adjustSideArrowArea(){
        $('.one-way-match-box .sided-arrow-area:visible').each(function(){
            var h = $(this).parent().find('.matches-wrapper').height();
            $(this).height(h/2);
        });

        $('.two-way-match-box .sided-arrow-area:visible').each(function(){
            var h = $(this).parent().find('.matches-wrapper').height();
            $(this).height(h/2);
        });
        $('.two-way-match-box.sample .sided-arrow-area:visible').each(function(){
            var h = $(this).parent().find('.matches-wrapper').height();
            $(this).height(h/1.5);
        });
        $('.three-way-match-box .sided-arrow-area:visible').each(function(){
            var h = $(this).parent().find('.matches-wrapper').height();
            $(this).height(h - (h/4));
        });
        $('.four-way-match-box .sided-arrow-area:visible').each(function(){
            var h = $(this).parent().find('.matches-wrapper').height();
            $(this).height(h - (h/5));
        });
        $('.five-way-match-box .sided-arrow-area:visible').each(function(){
            var h = $(this).parent().find('.matches-wrapper').height();
            $(this).height(h - (h/6.5));
        });
    }
    window.addEventListener('resize', adjustSideArrowArea, false);

    function resizeCanvas(){
        var matchexBox = $('.matches-box').width();
        $('.canvas-arrow').each(function(){
            var matchesBoxWidth = 0;
            $(this).parent().find('.matches-container:visible').each(function(){
                matchesBoxWidth += ($(this).outerWidth());
            });
            var x = matchexBox / 2;
            var y = (matchesBoxWidth - x);
            var p1 = [x, 55];
            var p2 = [x, 10];
            var p3 = [y, 10];
            var p4 = [y, 55];
            var line=new Line(p1, p2, p3, p4);
            $(this)[0].setAttribute('width', matchesBoxWidth);
            $(this)[0].setAttribute('height', 80);
            var ctx = $(this)[0].getContext('2d');
            line.drawWithArrowheads(ctx);
        });
    }

    window.addEventListener('resize', resizeCanvas, false);

    drawSideArrow();
    function drawSideArrow(){
        var x = 20;
        var y = 10;
        var p1 = [100, 20];
        var p2 = [50, 20];
        $('.match-box .sided-arrow-area:visible').each(function(){
            var target = $(this).attr('data-target');
            var hx = (target === 'small') ? x : 0;
            var innerHeight = $(this).height() - hx;
            var py = (target === 'small') ? innerHeight : innerHeight - y;
            var p3 = [50, py];
            var p4 = [100, py];

            $(this).find('canvas')[0].setAttribute('width', $(this).width());
            $(this).find('canvas')[0].setAttribute('height', $(this).height());
            var line=new Line(p1, p2, p3, p4);
            var ctx = $(this).find('canvas')[0].getContext('2d');
            line.drawWithArrowheadsSide(ctx);
        });
    }
    window.addEventListener('resize', drawSideArrow, false);

}); // end $(function(){});

var listing_id;


$(document).ready(function(){
  var dialog_context = $('#matches_dialog');
  var loader = '<i class="fa fa-spinner fa-spin"></i>Processing...';

  $('.save').click(function(){
    $.get( baseUrl + 'realtors/match/later.html', { listing_id: $(this).attr('id') }, function(html){
      dialog_context.html(html);
      dialog_context.foundation('open');
    });
  });

  $('.Optimize').click(function(){
    $.get( baseUrl + 'realtors/match/optimize.html', function(html){
      dialog_context.html(html);
      dialog_context.foundation('open');
    });
  });

  $('.reset').click(function(){
      var btn = $(this);
      var txt = btn.html();
      btn.prop('disabled', true);
      btn.html(loader);
    $.get( baseUrl + 'realtors/match/reset.html', {listing_id: $(this).attr('id')}, function(html){
        btn.prop('disabled', false);
        btn.html(txt);
      dialog_context.html(html);
      dialog_context.foundation('open');
    });
  });

  $('.Send_It').click(function(){
      var btn = $(this);
      var txt = btn.html();
      btn.prop('disabled', true);
      btn.html(loader);
    $.get( baseUrl + 'realtors/match/send.html', {listing_id: id}, function(html){
        btn.prop('disabled', false);
        btn.html(txt);
      dialog_context.html(html);
      dialog_context.foundation('open');
    });
  });

  $('.ignore').click(function(){
      var btn = $(this);
      var txt = btn.html();
      btn.prop('disabled', true);
      btn.html(loader);
    $.get( baseUrl + 'realtors/match/ignore.html', {listing_id: $(this).attr('id')}, function(html){
        btn.prop('disabled', false);
        btn.html(txt);
      dialog_context.html(html);
      dialog_context.foundation('open');
    });
  });

  //post actions
  dialog_context.on('click', '.ignore_submit', function(){
    $.post($(this).closest('form').attr('action'), {this_listing: $(this).attr('id'), listing: id}, function(data){
        if(data.status === 'ok'){
            dialog_context.foundation('close');
            window.location.reload();
        }
    });
  });

  dialog_context.on('click', '.later_submit', function(){
      $.post($(this).closest('form').attr('action'), {this_listing: $(this).attr('id'), listing: id}, function(data){
          if(data.status === 'ok'){
              dialog_context.foundation('close');
              window.location.reload();
          }
      });
  });

  dialog_context.on('click', '.reset_submit', function(){
    $.post($(this).closest('form').attr('action'), {this_listing: id}, function(data){
        if(data.status === 'ok'){
            dialog_context.foundation('close');
            window.location.reload();
        }
    });
  });

  dialog_context.on('click', '.send_submit', function(){
      var btn = $(this);
      var txt = btn.html();
      btn.prop('disabled', true);
      btn.html(loader);
      var form = $(this).closest('form');
      form.find('.callout').remove();
      $.post(form.attr('action'), form.serialize() + '&listing_id=' + id, function(json){
          btn.prop('disabled', false);
          btn.html(txt);
          if(json.status === 'OK'){
              form.html('<div class="callout success small text-left">' + json.message + '</div>');
          } else {
              form.before('<div class="callout alert small text-left">' + json.message + '</div>');
          }
      });
  });

  dialog_context.on('click', '.dismiss', function(){
      dialog_context.foundation('close');
  });

  $('.include').click(function(){
    window.location = '/realtors/match/id/' + id + '?inc=true';
  });

  $('#return_home').click(function() {
    window.location = baseUrl + $(this).attr('data-type');
  });

  $('.hiw').click(function(){
      window.location = '/hiw';
  });

    $('.tooltip').tooltipster({
       position: 'top',
       theme: 'tooltip-white-theme'
    });

    $('.contact-agent').click(function(){
        $.get('/contact/agent.html', {id: $(this).attr('id')}, function(html){
            $('#matches_dialog').html(html);
            $('#matches_dialog').foundation('open');
        });
    });


    $('#export_match').on('click', function(){
    	window.open(baseUrl + 'realtors/match/export?id='+$(this).attr('data-id'), 'export');
    });

});