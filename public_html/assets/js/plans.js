$(document).ready(function(){
    if($('.editor').length > 0) {
        $('.editor').jqte();
    }

    if($('#promo').length > 0) {
        $('#promo').DataTable({
			processing: true,
			serverSide: true,
			ajax: {
				url: '/admins/plans/datatable.json'
			},
			'order': [[6,'asc'],[2,'asc']],
			"columnDefs": [
				{ 'bSortable': false, 'aTargets': [-1] }
			]
		});
    }

    $('#country_id').on('change', function(){
        $('#state_id').prop('disabled', true);
        $.get('/admins/plans/state.html', {id: $(this).val()}, function(html){
            $('#state_id').parent().html(html);
            $('#state_id').prop('disabled', false);
        });
    });

    $('#btn_save').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html('Processing ...');
        var form = $(this).closest('form');
        form.find('.input-error').remove();
        $.post(form.attr('action'), form.serialize(), function(json){
            btn.prop('disabled', false);
            btn.html(txt);
            if(json.errors) {
                $.each(json.errors, function(k,v){
                    if(k === 'terms') {
                        $('#'+k).closest('.jqte').after('<div class="input-error">' + v + '</div>');
                    } else {
                        $('#'+k).after('<div class="input-error">' + v + '</div>');
                    }
                });
            } else if (json.success){
                window.location.href = '/admins/plans/edit/'+ json.id;
            }
        });
    });

    $('#btn_active').on('click', function(){
        var form = $(this).closest('form');
        var btn = $(this);
        var txt = btn.html();
        btn.html('Processing ...');
        btn.prop('disabled', true);
        $.get('/admins/plans/active.html', function(html){
            btn.html(txt);
            btn.prop('disabled', false);
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '#btn_active_submit', function(){
                $('#dialog').foundation('close');
                btn.html('Processing ...');
                btn.prop('disabled', true);
                $.post('/admins/plans/active.json', form.serialize(), function(json){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    if(json.error) {
                        var m = json.error;
                        if(m.status_id) {
                            m = m.status_id;
                        }
                        $('#btn_save').before('<div class="input-error api-error">'+m+'</div>');
                    } else {
                        window.location.reload();
                    }

                });
            });
        });
    });

    $('#btn_pause').on('click', function(){
        var form = $(this).closest('form');
        var btn = $(this);
        var txt = btn.html();
        btn.html('Processing ...');
        btn.prop('disabled', true);
        $.get('/admins/plans/pause.html', function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '#btn_pause_submit', function(){
                $('#dialog').foundation('close');
                btn.html('Processing ...');
                btn.prop('disabled', true);
                $.post('/admins/plans/pause.json', form.serialize(), function(json){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    if(json.error) {
                        $('#btn_save').before('<div class="input-error api-error">'+json.error+'</div>');
                    } else {
                        window.location.reload();
                    }
                });
            });
        });
    });

    $('#btn_delete').on('click', function(){
        var form = $(this).closest('form');
        var btn = $(this);
        var txt = btn.html();
        btn.html('Processing ...');
        btn.prop('disabled', true);
        $.get('/admins/plans/delete.html', function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '#btn_delete_submit', function(){
                $('#dialog').foundation('open');
                btn.html('Processing ...');
                btn.prop('disabled', true);
                $.post('/admins/plans/delete.json', form.serialize(), function(json){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    if(json.error) {
                        $('#btn_save').before('<div class="input-error api-error">'+json.error+'</div>');
                    } else if (json.success){
                        window.location.href = json.success;
                    }
                });
            });
        });
    });
});