$(document).ready(function(){
    $.get('/user/unsubscribed.html', function(html){
        $('#login_dialog').html(html);
        $('#login_dialog').foundation('open');
    });
    
    $('#login_dialog').on('click', '.btn-close', function(){
        $('#login_dialog').foundation('close');
    });
});