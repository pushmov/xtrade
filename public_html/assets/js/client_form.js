$( document ).ajaxError(function( event, request, settings, error ) {
  var errmsg = '1';
});
$(document).ready(function(){
    neighinline();
    $('#prop_style_w').SumoSelect();
    customSumoSelect($('.prop-style-w-wrapper'), $('#prop_style_w'));
    $('#siding_type_w').SumoSelect();
    customSumoSelect($('.siding-type-w-wrapper'), $('#siding_type_w'));
    $('#garage_type_w').SumoSelect();
    customSumoSelect($('.garage-type-w-wrapper'), $('#garage_type_w'));
    $('#floor_type_w').SumoSelect();
    customSumoSelect($('.floor-type-w-wrapper'), $('#floor_type_w'));
    $('#roof_type_w').SumoSelect();
    customSumoSelect($('.roof-type-w-wrapper'), $('#roof_type_w'));
    $('#listing_price_low').SumoSelect();
    $('#listing_price_high').SumoSelect();
    $('#sq_footage').SumoSelect();
    $('#prop_type_w').SumoSelect();
    $('#bedrooms_w').SumoSelect();
    $('#bathrooms_w').SumoSelect();
    $('#p_title_w').SumoSelect();
    $('#total_lot_size_w').SumoSelect();
    $('#total_acreage_w').SumoSelect();
    $('#finished_basement_w').SumoSelect();
    $('#view_w').SumoSelect();
    $('#heat_source_w').SumoSelect();
    $('#heating_w').SumoSelect();
    $('#cooling_w').SumoSelect();
    $('#water_w').SumoSelect();
    $('#sewer_w').SumoSelect();

    $('#match_notification').hide();

    function neighinline(){
        $.post(baseUrl + 'listing/neighinline.html', {type: 2, location: $('#wants_address').val(), lid: listing_id}, function(data){
            $('#neighbourhood_w_wrapper').html(data);
            $('label[for="'+$('#neighbourhood_w').attr('name')+'"]').removeClass('req_text').addClass('req_text_done');
//            $('#neighbourhood_w').val('none');
        });
    }

    new autoComplete({
		selector: '#wants_address',
		source: function(term, response){
			$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data) { response(data); });
		}
	});

    $('#wants_address').on('blur', function(){
        neighinline();
    });

    $('input.req').each(function(){
        var label = $(this).attr('name');
        if($(this).val() !== ''){
           $(document).find($('label[for="'+label+'"]')).removeClass('req_text').addClass('req_text_done');
        } else {
           $(document).find($('label[for="'+label+'"]')).removeClass('req_text_done').addClass('req_text');
        }
    });

    $('input.req').on('blur', function(){
        var label = $(this).attr('name');
        if($(this).val() !== ''){
           $(document).find($('label[for="'+label+'"]')).removeClass('req_text').addClass('req_text_done');
        } else {
           $(document).find($('label[for="'+label+'"]')).removeClass('req_text_done').addClass('req_text');
        }
    });

    $('select.req').each(function(){
        var label = $(this).attr('name');
        if($(this).val() !== ''){
           $(document).find($('label[for="'+label+'"]')).removeClass('req_text').addClass('req_text_done');
        } else {
           $(document).find($('label[for="'+label+'"]')).removeClass('req_text_done').addClass('req_text');
        }
    });

    $('select.req').on('change', function(){
        var label = $(this).attr('name');
        if($(this).val() !== ''){

           $(document).find($('label[for="'+label+'"]')).removeClass('req_text').addClass('req_text_done');
        } else {
           $(document).find($('label[for="'+label+'"]')).removeClass('req_text_done').addClass('req_text');
        }
    });

    $('select').on('change', function(){
        $('#submit_content').show();
    });
    $('input[type="text"]').on('blur', function(){
        $('#submit_content').show();
    });

    $('#submit_content').click(function(){
        $('.input-error').remove();
        var currTxt = $(this).text();
        $(this).html('<i class="fa fa-spinner fa-spin"></i>Processing...');
        $(this).prop('disabled', true);
        $.post(baseUrl + 'listing/client_update.json', $('#main_form').serialize(), function(json){
            $('#submit_content').html(currTxt);
            $('#submit_content').prop('disabled', false);
            if (json.errors) {
                $.each(json.errors, function(k,v) {
                    if(k === 'listing_price' && json.fields === 'has'){
                        $('[name="' + json.fields + '[' + k + ']"]').parent().find('.SlectBox').after('<div class="input-error input-error-listing-price">' + v + '</div>');
                    } else if(k === 'address' && json.fields === 'wants') {
                        $('[name="' + json.fields + '[' + k + ']"]').after('<div class="input-error">' + v + '</div>');
                    } else {
                        $('[name="' + json.fields + '[' + k + ']"]').parent().find('.SlectBox').after('<div class="input-error input-error-nomargin">' + v + '</div>');
                    }
				});
            } else if(json.status === 'OK'){
            	$('#submit_content').hide();
                $.get(baseUrl + 'listing/client_form_success.html', function(html){
                    $('#edit_listing_dialog').html(html);
                    $('#edit_listing_dialog').foundation('open');
                });
            }
        });
    });

    $('input.req, select.req').each(function(){
        if($(this).val() !== ''){
            $('label[for="' + $(this).attr('id') + '"].req-text').addClass('req-text-done');
        }
    });

    $('input.req, select.req').on('change blur keyup', function(){
        if($(this).val() !== '' && $(this).val() !== '0'){
            $('label[for="' + $(this).attr('id') + '"].req-text').addClass('req-text-done');
            $('label[for="' + $(this).attr('id') + '"].req-text').parent().find('.input-error').remove();
        } else {
            $('label[for="' + $(this).attr('id') + '"].req-text').removeClass('req-text-done');
        }
    });

    $('#edit_listing_dialog').on('click', '.close-reveal-modal', function(){
        window.location = '/';
    });

    //client_wants asynchronous single field save
    $('#main_form input, #main_form select').on('change blur keyup', function(){
        var data = {
            listing_id: listing_id,
            area: 'want',
            field: $(this).attr('data-field'),
            value: $(this).val()
        };
        var btn = $('#submit_content');
        btn.html('<i class="fa fa-spinner fa-spin"></i>Updating...');
        btn.prop('disabled', true);
        $.post('/listing/update_field.json', data, function(json){
            //done, then checking and updating the completed_section flag
            $.post('/listing/completed_section.json', {listing_id: listing_id, area: 'want'}, function(json){
                btn.prop('disabled', false);
                btn.html('Save Now');
            });
        });
    });

    //special case : wants neighbourhood to be saved to different table
    $(document).on('change', '#neighbourhood_w', function(){
        console.log($(this).val());
        var data = {
            listing_id: listing_id,
            field: $(this).attr('data-field'),
            value: $(this).val()
        };
        var btn = $('#submit_content');
        btn.html('<i class="fa fa-spinner fa-spin"></i>Updating...');
        btn.prop('disabled', true);
        $.post(baseUrl + 'listing/update_wants_neighbourhood.json', data, function(json){
            //done, then checking and updating the completed_section flag
            $.post('/listing/completed_section.json', {listing_id: listing_id, area: 'has'}, function(json){
                btn.prop('disabled', false);
                btn.html('Save Now');
            });
        });
    });
});

function customSumoSelect(wrapper, target){
    wrapper.find('.options > li').on('click', function(){
        if($(this).hasClass('selected') && ($(this).attr('data-val') === '' || $(this).attr('data-val') === '9')){
            target[0].sumo.unSelectAll();
            target[0].sumo.selectItem(0);
        } else {
            target[0].sumo.unSelectItem(0);
        }
    });
}