$(document).ready(function(){
    var spinner = '<i class="fa fa-spinner fa-spin"></i>Processing...';
    load_vendor_logo();
    $('#file_upload').uploadFile({
		url: $('#target-upload').val(),
		fileName: 'myfile',
        formData: { id: $('#target-upload-id').val() },
		allowedTypes: 'jpg,jpeg,png',
		returnType: 'json',
		showPreview: false,
		sequential: true,
		sequentialCount: 1,
		onSuccess: function() {
            load_vendor_logo();
		}
	});
    
    $(document).on('click', '.preview_ad', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(spinner);
        btn.prop('disabled', true);
		$.get('/vendors/dashboard/ad_preview.html', function(html){
            btn.html(txt);
            btn.prop('disabled', false);
            $('#page_dialog').html(html);
            $('#page_dialog').foundation('open');
            load_vendor_logo();
        });
	});
    
    $('.downgrade').on('click', function(){
        //modal
        var btn = $(this);
        var txt = btn.html();
        btn.html(spinner);
        btn.prop('disabled', true);
        $.get('/vendors/dashboard/downgrade.html', function(html){
            btn.html(txt);
            btn.prop('disabled', false);
            $('#dialog').html(html);
            $('#dialog').foundation('open');
        });
    });
    
    $('#dialog').on('click', '.downgrade_vendor', function(){
        var btn = $(this);
        var currTxt = btn.html();
        btn.html(spinner);
        btn.prop('disabled', true);
        $.post('/vendors/dashboard/downgrade.json', {}, function(json){
            btn.html(currTxt);
            btn.prop('disabled', false);
            if(json.status === 'OK'){
                $.get('/vendors/dashboard/success.html', {type: 'downgrade'}, function(html){
                    $('#dialog').html(html);
                    $('#dialog').foundation('open');
                    $('#dialog').on('click', '.continue, .close-reveal-modal', function(){
                        window.location.reload();
                    });
                });
            }
        });
    });
    
    $('#dialog').on('click', '#btn_close', function(){
        $('#dialog').foundation('close');
    });
    
    $('.upgrade').on('click', function(){
        var btn = $(this);
        var currTxt = btn.html();
        btn.html(spinner);
        btn.prop('disabled', true);
        $.get('/vendors/dashboard/upgrade.html', function(html){
            btn.html(currTxt);
            btn.prop('disabled', false);
            $('#page_dialog').html(html);
            $('#page_dialog').foundation('open');
        });
    });
    
    $('#page_dialog').on('click', '#process_payment', function(e){
        var btn = $(this);
        var currTxt = btn.html();
        btn.html(spinner);
        btn.prop('disabled', true);
		e.preventDefault();
		var form = $('#cc_form');
		$('#post_error').html('').removeClass('error-list');
		$.post(baseUrl + 'vendors/dashboard/upgrade.json', form.serialize(), function(json){
			if (json.status === 'ERROR') {
				$('#post_error').html(json.message).addClass('error-list');
                btn.html(currTxt);
                btn.prop('disabled', false);
			} else if(json.status === 'OK') {
                $.get('/vendors/dashboard/success.html', {type: 'upgrade'}, function(html){
                    $('#dialog').html(html);
                    $('#dialog').foundation('open');
                    $('#dialog').on('click', '.continue, .close-reveal-modal', function(){
                        window.location.reload();
                    });
                });
			}
		});
	});
    
});


function load_vendor_logo(){
    $('.vendor-logo div').remove();
    $.ajax({
        url: '/vendor/logo.html',
        data: {vendor_id: vendor_id},
        cache: false,
        type: 'GET',
        success: function(html){
            $('.vendor-logo').html(html);
            //clear cache
            var d = new Date();
            var s = $('.vendor-logo').find('img').attr('src');
            $('.vendor-logo').find('img').attr('src', s+'?'+d.getTime());
        }
    });
}