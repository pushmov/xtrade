$(window).load(function(){
    var message = '';
    $.post('/user/active.json', {key: key, type: type, email: email}, function(r){
        message = r.message;
        $.get('/login/login.html', {type: r.type}, function(html){
            $('#login_dialog').html(html);
            $('#login_dialog').foundation('open');
            $('#login_error').html(message);
            $('#login_email').val(r.email);
        });
    });
});