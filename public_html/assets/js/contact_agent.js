$(document).ready(function(){
   $('.contact_agent').on('click', function(){
        $.get('/contact/agent.html', {id : $(this).attr('data-id')},function(html) {
            $('#contact_agent_dialog').html(html);
            $('#contact_agent_dialog').foundation('open');
        });
    });

    $('#results_div').on('click', '.contact_agent', function(){
        $.get('/contact/agent.html', {id : $(this).attr('data-id')},function(html) {
            $('#refine_dialog').html(html);
            $('#refine_dialog').foundation('open');
        });
    });

    $('.reveal').on('click', '#submit', function(e){
        var that = $(this);
        var currTxt = that.text();
        that.html('<i class="fa fa-spinner fa-spin"></i>Processing...');
        that.prop('disabled', true);
        e.preventDefault();
        $('.alert-box,.input-error').remove();
        var form = $(this).closest('form');
        $.post(form.attr('action'), form.serialize(), function(json) {
            $('.callout').remove();
            if (json.errors) {
				$.each(json.errors, function(k,v) {
                    if(k === 'working_realtor' || k === 'trade'){
                        $('[name="contact[' + k + ']"]').parent().after('<div class="input-error input-error-nomargin">' + v + '</div>');
                    } else {
                        $('[name="contact[' + k + ']"]').after('<div class="input-error">' + v + '</div>');
                    }
				});
            	that.html(currTxt);
            	that.prop('disabled', false);
			} else if(json.status === 'OK'){
                form.before('<div class="callout success small">'+json.message+'</div>');
                that.hide();
            }
        }).fail( function(xhr, textStatus, errorThrown) {
            $('.notifier#sending').hide();
            $('.notifier#error').show();
            alert(xhr.responseText);
        });
        return false;
    });

    $(document).find('input[type="tel"]').mask('999-999-9999');
});