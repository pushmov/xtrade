var isAdmin = true;
var loader = '<i class="fa fa-spinner fa-spin"></i>Processing...';

$(document).ready(function(){
	$('#logout').click(function() {
		window.location = '/admins/login/logout';
	});

	$(document).on('click', '.dashboard', function(){
		window.location = '/admins/dashboard';
	});

	var dialog = $('#dialog');
	var right_col = $('#details_div');

	$('#admin_select').on('change', function(){
		window.location = '/admins/' + $(this).val();
	});

	$(document).on('click', '.close_button', function(){
		dialog.foundation('close');
	});

	$(document).on('click', '.change_broker', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.get('/admins/user/change.html', {realtor_id: $(this).closest('dl').attr('id')}, function(html){
			btn.html(txt);
			btn.prop('disabled', true);
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit_change_broker', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				$('dl#'+data.id).find('.realtor-detail').click();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.reset_password', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.get('/admins/user/reset.html', {realtor_id: $(this).closest('dl').attr('id')}, function(html){
			btn.html(txt);
			btn.prop('disabled', false);
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit_reset', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				$('dl#'+data.id).find('.realtor-detail').click();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.suspend_account_realtor', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.get('/admins/user/suspend.html', {realtor_id: $(this).closest('dl').attr('id')}, function(html){
			btn.html(txt);
			btn.prop('disabled', false);
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit_suspend', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				$('dl#'+data.id).find('.realtor-detail').click();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$('.approve_btn').on('click', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post('/admins/user/approve.json', {id: $(this).closest('dl').attr('id')}, function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				window.location.reload();
			}
		});
	});

	$(document).on('click', '.unsuspend_realtor', function(){
		$.get('/admins/user/unsuspend.html', {realtor_id: $(this).closest('dl').attr('id')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.unsuspend_submit', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				$('dl#'+data.id).find('.realtor-detail').click();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.delete_account', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.get('/admins/user/delete.html', {realtor_id: $(this).closest('dl').attr('id')}, function(html){
			btn.prop('disabled', false);
			btn.html(txt);
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit_delete', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				window.location.reload();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});


	//brokers area
	$('.change_field').on('keyup', function(){
		$(this).next().show();
	});
	$('button.change_discount').click(function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post('/admins/broker/discount.json', {broker_id: $(this).closest('dl').attr('id'), discount: $(this).prev('input').val() }, function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			window.location.reload();
		});
	});
	$('.broker_detail').click(function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		right_col.html(loader);
		$.post('/admins/broker/detail.html', {broker_id: $(this).closest('dl').attr('id')}, function(html){
			btn.html(txt);
			btn.prop('disabled', false);
			right_col.html(html);
		});
	});
	$('.num_realtors').click(function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		right_col.html(loader);
		$.post('/admins/broker/realtor.html', {broker_id: $(this).closest('dl').attr('id')}, function(html){
			btn.html(txt);
			btn.prop('disabled', false);
			right_col.html(html);
		});
	});

	$(document).on('click', '.add_realtor', function(){
		$.get('/admins/broker/add.html', {broker_id: $(this).closest('dl').attr('id')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.add_realtor_submit', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				window.location.reload();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.broker_reset_password', function(){
		$.get('/admins/broker/reset.html', {broker_id: $(this).closest('dl').attr('id')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
		});
	});
	$(document).on('click', '.broker_submit_reset', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				window.location.reload();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.suspend_account_broker', function(){
		$.get('/admins/broker/suspend.html', {broker_id: $(this).closest('dl').attr('id')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
		});
	});
	$(document).on('click', '.submit_suspend_broker', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				$('dl#'+data.id).find('.broker_detail').click();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});
	$(document).on('click', '.unsuspend_broker', function(){
		$.get('/admins/broker/unsuspend.html', {broker_id: $(this).closest('dl').attr('id')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit_unsuspend_broker', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				$('dl#'+data.id).find('.broker_detail').click();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.delete_broker', function(){
		$.get('/admins/broker/delbroker.html', {broker_id: $(this).closest('dl').attr('id')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit_delbroker', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				window.location.reload();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.remove_realtor', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.get('/admins/broker/delrealtor.html', {realtor_id: $(this).attr('id')}, function(html){
			btn.prop('disabled', false);
			btn.html(txt);
			dialog.html(html);
			dialog.foundation('open');
		});
	});
	$(document).on('click', '.submit_delrealtor', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				window.location.reload();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	//vendor area
	$('.vendor-detail').click(function(){
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		right_col.html(loader);
		$.get('/admins/vendor/detail.html', {vendor_id: $(this).closest('dl').attr('id')}, function(html){
			btn.html(txt);
			btn.prop('disabled', false);
			right_col.html(html);
		});
	});

	$(document).on('click', '.vendor_preview', function(){
		var id = $(this).attr('data-id');
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		$.get('/admins/vendor/preview.html', {vendor_id: id}, function(html){
			btn.html(txt);
			btn.prop('disabled', false);
			$('#dialog_preview').html(html);
			$('#dialog_preview').foundation('open');
			$('.vendor-logo div').remove();
			$.ajax({
				url: '/vendor/logo.html',
				data: {vendor_id: id},
				cache: false,
				type: 'GET',
				success: function(html){
					$('.vendor-logo').html(html);
					//clear cache
					var d = new Date();
					var s = $('.vendor-logo').find('img').attr('src');
					$('.vendor-logo').find('img').attr('src', s+'?'+d.getTime());
				}
			});
		});
	});
	$(document).on('click', '.vendor_reset', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.get('/admins/vendor/reset.html', {vendor_id: $(this).attr('data-id')}, function(html){
			btn.html(txt);
			btn.prop('disabled', false);
			dialog.html(html);
			dialog.foundation('open');
		});
	});
	$(document).on('click', '.vendor_submit_reset', function(){
		var btn = $(this);
		var txt = btn.html();
		var form = $(this).closest('form');
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.html(txt);
			btn.prop('disabled', false);
			if(data.status === 'ok'){
				dialog.foundation('close');
				window.location.reload();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});
	$(document).on('click', '.vendor_delete', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		$.get('/admins/vendor/delete.html', {vendor_id: $(this).attr('data-id')}, function(html){
			btn.prop('disabled', false);
			btn.html(txt);
			dialog.html(html);
			dialog.foundation('open');
		});
	});
	$(document).on('click', '.submit_delete_vendor', function(){
		var btn = $(this);
		var txt = btn.html();
		var form = $(this).closest('form');
		btn.html(loader);
		btn.prop('disabled', true);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.prop('disabled', false);
			btn.html(txt);
			if(data.status === 'ok'){
				dialog.foundation('close');
				window.location.reload();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.suspend_account_vendor', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.html(loader);
		btn.prop('disabled', true);
		$.get('/admins/vendor/suspend.html', {vendor_id: $(this).attr('data-id')}, function(html){
			btn.html(txt);
			btn.prop('disabled', false);
			dialog.html(html);
			dialog.foundation('open');
		});
	});
	$(document).on('click', '.submit_suspend_vendor', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.prop('disabled', false);
			btn.html(txt);
			if(data.status === 'ok'){
				dialog.foundation('close');
				$('dl#'+data.id).find('.vendor-detail').click();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	$(document).on('click', '.resend_activation', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		var params, type;
		if($(this).attr('data-type') === 'vendor'){
			params = {vendor_id: $(this).attr('data-id')};
			type = $(this).attr('data-type');
		} else if($(this).attr('data-type') === 'realtor'){
			params = {realtor_id: $(this).attr('data-id')};
			type = 'user';
		} else if($(this).attr('data-type') === 'broker'){
			params = {broker_id: $(this).attr('data-id')};
			type = $(this).attr('data-type');
		}
		$.get('/admins/' + type + '/resend_activation_confirm.html', params, function(html){
			btn.prop('disabled', false);
			btn.html(txt);
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit-resend-activation', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		var form = $(this).closest('form');
		$.post(form.attr('action'), form.serialize(), function(json){
			btn.prop('disabled', false);
			btn.html(txt);
			if(json){
				dialog.foundation('close');
				$.get('/admins/vendor/resend_success.html', function(html){
					dialog.html(html);
					dialog.foundation('open');
				});
			}
		});
	});

	$(document).on('click', '.unsuspend_vendor', function(){
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		$.get('/admins/vendor/unsuspend.html', {vendor_id: $(this).attr('data-id')}, function(html){
			btn.prop('disabled', false);
			btn.html(txt);
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit_unsuspend_vendor', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html(loader);
		$.post(form.attr('action'), form.serialize(), function(data){
			btn.prop('disabled', false);
			btn.html(txt);
			if(data.status === 'ok'){
				dialog.foundation('close');
				$('dl#'+data.id).find('.vendor-detail').click();
			} else {
				dialog.find('.er').html(data.message);
			}
		});
	});

	if($('.agent-left').length){
		$('.agent-left').html(loader);
		$.get('/admins/agent/filter.html', function(html){
			$('.agent-left').html(html);
		});
	}

	$(document).on('click', '.view_profile', function(){
		window.location = '/admins/profile/'+$(this).attr('data-name')+'/'+$(this).attr('data-id');
	});

	if($('[data-reset]').length > 0){
		$.get('/admins/dashboard/password.html', {'id': $('[data-reset]').attr('data-id')}, function(html) {
			$('#page_dialog').html(html).foundation('open');
		});
	};

	$('#page_dialog').on('click', '#btn_change_pass', function(){
		$.post('/admins/dashboard/pass_update.json', $(this).closest('form').serialize(), function(json){
			if(json.status === 'OK'){
				window.location.reload();
			}
		});
	});

	$('.save_setting').click(function(){
		var form = $(this).closest('form');
		form.find('.er').html('');
		$.post(form.attr('action'), form.serialize(), function(json){
			form.find('.er').html(json.message);
		});
	});

	if($('.jqte-textarea').length > 0){
		$('.jqte-textarea').jqte();
	}

	if($('.fdatepicker').length > 0){
		var from = $('#valid_from').fdatepicker({
			format: 'yyyy-mm-dd'
		}).on('changeDate', function (ev) {
			if (ev.date.valueOf() > till.date.valueOf()) {
				var newDate = new Date(ev.date);
				newDate.setDate(newDate.getDate() + 1);
				till.update(newDate);
			}
			from.hide();
			$('#valid_till')[0].focus();
		}).data('datepicker');
		var till = $('#valid_till').fdatepicker({
			format: 'yyyy-mm-dd',
			onRender: function (date) {
				return date.valueOf() <= from.date.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function (ev) {
			till.hide();
		}).data('datepicker');
	}

	$(document).on('click', '.delete-listing', function(e){
		e.preventDefault();
		$.get('/admins/listing/delete.html', {listing_id: $(this).attr('data-target')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
		});
	});

	$(document).on('click', '.submit-form', function(){
		var form = $(this).closest('form');
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html('Loading...');
		$.post(form.attr('action'), form.serialize(), function(json){
			if(json.status === 'ok' || json.status === 'OK'){
				window.location.reload();
			}
			btn.prop('disabled', false);
			btn.html(txt);
		});
	});

	$(document).on('click', '.log-email-delete', function(e){
		e.preventDefault();
		$.get('/admins/log/delete.html', {id: $(this).attr('data-target')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
			dialog.find('form').attr('action', '/admins/log/emaillog_delete.json');
		});
	});

	$(document).on('click', '.log-login-delete', function(e){
		e.preventDefault();
		$.get('/admins/log/delete.html', {id: $(this).attr('data-target')}, function(html){
			dialog.html(html);
			dialog.foundation('open');
			dialog.find('form').attr('action', '/admins/log/loginlog_delete.json');
		});
	});

	$('.disable-discount').on('click', function(){
		$(this).attr('href','#').html('<span>Loading...</span>');
		$.post('/admins/broker/discount.json', {broker_id : $(this).attr('data-id'), discount: 0}, function(json){
			$('input[name="realtor_discount"]').val('');
			window.location.reload();
		});
	});

	if($('#admin_brokers').length > 0) {
		var dt = $('#admin_brokers').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			ajax: {
				url: '/admins/broker/list.json'
			},
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 6 ] }
			]
		});
	}

	//matches link
	$(document).on('click', '.broker-realtors', function(){
		var l = $(this);
		var txt = l.html();
		var tr = $(this).closest('tr');
		var row = dt.row(tr);
		if(row.child.isShown()){
			row.child.hide();
			tr.removeClass('shown');
		} else {
			l.html(loader);
			l.prop('disabled', true);
			$.post('/admins/broker/realtor.html', {broker_id: $(this).attr('id')}, function(html){
				row.child(html).show();
				tr.addClass('shown');
				l.html(txt);
				l.prop('disabled', false);
			});
		}
	});


	if($('#admin_vendors').length > 0){
		var dt = $('#admin_vendors').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			ajax: {
				url: '/admins/vendor/list.json'
			},
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 3,4 ] }
			]
		});
	}

	if($('#agents').length > 0){
		var dt = $('#agents').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			ajax: {
				url: '/admins/agent/offices.json'
			}
		});

		//crea agents office child
		$(document).on('click', '.office-filter', function(){
			var l = $(this);
			var txt = l.html();
			var tr = $(this).closest('tr');
			var row = dt.row(tr);
			if(row.child.isShown()){
				row.child.hide();
				tr.removeClass('shown');
			} else {
				l.html(loader);
				l.prop('disabled', true);
				$.get('/admins/agent/filter.html', {office_id: $(this).attr('data-id')}, function(html){
					row.child(html).show();
					tr.addClass('shown');
					l.html(txt);
					l.prop('disabled', false);
				});
			}
		});

		$(document).on('click', '#btn_agent_export', function(){
			var data = {
				'search' : $('#agents_filter input[type="search"]').val(),
				'ordercol' : $('#agents').dataTable().fnSettings().aaSorting[0][0],
				'orderdir' : $('#agents').dataTable().fnSettings().aaSorting[0][1]
			};
			var r = [];
			for (var d in data)
				r.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
			document.location.href = '/admins/agent/export.html?'+r.join('&');
		});
	}

	//admin listing/dashboard page
	if($('#listings').length > 0){
		var dt = $('#listings').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			ajax: {
				url: '/admins/dashboard/listings.json',
				data: function(d) {
					d.view = $('#view_filter').val();
				}
			},
			"aoColumnDefs": [
				{ 'bSortable': false, 'aTargets': [ 9 ] }
			],
			responsive: true
		});

		$('#view_filter').on('change', function(){
			dt.ajax.reload();
		});
	}

});
