$(document).ready(function(){
  
  var form = $('form[name="cc_form"]');
  
  //todo : implement autocomplete for address
  $('#process').on('click', function(e){
    e.preventDefault();
    $.post('/realtors/payment/upgrade.json', form.serialize()).done(function(r){
      
      if(r.status === 'error'){
        $('#cc_error').html(r.message).show();
      } else if(r.status === 'SuccessWithWarning'){
        
        $.post( '/realtors/payment/recurring.json', form.serialize())
          .done(function(data) {
						//console.log(data);
						if(data.status === 'OK'){
              window.location.reload();
						}else{
							$("#cc_error").html("Credit Card Declined.");
							$("#process").fadeToggle("fast", "linear");
							$("#cc_error").delay(600).fadeToggle("slow", "linear");
							
						}
					});
      } else if (r.status === 'Failure'){
				$('#cc_error').html(r.reply.L_LONGMESSAGE0).show();
			}
    });
    
    return false;
  });
  
});