$(document).ready(function(){
	var spinner = '<i class="fa fa-spinner fa-spin"></i>Processing...';
	$("#phone").mask("999-999-9999");
	$("#cell").mask("999-999-9999");
	$("#toll_free").mask("1-999-999-9999");

	$("#show_pass").on("mouseenter", function(){
		$("#password").attr("type", "text");
		$("#cpassword").attr("type", "text");
	});

	$("#show_pass").on("mouseleave", function(){
		$("#password").attr("type", "password");
		$("#cpassword").attr("type", "password");
	});

	new autoComplete({
		selector: '#cpc_address',
		minChars: 3,
		source: function(term, response){
			$.get(baseUrl + 'autocomplete/cpc.json', { q: term }, function(data){ response(data); });
		}
	});

	$('.tooltip1').tooltipster({
		theme: 'tooltip-white-theme',
		contentAsHTML: true,
		content: $('#crea_exp').html(),
		interactive: true
	});
	$('.tooltip2').tooltipster({
		theme: 'tooltip-white-theme',
		contentAsHTML: true,
		content: $('#nrds_exp').html(),
		interactive: true
	});
	$('#btn_join').on('click', function() {
		var that = $(this);
		var currTxt = that.text();
		that.html(spinner);
		that.prop('disabled', true);
		$('.input-error').remove();
		$.post(baseUrl + 'signup/save.json', $('#client').serialize(), function(json) {
			if (json.errors) {
				$.each(json.errors, function(k,v) {
					if (k === 'type') {
						var m = '<div class="input-error input-error-nomargin">' + v + '</div>';
						$(document).find('.SlectBox').after(m);
					} else if(k === 'comments') {
						var m = '<div class="input-error input-error-nomargin">' + v + '</div>';
						$(document).find('label[for="' + k + '"]').after(m);
					} else {
						var m = '<div class="input-error">' + v + '</div>';
						$('#' + k).after(m);
					}
				});
			} else if(json.status == 'OK') {
				if ($('#user_type').val() == 'broker') {
					$.get(baseUrl + 'signup/broker_join.html', function(html) {
						$('#client').trigger('reset');
						$('#page_dialog').removeClass('large').addClass('tiny');
						$('#page_dialog').html(html);
						$('#page_dialog').foundation('open');
						$('#btn_close').click(function() {
							$('#page_dialog').foundation('close');
							window.location = '/';
						});
					});
				} else {
					$.get(baseUrl + 'signup/payment.html', function(html) {
						$('#payment_dialog').html(html);
						$('#payment_dialog').foundation('open');
                        $('#payment_dialog').find('#cc_number, #cc_type, #cc_cvv, #exp_month, #exp_year').prop('disabled', false);
						$('#change_button').click(function() {
							$('#payment_dialog').foundation('close');
						});
					});
				}
			}
			that.html(currTxt);
			that.prop('disabled', false);
		});
	});
$( document ).ajaxError(function( event, request, settings, error ) {
  var errmsg = '1';
});

	$('#payment_dialog').on('click', '#process_payment', function(e){
		e.preventDefault();
		var form = $('#cc_form');
		var that = $(this);
		var currTxt = that.text();
		that.html(spinner);
		that.prop('disabled', true);
		$('#post_error ul').remove();
		$.post(baseUrl + 'signup/payment.json', form.serialize(), function(json){
			that.html(currTxt);
			that.prop('disabled', false);
			if (json.status === 'ERROR') {
				$('#post_error').html(json.message).addClass('error-list');
			} else if(json.status === 'FAIL') {
				$('#post_error').html(json.result).addClass('error-list');
			} else if(json.status === 'OK') {
				$.get(baseUrl + 'signup/success.html', function(html) {
					$('#page_dialog').removeClass('large');
					$('#page_dialog').addClass('tiny');
					$('#page_dialog').html(html);

					$('#btn_close').click(function() {
						$('#page_dialog').removeClass('tiny');
						$('#page_dialog').addClass('large');
						$('#page_dialog').foundation('close');
						$('#client').trigger('reset');
					});
					$('#page_dialog').foundation('open');
				});
			}
		});
	});
	$('#payment_dialog').on('blur', '#pay_cpc_address', function(e){
		$.get(baseUrl + 'signup/taxes.json', {cpc: $('#pay_cpc_address').val(), fee: $('#fee').val(), featured_ad: $('#featured_fee').val()}, function(json) {
			$('#tax_code').val(json.tax_code);
			$('#tax').val(json.tax);
            $('#featured_tax').val(json.featured_tax);
			$('#cc_monthly_total_fee').html(json.total);
			if (json.tax > 0) {
                $('#tax_txt').remove();
				$('#summary').before('<span id="tax_txt">' + json.tax_txt + '</span>');
			} else {
				$('#tax_txt').remove();
			}
		});
	});
});