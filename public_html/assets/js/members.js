$(document).ready(function(){
	$('#new_listing').click(function(){
		$.get('/realtors/listing/new.html', function(data){
            $('#page_dialog').html(data);
            $('#page_dialog').foundation('open');
        });
	});

  $(document).on('click', '#buy_sell', function(){
    $.post( '/realtors/client/xtrade.json', { type: $(this).attr('id') }, function(data){
      $(location).attr('href', data.url);
    });
  });

  $(document).on('click', '#my_matches', function(){
	   window.location = baseUrl + 'realtors/dashboard/list';
	});

  $(document).on('click', '#sell', function(){
    $.post('/realtors/client/sell.json', {type: $(this).attr('id')}, function(data){
      $(location).attr('href', data.url);
    });
  });

  $(document).on('click', '#buy', function(){
    $.post('/realtors/client/buy.json', {type: $(this).attr('id')}, function(data){
      $(location).attr('href', data.url);
    });
  });

	var menu_timer;
	var rs_menu = false;

	$('#resource_button, #resources, #resources_b').on("mouseenter", function(){
		window.clearTimeout(menu_timer);
		if(!rs_menu){
			$('#resources').fadeIn("fast", "linear");
			rs_menu = true;
		}
	});

	$('#resource_button, #resources, #resources_b').on("mouseleave", function(){
		menu_timer = setTimeout( function(){
			if(rs_menu){
				$('#resources').fadeOut("fast", "linear");
				rs_menu = false;
			}
		}, 100);
	});

	var setting_active = false;
	var active_req;
	function update_active(){
		if(setting_active){
			if( active_req ) {
				active_req.abort();
			}
			setting_active = false;
		}
	}

	function set_check(){
		$(document).one("mousemove click keypress load", function( event ){
			update_active();
			setting_active = true;
			$(this).off(event);
		});
	}

	setTimeout(function(){
		set_check();
	}, 10000 );
});