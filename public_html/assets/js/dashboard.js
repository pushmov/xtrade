var view = 'all_listings';
var page_select = 1;
var order = 'lp_hl';
var action_type = 'load';
var loader = '<div class="text-center"><i class="fa fa-spinner fa-spin"></i>Processing...</div>';
$(document).ready(function() {
	$('.members_order, .members_view').on('change', function(){
		page_select = 1;
		action_type = 'Load';
		if($(this).hasClass('members_view')){
			view = $(this).val();
			submit_form();
		}
		if($(this).hasClass('members_order')){
			order = $(this).val();
			submit_form();
		}
	});

	$('#search').on('keyup', function(e){
		submit_form();
	});

	submit_form();

    var dialog_context = $('#page_dialog');
	var target = baseUrl + 'realtors/dashboard/';

    $(document).on('click', '.modal-close', function(){
		dialog_context.removeClass('payment-dialog');
        dialog_context.foundation('close');
    });

    $(document).on('click', '.upgrade', function(){
        var listing_id = $(this).prop('id');
        var link = $(this);
        var txt = link.html();
        link.html('<i class="fa fa-spinner fa-spin"></i>Processing...');
        $.get( target + 'upgrade.html', {listing_id: listing_id} , function(html){
            link.html(txt);
            dialog_context.html(html);
			dialog_context.addClass('payment-dialog');
            dialog_context.foundation('open');
            $('#page_dialog').on('click', '.modal-close', function(){
				dialog_context.removeClass('payment-dialog');
                window.location.reload();
            });

        });
        force_refresh = true;
    });

    $(document).on('click', '.share', function(){
        $.get( target + 'share.html', function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', '.renew', function(){
        $.get(target + 'renew.html', {listing_id: $(this).attr('id')}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', '.btn-renew', function(){
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post($(this).closest('form').attr('action'), {listing_id: $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    $(document).on('click', '.change_listing_type', function(){
        var _import = $(this).attr('name');
        var listing_id = $(this).attr('id');
        action_type = 'Change_Status';
        $.get( target + 'change.html', {import: _import, listing_id : listing_id}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', '.downgrade', function(){
        $.get( target + 'downgrade.html', {listing_id : $(this).attr('id')}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', '.delete', function(){
        $.get( target + 'delete.html', {listing_id: $(this).attr('id')}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', '.btn-delete', function(){
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post($(this).closest('form').attr('action'), {listing_id: $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    $(document).on('click', '.deactivate', function(){
        $.get( target + 'deactivate.html', {listing_id: $(this).attr('id')}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', 'form .btn-deactivate', function(){
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post($(this).closest('form').attr('action'), {listing_id: $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    $(document).on('click', 'form .btn-downgrade', function(){
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post($(this).closest('form').attr('action'), {listing_id: $(this).attr('data-id')},function(data){
            currBtn.prop('disabled', false);
            currBtn.html(currTxt);
            if(data.status === 'ok'){
               dialog_context.foundation('close');
               submit_form();
            }
        });
    });

    $(document).on('click', '.activate', function(){
        $.get( target + 'activate.html', {listing_id: $(this).attr('id')}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', 'form .btn-activate', function(){
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post($(this).closest('form').attr('action'), {listing_id: $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    $(document).on('click', '.disable', function(){
        $.get( target + 'disable.html', {listing_id: $(this).attr('id')}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', 'form .btn-disable', function(){
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post($(this).closest('form').attr('action'), {listing_id: $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    $(document).on('click', '.enable', function(){
        $.get( target + 'enable.html', {listing_id: $(this).attr('id')},function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', 'form .btn-enable', function(){
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post($(this).closest('form').attr('action'), {listing_id: $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    $(document).on('click', '.xtraded', function(){
        $.get( target + 'xtraded.html', {listing_id : $(this).attr('id')}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', '.mark-as-xtraded', function(){
        var form = $(this).closest('form');
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post(form.attr('action'), {listing_id : $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    $(document).on('click', '.not_xtraded', function(){
        action_type = 'Not_xtraded';
        $.get( target + 'notxtraded.html', {listing_id : $(this).attr('id')}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    });

    $(document).on('click', '.mark-notxtraded', function(){
        var form = $(this).closest('form');
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post(form.attr('action'), {listing_id : $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    $(document).on('click', '.change-type', function(){
        var name = $(this).attr('name');
        var form = $(this).closest('form');
        var currBtn = $(this);
        var currTxt = currBtn.html();
        currBtn.html(loader);
        currBtn.prop('disabled', true);
        $.post(form.attr('action'), {name: name, listing_id : $(this).attr('data-id')}, function(data){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
            if(data.status === 'ok'){
                dialog_context.foundation('close');
                submit_form();
            }
        });
    });

    dialog_context.on('click', '#btn_close', function() {
		dialog_context.foundation('close');
	});

	$('#search').on('keyup', function(){
		if($(this).val() === ''){
			$(this).parent().find('.fa').removeClass('fa-times').removeClass('fa-red').addClass('fa-search');
		}else{
			$(this).parent().find('.fa').removeClass('fa-search').addClass('fa-times').addClass('fa-red');
		}
	});

    $('.dashboard-search-input-wrapper .fa').click(function(){
        $('#search').val('');
        $(this).removeClass('fa-times').removeClass('fa-red').addClass('fa-search');
        submit_form();
    });

    if($('#search').val() !== ''){
        $('.dashboard-search-input-wrapper').find('.fa').removeClass('fa-search').addClass('fa-times').addClass('fa-red');
    }

    $(document).on('click', '#process_payment', function(e){
        var currBtn = $(this);
        var currTxt = currBtn.html();
        e.preventDefault();
		var form = $('#cc_form');
        currBtn.html(loader);
        currBtn.prop('disabled', true);
		$('#post_error').html('').removeClass('error-list');
		$.post(baseUrl + 'realtors/dashboard/upgrade.json', form.serialize(), function(json){
            currBtn.html(currTxt);
            currBtn.prop('disabled', false);
			if (json.status === 'ERROR') {
				$('#post_error').html(json.message).addClass('error-list');
			} else if(json.status === 'OK') {
                $.get('/realtors/dashboard/success.html', function(html){
                    $('#page_dialog').removeClass('payment-dialog');
                    $('#page_dialog').html(html);
                    $('#page_dialog').foundation('open');
                    $('#page_dialog').on('click', '.continue, .close-reveal-modal', function(){
                        window.location.reload();
                    });
                });
			}
		});
    });

	$('#page_dialog').on('blur', '#pay_cpc_address', function(e){
		$.get(baseUrl + 'signup/taxes.json', {cpc: $('#pay_cpc_address').val(), fee: $('#fee').val(), featured_ad: $('#featured_fee').val()}, function(json) {
			$('#tax_code').val(json.tax_code);
			$('#tax').val(json.tax);
            $('#featured_tax').val(json.featured_tax);
			$('#cc_monthly_total_fee').html(json.total);
			if (json.tax > 0) {
                $('#tax_txt').remove();
				$('#summary').before('<span id="tax_txt">' + json.tax_txt + '</span>');
			} else {
				$('#tax_txt').remove();
			}
		});
	});


    $('#view_select, #view_order').on('change', function(){
        submit_form();
    });
	$('#my_matches').on('click', function(){
	    $('#view_select').val('check_for_matches');
        submit_form();
	});
    if(action === 'listing_type'){
        $.get('/realtors/dashboard/change.html', {import: imported, listing_id: listing_id}, function(html){
            dialog_context.html(html);
            dialog_context.foundation('open');
        });
    }
});

function submit_form(p){
	if (p) {
		page_select = p;
	}
	var param = {
		page: page_select,
		sort_view: $('#view_select').val(),
		sort_order: $('#view_order').val(),
		term: $('#search').val()
	};
    $('#all_listings').html(loader);
	$.post(baseUrl + 'realtors/listing/all.html?page=' + page_select, param, function(html){
        $('#all_listings').html(html);
        $('#all_listings').find('.tooltip').tooltipster({
            position: 'bottom-left',
            theme: 'tooltip-white-theme'
        });
		$('#l_loading').hide();
    });
}

function do_import(){
	$.post(baseUrl + 'realtors/listing/import.html', function(html){
        submit_form();
    });
}